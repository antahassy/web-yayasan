<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '08507effb8c8bccfd13d5c8a0b7276a4665a583d',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '08507effb8c8bccfd13d5c8a0b7276a4665a583d',
    ),
    'composer/package-versions-deprecated' => 
    array (
      'pretty_version' => '1.11.99.1',
      'version' => '1.11.99.1',
      'aliases' => 
      array (
      ),
      'reference' => '7413f0b55a051e89485c5cb9f765fe24bb02a7b6',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9cf661f4eb38f7c881cac67c75ea9b00bf97b210',
    ),
    'illuminate/bus' => 
    array (
      'pretty_version' => 'v8.41.0',
      'version' => '8.41.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7b5c0f1aa52cc70259352ff6b7adb67c7d46bcc5',
    ),
    'illuminate/collections' => 
    array (
      'pretty_version' => 'v8.41.0',
      'version' => '8.41.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'deccb956d38710f3f8baf36dd876c3fa1585ec22',
    ),
    'illuminate/container' => 
    array (
      'pretty_version' => 'v8.41.0',
      'version' => '8.41.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0e38ee1632d470e56aece0079e6e22d13e6bea8e',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => 'v8.41.0',
      'version' => '8.41.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '64abbe2aeee0855a11cfce49d0ea08a0aa967cd2',
    ),
    'illuminate/database' => 
    array (
      'pretty_version' => 'v8.41.0',
      'version' => '8.41.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6277b39728bce436d2509d215223137d87265792',
    ),
    'illuminate/events' => 
    array (
      'pretty_version' => 'v8.41.0',
      'version' => '8.41.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bd2941d4d55f5d357b203dc2ed81ac5c138593dc',
    ),
    'illuminate/macroable' => 
    array (
      'pretty_version' => 'v8.41.0',
      'version' => '8.41.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '300aa13c086f25116b5f3cde3ca54ff5c822fb05',
    ),
    'illuminate/pipeline' => 
    array (
      'pretty_version' => 'v8.41.0',
      'version' => '8.41.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '23aeff5b26ae4aee3f370835c76bd0f4e93f71d2',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => 'v8.41.0',
      'version' => '8.41.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '31e91a12f0aac770d02a05b5d5771829132213b4',
    ),
    'jean85/pretty-package-versions' => 
    array (
      'pretty_version' => '1.6.0',
      'version' => '1.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1e0104b46f045868f11942aea058cd7186d6c303',
    ),
    'jenssegers/mongodb' => 
    array (
      'pretty_version' => 'v3.8.3',
      'version' => '3.8.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '09fcda8d21edfeb49416893bf916e13647d79f4b',
    ),
    'mongodb/mongodb' => 
    array (
      'pretty_version' => '1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '953dbc19443aa9314c44b7217a16873347e6840d',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.48.0',
      'version' => '2.48.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd3c447f21072766cddec3522f9468a5849a76147',
    ),
    'ocramius/package-versions' => 
    array (
      'replaced' => 
      array (
        0 => '1.11.99',
      ),
    ),
    'paragonie/constant_time_encoding' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f34c2b11eb9d2c9318e13540a1dbc2a3afbd939c',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.100',
      'version' => '9.99.100.0',
      'aliases' => 
      array (
      ),
      'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
    ),
    'php-amqplib/php-amqplib' => 
    array (
      'pretty_version' => 'v3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c0a8eade209b7e43d6a405303d8de716dfd02749',
    ),
    'phpseclib/phpseclib' => 
    array (
      'pretty_version' => '3.0.8',
      'version' => '3.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd9615a6fb970d9933866ca8b4036ec3407b020b6',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v5.2.8',
      'version' => '5.2.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '864568fdc0208b3eba3638b6000b69d2386e6768',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6c942b1ac76c82448322025e084cadc56048b4e',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5601e09b69f26c1828b13b6bb87cb07cddba3170',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '43a0283138253ed1d48d352ab6d0bdb3f809f248',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5232de97ee3b75b0360528dae24e73db49566ab1',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a678b42e92f86eca04b7fa4c0f6f19d097fb69e2',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dc3063ba22c2a1fd2f45ed856374d79114998f91',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f040a30e04b57fbcc9c6cbcf4dbaa96bd318b9bb',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v5.2.8',
      'version' => '5.2.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '01b35eb64cac8467c3f94cd0ce2d0d376bb7d1db',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v5.2.8',
      'version' => '5.2.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '445caa74a5986f1cc9dd91a2975ef68fa7cb2068',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '95c812666f3e91db75385749fe219c5e494c7f95',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3',
      ),
    ),
    'videlalvaro/php-amqplib' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.0.0',
      ),
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '1.5.6',
      'version' => '1.5.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '80953678b19901e5165c56752d087fc11526017c',
    ),
  ),
);
