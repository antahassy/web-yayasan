$(window).resize(function() {
    if($(window).width() > 767){
        $('#mobile_menu').hide();
    }
});
$('.carousel').carousel({
    interval: 5000,
    pause: false
});
$(document).ready(function(){
    sosmed();
    function sosmed(){
        var mobile_app = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            any: function() {
                return (mobile_app.Android() || mobile_app.iOS());
            }
        };
        $(".facebook_sosmed").on("click", function(){
            var data_url = 'https://www.facebook.com/kaizenpeduliindonesia';
            $(this).attr('target', '_blank');
            $(this).attr('href', data_url);
        });
        $(".instagram_sosmed").on("click", function(){
            var data_url = 'https://www.instagram.com/peduliindo/';
            $(this).attr('target', '_blank');
            $(this).attr('href', data_url);
        });
        $(".whatsapp_sosmed").on("click", function(){
            var whatsapp_contact = 6287888335084;
            if(mobile_app.any()){
                var whatsapp_API_url = "whatsapp://send";
                $(this).attr('href', whatsapp_API_url + '?phone=' + whatsapp_contact);
            }else{
                var whatsapp_API_url = "https://web.whatsapp.com/send";
                $(this).attr('target', '_blank');
                $(this).attr('href', whatsapp_API_url +'?phone=' + whatsapp_contact);
            }
        });
    }
    slider_contents();
    function slider_contents(){
        $('.c_rule').on('click', function(){
            var selected_rule = $(this).attr('id');
            if(!$('#c_d_' + selected_rule).is(':visible')){
                $('#c_d_' + selected_rule).slideUp(500);
            }else{
                $('#c_d_' + selected_rule).slideDown(500);
            }
        });
    }
    main();
    function main(){
        $('#subscribe').on('keydown', function(event){
            var vals = $(this).val();
            if(vals != '' && (event.which === 13 || event.keyCode === 13)){
                event.preventDefault();
                event.stopImmediatePropagation();
                var email = /\@+[a-zA-Z0-9]+\.+[a-zA-Z0-9]/m;
                var errormessage = '';
                if(! email.test(vals)){
                    errormessage += 'Invalid email \n';
                }
                if(errormessage !== ''){
                    swal({
                        html          : '<pre>' + errormessage + '</pre>',
                        background  : 'transparent'
                    });
                }else{
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type           : 'ajax',
                                    method         : 'post',
                                    url            : site + 'main/subscribe',
                                    data           : {
                                        email     : vals
                                    },
                                    async          : true,
                                    dataType       : 'json',
                                    success        : function(response){
                                        if(response.success){
                                            swal({
                                                html        : '<pre>Email berhasil didaftarkan</pre>',
                                                type        : "success",
                                                background  : 'transparent'
                                            });
                                        }
                                        if(response.subscriber){
                                            swal({
                                                html        : '<pre>Email sudah terdaftar</pre>',
                                                background  : 'transparent'
                                            });
                                        }
                                    },
                                    error     : function(){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            }
        });
        $('.web_search').on('keydown', function(event){
            var vals = $(this).val();
            if(vals != '' && (event.which === 13 || event.keyCode === 13)){
                location.href = site + 'home/pencarian/' + vals.replace(' ', '_');
            }
        });
        $('#register_phone').on('keyup', function(){
            $(this).val(function(index, value) {
                return value.replace(/\D/g, "").replace(/\B(?=(\d{4})+(?!\d))/g, "-");
            });
        });
        $('#register_email').on('keyup', function(){
            $(this).val($(this).val().toLowerCase());
        });
        $('.must_logged_in').on('click', function(){
            swal({
                html        : '<pre>Silahkan Login atau Daftar' + '<br>' +
                              'Terlebih dahulu' + '<br>' +
                              'Untuk mengakses menu ini</pre>',
                type        : "warning",
                background  : 'transparent'
            });
            return false;
        });
        $('.sess_logged_in').on('click', function(){
            location.href = $(this).attr('href');
            // console.log($(this).attr('href'));
            // swal({
            //     html        : '<pre>Silahkan Login atau Daftar' + '<br>' +
            //                   'Terlebih dahulu' + '<br>' +
            //                   'Untuk mengakses menu ini</pre>',
            //     type        : "warning",
            //     background  : 'transparent'
            // });
            // return false;
        });
        $('#table_case, #table_data, #table_search_donasi').on('click', '.sess_logged_in', function(){
            location.href = $(this).attr('href');
            // console.log($(this).attr('href'));
            // swal({
            //     html        : '<pre>Silahkan Login atau Daftar' + '<br>' +
            //                   'Terlebih dahulu' + '<br>' +
            //                   'Untuk mengakses menu ini</pre>',
            //     type        : "warning",
            //     background  : 'transparent'
            // });
            // return false;
        });
        $('#mobile_logo').on('click', function(){
            if($('#mobile_menu').is(':visible')){
                $('#mobile_menu').hide(250);
            }else{
                $('#mobile_menu').show(250);
            }
        });
        $('.c_rule').on('click', function(){
            var selected_rule = $(this).attr('id');
            //$('.c_d_rule').slideUp(500);
            if($('#c_d_' + selected_rule).is(':visible')){
                $('#c_d_' + selected_rule).slideUp(500);
            }else{
                $('#c_d_' + selected_rule).slideDown(500);
            }
        });
        var modal_login;
        $('#modal_login').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_login = true;
        });
        $('#modal_login').on('hide.bs.modal', function(){
            if(modal_login){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_login = false;
                setTimeout(function(){
                    $('#modal_login').modal('hide');
                },500);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var modal_register;
        $('#modal_register').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_register = true;
        });
        $('#modal_register').on('hide.bs.modal', function(){
            if(modal_register){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_register = false;
                setTimeout(function(){
                    $('#modal_register').modal('hide');
                    $('#modal_login').modal('show');
                },500);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var modal_search_email;
        $('#modal_search_email').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_search_email = true;
        });
        $('#modal_search_email').on('hide.bs.modal', function(){
            if(modal_search_email){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_search_email = false;
                setTimeout(function(){
                    $('#modal_search_email').modal('hide');
                    $('#modal_login').modal('show');
                },500);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('.user_login').on('click', function(){
            $('#modal_login').modal('show');
        });
        $('.user_register').on('click', function(){
            $('#modal_login').modal('hide');
            setTimeout(function(){
                $('#modal_register').modal('show');
            },350);
        });
        $('#email_link').on('click', function () {
            $('#modal_login').modal('hide');
            setTimeout(function(){
                $('#modal_search_email').modal('show');
            },350);
        });
    }
    password_recovery();
    function password_recovery(){
        $('#password_reset').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var recovery_email = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type           : 'ajax',
                            method         : 'post',
                            url            : site + 'main/recovery',
                            data           : {
                                email     : recovery_email
                            },
                            async          : true,
                            dataType       : 'json',
                            success        : function(response){
                                if(response.success){
                                    swal({
                                        html        : '<pre>Kode pemulihan terkirim ke' + '<br>' +
                                                      recovery_email + '</pre>',
                                        type        : "success",
                                        background  : 'transparent'
                                    });
                                }
                                if(response.unsent){
                                    swal({
                                        html        : '<pre>Gagal mengirim kode pemulihan' + '<br>' +
                                                     'Cobalah beberapa saat lagi</pre>',
                                        type        : "warning",
                                        background  : 'transparent'
                                    });
                                }
                            },
                            error     : function(){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
       $('#btn_search_email').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var email = /\@+[a-zA-Z0-9]+\.+[a-zA-Z0-9]/m;
            var errormessage = '';
            if(! $('#search_email').val()){
                errormessage += 'Email dibutuhkan \n';
            }else{
                email_reg = $('#search_email').val();
                if(!email.test(email_reg)){
                    errormessage += 'Invalid email \n';
                }
            }
            if(errormessage !== ''){
                swal({
                    html          : '<pre>' + errormessage + '</pre>',
                    background  : 'transparent'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type           : 'ajax',
                                method         : 'post',
                                url            : site + 'main/search_email',
                                data           : {
                                    email     : $('#search_email').val()
                                },
                                async          : true,
                                dataType       : 'json',
                                success        : function(data){
                                    if(data.empty){
                                        $('#password_reset_text').hide();
                                        $('#password_reset').attr('data', '');
                                        swal({
                                            html        : '<pre>Email tidak ditemukan</pre>',
                                            type        : "warning",
                                            background  : 'transparent'
                                        });
                                    }else{
                                        $('#password_reset_text').show();
                                        $('#password_reset').attr('data', data.email);
                                        swal.close();
                                    }
                                },
                                error     : function(){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
    }
    register();
    function register(){
        $('#btn_register').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var email = /\@+[a-zA-Z0-9]+\.+[a-zA-Z0-9]/m;
            var errormessage = '';
            if(! $('#register_name').val()){
                errormessage += 'Nama dibutuhkan \n';
            }
            if(! $('#register_email').val()){
                errormessage += 'Email dibutuhkan \n';
            }else{
                if(!email.test($('#register_email').val())){
                    errormessage += 'Invalid email \n';
                }
            }
            if(! $('#register_phone').val()){
                errormessage += 'No. Handphone dibutuhkan \n';
            }
            if(! $('#register_password').val()){
                errormessage += 'Password dibutuhkan \n';
            }
            if(! $('#register_password_confirm').val()){
                errormessage += 'Konfirmasi password dibutuhkan \n';
            }else{
                if($('#register_password_confirm').val() !== $('#register_password').val()){
                    errormessage += 'Konfirmasi password tidak cocok \n';
                }
            }
            if(errormessage !== ''){
                swal({
                    html          : '<pre>' + errormessage + '</pre>',
                    background  : 'transparent'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type           : 'ajax',
                                method         : 'post',
                                url            : site + 'main/register',
                                data           : {
                                    name      : $('#register_name').val(),
                                    email     : $('#register_email').val(),
                                    phone     : $('#register_phone').val(),
                                    password  : $('#register_password_confirm').val()
                                },
                                async          : true,
                                dataType       : 'json',
                                success        : function(response){
                                    if(response.email){
                                        swal({
                                            html           : '<pre>Email sudah terdaftar' + '<br>' +
                                                             'Harap gunakan email lain</pre>',
                                            type           : "warning",
                                            background     : 'transparent'
                                        });
                                    }else if(response.success){
                                        swal({
                                            html           : '<pre>Pendaftaran berhasil' + '<br>' +
                                                             'Kode verifikasi terkirim ke' + '<br>' + 
                                                             $('#register_email').val() + '</pre>',
                                            type           : "success",
                                            background     : 'transparent'
                                        });
                                    }else if(response.unsent){
                                        swal({
                                            html           : '<pre>Pendaftaran gagal' + '<br>' +
                                                             'Cobalah beberapa saat lagi</pre>',
                                            type           : "warning",
                                            background     : 'transparent'
                                        });
                                    }
                                },
                                error     : function(){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });    
            }
        });
    }
    login();
    function login(){
        $('#btn_login').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var email = /\@+[a-zA-Z0-9]+\.+[a-zA-Z0-9]/m;
            var errormessage = '';
            if(! $('#login_email').val()){
                errormessage += 'Email dibutuhkan \n';
            }else{
                if(!email.test($('#login_email').val())){
                    errormessage += 'Invalid email \n';
                }
            }
            if(! $('#login_password').val()){
                errormessage += 'Password dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    html        : '<pre>' + errormessage + '</pre>',
                    background  : 'transparent'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type           : 'ajax',
                                method         : 'post',
                                url            : site + 'main/login_process',
                                data           : $('#form_login').serialize(),
                                async          : true,
                                dataType       : 'json',
                                success        : function(response){
                                    if(response.match){
                                        swal({
                                            html        : "<pre>Password didn't match</pre>",
                                            background  : 'transparent'
                                        });
                                    }else if(response.success){
                                        $('#modal_login').modal('hide');
                                        swal({
                                            html                : '<pre>Login sukses</pre>',
                                            type                : "success",
                                            background          : 'transparent',
                                        }).then(function(){
                                            location.reload(true);
                                        });
                                    }else if(response.username){
                                        swal({
                                            html        : '<pre>Akun tidak ditemukan</pre>',
                                            background  : 'transparent'
                                        });
                                    }else if(response.inactive){
                                        swal({
                                            html        : '<pre>Akun belum terverifikasi' + '<br>' + 
                                                          'Harap hubungi administrator</pre>',
                                            background  : 'transparent'
                                        });
                                    }else if(response.captcha_false){
                                        swal({
                                            html        : "<pre>Captcha belum tercentang</pre>",
                                            background  : 'transparent'
                                        });
                                    }
                                },
                                error     : function(){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    }).then(function(){
                                        location.reload(true);
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
        $('#c_captcha_login').on('click', function(){
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'get',
                            url         : site + 'main/login_captcha',
                            async       : true,
                            dataType    : 'json',
                            success     : function(data){
                                swal.close();
                                setTimeout(function(){
                                    $('#captcha_content_login').html(data.captcha);
                                },500);
                                $('#captcha_content_login').html(data.captcha);
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
    }
    navigation();
    function navigation(){
        if($(window).width() <= 767){
            $('.ul_menu_agenda').on('click', function(){
                $('.ul_menu_dropdown').hide(500);
                if($('.agenda_dropdown').is(':visible')){
                    $('.agenda_dropdown').hide(500);
                }else{
                    $('.agenda_dropdown').show(500);
                }
            });
            $('.ul_menu_akun').on('click', function(){
                $('.ul_menu_dropdown').hide(500);
                if($('.akun_dropdown').is(':visible')){
                    $('.akun_dropdown').hide(500);
                }else{
                    $('.akun_dropdown').show(500);
                }
            });
        }else{
            $('.ul_menu_agenda').on('mouseover', function(){
                $('.agenda_dropdown').show(500);
            });
            $('.ul_menu_agenda').on('mouseleave', function(){
                $('.agenda_dropdown').hide(500);
            });
             $('.ul_menu_akun').on('mouseover', function(){
                $('.akun_dropdown').show(500);
            });
            $('.ul_menu_akun').on('mouseleave', function(){
                $('.akun_dropdown').hide(500);
            });
        }
    }
    logout_process();
    function logout_process(){
        $('.user_logout').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type           : 'ajax',
                            method         : 'get',
                            url            : site + 'main/logout_process',
                            async          : true,
                            dataType       : 'json',
                            success        : function(response){
                                if(response.success){
                                    location.href = site;
                                }
                            },
                            error     : function(){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
            return false;
        });
    }
});
// $(window).scroll(function(){
//     var bot_h = $('#bottom_header');
//     var scroll = $(window).scrollTop();
//     if($(window).width() >= 975){
//         if (scroll >= 70){
//             bot_h.addClass('sticks');
//             $('#main_content').css('padding-top', '75px');
//         }else{
//             bot_h.removeClass('sticks');
//             $('#main_content').css('padding-top', '0px');
//         }
//     }else{
//         bot_h.addClass('sticks');
//         $('#main_content').css('padding-top', bot_h.height() + 'px');
//     }
// });
// $(document).ready(function(){
//     navigation();
//     function navigation(){
//         $('#ul_menu_product').on('click', function(){
//             $('.ul_menu_dropdown').slideUp(500);
//             if($('#product_dropdown').is(':visible')){
//                 $('#product_dropdown').slideUp(500);
//             }else{
//                 $('#product_dropdown').slideDown(500);
//             }
//         });
//         $('#ul_menu_tools').on('click', function(){
//             $('.ul_menu_dropdown').slideUp(500);
//             if($('#tools_dropdown').is(':visible')){
//                 $('#tools_dropdown').slideUp(500);
//             }else{
//                 $('#tools_dropdown').slideDown(500);
//             }
//         });
//         $('#ul_menu_form').on('click', function(){
//             $('.ul_menu_dropdown').slideUp(500);
//             if($('#form_dropdown').is(':visible')){
//                 $('#form_dropdown').slideUp(500);
//             }else{
//                 $('#form_dropdown').slideDown(500);
//             }
//         });
//     }
// });