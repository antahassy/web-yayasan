$(document).ready(function(){
    function medis_editor_upload_img(image){
        if(image.size > 3000000){
            swal({
                background  : 'transparent',
                html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                              '3 mb</pre>',
                type        : "warning"
            });
        }else{
            var data = new FormData();
            data.append("image", image);
            $.ajax({
                url: site + 'log/medis_kabar_terbaru_upload_img',
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: "POST",
                success: function(url) {
                    $('#info_terbaru_medis').summernote("insertImage", url);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        }
    }
    function medis_editor_delete_img(src) {
        $.ajax({
            data: {src : src},
            type: "POST",
            url: site + 'log/medis_kabar_terbaru_delete_img',
            cache: false,
            success: function(response) {
                console.log(response);
            }
        });
    }
    function non_medis_editor_upload_img(image){
        if(image.size > 3000000){
            swal({
                background  : 'transparent',
                html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                              '3 mb</pre>',
                type        : "warning"
            });
        }else{
            var data = new FormData();
            data.append("image", image);
            $.ajax({
                url: site + 'log/non_medis_kabar_terbaru_upload_img',
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: "POST",
                success: function(url) {
                    $('#info_terbaru_non_medis').summernote("insertImage", url);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        }
    }
    function non_medis_editor_delete_img(src) {
        $.ajax({
            data: {src : src},
            type: "POST",
            url: site + 'log/non_medis_kabar_terbaru_delete_img',
            cache: false,
            success: function(response) {
                console.log(response);
            }
        });
    }
	var table_medis, table_non_medis;
	swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            $('#content_medis').slideDown(500);
            setTimeout(function(){
                riwayat_galang_dana_medis();
            },500);
        }
    });
	function riwayat_galang_dana_medis(){
        table_medis = $('#table_galang_dana_medis').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            info                : false, 
            lengthMenu          : [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, 'Semua']],
            initComplete: function(){
                swal.close();
            },
            ajax            : {
                url         : site + 'log/riwayat_galang_dana_medis',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
	function riwayat_galang_dana_non_medis(){
        table_non_medis = $('#table_galang_dana_non_medis').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            info                : false, 
            lengthMenu          : [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, 'Semua']],
            initComplete: function(){
                swal.close();
            },
            ajax            : {
                url         : site + 'log/riwayat_galang_dana_non_medis',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
	main();
	function main(){
        var modal_kabar_terbaru_medis;
        $('#modal_kabar_terbaru_medis').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_kabar_terbaru_medis = true;
        });
        $('#modal_kabar_terbaru_medis').on('hide.bs.modal', function(){
            if(modal_kabar_terbaru_medis){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_kabar_terbaru_medis = false;
                setTimeout(function(){
                    $('#modal_kabar_terbaru_medis').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var modal_kabar_terbaru_non_medis;
        $('#modal_kabar_terbaru_non_medis').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_kabar_terbaru_non_medis = true;
        });
        $('#modal_kabar_terbaru_non_medis').on('hide.bs.modal', function(){
            if(modal_kabar_terbaru_non_medis){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_kabar_terbaru_non_medis = false;
                setTimeout(function(){
                    $('#modal_kabar_terbaru_non_medis').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var modal_dokumen;
        $('#modal_dokumen').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_dokumen = true;
        });
        $('#modal_dokumen').on('hide.bs.modal', function(){
            if(modal_dokumen){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_dokumen = false;
                setTimeout(function(){
                    $('#modal_dokumen').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#table_galang_dana_medis').on('click', '.btn_dokumen', function(){
            var id_kasus = $(this).attr('id');
            $('#form_dokumen')[0].reset();
            $('#id_data').val(id_kasus);
            $('#modal_form').find('.modal-title').text('Dokumen Pencairan Dana');
            $('#form_dokumen').attr('action', site + 'log/dokumen_pencairan_medis');
            $('#btn_save_dokumen').text('Simpan');
            $('#delete_preview_items, #delete_preview_itemss, #delete_preview_itemsss').css('display','none');
            $('#preview_items, #preview_itemss, #preview_itemsss').attr('src', '');
            $('#hide_rekening, #hide_ktp, #hide_kk').val('');
            swal.close();
            display_image();
            display_images();
            display_imagess();
            $('#modal_dokumen').modal('show');
        });
        $('#table_galang_dana_non_medis').on('click', '.btn_dokumen', function(){
            var id_kasus = $(this).attr('id');
            $('#form_dokumen')[0].reset();
            $('#id_data').val(id_kasus);
            $('#modal_form').find('.modal-title').text('Dokumen Pencairan Dana');
            $('#form_dokumen').attr('action', site + 'log/dokumen_pencairan_non_medis');
            $('#btn_save_dokumen').text('Simpan');
            $('#delete_preview_items, #delete_preview_itemss, #delete_preview_itemsss').css('display','none');
            $('#preview_items, #preview_itemss, #preview_itemsss').attr('src', '');
            $('#hide_rekening, #hide_ktp, #hide_kk').val('');
            swal.close();
            display_image();
            display_images();
            display_imagess();
            $('#modal_dokumen').modal('show');
        });
        $('#btn_save_dokumen').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var url             = $('#form_dokumen').attr('action');
            var form_dokumen       = $('#form_dokumen')[0];
            form_dokumen           = new FormData(form_dokumen);
            var errormessage    = '';
            if(! $('#hide_rekening').val()){
                 errormessage += 'Foto buku rekening dibutuhkan \n';
            }
            if(! $('#hide_ktp').val()){
                 errormessage += 'Foto ktp dibutuhkan \n';
            }
            if(! $('#hide_kk').val()){
                 errormessage += 'Foto kartu keluarga dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    html                : '<pre>Dokumen pencairan dana' + '<br>' + 
                                          'Tidak dapat diubah kembali' + '<br>' + 
                                          'Apakah data sudah benar ?</pre>',
                    type                : "question",
                    background          : 'transparent',
                    showCancelButton    : true,
                    cancelButtonText    : 'Tidak',
                    confirmButtonText   : 'Ya'
                }).then((result) => {
                    if(result.value){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                setTimeout(function(){
                                    $.ajax({
                                        type        : 'ajax',
                                        method      : 'post',
                                        url         : url,
                                        data        : form_dokumen,
                                        async       : true,
                                        processData : false,
                                        contentType : false,
                                        cache       : false,
                                        dataType    : 'json',
                                        success     : function(response){
                                            if(response.success){
                                                $('#modal_dokumen').modal('hide');
                                                swal({
                                                    html        : '<pre>Dokumen berhasil disimpan' + '<br>' + 
                                                                  'Kami akan segera menghubungi anda</pre>',
                                                    background  : 'transparent',
                                                    type        : "success"
                                                });
                                                setTimeout(function(){
                                                    table_medis.ajax.reload();
                                                    table_non_medis.ajax.reload();
                                                },500);
                                            }
                                            if(response.slideshow){
                                                swal({
                                                    html        : '<pre>Image required</pre>',
                                                    background  : 'transparent'
                                                });
                                            }
                                        },
                                        error: function (){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Connection lost' + '<br>' + 
                                                              'Please try again</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    });
                                },500);
                            }
                        });
                    }
                });
            }
        });
        $('#table_galang_dana_medis').on('click', '.btn_terbaru', function(){
            var id_kasus = $(this).attr('id');
            var judul_kasus = $(this).attr('alt');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'log/kabar_terbaru_medis',
                            data        : {id_kasus : id_kasus},
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                var modal_content = '';
                                if(data.empty){
                                    modal_content =
                                    '<div>' +
                                        '<div style="margin-bottom: 15px;">Penggalangan belum memiliki kabar terbaru</div>' +
                                        '<button type="button" class="btn btn-warning add_terbaru_medis" data="' + id_kasus + '">Tambah Kabar Terbaru</button>' +
                                    '</div>';
                                }else{
                                    for(i = 0; i < data.length; i ++){
                                        var createds = '';
                                        if(data[i].status == '0'){
                                            createds = data[i].created + ' yang lalu<br><i style="color: rgba(0,0,0,0.5)">Menunggu Verifikasi Peduli Indonesia</i>';
                                        }else{
                                            createds = data[i].created + ' yang lalu<br><i style="color: rgba(0,0,0,0.5)">Publish</i>';
                                        }
                                        modal_content += 
                                        '<div class="text_title" style="text-transform: capitalize; font-weight: 600;">' + data[i].judul + '</div>' +
                                        '<div style="font-size: 14px; margin-bottom: 15px;">' + createds + '</div>' +
                                        '<div style="border-bottom: 3px solid rgb(255,197,70); margin-bottom: 15px;">' + data[i].kabar_terbaru + '</div>';
                                    }
                                    modal_content +=
                                    '<div>' +
                                        '<button type="button" class="btn btn-warning add_terbaru_medis" data="' + id_kasus + '">Tambah Kabar Terbaru</button>' +
                                    '</div>';
                                }
                                $('#modal_kabar_terbaru_medis').find('.modal-body').html(modal_content);
                                $('#modal_kabar_terbaru_medis').find('.modal-title').text(judul_kasus);
                                swal.close();
                                $('#modal_kabar_terbaru_medis').modal('show');
                                textarea_medis();
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_galang_dana_non_medis').on('click', '.btn_terbaru', function(){
            var id_kasus = $(this).attr('id');
            var judul_kasus = $(this).attr('alt');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'log/kabar_terbaru_non_medis',
                            data        : {id_kasus : id_kasus},
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                var modal_content = '';
                                if(data.empty){
                                    modal_content =
                                    '<div>' +
                                        '<div style="margin-bottom: 15px;">Penggalangan belum memiliki kabar terbaru</div>' +
                                        '<button type="button" class="btn btn-warning add_terbaru_non_medis" data="' + id_kasus + '">Tambah Kabar Terbaru</button>' +
                                    '</div>';
                                }else{
                                    for(i = 0; i < data.length; i ++){
                                        var createds = '';
                                        if(data[i].status == '0'){
                                            createds = data[i].created + ' yang lalu<br><i style="color: rgba(0,0,0,0.5)">Menunggu Verifikasi Peduli Indonesia</i>';
                                        }else{
                                            createds = data[i].created + ' yang lalu<br><i style="color: rgba(0,0,0,0.5)">Publish</i>';
                                        }
                                        modal_content += 
                                        '<div class="text_title" style="text-transform: capitalize; font-weight: 600;">' + data[i].judul + '</div>' +
                                        '<div style="font-size: 14px; margin-bottom: 15px;">' + createds + '</div>' +
                                        '<div style="border-bottom: 3px solid rgb(255,197,70); margin-bottom: 15px;">' + data[i].kabar_terbaru + '</div>';
                                    }
                                    modal_content +=
                                    '<div>' +
                                        '<button type="button" class="btn btn-warning add_terbaru_non_medis" data="' + id_kasus + '">Tambah Kabar Terbaru</button>' +
                                    '</div>';
                                }
                                $('#modal_kabar_terbaru_non_medis').find('.modal-body').html(modal_content);
                                $('#modal_kabar_terbaru_non_medis').find('.modal-title').text(judul_kasus);
                                swal.close();
                                $('#modal_kabar_terbaru_non_medis').modal('show');
                                textarea_non_medis();
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
		$('.galang_dana_medis').css({'background':'rgb(255,197,70)', 'color':'#fff'});
		$('.galang_dana_medis').on('click', function(){
			$('.l_donasi').css({'background':'#fff', 'color':'rgb(255,197,70)'});
			$(this).css({'background':'rgb(255,197,70)', 'color':'#fff'});
			$('.content_table').hide();
			swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    $('#content_medis').slideDown(500);
                    setTimeout(function(){
                        riwayat_galang_dana_medis();
                    },500);
                }
            });
		});
		$('.galang_dana_non_medis').on('click', function(){
			$('.l_donasi').css({'background':'#fff', 'color':'rgb(255,197,70)'});
			$(this).css({'background':'rgb(255,197,70)', 'color':'#fff'});
			$('.content_table').hide();
			swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    $('#content_non_medis').slideDown(500);
                    setTimeout(function(){
                        riwayat_galang_dana_non_medis();
                    },500);
                }
            });
		});
	}
    function last_medis_process(){
        $('#info_terbaru_medis').summernote({
            popover: {
                image: [
                    ['remove', ['removeMedia']]
                ],
            },
            callbacks: {
                onImageUpload: function(image) {
                    medis_editor_upload_img(image[0]);
                },
                onMediaDelete : function(target) {
                    medis_editor_delete_img(target[0].src);
                }
            },
            placeholder: 'Ceritakan kabar terbaru tentang penggalangan anda',
            tabsize: 2,
            height: 300,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'italic']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'picture']]
            ]
        });
        $('#btn_save_medis').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var url             = site + 'log/save_kabar_terbaru_medis';
            var form_data       = $('#form_terbaru_medis')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#judul_terbaru_medis').val()){
                 errormessage += 'Judul kabar terbaru dibutuhkan \n';
            }
            if(! $('#info_terbaru_medis').val()){
                 errormessage += 'Deskripsi kabar terbaru dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    html                : '<pre>Data tidak dapat diubah & dihapus' + '<br>' +
                                          'Apakah kabar terbaru sudah benar ?</pre>',
                    type                : "question",
                    background          : 'transparent',
                    showCancelButton    : true,
                    cancelButtonText    : 'Tidak',
                    confirmButtonText   : 'Ya'
                }).then((result) => {
                    if(result.value){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                setTimeout(function(){
                                    $.ajax({
                                        type        : 'ajax',
                                        method      : 'post',
                                        url         : url,
                                        data        : form_data,
                                        async       : true,
                                        processData : false,
                                        contentType : false,
                                        cache       : false,
                                        dataType    : 'json',
                                        success     : function(response){
                                            if(response.success){
                                                $('#modal_kabar_terbaru_medis').modal('hide');
                                                setTimeout(function(){
                                                    swal.close();
                                                    table_medis.ajax.reload();
                                                },500);
                                            }
                                        },
                                        error: function (){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Connection lost' + '<br>' + 
                                                              'Please try again</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    });
                                },500);
                            }
                        });
                    }
                });
            }
        });
    }
    function textarea_medis(){
        $('.add_terbaru_medis').on('click', function(){
            var id_data = $(this).attr('data');
            var form_content = 
            '<form id="form_terbaru_medis">' +
                '<input type="hidden" name="id_data" value="' + id_data + '">' +
                '<input type="text" name="judul_terbaru_medis" id="judul_terbaru_medis" class="form-control" placeholder="Judul kabar terbaru">' +
                '<textarea class="form-control" name="info_terbaru_medis" id="info_terbaru_medis" rows="10"></textarea>' +
            '</form>' +
            '<div class="w-100 text-center" style="margin-top: 25px;">' +
                '<button type="button" class="btn btn-warning" id="btn_save_medis">Simpan Kabar Terbaru</button>' +
            '</div>';
            $(this).parent().html(form_content);
            last_medis_process();
        });
    }
    function last_non_medis_process(){
        $('#info_terbaru_non_medis').summernote({
            popover: {
                image: [
                    ['remove', ['removeMedia']]
                ],
            },
            callbacks: {
                onImageUpload: function(image) {
                    non_medis_editor_upload_img(image[0]);
                },
                onMediaDelete : function(target) {
                    non_medis_editor_delete_img(target[0].src);
                }
            },
            placeholder: 'Ceritakan kabar terbaru tentang penggalangan anda',
            tabsize: 2,
            height: 300,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'italic']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'picture']]
            ]
        });
        $('#btn_save_non_medis').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var url             = site + 'log/save_kabar_terbaru_non_medis';
            var form_data       = $('#form_terbaru_non_medis')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#judul_terbaru_non_medis').val()){
                 errormessage += 'Judul kabar terbaru dibutuhkan \n';
            }
            if(! $('#info_terbaru_non_medis').val()){
                 errormessage += 'Deskripsi kabar terbaru dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    html                : '<pre>Data tidak dapat diubah & dihapus' + '<br>' +
                                          'Apakah kabar terbaru sudah benar ?</pre>',
                    type                : "question",
                    background          : 'transparent',
                    showCancelButton    : true,
                    cancelButtonText    : 'Tidak',
                    confirmButtonText   : 'Ya'
                }).then((result) => {
                    if(result.value){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                setTimeout(function(){
                                    $.ajax({
                                        type        : 'ajax',
                                        method      : 'post',
                                        url         : url,
                                        data        : form_data,
                                        async       : true,
                                        processData : false,
                                        contentType : false,
                                        cache       : false,
                                        dataType    : 'json',
                                        success     : function(response){
                                            if(response.success){
                                                $('#modal_kabar_terbaru_non_medis').modal('hide');
                                                setTimeout(function(){
                                                    swal.close();
                                                    table_non_medis.ajax.reload();
                                                },500);
                                            }
                                        },
                                        error: function (){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Connection lost' + '<br>' + 
                                                              'Please try again</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    });
                                },500);
                            }
                        });
                    }
                });
            }
        });
    }
    function textarea_non_medis(){
        $('.add_terbaru_non_medis').on('click', function(){
            var id_data = $(this).attr('data');
            var form_content = 
            '<form id="form_terbaru_non_medis">' +
                '<input type="hidden" name="id_data" value="' + id_data + '">' +
                '<input type="text" name="judul_terbaru_non_medis" id="judul_terbaru_non_medis" class="form-control" placeholder="Judul kabar terbaru">' +
                '<textarea class="form-control" name="info_terbaru_non_medis" id="info_terbaru_non_medis" rows="10"></textarea>' +
            '</form>' +
            '<div class="w-100 text-center" style="margin-top: 25px;">' +
                '<button type="button" class="btn btn-warning" id="btn_save_non_medis">Simpan Kabar Terbaru</button>' +
            '</div>';
            $(this).parent().html(form_content);
            last_non_medis_process();
        });
    }
    function display_image(){
        function preview(image){
            if(image.files[0].size > 3000000){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                                  '3 mb</pre>',
                    type        : "warning"
                });
                $("#rekening").val('');
            }else{
                if(image.files && image.files[0]){
                    var reader      = new FileReader();
                    reader.onload   = function(event){
                        $('#preview_items').attr('src', event.target.result);
                        $('#preview_items').attr('title', image.files[0].name);
                        $('#hide_rekening').val(image.files[0].name);
                        $('#delete_preview_items').css('display','block');
                    }
                    reader.readAsDataURL(image.files[0]);
                }
            }
        }
        $("#rekening").on('change', function(){
            preview(this);
            var names = $(this).val();
            var file_names = names.replace(/^.*\\/, "");
        });
        $('#delete_preview_items').on('click', function(){
            $('#delete_preview_items').css('display','none');
            $('#preview_items').attr('src', '');
            $('#rekening, #get_rekening, #hide_rekening').val('');
        });
    }
    function display_images(){
        function preview(image){
            if(image.files[0].size > 3000000){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                                  '3 mb</pre>',
                    type        : "warning"
                });
                $("#ktp").val('');
            }else{
                if(image.files && image.files[0]){
                    var reader      = new FileReader();
                    reader.onload   = function(event){
                        $('#preview_itemss').attr('src', event.target.result);
                        $('#preview_itemss').attr('title', image.files[0].name);
                        $('#hide_ktp').val(image.files[0].name);
                        $('#delete_preview_itemss').css('display','block');
                    }
                    reader.readAsDataURL(image.files[0]);
                }
            }
        }
        $("#ktp").on('change', function(){
            preview(this);
            var names = $(this).val();
            var file_names = names.replace(/^.*\\/, "");
        });
        $('#delete_preview_itemss').on('click', function(){
            $('#delete_preview_itemss').css('display','none');
            $('#preview_itemss').attr('src', '');
            $('#ktp, #get_ktp, #hide_ktp').val('');
        });
    }
    function display_imagess(){
        function preview(image){
            if(image.files[0].size > 3000000){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                                  '3 mb</pre>',
                    type        : "warning"
                });
                $("#kk").val('');
            }else{
                if(image.files && image.files[0]){
                    var reader      = new FileReader();
                    reader.onload   = function(event){
                        $('#preview_itemsss').attr('src', event.target.result);
                        $('#preview_itemsss').attr('title', image.files[0].name);
                        $('#hide_kk').val(image.files[0].name);
                        $('#delete_preview_itemsss').css('display','block');
                    }
                    reader.readAsDataURL(image.files[0]);
                }
            }
        }
        $("#kk").on('change', function(){
            preview(this);
            var names = $(this).val();
            var file_names = names.replace(/^.*\\/, "");
        });
        $('#delete_preview_itemsss').on('click', function(){
            $('#delete_preview_itemsss').css('display','none');
            $('#preview_itemsss').attr('src', '');
            $('#kk, #get_kk, #hide_kk').val('');
        });
    }
});