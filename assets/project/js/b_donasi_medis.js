$(document).ready(function(){
    var nama_bulan = new Array();
    nama_bulan[1] = 'January';
    nama_bulan[2] = 'February';
    nama_bulan[3] = 'March';
    nama_bulan[4] = 'April';
    nama_bulan[5] = 'May';
    nama_bulan[6] = 'June';
    nama_bulan[7] = 'July';
    nama_bulan[8] = 'August'; 
    nama_bulan[9] = 'September'; 
    nama_bulan[10] = 'October';
    nama_bulan[11] = 'November'; 
    nama_bulan[12] = 'December';
    var table;
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                s_side_data_table();
            },500);
        }
    });
    function autocomplete(){
        $('#judul_kasus').autocomplete({
            delay   : 1500,
            source  : site + 'b_donasi_medis/autocomplete_judul_kasus',
            select  : function(event, ui){
                var value = ui.item.value;
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type        : 'ajax',
                                method      : 'post',
                                url         : site + 'b_donasi_medis/id_judul_kasus',
                                data        : {judul : value},
                                async       : true,
                                dataType    : 'json',
                                success     : function(data){
                                    $('#id_kasus').val(data.id_kasus);
                                    swal.close();
                                },
                                error       : function(){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
    }
    function s_side_data_table(){
        table = $('#table_data').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            lengthMenu          : [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, 'Semua']],
            initComplete: function(){
                swal.close();
                main();
                autocomplete();
            },
            ajax            : {
                url         : site + 'b_donasi_medis/s_side_data',
                method      : 'post'
            }
        });
    }
    function list_bank(bank_val){
        $.ajax({
            type        : 'ajax',
            method      : 'get',
            url         : site + 'b_donasi_medis/list_bank',
            dataType    : "json",
            async       : true,
            success: function(bank_data){
                if(bank_data.empty){
                    swal({
                        background  : 'transparent',
                        html        : '<pre>Bank belum tersedia</pre>',
                        type        : "warning"
                    });
                }else{
                    var s_bank = '<option value="">Pilih Bank</option>';
                    for(i = 0; i < bank_data.length; i ++){
                        s_bank += '<option value="' + bank_data[i].id_bank + '">' + bank_data[i].name + '</option>';
                    }
                    $('#s_bank').html(s_bank);
                    if(bank_val != ''){
                        $('#s_bank').val(bank_val);
                    }else{
                        $('#s_bank').val('');
                    }
                    swal.close();
                    $('#modal_form').modal('show');
                    data_process();
                }
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
    }
    function main(){
        $('#tgl_donasi').datepicker({
            maxDate: new Date(),
            yearRange : '-1:+1',
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            dayNames: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
            dayNamesMin: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
            monthNamesShort: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            monthNames: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            beforeShow: function() {
                $(document).off('focusin.bs.modal');
            },
            onClose: function(){
                $(document).on('focusin.bs.modal');
            }
        });
        $('#s_metode').on('change', function(){
            if($(this).val() == '1'){
                $('#va_content').slideDown(250);
            }else{
                $('#va_content').slideUp(250);
                $('#va_name').val('');
                $('#va_number').val('');
            }
        });
        $('#donasi').on('keyup', function(){
            $(this).val(function(index, value) {
                return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            });
        });
        var column_gambar_items
        $('#column_gambar_items').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            column_gambar_items = true;
        });
        $('#column_gambar_items').on('hide.bs.modal', function(){
            if(column_gambar_items){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                column_gambar_items = false;
                setTimeout(function(){
                    $('#column_gambar_items').modal('hide');
                },500);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var modal_form;
        $('#modal_form').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_form = true;
        });
        $('#modal_form').on('hide.bs.modal', function(){
            if(modal_form){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_form = false;
                setTimeout(function(){
                    $('#modal_form').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#btn_add').on('click', function(){
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $('#form_data')[0].reset();
                        $('#id_kasus').val('');
                        $('#modal_form').find('.modal-title').text('Add');
                        $('#form_data').attr('action', site + 'b_donasi_medis/save');
                        $('#btn_process').text('Save');
                        $('#va_content').hide();
                        var bank_val = '';
                        list_bank(bank_val);
                    },500);
                }
            });
        });
        $('#table_data').on('click', '#btn_edit', function(){
            var id = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type           : 'ajax',
                            method         : 'post',
                            url            : site + 'b_donasi_medis/edit',
                            data           : {id : id},
                            async          : true,
                            dataType       : 'json',
                            success        : function(data){
                                $('#form_data')[0].reset();
                                $('#id_data').val(data.id_donasi_medis);
                                $('#id_kasus').val(data.id_kasus_medis);
                                $('#judul_kasus').val(data.judul);
                                $('#s_metode').val(data.metode);
                                $('#va_name').val(data.va_name);
                                $('#va_number').val(data.va_number);
                                $('#donasi').val(data.donasi.replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                                $('#tgl_donasi').val(data.tgl_donasi);
                                $('#s_status').val(data.status);
                                $('#doa').val(data.doa);
                                $('#modal_form').find('.modal-title').text('Edit');
                                $('#form_data').attr('action', site + 'b_donasi_medis/update');
                                $('#btn_process').text('Update');
                                var bank_val = '';
                                if(data.id_bank != '0'){
                                    bank_val = data.id_bank;
                                }
                                list_bank(bank_val);
                            },
                            error     : function(){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_data').on('click', '.file_data', function(){
            $('#image_column_items').attr('src', $(this).attr('src'));
            $('#column_gambar_items').modal('show');
        });
        $('#table_data').on('click', '.btn_pending', function(){
            var id_data = $(this).attr('data');
            swal({
                html                : '<pre>Apakah donatur' + '<br>' + 
                                      'Sudah melakukan donasi ?</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'No',
                confirmButtonText   : 'Yes'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    data        : {id_data : id_data},
                                    url         : site + 'b_donasi_medis/donasi_sukses',
                                    dataType    : "json",
                                    async       : true,
                                    success: function(response){
                                        if(response.success){
                                            swal.close();
                                            setTimeout(function(){
                                                table.ajax.reload();
                                            },500);
                                        }
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
    }
    function data_process(){
        $('#btn_process').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var url             = $('#form_data').attr('action');
            var form_data       = $('#form_data')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#judul_kasus').val()){
                    errormessage += 'Judul kasus dibutuhkan \n';
            }else{
                if(! $('#id_kasus').val()){
                    errormessage += 'Judul kasus tidak sesuai \n';
                }
            }
            if(! $('#s_metode').val()){
                 errormessage += 'Metode dibutuhkan \n';
            }else{
                if($('#s_metode').val() == '1'){
                    if(! $('#va_name').val()){
                        errormessage += 'Nama virtual account dibutuhkan \n';
                    }
                    if(! $('#va_number').val()){
                        errormessage += 'Nomor virtual account dibutuhkan \n';
                    }
                }
            }
            if(! $('#s_bank').val()){
                 errormessage += 'Bank dibutuhkan \n';
            }
            if(! $('#donasi').val()){
                 errormessage += 'Nominal donasi dibutuhkan \n';
            }else{
                if(Number($('#donasi').val().replaceAll(".", "")) < 10000){
                    errormessage += 'Minimal donasi Rp. 10.000 \n';
                }
            }
            if(! $('#tgl_donasi').val()){
                 errormessage += 'Tanggal donasi dibutuhkan \n';
            }
            if(! $('#s_status').val()){
                 errormessage += 'Status donasi dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                if($('#s_status').val() == '1'){
                    swal({
                        html                : '<pre>Data tidak dapat diubah' + '<br>' + 
                                              'Bila status donasi sukses' + '<br>' + 
                                              'Apakah data sudah benar ?</pre>',
                        type                : "question",
                        background          : 'transparent',
                        showCancelButton    : true,
                        cancelButtonText    : 'Tidak',
                        confirmButtonText   : 'Ya'
                    }).then((result) => {
                        if(result.value){
                            swal({
                                showConfirmButton   : false,
                                allowOutsideClick   : false,
                                allowEscapeKey      : false,
                                background          : 'transparent',
                                onOpen  : function(){
                                    swal.showLoading();
                                    setTimeout(function(){
                                        $.ajax({
                                            type           : 'ajax',
                                            method         : 'post',
                                            url            : url,
                                            data           : form_data,
                                            async          : true,
                                            processData    : false,
                                            contentType    : false,
                                            cache          : false,
                                            dataType       : 'json',
                                            success        : function(response){
                                                if(response.success){
                                                    $('#modal_form').modal('hide');
                                                    $('#form_data')[0].reset();
                                                    swal({
                                                        background  : 'transparent',
                                                        html        : '<pre>Data berhasil ' + response.type + '</pre>',
                                                        type        : "success"
                                                    }).then(function(){
                                                        setTimeout(function(){
                                                            table.ajax.reload();
                                                        },500);
                                                    });
                                                }
                                            },
                                            error   : function(){
                                                swal({
                                                    background  : 'transparent',
                                                    html        : '<pre>Connection lost' + '<br>' + 
                                                                  'Please try again</pre>',
                                                    type        : "warning"
                                                });
                                            }
                                        });
                                    },500);
                                }
                            });
                        }
                    });
                }else{
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type           : 'ajax',
                                    method         : 'post',
                                    url            : url,
                                    data           : form_data,
                                    async          : true,
                                    processData    : false,
                                    contentType    : false,
                                    cache          : false,
                                    dataType       : 'json',
                                    success        : function(response){
                                        if(response.success){
                                            $('#modal_form').modal('hide');
                                            $('#form_data')[0].reset();
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Data berhasil ' + response.type + '</pre>',
                                                type        : "success"
                                            }).then(function(){
                                                setTimeout(function(){
                                                    table.ajax.reload();
                                                },500);
                                            });
                                        }
                                    },
                                    error   : function(){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            }
            return false;
        });
    }
});