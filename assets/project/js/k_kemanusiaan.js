$(document).ready(function(){
    var nama_bulan = new Array();
    nama_bulan[1] = 'January';
    nama_bulan[2] = 'February';
    nama_bulan[3] = 'March';
    nama_bulan[4] = 'April';
    nama_bulan[5] = 'May';
    nama_bulan[6] = 'June';
    nama_bulan[7] = 'July';
    nama_bulan[8] = 'August'; 
    nama_bulan[9] = 'September'; 
    nama_bulan[10] = 'October';
    nama_bulan[11] = 'November'; 
    nama_bulan[12] = 'December';
    var table;
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                s_side_data_table();
            },500);
        }
    });
    function s_side_data_table(){
        table = $('#table_data').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            lengthMenu          : [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, 'Semua']],
            initComplete: function(){
                swal.close();
            },
            ajax            : {
                url         : site + 'k_kemanusiaan/s_side_data',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
    main();
    function main(){
        var column_gambar_items
        $('#column_gambar_items').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            column_gambar_items = true;
        });
        $('#column_gambar_items').on('hide.bs.modal', function(){
            if(column_gambar_items){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                column_gambar_items = false;
                setTimeout(function(){
                    $('#column_gambar_items').modal('hide');
                },500);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var modal_detail;
        $('#modal_detail').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_detail = true;
        });
        $('#modal_detail').on('hide.bs.modal', function(){
            if(modal_detail){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_detail = false;
                setTimeout(function(){
                    $('#modal_detail').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var modal_kabar_terbaru;
        $('#modal_kabar_terbaru').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_kabar_terbaru = true;
        });
        $('#modal_kabar_terbaru').on('hide.bs.modal', function(){
            if(modal_kabar_terbaru){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_kabar_terbaru = false;
                setTimeout(function(){
                    $('#modal_kabar_terbaru').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var modal_transfer;
        $('#modal_transfer').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_transfer = true;
        });
        $('#modal_transfer').on('hide.bs.modal', function(){
            if(modal_transfer){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_transfer = false;
                setTimeout(function(){
                    $('#modal_transfer').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#table_data').on('click', '#btn_transfer', function(){
            var id_kasus = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'k_kemanusiaan/get_transfer',
                            data        : {id_kasus : id_kasus},
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                //KTP
                                var ktp_detail = 'KTP belum tersedia';
                                if(data.file_ktp != ''){
                                    ktp_detail = '<img src="' + site + 'assets/project/pencairan_non_medis/' + data.file_ktp + '" style="width: 300px; cursor: pointer;" class="file_data">';
                                }
                                $('#ktp_content').html(ktp_detail);
                                //KK
                                var kk_detail = 'Kartu Keluarga belum tersedia';
                                if(data.file_kk != ''){
                                    kk_detail = '<img src="' + site + 'assets/project/pencairan_non_medis/' + data.file_kk + '" style="width: 300px; cursor: pointer;" class="file_data">';
                                }
                                $('#kk_content').html(kk_detail);
                                //Tabungan
                                var tabungan_detail = 'Buku tabungan belum tersedia';
                                if(data.file_rekening != ''){
                                    tabungan_detail = '<img src="' + site + 'assets/project/pencairan_non_medis/' + data.file_rekening + '" style="width: 300px; cursor: pointer;" class="file_data">';
                                }
                                $('#tabungan_content').html(tabungan_detail);
                                //Donasi
                                $('#donasi_content').html('Rp. ' + data.donasi.replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                                //Transfer
                                var transfer_val = Number(data.donasi - ((7/100) * Number(data.donasi)));
                                transfer_val = transfer_val.toFixed(0);
                                $('#transfer_content').html('Rp. ' + transfer_val.replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                                swal.close();
                                $('#modal_transfer').modal('show');
                                transfer_act();
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_data').on('click', '#btn_detail', function(){
            var id_kasus = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'k_kemanusiaan/detail',
                            data        : {id_kasus : id_kasus},
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                //Identitas
                                $('#detail_identitas_1').text(data.pekerjaan);
                                $('#detail_identitas_2').text(data.organisasi);
                                $('#detail_identitas_3').text(data.phone.replace(/\B(?=(\d{4})+(?!\d))/g, "-"));
                                $('#detail_identitas_4').text(data.medsos);
                                $('#detail_identitas_5').text(data.akun_medsos);
                                $('#detail_identitas_6').text(data.domisili);
                                $('#detail_identitas_7').text(data.tentang);
                                //Donasi
                                $('#detail_donasi_1').text('Rp. ' + data.donasi.replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                                $('#detail_donasi_2').text(data.terakhir);
                                
                                var start = new Date(data.now_date);
                                var end = new Date(data.jatuh_tempo);
                                var day_left = Math.abs(end - start);
                                day_left = Math.ceil(day_left / (1000 * 60 * 60 * 24)); 

                                $('#detail_donasi_3').text(day_left + ' Hari');
                                $('#detail_donasi_4').text(data.keperluan);
                                $('#detail_donasi_5').text(data.penerima);
                                $('#detail_donasi_6').text(data.penggunaan);
                                //Detail
                                $('#detail_detail_1').text(data.bantuan);
                                $('#detail_detail_2').text(data.judul);
                                $('#detail_detail_3').text(data.link);
                                $('#detail_detail_4').text(data.tujuan);
                                $('#detail_detail_5').attr('src', site + 'assets/project/kasus_non_medis/' + data.sampul);
                                //Cerita
                                $('#detail_cerita').html(data.cerita);
                                //Ajakan
                                $('#detail_ajakan').text(data.ajakan);
                                swal.close();
                                $('#modal_detail').modal('show');
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_data').on('click', '#btn_homepage', function(){
            var id_kasus = $(this).attr('data');
            var homepage = $(this).attr('alt');
            var question = '', link_url = '';
            if(homepage == '0'){
                question = 'Set Public ?';
                link_url = site + 'k_kemanusiaan/public_case';
            }else{
                question = 'Set Privat ?';
                link_url = site + 'k_kemanusiaan/privat_case';
            }
            swal({
                html                : '<pre>' + question + '</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'No',
                confirmButtonText   : 'Yes'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    data        : {id_kasus : id_kasus},
                                    url         : link_url,
                                    dataType    : "json",
                                    async       : true,
                                    success: function(data){
                                        swal.close();
                                        setTimeout(function(){
                                            table.ajax.reload();
                                        },500);
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
        $('#table_data').on('click', '#btn_delete', function(){
            var id_kasus = $(this).attr('data');
            var img_kasus = $(this).attr('alt');
            swal({
                html                : '<pre>Delete this data ?</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'No',
                confirmButtonText   : 'Yes'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    url         : site + 'k_kemanusiaan/delete',
                                    data        : {
                                        id_kasus   : id_kasus,
                                        img_kasus  : img_kasus
                                    },
                                    dataType    : "json",
                                    async       : true,
                                    success: function(response){
                                        if(response.success){
                                            swal.close();
                                            setTimeout(function(){
                                                table.ajax.reload();
                                            },500);
                                        }
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
        $('#table_data').on('click', '#btn_verify', function(){
            var id_kasus = $(this).attr('data');
            swal({
                html                : '<pre>Verify Data ?</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'No',
                confirmButtonText   : 'Yes'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    url         : site + 'k_kemanusiaan/verify',
                                    data        : {
                                        id_kasus   : id_kasus
                                    },
                                    dataType    : "json",
                                    async       : true,
                                    success: function(response){
                                        if(response.success){
                                            swal.close();
                                            setTimeout(function(){
                                                table.ajax.reload();
                                            },500);
                                        }
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
        $('#table_data').on('click', '#btn_terbaru', function(){
            var id_kasus = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'k_kemanusiaan/kabar_terbaru',
                            data        : {
                                id_kasus   : id_kasus
                            },
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                if(data.empty){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Penggalangan belum memiliki' + '<br>' + 
                                                      'Kabar terbaru</pre>'
                                    });
                                }else{
                                    var modal_content = '';
                                    for(i = 0; i < data.length; i ++){
                                        if(data[i].status != '0'){
                                            modal_content += 
                                            '<div style="border-bottom: 3px solid rgb(255,197,70); margin-bottom: 15px;">' +
                                                '<div class="text_title" style="text-transform: capitalize; font-weight: 600;">' + data[i].judul + '</div>' +
                                                '<div style="font-size: 14px;">' + data[i].created + ' yang lalu</div>' +
                                                '<div>' + data[i].kabar_terbaru + '</div>' +
                                                '<div style="padding-bottom: 5px;">' +
                                                    '<i>Terverifikasi pada ' + data[i].verified + '</i>' +
                                                '</div>' +
                                            '</div>';
                                        }else{
                                            modal_content += 
                                            '<div style="border-bottom: 3px solid rgb(255,197,70); margin-bottom: 15px;">' +
                                                '<div class="text_title" style="text-transform: capitalize; font-weight: 600;">' + data[i].judul + '</div>' +
                                                '<div style="font-size: 14px;">' + data[i].created + ' yang lalu</div>' +
                                                '<div>' + data[i].kabar_terbaru + '</div>' +
                                                '<div style="padding-bottom: 5px;">' +
                                                    '<button type="button" class="btn btn-outline-danger btn_news_delete" data="' + data[i].id_kabar_non_medis + '">Hapus</button>' +
                                                '</div>' +
                                                '<div style="border-bottom: 3px solid rgb(255,197,70); margin-bottom: 15px; padding-bottom: 5px;">' +
                                                    '<button type="button" class="btn btn-outline-primary btn_news_verif" data="' + data[i].id_kabar_non_medis + '">Verifikasi</button>' +
                                                '</div>' +
                                            '</div>';
                                        }
                                    }
                                    $('#modal_kabar_terbaru').find('.modal-body').html(modal_content);
                                    swal.close();
                                    $('#modal_kabar_terbaru').modal('show');
                                    kabar_terbaru_action();
                                }
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_data').on('click', '.file_data', function(){
            $('#image_column_items').attr('src', $(this).attr('src'));
            $('#column_gambar_items').modal('show');
        });
        $('.file_data').on('click', function(){
            $('#image_column_items').attr('src', $(this).attr('src'));
            $('#column_gambar_items').modal('show');
        });
    }
    function kabar_terbaru_action(){
        $('.btn_news_delete').on('click', function(){
            var id_kabar_non_medis = $(this).attr('data');
            swal({
                html                : '<pre>Hapus Kabar Terbaru ?</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'No',
                confirmButtonText   : 'Yes'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    url         : site + 'k_kemanusiaan/delete_kabar_terbaru',
                                    data        : {
                                        id_kabar_non_medis   : id_kabar_non_medis
                                    },
                                    dataType    : "json",
                                    async       : true,
                                    success: function(response){
                                        if(response.success){
                                            swal.close();
                                            setTimeout(function(){
                                                $('#modal_kabar_terbaru').modal('hide');
                                                table.ajax.reload();
                                            },500);
                                        }
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
        $('.btn_news_verif').on('click', function(){
            var id_kabar_non_medis = $(this).attr('data');
            swal({
                html                : '<pre>Verifikasi Kabar Terbaru ?</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'No',
                confirmButtonText   : 'Yes'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    url         : site + 'k_kemanusiaan/verifikasi_kabar_terbaru',
                                    data        : {
                                        id_kabar_non_medis   : id_kabar_non_medis
                                    },
                                    dataType    : "json",
                                    async       : true,
                                    success: function(response){
                                        if(response.success){
                                            swal.close();
                                            setTimeout(function(){
                                                $('#modal_kabar_terbaru').modal('hide');
                                                table.ajax.reload();
                                            },500);
                                        }
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
    }
    function transfer_act(){
        $('.file_data').on('click', function(){
            $('#image_column_items').attr('src', $(this).attr('src'));
            $('#column_gambar_items').modal('show');
        });
    }
});