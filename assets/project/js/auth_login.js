$(document).ready(function(){
    $.get('https://jsonip.com', function(address){
        $('#ip_address').val(address.ip);
    });
    main();
    function main(){
        $('#c_captcha').on('click', function(){
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'get',
                            url         : site + 'main/login_captcha',
                            async       : true,
                            dataType    : 'json',
                            success     : function(data){
                                swal.close();
                                setTimeout(function(){
                                    $('#captcha_content').html(data.captcha);
                                },500);
                                $('#captcha_content').html(data.captcha);
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#form_login').on('submit', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var errormessage = '';
            if(! $('#login_email').val()){
                errormessage += 'Username required \n';
            }
            if(! $('#login_password').val()){
                errormessage += 'Password required \n';
            }
            if(errormessage !== ''){
                swal({
                    html          : '<pre>' + errormessage + '</pre>',
                    background  : 'transparent'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type           : 'ajax',
                                method         : 'post',
                                url            :  site + 'main/login_process',
                                data           : $('#form_login').serialize(),
                                async          : true,
                                dataType       : 'json',
                                success        : function(response){
                                    if(response.match){
                                        swal({
                                            html        : "<pre>Password didn't match</pre>",
                                            background  : 'transparent'
                                        });
                                    }else if(response.success){
                                        location.href = site + 'dashboard';
                                    }else if(response.username){
                                        swal({
                                            html        : '<pre>Account not found</pre>',
                                            background  : 'transparent'
                                        });
                                    }else if(response.inactive){
                                        swal({
                                            html        : '<pre>Accoun disabled' + '<br>' + 
                                                          'Please contact administrator</pre>',
                                            background  : 'transparent'
                                        });
                                    }else if(response.captcha_false){
                                        swal({
                                            html        : "<pre>Captcha didn't match</pre>",
                                            background  : 'transparent'
                                        });
                                    }
                                },
                                error     : function(){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    }).then(function(){
                                        location.reload(true);
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
    }
});