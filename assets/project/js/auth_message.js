$(document).ready(function(){
    $('#reply').summernote({
        popover: {
            image: [
                ['remove', ['removeMedia']]
            ],
        },
        callbacks: {
            onImageUpload: function(image) {
                editor_upload_img(image[0]);
            },
            onMediaDelete : function(target) {
                editor_delete_img(target[0].src);
            }
        },
        // placeholder: 'Set Placeholder',
        tabsize: 2,
        height: 300,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'italic']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']]
        ]
    });
    function editor_upload_img(image){
        if(image.size > 3000000){
            swal({
                background  : 'transparent',
                html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                              '3 mb</pre>',
                type        : "warning"
            });
        }else{
            var data = new FormData();
            data.append("image", image);
            $.ajax({
                url: site + 'message/editor_upload_img',
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: "POST",
                success: function(url) {
                    $('#reply').summernote("insertImage", url);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        }
    }
    function editor_delete_img(src) {
        $.ajax({
            data: {src : src},
            type: "POST",
            url: site + 'message/editor_delete_img',
            cache: false,
            success: function(response) {
                console.log(response);
            }
        });
    }
    var nama_bulan = new Array();
    nama_bulan[1] = 'January';
    nama_bulan[2] = 'February';
    nama_bulan[3] = 'March';
    nama_bulan[4] = 'April';
    nama_bulan[5] = 'May';
    nama_bulan[6] = 'June';
    nama_bulan[7] = 'July';
    nama_bulan[8] = 'August'; 
    nama_bulan[9] = 'September'; 
    nama_bulan[10] = 'October';
    nama_bulan[11] = 'November'; 
    nama_bulan[12] = 'December';
    var table;
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                s_side_data_table();
            },500);
        }
    });
    function s_side_data_table(){
        table = $('#table_data').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            lengthMenu          : [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, 'Semua']],
            initComplete: function(){
                swal.close();
                main();
            },
            ajax            : {
                url         : site + 'message/s_side_data',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
    function main(){
        var modal_form;
        $('#modal_form').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_form = true;
        });
        $('#modal_form').on('hide.bs.modal', function(){
            if(modal_form){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_form = false;
                setTimeout(function(){
                    $('#modal_form').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#table_data').on('click', '#btn_reply', function(){
            var id = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'message/edit',
                            data        : {id : id},
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                $('#form_data')[0].reset();
                                $('#id_data').val(data.id);
                                $('#user_email').val(data.email);
                                $('#user_name').val(data.name);
                                $('#user_message').val(data.message);
                                $('#modal_form').find('.modal-title').text('Reply');
                                $('#form_data').attr('action', site + 'message/respond');
                                $('#btn_process').text('Respond').show();
                                $('#reply').summernote('code', '');
                                swal.close();
                                $('#modal_form').modal('show');
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_data').on('click', '#btn_respond', function(){
            var id = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'message/edit',
                            data        : {id : id},
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                $('#form_data')[0].reset();
                                $('#id_data').val(data.id);
                                $('#user_email').val(data.email);
                                $('#user_message').val(data.message);
                                $('#modal_form').find('.modal-title').text('Responded');
                                $('#btn_process').hide();
                                $('#reply').summernote('code', data.respond);
                                swal.close();
                                $('#modal_form').modal('show');
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#btn_process').on('click', function(event){
            $('#reply').val($('#reply').summernote('code'));
            event.preventDefault();
            event.stopImmediatePropagation();
            var url             = $('#form_data').attr('action');
            var form_data       = $('#form_data')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#user_name').val()){
                 errormessage += 'Visitor name required \n';
            }
            if(! $('#user_email').val()){
                 errormessage += 'Visitor email required \n';
            }
            if(! $('#reply').val()){
                 errormessage += 'Reply message required \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    html                : '<pre>Is the reply message correct ?</pre>',
                    type                : "question",
                    background          : 'transparent',
                    showCancelButton    : true,
                    cancelButtonText    : 'No',
                    confirmButtonText   : 'Yes'
                }).then((result) => {
                    if(result.value){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                setTimeout(function(){
                                    $.ajax({
                                        type        : 'ajax',
                                        method      : 'post',
                                        url         : url,
                                        data        : form_data,
                                        async       : true,
                                        processData : false,
                                        contentType : false,
                                        cache       : false,
                                        dataType    : 'json',
                                        success     : function(response){
                                            if(response.success){
                                                $('#modal_form').modal('hide');
                                                swal({
                                                    background  : 'transparent',
                                                    html        : '<pre>Respond has been sent</pre>',
                                                    type        : "success"
                                                }).then(function(){
                                                    setTimeout(function(){
                                                        table.ajax.reload();
                                                    },500);
                                                });
                                            }
                                            if(response.unsent){
                                                swal({
                                                    background  : 'transparent',
                                                    html        : '<pre>Failed to send email' + '<br>' + 
                                                                  'Please try again</pre>',
                                                    type        : "warning"
                                                });
                                            }
                                        },
                                        error: function (){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Connection lost' + '<br>' + 
                                                              'Please try again</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    });
                                },500);
                            }
                        });
                    }
                });
            }
        });
    }
});