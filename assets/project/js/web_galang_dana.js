$(document).ready(function(){
	main();
	function main(){
		var site_link;
		var medis;
		var non_medis;
		var modal_medic;
        $('#modal_medic').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_medic = true;
        });
        $('#modal_medic').on('hide.bs.modal', function(){
            if(modal_medic){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_medic = false;
                setTimeout(function(){
                    $('#modal_medic').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var modal_non_medic;
        $('#modal_non_medic').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_non_medic = true;
        });
        $('#modal_non_medic').on('hide.bs.modal', function(){
            if(modal_non_medic){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_non_medic = false;
                setTimeout(function(){
                    $('#modal_non_medic').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('.s_medic').on('click', function(){
        	site_link = $(this).attr('data');
        	medis = $(this).attr('alt');
        	$('#modal_medic').find('.modal-title').text('Galang Dana ' + medis);
        	var terms = '';
        	if(medis == 'kesehatan'){
        		terms = 
                '<table style="width: 100%;">' +
                    '<tr>' +
                        '<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>' +
                        '<td>KTP Anda</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>' +
                        '<td>Usia Pasien Diatas 12 Tahun</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>' +
                        '<td>Kartu Keluarga Pasien</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>' +
                        '<td>Surat Keterangan Diagnosa Penyakit Medis</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>' +
                        '<td>Surat Hasil Pemeriksaan (Lab, Rontgen, Dll)</td>' +
                    '</tr>' +
                '</table>';
        	}else{
        		terms = 
                '<table style="width: 100%;">' +
                    '<tr>' +
                        '<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>' +
                        '<td>KTP Anda</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>' +
                        '<td>Usia Pasien 12 Tahun Ke Bawah</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>' +
                        '<td>Kartu Keluarga Pasien</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>' +
                        '<td>Surat Keterangan Diagnosa Penyakit Medis</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>' +
                        '<td>Surat Hasil Pemeriksaan (Lab, Rontgen, Dll)</td>' +
                    '</tr>' +
                '</table>';
        	}
        	$('#body_content').html(terms);
            $('#modal_medic').modal('show');
        });
        $('.s_non_medic').on('click', function(){
        	site_link = $(this).attr('data');
        	non_medis = $(this).attr('alt');
        	$('#modal_non_medic').find('.modal-title').text('Galang Dana ' + non_medis);
            $('#modal_non_medic').modal('show');
        });
        $('#btn_medic_next').on('click', function(){
        	swal({
		        showConfirmButton   : false,
		        allowOutsideClick   : false,
		        allowEscapeKey      : false,
		        background          : 'transparent',
		        onOpen  : function(){
		            swal.showLoading();
		            $('#modal_medic').modal('hide');
		            setTimeout(function(){
		                $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'log/medis_registration',
                            data        : {
                            	medis 	: medis
                            },
                            async       : true,
                            dataType    : 'json',
                            success     : function(data){
                            	location.href = site + site_link + '/' + data.id;
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
		            },500);
		        }
		    });
        });
        $('#btn_non_medic_next').on('click', function(){
        	swal({
		        showConfirmButton   : false,
		        allowOutsideClick   : false,
		        allowEscapeKey      : false,
		        background          : 'transparent',
		        onOpen  : function(){
		            swal.showLoading();
		            $('#modal_non_medic').modal('hide');
		            setTimeout(function(){
		                $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'log/non_medis_registration',
                            data        : {
                            	non_medis 	: non_medis
                            },
                            async       : true,
                            dataType    : 'json',
                            success     : function(data){
                            	location.href = site + site_link + '/' + data.id;
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
		            },500);
		        }
		    });
        });
	}
});