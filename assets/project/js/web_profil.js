$(document).ready(function(){
    main();
    function main(){
        $('#user_phone').val($('#user_phone').val().replace(/\B(?=(\d{4})+(?!\d))/g, "-"));
        $('#user_phone').on('keyup', function(){
            $(this).val(function(index, value) {
                return value.replace(/\D/g, "").replace(/\B(?=(\d{4})+(?!\d))/g, "-");
            });
        });
        $('#btn_profil').on('click', function(event){
            var image_process_url = site + 'log/profil_update_image';
            var form_foto       = $('#form_foto')[0];
            form_foto           = new FormData(form_foto);

            var data_process_url = site + 'log/profil_update';
            var form_data       = $('#form_data')[0];
            form_data           = new FormData(form_data);
            event.preventDefault();
            event.stopImmediatePropagation();
            var errormessage    = '';
            if(! $('#user_name').val()){
                 errormessage += 'Nama lengkap dibutuhkan \n';
            }
            if(! $('#user_phone').val()){
                 errormessage += 'No. Ponsel dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type        : 'ajax',
                                method      : 'post',
                                url         : image_process_url,
                                data        : form_foto,
                                async       : true,
                                processData : false,
                                contentType : false,
                                cache       : false,
                                dataType    : 'json',
                                success     : function(response){
                                    if(response.success){
                                        $.ajax({
                                            type        : 'ajax',
                                            method      : 'post',
                                            url         : data_process_url,
                                            data        : form_data,
                                            async       : true,
                                            processData : false,
                                            contentType : false,
                                            cache       : false,
                                            dataType    : 'json',
                                            success     : function(n_response){
                                                if(n_response.success){
                                                    setTimeout(function(){
                                                        swal({
                                                            background  : 'transparent',
                                                            html        : '<pre>Profil berhasil diupdate</pre>',
                                                            type        : "success"
                                                        }).then(function(){
                                                            location.reload(true);
                                                        });
                                                    },500);
                                                }
                                                if(n_response.chpass){
                                                    setTimeout(function(){
                                                        swal({
                                                            background  : 'transparent',
                                                            html        : '<pre>Profil & Password berhasil diupdate' + '<br>' + 
                                                                          'Silahkan login kembali</pre>',
                                                            type        : "success"
                                                        }).then(function(){
                                                            location.reload(true);
                                                        });
                                                    },500);
                                                }
                                            },
                                            error: function (){
                                                swal({
                                                    background  : 'transparent',
                                                    html        : '<pre>Connection lost' + '<br>' + 
                                                                  'Please try again</pre>',
                                                    type        : "warning"
                                                });
                                            }
                                        });
                                    }
                                },
                                error: function (){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
    }
    display_image();
    function display_image(){
        function preview(image){
            if(image.files[0].size > 3000000){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                                  '3 mb</pre>',
                    type        : "warning"
                });
                $("#foto_profil").val('');
            }else{
                if(image.files && image.files[0]){
                    var reader      = new FileReader();
                    reader.onload   = function(event){
                        $('#preview_items').attr('src', event.target.result);
                        $('#preview_items').attr('title', image.files[0].name);
                        $('#hide_foto_profil').val(image.files[0].name);
                        $('#delete_preview_items').css('display','block');
                    }
                    reader.readAsDataURL(image.files[0]);
                }
            }
        }
        $("#foto_profil").on('change', function(){
            preview(this);
            var names = $(this).val();
            var file_names = names.replace(/^.*\\/, "");
        });
        $('#delete_preview_items').on('click', function(){
            $('#delete_preview_items').css('display','none');
            $('#preview_items').attr('src', site + 'assets/project/blank.png');
            $('#foto_profil, #get_foto_profil, #hide_foto_profil').val('');
        });
    }
});