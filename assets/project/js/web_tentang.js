$(document).ready(function(){
    main();
    function main(){
        var modal_form;
        $('#modal_form').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_form = true;
        });
        $('#modal_form').on('hide.bs.modal', function(){
            if(modal_form){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_form = false;
                setTimeout(function(){
                    $('#modal_form').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#btn_contact').on('click', function(){
            $('#form_data')[0].reset();
            $('#modal_form').find('.modal-title').text('Pesan Anda');
            $('#btn_process').text('Kirim');
            $('#modal_form').modal('show');
        });
        $('#c_phone').on('keyup', function(){
            $(this).val(function(index, value) {
                return value.replace(/\D/g, "").replace(/\B(?=(\d{4})+(?!\d))/g, "-");
            });
        });
        $('#c_email').on('keyup', function(){
            $(this).val($(this).val().substr(0, 1).toLowerCase() + $(this).val().substr(1));
            var email = /\@+[a-zA-Z0-9]+\.+[a-zA-Z0-9]/m;
            if($(this).val().includes(' ')){
                $(this).val('');
            }
        });
        $('#btn_process').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var email = /\@+[a-zA-Z0-9]+\.+[a-zA-Z0-9]/m;
            var url             = site + 'log/message_process';
            var form_data       = $('#form_data')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#c_name').val()){
                 errormessage += 'Nama dibutuhkan \n';
            }
            if(! $('#c_email').val()){
                 errormessage += 'Email dibutuhkan \n';
            }else{
                 if(! email.test($('#c_email').val())){
                      errormessage += 'Invalid email \n';
                 }
            }
            if(! $('#c_phone').val()){
                 errormessage += 'No. Handphone dibutuhkan \n';
            }
            if(! $('#c_message').val()){
                 errormessage += 'Pesan dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type        : 'ajax',
                                method      : 'post',
                                url         : url,
                                data        : form_data,
                                async       : true,
                                processData : false,
                                contentType : false,
                                cache       : false,
                                dataType    : 'json',
                                success     : function(response){
                                    $('#modal_form').modal('hide');
                                    if(response.success){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Terima kasih telah menghubungi kami' + '<br>' + 
                                                          'Kami akan merespon anda secepatnya</pre>',
                                            type        : "success"
                                        }).then(function(){
                                            location.href = site;
                                        });
                                    }
                                },
                                error: function (){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please Try Again Later</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
    }
});