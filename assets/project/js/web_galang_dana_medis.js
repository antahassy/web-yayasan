$(document).ready(function(){
	$('#deskripsi_cerita').summernote({
    	popover: {
            image: [
                ['remove', ['removeMedia']]
            ],
        },
        callbacks: {
            onImageUpload: function(image) {
                editor_upload_img(image[0]);
            },
            onMediaDelete : function(target) {
                editor_delete_img(target[0].src);
            }
        },
        // placeholder: 'Set Placeholder',
        tabsize: 2,
        height: 300,
        toolbar: [
          	['style', ['style']],
          	['font', ['bold', 'underline', 'italic']],
          	['color', ['color']],
          	['para', ['ul', 'ol', 'paragraph']],
          	['insert', ['link', 'picture']]
        ]
  	});
  	function editor_upload_img(image){
  		if(image.size > 3000000){
            swal({
                background  : 'transparent',
                html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                              '3 mb</pre>',
                type        : "warning"
            });
        }else{
      		var data = new FormData();
            data.append("image", image);
            $.ajax({
                url: site + 'log/medis_cerita_upload_img',
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: "POST",
                success: function(url) {
                    $('#deskripsi_cerita').summernote("insertImage", url);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        }
    }
    function editor_delete_img(src) {
        $.ajax({
            data: {src : src},
            type: "POST",
            url: site + 'log/medis_cerita_delete_img',
            cache: false,
            success: function(response) {
                console.log(response);
            }
        });
    }
	main();
	function main(){
        var modal_tips_foto
        $('#modal_tips_foto').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_tips_foto = true;
        });
        $('#modal_tips_foto').on('hide.bs.modal', function(){
            if(modal_tips_foto){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_tips_foto = false;
                setTimeout(function(){
                    $('#modal_tips_foto').modal('hide');
                },500);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var column_gambar_items
        $('#column_gambar_items').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            column_gambar_items = true;
        });
        $('#column_gambar_items').on('hide.bs.modal', function(){
            if(column_gambar_items){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                column_gambar_items = false;
                setTimeout(function(){
                    $('#column_gambar_items').modal('hide');
                },500);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#tips_foto').on('click', function(){
            $('#modal_tips_foto').modal('show');
        });
        $('#contoh_doc_medis').on('click', function(){
            $('#image_column_items').attr('src', site + 'assets/project/web/doc_medis.jpg');
            $('#column_gambar_items').modal('show');
        });
        $('#contoh_doc_pemeriksaan').on('click', function(){
            $('#image_column_items').attr('src', site + 'assets/project/web/doc_pemeriksaan.jpg');
            $('#column_gambar_items').modal('show');
        });
		$('.steps').on('click', function(){
			$('.steps').css({'color':'#212529', 'background':'#fff'});
			$(this).css({'color':'#fff', 'background':'rgb(255,197,70)'});
			var data_c = $(this).attr('data');
			$('.c_tab').hide();
			swal({
		        showConfirmButton   : false,
		        allowOutsideClick   : false,
		        allowEscapeKey      : false,
		        background          : 'transparent',
		        onOpen  : function(){
		            swal.showLoading();
		            setTimeout(function(){
		            	if(data_c == 'tujuan'){
		            		get_tujuan();
		            	}
		            	if(data_c == 'pasien'){
		            		get_pasien();
		            	}
		            	if(data_c == 'medis'){
		            		get_medis();
		            	}
		            	if(data_c == 'donasi'){
		            		get_donasi();
		            	}
		            	if(data_c == 'judul'){
		            		get_judul();
		            	}
		            	if(data_c == 'cerita'){
		            		get_cerita();
		            	}
		            	if(data_c == 'ajakan'){
		            		get_ajakan();
		            	}
		            },500);
		        }
		    });
		});
	}
	swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
            	get_tujuan();
            },500);
        }
    });
	function get_tujuan(){
		$.ajax({
            type        : 'ajax',
            method      : 'post',
            data 		: {id_kasus : id_kasus},
            url         : site + 'log/medis_tujuan',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                $('.steps').css({'color':'#212529', 'background':'#fff'});
                $('#steps_tujuan').css({'color':'#fff', 'background':'rgb(255,197,70)'});

            	$('input[name="pasien"]').val([data.pasien]);
                $('.s_pasien').css('background-color', '#fff').find('label').children().attr('data-prefix', 'far');
                $('#pasien' + data.pasien).parent().css('background-color', 'rgb(255,239,212)').find('label').children().attr('data-prefix', 'fas');

            	$('#tlp_penggalang').val(data.tlp_penggalang.replace(/\B(?=(\d{4})+(?!\d))/g, "-"));
            	$('input[name="rekening"]').val([data.rekening]);
                $('.s_rekening').css('background-color', '#fff').find('label').children().attr('data-prefix', 'far');
                $('#rekening' + data.rekening).parent().css('background-color', 'rgb(255,239,212)').find('label').children().attr('data-prefix', 'fas');
				$('#c_tujuan').slideDown(500);
				swal.close();
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
	}
    f_tujuan();
    function f_tujuan(){
        var modal_tujuan;
        $('#modal_tujuan').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_tujuan = true;
        });
        $('#modal_tujuan').on('hide.bs.modal', function(){
            if(modal_tujuan){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_tujuan = false;
                setTimeout(function(){
                    $('#modal_tujuan').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#tlp_penggalang').on('keyup', function(){
            $(this).val(function(index, value) {
                return value.replace(/\D/g, "").replace(/\B(?=(\d{4})+(?!\d))/g, "-");
            });
        });
        var url = '';
        var form_data = '';
        $('#btn_tujuan').on('click', function(){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('#form_tujuan').attr('action', site + 'log/medis_tujuan_update')
            url             = $('#form_tujuan').attr('action');
            form_data       = $('#form_tujuan')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('input[name="pasien"]').is(':checked')){
                 errormessage += 'Pasien dibutuhkan \n';
            }
            if(! $('#tlp_penggalang').val()){
                 errormessage += 'No. Ponsel dibutuhkan \n';
            }
            if(! $('input[name="rekening"]').is(':checked')){
                 errormessage += 'Rekening dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                $('input[name="syarat_penggalangan[]"]').prop('checked', false);
                arr_syarat = [];
                $('.check_syarat').parent().css('background-color', '#fff');
                $('.check_syarat').children().removeClass('fa-check-square').addClass('fa-square').attr('data-prefix', 'far');
                $('#modal_tujuan').modal('show');
            }
        });
        $('.check_syarat').on('click', function(){
            var vals = $(this).attr('data');
            if($(this).children().attr('data-prefix') == 'fas'){
                $(this).children().removeClass('fa-check-square').addClass('fa-square').attr('data-prefix', 'far');
                $(this).parent().css('background-color', '#fff');
                arr_syarat = arr_syarat.filter(function(e) { return e !== vals });
            }else{
                $(this).children().removeClass('fa-square').addClass('fa-check-square').attr('data-prefix', 'fas');
                $(this).parent().css('background-color', 'rgb(255,239,212)');
                arr_syarat.push(vals);
                if(arr_syarat.length == 4){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $('#modal_tujuan').modal('hide');
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    url         : url,
                                    data        : form_data,
                                    async       : true,
                                    processData : false,
                                    contentType : false,
                                    cache       : false,
                                    dataType    : 'json',
                                    success     : function(response){
                                        if(response.success){
                                            $('.c_tab').hide();
                                            setTimeout(function(){
                                                get_pasien();
                                            },500);
                                        }
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            }
        });
        $('.radio_pasien').on('click', function(){
            $('.s_pasien').css('background-color', '#fff');
            $(this).parent().css('background-color', 'rgb(255,239,212)');

            $('.radio_pasien').children().attr('data-prefix', 'far');
            $(this).children().attr('data-prefix', 'fas');
            $('input[name="pasien"]:checked').val($(this).attr('data'));
        });
        $('.radio_rekening').on('click', function(){
            $('.s_rekening').css('background-color', '#fff');
            $(this).parent().css('background-color', 'rgb(255,239,212)');

            $('.radio_rekening').children().attr('data-prefix', 'far');
            $(this).children().attr('data-prefix', 'fas');
            $('input[name="rekening"]:checked').val($(this).attr('data'));

            $('.ket_rekening').hide();
            $(this).parent().find('label div').show();
        });
    }
	function get_pasien(){
		$.ajax({
            type        : 'ajax',
            method      : 'post',
            data 		: {id_kasus : id_kasus},
            url         : site + 'log/medis_pasien',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                $('.steps').css({'color':'#212529', 'background':'#fff'});
                $('#steps_pasien').css({'color':'#fff', 'background':'rgb(255,197,70)'});

            	$('#nm_pasien').val([data.nm_pasien]);
            	$('#nm_penyakit').val([data.nm_penyakit]);
            	$('#get_dokumen_medis, #delete_dokumen_medis, #hide_dokumen_medis').val(data.dokumen_medis);
            	if(data.dokumen_medis != ''){
                    $('#preview_itemss').attr('src', site + 'assets/project/kasus_medis/' + data.dokumen_medis);
                    $('#delete_preview_itemss').css('display','block');
                }else{
                    $('#preview_itemss').attr('src', '');
                    $('#delete_preview_itemss').css('display','none');
                }
				$('#c_pasien').slideDown(500);
				display_images();
				swal.close();
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
	}
    f_pasien();
    function f_pasien(){
        var url = '';
        var form_data = '';
        $('#btn_pasien').on('click', function(){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('#form_pasien').attr('action', site + 'log/medis_pasien_update')
            url             = $('#form_pasien').attr('action');
            form_data       = $('#form_pasien')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#nm_pasien').val()){
                 errormessage += 'Nama pasien dibutuhkan \n';
            }
            if(! $('#nm_penyakit').val()){
                 errormessage += 'Nama penyakit dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type        : 'ajax',
                                method      : 'post',
                                url         : url,
                                data        : form_data,
                                async       : true,
                                processData : false,
                                contentType : false,
                                cache       : false,
                                dataType    : 'json',
                                success     : function(response){
                                    if(response.success){
                                        $('.c_tab').hide();
                                        setTimeout(function(){
                                            get_medis();
                                        },500);
                                    }
                                },
                                error: function (){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
    }
	function get_medis(){
		$.ajax({
            type        : 'ajax',
            method      : 'post',
            data 		: {id_kasus : id_kasus},
            url         : site + 'log/medis_medis',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                $('.steps').css({'color':'#212529', 'background':'#fff'});
                $('#steps_medis').css({'color':'#fff', 'background':'rgb(255,197,70)'});

            	$('#get_hasil_pemeriksaan, #delete_hasil_pemeriksaan, #hide_hasil_pemeriksaan').val(data.hasil_pemeriksaan);
            	if(data.hasil_pemeriksaan != ''){
                    $('#preview_itemsss').attr('src', site + 'assets/project/kasus_medis/' + data.hasil_pemeriksaan);
                    $('#delete_preview_itemsss').css('display','block');
                }else{
                    $('#preview_itemsss').attr('src', '');
                    $('#delete_preview_itemsss').css('display','none');
                }
                $('input[name="r_inap"]').val([data.inap]);
                $('.s_r_inap').css('background-color', '#fff').find('label').children().attr('data-prefix', 'far');
                $('#r_inap' + data.inap).parent().css('background-color', 'rgb(255,239,212)').find('label').children().attr('data-prefix', 'fas');
                if(data.inap == '1'){
                    $('#rs_r_inap').val(data.rs_inap);
                    $('#ket_r_inap').show();
                }else{
                    $('#rs_r_inap').val(' ');
                    $('#ket_r_inap').hide();
                }

            	$('#pengobatan').val(data.pengobatan);

            	$('input[name="biaya"]').val([data.biaya]);
                $('.s_biaya').css('background-color', '#fff').find('label').children().attr('data-prefix', 'far');
                $('#biaya' + data.biaya).parent().css('background-color', 'rgb(255,239,212)').find('label').children().attr('data-prefix', 'fas');

				$('#c_medis').slideDown(500);
				display_imagess();
				swal.close();
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
	}
    f_medis();
    function f_medis(){
        var url = '';
        var form_data = '';
        $('#btn_medis').on('click', function(){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('#form_medis').attr('action', site + 'log/medis_medis_update')
            url             = $('#form_medis').attr('action');
            form_data       = $('#form_medis')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('input[name="r_inap"]').is(':checked')){
                 errormessage += 'Keterangan rawat inap dibutuhkan \n';
            }else{
                if($('input[name="r_inap"]:checked').val() == '1' && ! $('#rs_r_inap').val()){
                    errormessage += 'Rumah sakit rawat inap dibutuhkan \n';
                }
            }
            if(! $('#pengobatan').val()){
                 errormessage += 'Upaya pengobatan dibutuhkan \n';
            }
            if(! $('input[name="biaya"]').is(':checked')){
                 errormessage += 'Sumber biaya dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type        : 'ajax',
                                method      : 'post',
                                url         : url,
                                data        : form_data,
                                async       : true,
                                processData : false,
                                contentType : false,
                                cache       : false,
                                dataType    : 'json',
                                success     : function(response){
                                    if(response.success){
                                        $('.c_tab').hide();
                                        setTimeout(function(){
                                            get_donasi();
                                        },500);
                                    }
                                },
                                error: function (){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
        $('.radio_biaya').on('click', function(){
            $('.s_biaya').css('background-color', '#fff');
            $(this).parent().css('background-color', 'rgb(255,239,212)');

            $('.radio_biaya').children().attr('data-prefix', 'far');
            $(this).children().attr('data-prefix', 'fas');
            $('input[name="biaya"]:checked').val($(this).attr('data'));
        });
        $('.radio_inap').on('click', function(){
            if($(this).attr('data') == '2'){
                $('#rs_r_inap').val(' ');
            }else{
                $('#rs_r_inap').val('');
            }

            $('.s_r_inap').css('background-color', '#fff');
            $(this).parent().css('background-color', 'rgb(255,239,212)');

            $('.radio_inap').children().attr('data-prefix', 'far');
            $(this).children().attr('data-prefix', 'fas');
            $('input[name="r_inap"]:checked').val($(this).attr('data'));

            $('.ket_r_inap').hide();
            $(this).parent().find('label div').show();
        });
    }
	function get_donasi(){
		$.ajax({
            type        : 'ajax',
            method      : 'post',
            data 		: {id_kasus : id_kasus},
            url         : site + 'log/medis_donasi',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                $('.steps').css({'color':'#212529', 'background':'#fff'});
                $('#steps_donasi').css({'color':'#fff', 'background':'rgb(255,197,70)'});

            	if(data.dana == '0'){
            		$('#biaya_dana').val('');
            	}else{
            		$('#biaya_dana').val(data.dana.replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            	}
            	$('#jatuh_tempo').val(data.jatuh_tempo);

            	if(data.jatuh_tempo == ''){
            		$('#lama_tempo').val('');
            	}else{
            		var start = new Date(data.now_date);
                    var end = new Date(data.jatuh_tempo);
                    var day_left = Math.abs(end - start);
                    day_left = Math.ceil(day_left / (1000 * 60 * 60 * 24)); 
            		$('#lama_tempo').val(day_left);
            	}
            	$('#penggunaan_dana').val(data.penggunaan_dana);
				$('#c_donasi').slideDown(500);
				swal.close();
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
	}
    f_donasi();
    function f_donasi(){
        $('#jatuh_tempo').datepicker({
            minDate: new Date(),
            minDate: 30,
            yearRange : '-1:+1',
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            dayNames: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
            dayNamesMin: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
            monthNamesShort: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            monthNames: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            beforeShow: function() {
                $(document).off('focusin.bs.modal');
            },
            onClose: function(){
                $(document).on('focusin.bs.modal');
            }
        });
        $('#jatuh_tempo').on('change', function(){
            var end = $(this).val();
            var start = new Date();
            start = start.getFullYear() + '-' + (start.getMonth() + 1) + '-' + start.getDate();

            start = new Date(start);
            end = new Date(end);
            var d_time = Math.abs(end - start);
            var d_day = Math.ceil(d_time / (1000 * 60 * 60 * 24)); 
            $('#lama_tempo').val(d_day);
        });
        $('#biaya_dana').on('keyup', function(){
            $(this).val(function(index, value) {
                return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            });
        });
        var url = '';
        var form_data = '';
        $('#btn_donasi').on('click', function(){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('#form_donasi').attr('action', site + 'log/medis_donasi_update')
            url             = $('#form_donasi').attr('action');
            form_data       = $('#form_donasi')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#biaya_dana').val()){
                 errormessage += 'Biaya dibutuhkan \n';
            }
            if(! $('#jatuh_tempo').val()){
                 errormessage += 'Batas akhir penggalangan dibutuhkan \n';
            }
            if(! $('#lama_tempo').val()){
                 errormessage += 'Lama waktu penggalangan dibutuhkan \n';
            }
            if(! $('#penggunaan_dana').val()){
                 errormessage += 'Deskripsi penggunaan dana dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type        : 'ajax',
                                method      : 'post',
                                url         : url,
                                data        : form_data,
                                async       : true,
                                processData : false,
                                contentType : false,
                                cache       : false,
                                dataType    : 'json',
                                success     : function(response){
                                    if(response.success){
                                        $('.c_tab').hide();
                                        setTimeout(function(){
                                            get_judul();
                                        },500);
                                    }
                                },
                                error: function (){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
    }
	function get_judul(){
		$.ajax({
            type        : 'ajax',
            method      : 'post',
            data 		: {id_kasus : id_kasus},
            url         : site + 'log/medis_judul',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                $('.steps').css({'color':'#212529', 'background':'#fff'});
                $('#steps_judul').css({'color':'#fff', 'background':'rgb(255,197,70)'});

            	$('#judul_penggalangan').val(data.judul);
            	$('#link_penggalangan').val(data.link);
            	$('#get_sampul, #delete_sampul, #hide_sampul').val(data.sampul);
            	if(data.sampul != ''){
                    $('#preview_items').attr('src', site + 'assets/project/kasus_medis/' + data.sampul);
                    $('#delete_preview_items').css('display','block');
                }else{
                    $('#preview_items').attr('src', '');
                    $('#delete_preview_items').css('display','none');
                }
				$('#c_judul').slideDown(500);
				display_image();
				swal.close();
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
	}
    f_judul();
    function f_judul(){
        $('#link_penggalangan').on('keyup', function(){
            var chars = /^[0-9a-z]+$/gm;
            if(! chars.test($(this).val())) {
                 $(this).val('');
            }
        });
        var url = '';
        var form_data = '';
        $('#btn_judul').on('click', function(){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('#form_judul').attr('action', site + 'log/medis_judul_update')
            url             = $('#form_judul').attr('action');
            form_data       = $('#form_judul')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#judul_penggalangan').val()){
                 errormessage += 'Judul dibutuhkan \n';
            }
            if(! $('#link_penggalangan').val()){
                 errormessage += 'Link dibutuhkan \n';
            }
            if(! $('#hide_sampul').val()){
                 errormessage += 'Foto dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type        : 'ajax',
                                method      : 'post',
                                url         : url,
                                data        : form_data,
                                async       : true,
                                processData : false,
                                contentType : false,
                                cache       : false,
                                dataType    : 'json',
                                success     : function(response){
                                    if(response.link){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Link sudah dipakai' + '<br>' + 
                                                          'Harap gunakan link lain</pre>'
                                        });
                                    }
                                    if(response.success){
                                        $('.c_tab').hide();
                                        setTimeout(function(){
                                            get_cerita();
                                        },500);
                                    }
                                },
                                error: function (){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
    }
    function get_cerita(){
		$.ajax({
            type        : 'ajax',
            method      : 'post',
            data 		: {id_kasus : id_kasus},
            url         : site + 'log/medis_cerita',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                $('.steps').css({'color':'#212529', 'background':'#fff'});
                $('#steps_cerita').css({'color':'#fff', 'background':'rgb(255,197,70)'});

            	$('#deskripsi_cerita').summernote('code', data.cerita);
				$('#c_cerita').slideDown(500);
				swal.close();
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
	}
    f_cerita();
    function f_cerita(){
        var url = '';
        var form_data = '';
        $('#btn_cerita').on('click', function(){
            $('#deskripsi_cerita').val($('#deskripsi_cerita').summernote('code'));
            event.preventDefault();
            event.stopImmediatePropagation();
            $('#form_cerita').attr('action', site + 'log/medis_cerita_update')
            url             = $('#form_cerita').attr('action');
            form_data       = $('#form_cerita')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#deskripsi_cerita').val()){
                 errormessage += 'Uraian cerita dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type        : 'ajax',
                                method      : 'post',
                                url         : url,
                                data        : form_data,
                                async       : true,
                                processData : false,
                                contentType : false,
                                cache       : false,
                                dataType    : 'json',
                                success     : function(response){
                                    if(response.success){
                                        $('.c_tab').hide();
                                        setTimeout(function(){
                                            get_ajakan();
                                        },500);
                                    }
                                },
                                error: function (){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
    }
	function get_ajakan(){
		$.ajax({
            type        : 'ajax',
            method      : 'post',
            data 		: {id_kasus : id_kasus},
            url         : site + 'log/medis_ajakan',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                $('.steps').css({'color':'#212529', 'background':'#fff'});
                $('#steps_ajakan').css({'color':'#fff', 'background':'rgb(255,197,70)'});
            	//tujuan
            	$('input[name="pasien"]').val([data.pasien]);
            	$('#tlp_penggalang').val(data.tlp_penggalang.replace(/\B(?=(\d{4})+(?!\d))/g, "-"));
            	$('input[name="rekening"]').val([data.rekening]);
            	//pasien
            	$('#nm_pasien').val([data.nm_pasien]);
            	$('#nm_penyakit').val([data.nm_penyakit]);
            	$('#get_dokumen_medis, #delete_dokumen_medis, #hide_dokumen_medis').val(data.dokumen_medis);
            	if(data.dokumen_medis != ''){
                    $('#preview_itemss').attr('src', site + 'assets/project/kasus_medis/' + data.dokumen_medis);
                    $('#delete_preview_itemss').css('display','block');
                }else{
                    $('#preview_itemss').attr('src', '');
                    $('#delete_preview_itemss').css('display','none');
                }
                //medis
                $('#get_hasil_pemeriksaan, #delete_hasil_pemeriksaan, #hide_hasil_pemeriksaan').val(data.hasil_pemeriksaan);
            	if(data.hasil_pemeriksaan != ''){
                    $('#preview_itemsss').attr('src', site + 'assets/project/kasus_medis/' + data.hasil_pemeriksaan);
                    $('#delete_preview_itemsss').css('display','block');
                }else{
                    $('#preview_itemsss').attr('src', '');
                    $('#delete_preview_itemsss').css('display','none');
                }
                $('input[name="r_inap"]').val([data.inap]);
            	if(data.inap == '1'){
            		$('#rs_r_inap').val(data.rs_inap);
            		$('#ket_r_inap').show();
            	}else{
            		$('#rs_r_inap').val(' ');
            		$('#ket_r_inap').hide();
            	}
            	$('#pengobatan').val(data.pengobatan);
            	$('input[name="biaya"]').val([data.biaya]);
            	//donasi
            	if(data.dana == '0'){
            		$('#biaya_dana').val('');
            	}else{
            		$('#biaya_dana').val(data.dana.replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            	}
            	$('#jatuh_tempo').val(data.jatuh_tempo);

            	if(data.jatuh_tempo == ''){
            		$('#lama_tempo').val('');
            	}else{
            		var start = new Date(data.now_date);
                    var end = new Date(data.jatuh_tempo);
                    var day_left = Math.abs(end - start);
                    day_left = Math.ceil(day_left / (1000 * 60 * 60 * 24)); 
            		$('#lama_tempo').val(day_left);
            	}
            	$('#penggunaan_dana').val(data.penggunaan_dana);
            	//judul
            	$('#judul_penggalangan').val(data.judul);
            	$('#link_penggalangan').val(data.link);
            	$('#get_sampul, #delete_sampul, #hide_sampul').val(data.sampul);
            	if(data.sampul != ''){
                    $('#preview_items').attr('src', site + 'assets/project/kasus_medis/' + data.sampul);
                    $('#delete_preview_items').css('display','block');
                }else{
                    $('#preview_items').attr('src', '');
                    $('#delete_preview_items').css('display','none');
                }
                //cerita
                $('#deskripsi_cerita').summernote('code', data.cerita);
                //ajakan
            	$('#deskripsi_ajakan').val(data.ajakan);
				$('#c_ajakan').slideDown(500);
				swal.close();
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
	}
	f_ajakan();
	function f_ajakan(){
		var arr_konfirmasi = [];
        $('.konfirm_persetujuan').on('click', function(){
            var vals = $(this).attr('data');
            if($(this).children().attr('data-prefix') == 'fas'){
                $(this).children().removeClass('fa-check-square').addClass('fa-square').attr('data-prefix', 'far');
                $(this).parent().css('background-color', '#fff');
                arr_konfirmasi = arr_konfirmasi.filter(item => item !== vals);
            }else{
                $(this).children().removeClass('fa-square').addClass('fa-check-square').attr('data-prefix', 'fas');
                $(this).parent().css('background-color', 'rgb(255,239,212)');
                arr_konfirmasi.push(vals);
            }
        });
        var url = '';
        var form_data = '';
		$('#btn_ajakan').on('click', function(){
			event.preventDefault();
            event.stopImmediatePropagation();
            $('#form_ajakan').attr('action', site + 'log/medis_ajakan_update')
            url             = $('#form_ajakan').attr('action');
            form_data       = $('#form_ajakan')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('input[name="pasien"]').is(':checked')){
                 errormessage += 'Pasien dibutuhkan \n';
            }
            if(! $('#tlp_penggalang').val()){
                 errormessage += 'No. Ponsel dibutuhkan \n';
            }
            if(! $('input[name="rekening"]').is(':checked')){
                 errormessage += 'Rekening dibutuhkan \n';
            }
            if(! $('#nm_pasien').val()){
                 errormessage += 'Nama pasien dibutuhkan \n';
            }
            if(! $('#nm_penyakit').val()){
                 errormessage += 'Nama penyakit dibutuhkan \n';
            }
            if(! $('#deskripsi_ajakan').val()){
                 errormessage += 'Deskripsi ajakan dibutuhkan \n';
            }
            if(! $('input[name="r_inap"]').is(':checked')){
                 errormessage += 'Keterangan rawat inap dibutuhkan \n';
            }else{
            	if($('input[name="r_inap"]:checked').val() == '1' && ! $('#rs_r_inap').val()){
					errormessage += 'Rumah sakit rawat inap dibutuhkan \n';
				}
            }
            if(! $('#pengobatan').val()){
                 errormessage += 'Upaya pengobatan dibutuhkan \n';
            }
            if(! $('input[name="biaya"]').is(':checked')){
                 errormessage += 'Sumber biaya dibutuhkan \n';
            }
            if(! $('#biaya_dana').val()){
                 errormessage += 'Biaya dibutuhkan \n';
            }
            if(! $('#jatuh_tempo').val()){
                 errormessage += 'Batas akhir penggalangan dibutuhkan \n';
            }
            if(! $('#lama_tempo').val()){
                 errormessage += 'Lama waktu penggalangan dibutuhkan \n';
            }
            if(! $('#penggunaan_dana').val()){
                 errormessage += 'Deskripsi penggunaan dana dibutuhkan \n';
            }
            if(! $('#judul_penggalangan').val()){
                 errormessage += 'Judul dibutuhkan \n';
            }
            if(! $('#link_penggalangan').val()){
                 errormessage += 'Link dibutuhkan \n';
            }
            if(! $('#hide_sampul').val()){
                 errormessage += 'Foto dibutuhkan \n';
            }
            if(! $('#deskripsi_cerita').val()){
                 errormessage += 'Uraian cerita dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else if(errormessage == '' && arr_konfirmasi.length < 1){
            	if(arr_konfirmasi.length == 0){
            		$('#btn_konfirmasi, #persetujuan1, .label_form_rule1').addClass('animated flash').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                    	$('#btn_process, #persetujuan1, .label_form_rule1').removeClass('animated flash');
                	});
            	}else{
            		if(! arr_konfirmasi.includes('1')){
            			$('#btn_konfirmasi, #persetujuan1, .label_form_rule1').addClass('animated flash').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                    		$('#btn_konfirmasi, #persetujuan1, .label_form_rule1').removeClass('animated flash');
                		});
            		}
            	}
            }else{
                swal({
                    html                : '<pre>Data pengajuan penggalangan dana' + '<br>' + 
                                          'Tidak dapat diubah kembali' + '<br>' + 
                                          'Apakah data sudah benar ?</pre>',
                    type                : "question",
                    background          : 'transparent',
                    showCancelButton    : true,
                    cancelButtonText    : 'Tidak',
                    confirmButtonText   : 'Ya'
                }).then((result) => {
                    if(result.value){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                setTimeout(function(){
                                    $.ajax({
                                        type        : 'ajax',
                                        method      : 'post',
                                        url         : url,
                                        data        : form_data,
                                        async       : true,
                                        processData : false,
                                        contentType : false,
                                        cache       : false,
                                        dataType    : 'json',
                                        success     : function(response){
                                            if(response.success){
                                                swal({
                                                    background  : 'transparent',
                                                    html        : '<pre>Penggalangan dana telah diajukan' + '<br>' + 
                                                                  'Menunggu verifikasi Peduli Indonesia</pre>',
                                                    type        : "success"
                                                }).then(function(){
                                                    location.href = site + 'home/riwayat_galang_dana';
                                                });
                                            }
                                            // if(response.success){
                                            //  swal({
                                            //         background  : 'transparent',
                                            //         html        : '<pre>Kode OTP telah dikirim ke' + '<br>' + 
                                            //                       $('#deskripsi_email').val() + '</pre>',
                                            //         type        : "success"
                                            //     }).then(function(){
                                            //      location.href = site + 'home/otp_medis/' + id_kasus;
                                            //     });
                                            // }
                                            if(response.unsent){
                                                swal({
                                                    background  : 'transparent',
                                                    html        : '<pre>Gagal menyimpan data' + '<br>' + 
                                                                  'Silahkan mencoba lagi</pre>',
                                                    type        : "warning"
                                                });
                                            }
                                        },
                                        error: function (){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Connection lost' + '<br>' + 
                                                              'Please try again</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    });
                                },500);
                            }
                        });
                    }
                });
            }
		});
	}
    function display_image(){
        function preview(image){
            if(image.files[0].size > 3000000){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                                  '3 mb</pre>',
                    type        : "warning"
                });
                $("#sampul").val('');
            }else{
                if(image.files && image.files[0]){
                    var reader      = new FileReader();
                    reader.onload   = function(event){
                        $('#preview_items').attr('src', event.target.result);
                        $('#preview_items').attr('title', image.files[0].name);
                        $('#hide_sampul').val(image.files[0].name);
                        $('#delete_preview_items').css('display','block');
                    }
                    reader.readAsDataURL(image.files[0]);
                }
            }
        }
        $("#sampul").on('change', function(){
            preview(this);
            var names = $(this).val();
            var file_names = names.replace(/^.*\\/, "");
        });
        $('#delete_preview_items').on('click', function(){
            $('#delete_preview_items').css('display','none');
            $('#preview_items').attr('src', '');
            $('#sampul, #get_sampul, #hide_sampul').val('');
        });
    }
    function display_images(){
        function preview(image){
            if(image.files[0].size > 3000000){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                                  '3 mb</pre>',
                    type        : "warning"
                });
                $("#dokumen_medis").val('');
            }else{
                if(image.files && image.files[0]){
                    var reader      = new FileReader();
                    reader.onload   = function(event){
                        $('#preview_itemss').attr('src', event.target.result);
                        $('#preview_itemss').attr('title', image.files[0].name);
                        $('#hide_dokumen_medis').val(image.files[0].name);
                        $('#delete_preview_itemss').css('display','block');
                    }
                    reader.readAsDataURL(image.files[0]);
                }
            }
        }
        $("#dokumen_medis").on('change', function(){
            preview(this);
            var names = $(this).val();
            var file_names = names.replace(/^.*\\/, "");
        });
        $('#delete_preview_itemss').on('click', function(){
            $('#delete_preview_itemss').css('display','none');
            $('#preview_itemss').attr('src', '');
            $('#dokumen_medis, #get_dokumen_medis, #hide_dokumen_medis').val('');
        });
    }
    function display_imagess(){
        function preview(image){
            if(image.files[0].size > 3000000){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                                  '3 mb</pre>',
                    type        : "warning"
                });
                $("#hasil_pemeriksaan").val('');
            }else{
                if(image.files && image.files[0]){
                    var reader      = new FileReader();
                    reader.onload   = function(event){
                        $('#preview_itemsss').attr('src', event.target.result);
                        $('#preview_itemsss').attr('title', image.files[0].name);
                        $('#hide_hasil_pemeriksaan').val(image.files[0].name);
                        $('#delete_preview_itemsss').css('display','block');
                    }
                    reader.readAsDataURL(image.files[0]);
                }
            }
        }
        $("#hasil_pemeriksaan").on('change', function(){
            preview(this);
            var names = $(this).val();
            var file_names = names.replace(/^.*\\/, "");
        });
        $('#delete_preview_itemsss').on('click', function(){
            $('#delete_preview_itemsss').css('display','none');
            $('#preview_itemsss').attr('src', '');
            $('#hasil_pemeriksaan, #get_hasil_pemeriksaan, #hide_hasil_pemeriksaan').val('');
        });
    }
});