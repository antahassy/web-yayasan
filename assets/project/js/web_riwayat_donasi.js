$(document).ready(function(){
	var table_medis, table_non_medis;
	swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            $('#content_medis').slideDown(500);
            setTimeout(function(){
                riwayat_donasi_medis();
            },500);
        }
    });
	function riwayat_donasi_medis(){
        table_medis = $('#table_donasi_medis').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            info                : false, 
            lengthMenu          : [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, 'Semua']],
            initComplete: function(){
                swal.close();
            },
            ajax            : {
                url         : site + 'log/riwayat_donasi_medis',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
	function riwayat_donasi_non_medis(){
        table_non_medis = $('#table_donasi_non_medis').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            info                : false, 
            lengthMenu          : [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, 'Semua']],
            initComplete: function(){
                swal.close();
            },
            ajax            : {
                url         : site + 'log/riwayat_donasi_non_medis',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
	main();
	function main(){
		$('.donasi_medis').css({'background':'rgb(255,197,70)', 'color':'#fff'});
		$('.donasi_medis').on('click', function(){
			$('.l_donasi').css({'background':'#fff', 'color':'rgb(255,197,70)'});
			$(this).css({'background':'rgb(255,197,70)', 'color':'#fff'});
			$('.content_table').hide();
			swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    $('#content_medis').slideDown(500);
                    setTimeout(function(){
                        riwayat_donasi_medis();
                    },500);
                }
            });
		});
		$('.donasi_non_medis').on('click', function(){
			$('.l_donasi').css({'background':'#fff', 'color':'rgb(255,197,70)'});
			$(this).css({'background':'rgb(255,197,70)', 'color':'#fff'});
			$('.content_table').hide();
			swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    $('#content_non_medis').slideDown(500);
                    setTimeout(function(){
                        riwayat_donasi_non_medis();
                    },500);
                }
            });
		});
		$('#table_donasi_medis').on('click', '.btn_status', function(){
            var id = $(this).attr('id');
            $(this).attr('href', site + 'donasi/medis_pay/' + id);
        });
        $('#table_donasi_non_medis').on('click', '.btn_status', function(){
            var id = $(this).attr('id');
            $(this).attr('href', site + 'donasi/non_medis_pay/' + id);
        });
	}
});