$(document).ready(function(){
	main();
	function main(){
        $('#hide_bank, #hide_donasi, #jml_nominal, #donasi4').val('');
        $('.check_hide_name').on('click', function(){
            var vals = $(this).attr('data');
            if($(this).children().attr('data-icon') == 'toggle-off'){
                $(this).children().removeClass('fa-toggle-off').addClass('fa-toggle-on');
                $('#donatur_name').val(vals);
            }else{
                $(this).children().removeClass('fa-toggle-on').addClass('fa-toggle-off');
                $('#donatur_name').val('0');
            }
        });
        $('.radio_bank').on('click', function(){
            $('.s_bank').css('background-color', 'rgba(0,0,0,0.1)');
            $('.s_bank').css('color', '#212529');
            $('.s_bank .i_icon').css('color', 'rgba(0,0,0,0.3)');
            $(this).parent().css('background-color', 'rgb(255,197,70)');
            $(this).parent().css('color', '#fff');
            $(this).parent().find('.i_icon').css('color', '#fff');
            $('.ver_data').hide();
            $(this).parent().find('.ver_data').show();
            $('#hide_bank').val($(this).attr('data'));
        });
        $('.radio_donasi').on('click', function(){
            $('.s_donasi').css('background-color', 'rgba(0,0,0,0.1)');
            $('.s_donasi').css('color', '#212529');
            $('.s_donasi .i_icon').css('color', 'rgba(0,0,0,0.3)');
            $(this).parent().css('background-color', 'rgb(255,197,70)');
            $(this).parent().css('color', '#fff');
            $(this).parent().find('.i_icon').css('color', '#fff');

            if($(this).attr('data') != ''){ 
                $('#hide_donasi').val($(this).attr('data'));
                $('#jml_nominal, #donasi4').val('');
            }else{
                $('#hide_donasi').val('');
            }
        });
        $('#user_phone').on('keyup', function(){
            $(this).val(function(index, value) {
                return value.replace(/\D/g, "").replace(/\B(?=(\d{4})+(?!\d))/g, "-");
            });
        });
		$('#jml_nominal').on('keyup', function(){
            $(this).val(function(index, value) {
                return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            });
            $('#hide_donasi').val(Number($(this).val().replaceAll(".", "")));
            $('#donasi4').val(Number($(this).val().replaceAll(".", "")));
        });
        $('#btn_donasi').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var emails = /\@+[a-zA-Z0-9]+\.+[a-zA-Z0-9]/m;
            var form_data       = $('#form_donasi')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#user_email').val()){
                 errormessage += 'Email dibutuhkan \n';
            }else{
                if(! emails.test($('#user_email').val())){
                    errormessage += 'Invalid email \n';
                }
            }
            if(! $('#user_nama').val()){
                 errormessage += 'Nama dibutuhkan \n';
            }
            if(! $('#user_phone').val()){
                 errormessage += 'No. Ponsel dibutuhkan \n';
            }
            if(! $('#hide_donasi').val()){
                 errormessage += 'Donasi dibutuhkan \n';
            }else{
                if(Number($('#hide_donasi').val()) < 10000){
                    errormessage += 'Minimal donasi Rp. 10.000 \n';
                }
            }
            if(! $('#hide_bank').val()){
                 errormessage += 'Bank dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            if($('#hide_bank').val() == '1'){
                                var url = site + 'payment/bca_manual';
                                bca_manual(url, form_data);
                            }else{
                                var url = site + 'payment/token';
                                midtrans_pay(url, form_data);
                            }
                        },500);
                    }
                });
            }
        });
	}
    function bca_manual(url, form_data){
        $.ajax({
            type        : 'ajax',
            method      : 'post',
            url         : url,
            data        : form_data,
            async       : true,
            processData : false,
            contentType : false,
            cache       : false,
            dataType    : 'json',
            success     : function(response) {
                if(response.failed){
                    swal({
                        background  : 'transparent',
                        html        : '<pre>Koneksi terputus' + '<br>' + 
                                      'Cobalah beberapa saat lagi</pre>',
                        type        : "warning"
                    }).then(function(){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                location.reload(true);
                            }
                        });
                    });
                }else{
                    location.href = site + response.success;
                }
            },
            error       : function(){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
    }
    function midtrans_pay(url, form_data){
        $.ajax({
            type        : 'ajax',
            method      : 'post',
            url         : url,
            data        : form_data,
            async       : true,
            processData : false,
            contentType : false,
            cache       : false,
            // dataType    : 'json',
            success: function(data) {
                if(data.failed){
                    swal({
                        background  : 'transparent',
                        html        : '<pre>Koneksi terputus' + '<br>' + 
                                      'Cobalah beberapa saat lagi</pre>',
                        type        : "warning"
                    });
                }else{
                    swal.close();
                    function changeResult(type,data){
                        $("#result_type").val(type);
                        $("#result_data").val(JSON.stringify(data));
                    }
                    snap.pay(data, {
                        onSuccess: function(result){
                            changeResult('success', result);
                            var snap_url = site + 'payment/finish';
                            snap_pay(result, snap_url);
                        },
                        onPending: function(result){
                            changeResult('pending', result);
                            var snap_url = site + 'payment/finish';
                            snap_pay(result, snap_url);
                        },
                        onError: function(result){
                            changeResult('error', result);
                            var snap_url = site + 'payment/finish';
                            snap_pay(result, snap_url);
                        },
                        onClose: function(){
                            swal({
                                showConfirmButton   : false,
                                allowOutsideClick   : false,
                                allowEscapeKey      : false,
                                background          : 'transparent',
                                onOpen  : function(){
                                    swal.showLoading();
                                    setTimeout(function(){
                                        $.ajax({
                                            type        : 'ajax',
                                            method      : 'post',
                                            url         : site + 'payment/hapus_donasi',
                                            data        : form_data,
                                            async       : true,
                                            processData : false,
                                            contentType : false,
                                            cache       : false,
                                            dataType    : 'json',
                                            success: function(response) {
                                                if(response.success){
                                                    swal({
                                                        background  : 'transparent',
                                                        html        : '<pre>Halaman akan dimuat ulang</pre>'
                                                    }).then(function(){
                                                        swal({
                                                            showConfirmButton   : false,
                                                            allowOutsideClick   : false,
                                                            allowEscapeKey      : false,
                                                            background          : 'transparent',
                                                            onOpen  : function(){
                                                                swal.showLoading();
                                                                setTimeout(function(){
                                                                    location.reload(true);
                                                                },500);
                                                            }
                                                        });
                                                    });
                                                }
                                            }
                                        });
                                    },500);
                                }
                            });
                        }
                    });
                }
            }
        });
    }
    function snap_pay(result, snap_url){
        if(typeof result.va_numbers == 'undefined'){
            result.va_numbers = [];
            result.va_numbers.push({
                bank        : 'Mandiri',
                va_number   : result.bill_key,//kode pembayaran mandiri
                va_code     : result.biller_code,//kode bank mandiri
            });
        }else{
            result.va_numbers[0].va_code = '';//kode bukan bank mandiri
        }
        var result_ordered = Object.keys(result).sort().reduce(
            (obj, key) => { 
                obj[key] = result[key]; 
                return obj;
            }, 
            {}
        );
        swal({
            showConfirmButton   : false,
            allowOutsideClick   : false,
            allowEscapeKey      : false,
            background          : 'transparent',
            onOpen  : function(){
                swal.showLoading();
                setTimeout(function(){
                    console.log(result_ordered);
                    $.ajax({
                        type        : 'ajax',
                        method      : 'post',
                        url         : snap_url,
                        data        : {
                            id_donasi   : result_ordered.order_id,
                            pdf_url     : result_ordered.pdf_url,
                            code        : result_ordered.status_code,
                            status      : result_ordered.transaction_status,
                            time        : result_ordered.transaction_time,
                            bank        : result_ordered.va_numbers[0].bank,
                            bank_number : result_ordered.va_numbers[0].va_number,
                            bank_code   : result_ordered.va_numbers[0].va_code
                        },
                        async       : true,
                        dataType    : 'json',
                        success: function(response) {
                            location.href = site + response.success;
                        }
                    });
                },500);
            }
        });
    }
});