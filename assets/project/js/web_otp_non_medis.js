$(document).bind('contextmenu', function(event){
    return false;
});
$(document).keydown(function(event){
    if($(event.target).is('#otp')){
        if(event.which === 17 || event.keyCode === 17 || 
            event.which === 67 || event.keyCode === 67 || 
            event.which === 86 || event.keyCode === 86){
            return false;
        }
    }
});
$(document).ready(function(){
    main();
    function main(){
        var delay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
            };
        })();
        $('#otp').focus();
        $('#otp').on('keyup', function(){
            $(this).val(function(index, value) {
                return value.replace(/\D/g, "").replace(/\B(?=(\d{0})+(?!\d))/g, "");
            });
            delay(function(){
                $.ajax({
                    type        : 'ajax',
                    method      : 'post',
                    url         : site + 'log/otp_non_medis_code',
                    data        : {
                        id_kasus : id_kasus,
                        otp_code : $('#otp').val()
                    },
                    async       : true,
                    dataType    : 'json',
                    success     : function(response){
                        if(response.success){
                            swal({
                                background  : 'transparent',
                                html        : '<pre>Penggalangan dana telah diajukan' + '<br>' + 
                                              'Menunggu verifikasi Peduli Indonesia</pre>',
                                type        : "success"
                            }).then(function(){
                                location.href = site;
                            });
                        }
                        if(response.match){
                            console.log("OTP didn't match")
                        }
                    },
                    error: function (){
                        swal({
                            background  : 'transparent',
                            html        : '<pre>Connection lost' + '<br>' + 
                                          'Please try again</pre>',
                            type        : "warning"
                        });
                    }
                });
            }, 1500 );
        });
        $('#resend').on('click', function(){
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'log/otp_non_medis_resend',
                            data        : {id_kasus : id_kasus},
                            async       : true,
                            dataType    : 'json',
                            success     : function(response){
                                if(response.unsent){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Gagal mengirim kode OTP' + '<br>' + 
                                                      'Silahkan mencoba lagi</pre>',
                                        type        : "warning"
                                    });
                                }else{
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Kode OTP telah dikirim ke' + '<br>' + 
                                                      response.success + '</pre>',
                                        type        : "success"
                                    }).then(function(){
                                        location.reload(true);
                                    });
                                }
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
    }
});