$(document).ready(function(){
    $('#deskripsi_cerita').summernote({
    	popover: {
            image: [
                ['remove', ['removeMedia']]
            ],
        },
        callbacks: {
            onImageUpload: function(image) {
                editor_upload_img(image[0]);
            },
            onMediaDelete : function(target) {
                editor_delete_img(target[0].src);
            }
        },
        // placeholder: 'Set Placeholder',
        tabsize: 2,
        height: 300,
        toolbar: [
          	['style', ['style']],
          	['font', ['bold', 'underline', 'italic']],
          	['color', ['color']],
          	['para', ['ul', 'ol', 'paragraph']],
          	['insert', ['link', 'picture']]
        ]
  	});
  	function editor_upload_img(image){
  		if(image.size > 3000000){
            swal({
                background  : 'transparent',
                html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                              '3 mb</pre>',
                type        : "warning"
            });
        }else{
      		var data = new FormData();
            data.append("image", image);
            $.ajax({
                url: site + 'log/non_medis_cerita_upload_img',
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: "POST",
                success: function(url) {
                    $('#deskripsi_cerita').summernote("insertImage", url);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        }
    }
    function editor_delete_img(src) {
        $.ajax({
            data: {src : src},
            type: "POST",
            url: site + 'log/non_medis_cerita_delete_img',
            cache: false,
            success: function(response) {
                console.log(response);
            }
        });
    }
	main();
	function main(){
		$('.steps').on('click', function(){
			$('.steps').css({'color':'#212529', 'background':'#fff'});
			$(this).css({'color':'#fff', 'background':'rgb(255,197,70)'});
			var data_c = $(this).attr('data');
			$('.c_tab').hide();
			swal({
		        showConfirmButton   : false,
		        allowOutsideClick   : false,
		        allowEscapeKey      : false,
		        background          : 'transparent',
		        onOpen  : function(){
		            swal.showLoading();
		            setTimeout(function(){
		            	if(data_c == 'identitas'){
		            		get_identitas();
		            	}
		            	if(data_c == 'donasi'){
		            		get_donasi();
		            	}
		            	if(data_c == 'detail'){
		            		get_detail();
		            	}
		            	if(data_c == 'foto'){
		            		get_foto();
		            	}
		            	if(data_c == 'deskripsi'){
		            		get_deskripsi();
		            	}
		            	if(data_c == 'konfirmasi'){
		            		get_konfirmasi();
		            	}
		            },500);
		        }
		    });
		});
	}
	swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
            	get_identitas();
            },500);
        }
    });
	function get_identitas(){
		$.ajax({
            type        : 'ajax',
            method      : 'post',
            data 		: {id_kasus : id_kasus},
            url         : site + 'log/non_medis_identitas',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                $('.steps').css({'color':'#212529', 'background':'#fff'});
                $('#steps_identitas').css({'color':'#fff', 'background':'rgb(255,197,70)'});

            	$('#identitas_pekerjaan').val(data.pekerjaan);
            	$('#identitas_organisasi').val(data.organisasi);
                if(data.medsos != '0'){
                    $('#identitas_medsos').val(data.medsos);
                }
            	$('#identitas_medsos_akun').val(data.akun_medsos);
            	$('#identitas_domisili').val(data.domisili);
            	$('#identitas_tentang').val(data.tentang);
				$('#c_identitas').slideDown(500);
				swal.close();
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
	}
    f_identitas();
    function f_identitas(){
        var url = '';
        var form_data = '';
        $('#btn_identitas').on('click', function(){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('#form_identitas').attr('action', site + 'log/non_medis_identitas_update')
            url             = $('#form_identitas').attr('action');
            form_data       = $('#form_identitas')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#identitas_pekerjaan').val()){
                 errormessage += 'Pekerjaan dibutuhkan \n';
            }
            if(! $('#identitas_organisasi').val()){
                 errormessage += 'Sekolah/Perusahaan/Lembaga dibutuhkan \n';
            }
            if(! $('#identitas_medsos').val()){
                 errormessage += 'Media sosial dibutuhkan \n';
            }
            if(! $('#identitas_medsos_akun').val()){
                 errormessage += 'Akun media sosial dibutuhkan \n';
            }
            if(! $('#identitas_domisili').val()){
                 errormessage += 'Domisili dibutuhkan \n';
            }
            if(! $('#identitas_tentang').val()){
                 errormessage += 'Deskripsi tentang anda dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type        : 'ajax',
                                method      : 'post',
                                url         : url,
                                data        : form_data,
                                async       : true,
                                processData : false,
                                contentType : false,
                                cache       : false,
                                dataType    : 'json',
                                success     : function(response){
                                    if(response.success){
                                        $('.c_tab').hide();
                                        setTimeout(function(){
                                            get_donasi();
                                        },500);
                                    }
                                },
                                error: function (){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
    }
	function get_donasi(){
		$.ajax({
            type        : 'ajax',
            method      : 'post',
            data 		: {id_kasus : id_kasus},
            url         : site + 'log/non_medis_donasi',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                $('.steps').css({'color':'#212529', 'background':'#fff'});
                $('#steps_donasi').css({'color':'#fff', 'background':'rgb(255,197,70)'});

                if(data.donasi != '0'){
                    $('#donasi_donasi').val(data.donasi.replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                }
            	$('#donasi_jatuh_tempo').val(data.jatuh_tempo);
            	if(data.jatuh_tempo == ''){
            		$('#lama_tempo').val('');
            	}else{
            		var start = new Date(data.now_date);
                    var end = new Date(data.jatuh_tempo);
                    var day_left = Math.abs(end - start);
                    day_left = Math.ceil(day_left / (1000 * 60 * 60 * 24)); 
            		$('#donasi_lama_tempo').val(day_left);
            	}
            	$('#donasi_penerima').val(data.penerima);
            	$('#donasi_penggunaan').val(data.penggunaan);
				$('#c_donasi').slideDown(500);
				swal.close();
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
	}
    f_donasi();
    function f_donasi(){
        $('#donasi_jatuh_tempo').datepicker({
            minDate: new Date(),
            minDate: 30,
            yearRange : '-1:+1',
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            dayNames: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
            dayNamesMin: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
            monthNamesShort: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            monthNames: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            beforeShow: function() {
                $(document).off('focusin.bs.modal');
            },
            onClose: function(){
                $(document).on('focusin.bs.modal');
            }
        });
        $('#donasi_jatuh_tempo').on('change', function(){
            var end = $(this).val();
            var start = new Date();
            start = start.getFullYear() + '-' + (start.getMonth() + 1) + '-' + start.getDate();

            start = new Date(start);
            end = new Date(end);
            var d_time = Math.abs(end - start);
            var d_day = Math.ceil(d_time / (1000 * 60 * 60 * 24)); 
            $('#donasi_lama_tempo').val(d_day);
        });
        $('#donasi_donasi').on('keyup', function(){
            $(this).val(function(index, value) {
                return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            });
        });
        var url = '';
        var form_data = '';
        $('#btn_donasi').on('click', function(){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('#form_donasi').attr('action', site + 'log/non_medis_donasi_update')
            url             = $('#form_donasi').attr('action');
            form_data       = $('#form_donasi')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#donasi_donasi').val()){
                 errormessage += 'Jumlah dana dibutuhkan \n';
            }
            if(! $('#donasi_jatuh_tempo').val()){
                 errormessage += 'Akhir penggalangan dana dibutuhkan \n';
            }
            if(! $('#donasi_lama_tempo').val()){
                 errormessage += 'Lama penggalangan dana dibutuhkan \n';
            }
            if(! $('#donasi_penerima').val()){
                 errormessage += 'Penerima dibutuhkan \n';
            }
            if(! $('#donasi_penggunaan').val()){
                 errormessage += 'Rincian penggunaan dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type        : 'ajax',
                                method      : 'post',
                                url         : url,
                                data        : form_data,
                                async       : true,
                                processData : false,
                                contentType : false,
                                cache       : false,
                                dataType    : 'json',
                                success     : function(response){
                                    if(response.success){
                                        $('.c_tab').hide();
                                        setTimeout(function(){
                                            get_detail();
                                        },500);
                                    }
                                },
                                error: function (){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
    }
	function get_detail(){
		$.ajax({
            type        : 'ajax',
            method      : 'post',
            data 		: {id_kasus : id_kasus},
            url         : site + 'log/non_medis_detail',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                $('.steps').css({'color':'#212529', 'background':'#fff'});
                $('#steps_detail').css({'color':'#fff', 'background':'rgb(255,197,70)'});

            	if(data.id_bantuan_non_medis == '0'){
            		$('#detail_kategori').val('');
            	}else{
            		$('#detail_kategori').val(data.id_bantuan_non_medis);
            	}
            	$('#detail_judul').val(data.judul);
            	$('#detail_link').val(data.link);
            	$('#detail_tujuan').val(data.tujuan);
				$('#c_detail').slideDown(500);
				swal.close();
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
	}
    f_detail();
    function f_detail(){
        $('#detail_link').on('keyup', function(){
            var chars = /^[0-9a-z]+$/gm;
            if(! chars.test($(this).val())) {
                 $(this).val('');
            }
        });
        var url = '';
        var form_data = '';
        $('#btn_detail').on('click', function(){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('#form_detail').attr('action', site + 'log/non_medis_detail_update')
            url             = $('#form_detail').attr('action');
            form_data       = $('#form_detail')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#detail_kategori').val()){
                 errormessage += 'Kategori dibutuhkan \n';
            }
            if(! $('#detail_judul').val()){
                 errormessage += 'Judul dibutuhkan \n';
            }
            if(! $('#detail_link').val()){
                 errormessage += 'Link dibutuhkan \n';
            }
            if(! $('#detail_tujuan').val()){
                 errormessage += 'Tujuan dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type        : 'ajax',
                                method      : 'post',
                                url         : url,
                                data        : form_data,
                                async       : true,
                                processData : false,
                                contentType : false,
                                cache       : false,
                                dataType    : 'json',
                                success     : function(response){
                                    if(response.link){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Link sudah dipakai' + '<br>' + 
                                                          'Harap gunakan link lain</pre>'
                                        });
                                    }
                                    if(response.success){
                                        $('.c_tab').hide();
                                        setTimeout(function(){
                                            get_foto();
                                        },500);
                                    }
                                },
                                error: function (){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
    }
	function get_foto(){
		$.ajax({
            type        : 'ajax',
            method      : 'post',
            data 		: {id_kasus : id_kasus},
            url         : site + 'log/non_medis_foto',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                $('.steps').css({'color':'#212529', 'background':'#fff'});
                $('#steps_foto').css({'color':'#fff', 'background':'rgb(255,197,70)'});

            	$('#get_sampul, #delete_sampul, #hide_sampul').val(data.sampul);
            	if(data.sampul != ''){
                    $('#preview_items').attr('src', site + 'assets/project/kasus_non_medis/' + data.sampul);
                    $('#delete_preview_items').css('display','block');
                }else{
                    $('#preview_items').attr('src', '');
                    $('#delete_preview_items').css('display','none');
                }
				$('#c_foto').slideDown(500);
				display_image();
				swal.close();
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
	}
    f_foto();
    function f_foto(){
        var url = '';
        var form_data = '';
        $('#btn_foto').on('click', function(){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('#form_foto').attr('action', site + 'log/non_medis_foto_update')
            url             = $('#form_foto').attr('action');
            form_data       = $('#form_foto')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#hide_sampul').val()){
                 errormessage += 'Foto dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type        : 'ajax',
                                method      : 'post',
                                url         : url,
                                data        : form_data,
                                async       : true,
                                processData : false,
                                contentType : false,
                                cache       : false,
                                dataType    : 'json',
                                success     : function(response){
                                    if(response.success){
                                        $('.c_tab').hide();
                                        setTimeout(function(){
                                            get_deskripsi();
                                        },500);
                                    }
                                },
                                error: function (){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
    }
	function get_deskripsi(){
		$.ajax({
            type        : 'ajax',
            method      : 'post',
            data 		: {id_kasus : id_kasus},
            url         : site + 'log/non_medis_deskripsi',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                $('.steps').css({'color':'#212529', 'background':'#fff'});
                $('#steps_deskripsi').css({'color':'#fff', 'background':'rgb(255,197,70)'});

            	$('#deskripsi_cerita').summernote('code', data.cerita);
            	$('#deskripsi_ajakan').val(data.ajakan);
				$('#c_deskripsi').slideDown(500);
				swal.close();
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
	}
    f_deskripsi();
    function f_deskripsi(){
        var url = '';
        var form_data = '';
        $('#btn_deskripsi').on('click', function(){
            $('#deskripsi_cerita').val($('#deskripsi_cerita').summernote('code'));
            event.preventDefault();
            event.stopImmediatePropagation();
            $('#form_deskripsi').attr('action', site + 'log/non_medis_deskripsi_update')
            url             = $('#form_deskripsi').attr('action');
            form_data       = $('#form_deskripsi')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#deskripsi_cerita').val()){
                 errormessage += 'Deskripsi cerita dibutuhkan \n';
            }
            if(! $('#deskripsi_ajakan').val()){
                 errormessage += 'Deskripsi ajakan dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type        : 'ajax',
                                method      : 'post',
                                url         : url,
                                data        : form_data,
                                async       : true,
                                processData : false,
                                contentType : false,
                                cache       : false,
                                dataType    : 'json',
                                success     : function(response){
                                    if(response.success){
                                        $('.c_tab').hide();
                                        setTimeout(function(){
                                            get_konfirmasi();
                                        },500);
                                    }
                                },
                                error: function (){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
    }
	function get_konfirmasi(){
		$.ajax({
            type        : 'ajax',
            method      : 'post',
            data 		: {id_kasus : id_kasus},
            url         : site + 'log/non_medis_konfirmasi',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                $('.steps').css({'color':'#212529', 'background':'#fff'});
                $('#steps_konfirmasi').css({'color':'#fff', 'background':'rgb(255,197,70)'});
            	//Identitas
            	$('#identitas_pekerjaan').val(data.pekerjaan);
            	$('#identitas_organisasi').val(data.organisasi);
            	if(data.medsos != '0'){
                    $('#identitas_medsos').val(data.medsos);
                }
            	$('#identitas_medsos_akun').val(data.akun_medsos);
            	$('#identitas_domisili').val(data.domisili);
            	$('#identitas_tentang').val(data.tentang);
            	//Donasi
            	$('#donasi_donasi').val(data.donasi.replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            	$('#donasi_jatuh_tempo').val(data.jatuh_tempo);
            	if(data.jatuh_tempo == ''){
            		$('#lama_tempo').val('');
            	}else{
            		var start = new Date(data.now_date);
                    var end = new Date(data.jatuh_tempo);
                    var day_left = Math.abs(end - start);
                    day_left = Math.ceil(day_left / (1000 * 60 * 60 * 24)); 
            		$('#donasi_lama_tempo').val(day_left);
            	}
            	$('#donasi_penerima').val(data.penerima);
            	$('#donasi_penggunaan').val(data.penggunaan);
            	//Detail
            	if(data.id_bantuan_non_medis == '0'){
            		$('#detail_kategori').val('');
            	}else{
            		$('#detail_kategori').val(data.id_bantuan_non_medis);
            	}
            	$('#detail_judul').val(data.judul);
            	$('#detail_link').val(data.link);
            	$('#detail_tujuan').val(data.tujuan);
            	//Foto Utama
            	$('#get_sampul, #delete_sampul, #hide_sampul').val(data.sampul);
            	if(data.sampul != ''){
                    $('#preview_items').attr('src', site + 'assets/project/kasus_non_medis/' + data.sampul);
                    $('#delete_preview_items').css('display','block');
                }else{
                    $('#preview_items').attr('src', '');
                    $('#delete_preview_items').css('display','none');
                }
                //Deskripsi
                $('#deskripsi_cerita').summernote('code', data.cerita);
            	$('#deskripsi_ajakan').val(data.ajakan);
            	//Konfirmasi
            	$('input[name="konfirmasi_keperluan"]').val([data.keperluan]);
                $('.s_keperluan').css('background-color', '#fff').find('label').children().attr('data-prefix', 'far');
                $('#keperluan' + data.keperluan).parent().css('background-color', 'rgb(255,239,212)').find('label').children().attr('data-prefix', 'fas');

            	$('input[name="konfirmasi_kota"]').val([data.kota]);
                $('.s_lokasi').css('background-color', '#fff').find('label').children().attr('data-prefix', 'far');
                $('#lokasi' + data.kota).parent().css('background-color', 'rgb(255,239,212)').find('label').children().attr('data-prefix', 'fas');

            	if(data.kota == '0'){
            		$('#konfirmasi_kota_content').hide();
            	}else{
            		$('#konfirmasi_kota_content').show();
            	}
				$('#c_konfirmasi').slideDown(500);
				swal.close();
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
	}
	f_konfirmasi();
	function f_konfirmasi(){
		var arr_konfirmasi = [];
        $('.radio_keperluan').on('click', function(){
            $('input[name="konfirmasi_kota"]:checked').val('');
            $('.radio_lokasi').children().attr('data-prefix', 'far');
            $('input[name="konfirmasi_kota"]').prop('checked', false);

            $('.s_keperluan').css('background-color', '#fff');
            $(this).parent().css('background-color', 'rgb(255,239,212)');

            $('.radio_keperluan').children().attr('data-prefix', 'far');
            $(this).children().attr('data-prefix', 'fas');
            $('input[name="konfirmasi_keperluan"]:checked').val($(this).attr('data'));

            if($(this).attr('data') != '1'){
                $('#konfirmasi_kota_content').slideDown(250);
            }else{
                $('#konfirmasi_kota_content').slideUp(250);
            }
        });
        $('.radio_lokasi').on('click', function(){
            $('.s_lokasi').css('background-color', '#fff');
            $(this).parent().css('background-color', 'rgb(255,239,212)');

            $('.radio_lokasi').children().attr('data-prefix', 'far');
            $(this).children().attr('data-prefix', 'fas');
            $('input[name="konfirmasi_kota"]:checked').val($(this).attr('data'));
        });
        $('.konfirm_persetujuan').on('click', function(){
            var vals = $(this).attr('data');
            if($(this).children().attr('data-prefix') == 'fas'){
                $(this).children().removeClass('fa-check-square').addClass('fa-square').attr('data-prefix', 'far');
                $(this).parent().css('background-color', '#fff');
                arr_konfirmasi = arr_konfirmasi.filter(item => item !== vals);
            }else{
                $(this).children().removeClass('fa-square').addClass('fa-check-square').attr('data-prefix', 'fas');
                $(this).parent().css('background-color', 'rgb(255,239,212)');
                arr_konfirmasi.push(vals);
            }
        });
        var url = '';
        var form_data = '';
		$('#btn_konfirmasi').on('click', function(){
			event.preventDefault();
            event.stopImmediatePropagation();
            $('#form_konfirmasi').attr('action', site + 'log/non_medis_konfirmasi_update') 
            url             = $('#form_konfirmasi').attr('action');
            form_data       = $('#form_konfirmasi')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#identitas_pekerjaan').val()){
                 errormessage += 'Pekerjaan dibutuhkan \n';
            }
            if(! $('#identitas_organisasi').val()){
                 errormessage += 'Sekolah/Perusahaan/Lembaga dibutuhkan \n';
            }
            if(! $('#identitas_medsos').val()){
                 errormessage += 'Media sosial dibutuhkan \n';
            }
            if(! $('#identitas_medsos_akun').val()){
                 errormessage += 'Akun media sosial dibutuhkan \n';
            }
            if(! $('#identitas_domisili').val()){
                 errormessage += 'Domisili dibutuhkan \n';
            }
            if(! $('#identitas_tentang').val()){
                 errormessage += 'Deskripsi tentang anda dibutuhkan \n';
            }
            if(! $('#donasi_donasi').val()){
                 errormessage += 'Jumlah dana dibutuhkan \n';
            }
            if(! $('#donasi_jatuh_tempo').val()){
                 errormessage += 'Akhir penggalangan dana dibutuhkan \n';
            }
            if(! $('#donasi_lama_tempo').val()){
                 errormessage += 'Lama penggalangan dana dibutuhkan \n';
            }
            if(! $('#donasi_penerima').val()){
                 errormessage += 'Penerima dibutuhkan \n';
            }
            if(! $('#donasi_penggunaan').val()){
                 errormessage += 'Rincian penggunaan dibutuhkan \n';
            }
            if(! $('#detail_kategori').val()){
                 errormessage += 'Kategori dibutuhkan \n';
            }
            if(! $('#detail_judul').val()){
                 errormessage += 'Judul dibutuhkan \n';
            }
            if(! $('#detail_link').val()){
                 errormessage += 'Link dibutuhkan \n';
            }
            if(! $('#detail_tujuan').val()){
                 errormessage += 'Tujuan dibutuhkan \n';
            }
            if(! $('#hide_sampul').val()){
                 errormessage += 'Foto dibutuhkan \n';
            }
            if(! $('#deskripsi_cerita').val()){
                 errormessage += 'Deskripsi cerita dibutuhkan \n';
            }
            if(! $('#deskripsi_ajakan').val()){
                 errormessage += 'Deskripsi ajakan dibutuhkan \n';
            }
            if(! $('#konfirmasi_email').val()){
                 errormessage += 'Email dibutuhkan \n';
            }
            if(! $('input[name="konfirmasi_keperluan"]').is(':checked')){
                 errormessage += 'Tujuan keperluan dibutuhkan \n';
            }else{
            	if($('input[name="konfirmasi_keperluan"]:checked').val() != '1' && ! $('input[name="konfirmasi_kota"]').is(':checked')){
            		errormessage += 'Keterangan kota dengan penerima dana dibutuhkan \n';
            	}
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else if(errormessage == '' && arr_konfirmasi.length < 1){
            	if(arr_konfirmasi.length == 0){
            		$('#btn_konfirmasi, #persetujuan1, .label_form_rule1').addClass('animated flash').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                    	$('#btn_process, #persetujuan1, .label_form_rule1').removeClass('animated flash');
                	});
            	}else{
            		if(! arr_konfirmasi.includes('1')){
            			$('#btn_konfirmasi, #persetujuan1, .label_form_rule1').addClass('animated flash').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                    		$('#btn_konfirmasi, #persetujuan1, .label_form_rule1').removeClass('animated flash');
                		});
            		}
            	}
            }else{
                swal({
                    html                : '<pre>Data pengajuan penggalangan dana' + '<br>' + 
                                          'Tidak dapat diubah kembali' + '<br>' + 
                                          'Apakah data sudah benar ?</pre>',
                    type                : "question",
                    background          : 'transparent',
                    showCancelButton    : true,
                    cancelButtonText    : 'Tidak',
                    confirmButtonText   : 'Ya'
                }).then((result) => {
                    if(result.value){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                setTimeout(function(){
                                    $.ajax({
                                        type        : 'ajax',
                                        method      : 'post',
                                        url         : url,
                                        data        : form_data,
                                        async       : true,
                                        processData : false,
                                        contentType : false,
                                        cache       : false,
                                        dataType    : 'json',
                                        success     : function(response){
                                            if(response.success){
                                                swal({
                                                    background  : 'transparent',
                                                    html        : '<pre>Penggalangan dana telah diajukan' + '<br>' + 
                                                                  'Menunggu verifikasi Peduli Indonesia</pre>',
                                                    type        : "success"
                                                }).then(function(){
                                                    location.href = site + 'home/riwayat_galang_dana';
                                                });
                                            }
                                            // if(response.success){
                                            //  swal({
                                            //         background  : 'transparent',
                                            //         html        : '<pre>Kode OTP telah dikirim ke' + '<br>' + 
                                            //                       $('#konfirmasi_email').val() + '</pre>',
                                            //         type        : "success"
                                            //     }).then(function(){
                                            //      location.href = site + 'home/otp_non_medis/' + id_kasus;
                                            //     });
                                            // }
                                            if(response.unsent){
                                                swal({
                                                    background  : 'transparent',
                                                    html        : '<pre>Gagal menyimpan data' + '<br>' + 
                                                                  'Silahkan mencoba lagi</pre>',
                                                    type        : "warning"
                                                });
                                            }
                                        },
                                        error: function (){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Connection lost' + '<br>' + 
                                                              'Please try again</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    });
                                },500);
                            }
                        });
                    }
                });
            }
		});
	}
	function display_image(){
        function preview(image){
        	if(image.files[0].size > 3000000){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                                  '3 mb</pre>',
                    type        : "warning"
                });
                $("#sampul").val('');
            }else{
	            if(image.files && image.files[0]){
	                var reader      = new FileReader();
	                reader.onload   = function(event){
	                    $('#preview_items').attr('src', event.target.result);
	                    $('#preview_items').attr('title', image.files[0].name);
	                    $('#hide_sampul').val(image.files[0].name);
	                    $('#delete_preview_items').css('display','block');
	                }
	                reader.readAsDataURL(image.files[0]);
	            }
	        }
        }
        $("#sampul").on('change', function(){
            preview(this);
            var names = $(this).val();
            var file_names = names.replace(/^.*\\/, "");
        });
        $('#delete_preview_items').on('click', function(){
            $('#delete_preview_items').css('display','none');
            $('#preview_items').attr('src', '');
            $('#sampul, #get_sampul, #hide_sampul').val('');
        });
    }
});