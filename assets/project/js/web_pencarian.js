$(document).ready(function(){
	var table_donasi, table_agenda;
	swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                donasi_search();
            },500);
        }
    });
	function donasi_search(){
        table_donasi = $('#table_search_donasi').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            // scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            info 				: false,
            searching			: false,
            lengthChange		: false,
            lengthMenu          : [[4], [4]],
            initComplete: function(){
                agenda_search();
            },
            ajax            : {
                url         : site + 'log/search_donasi',
                method      : 'post',
                data      	: {keysearch : keysearch}
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
    function agenda_search(){
        table_agenda = $('#table_search_agenda').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            // scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            info 				: false,
            searching			: false,
            lengthChange		: false,
            lengthMenu          : [[4], [4]],
            initComplete: function(){
                main();
            },
            ajax            : {
                url         : site + 'log/search_agenda',
                method      : 'post',
                data      	: {keysearch : keysearch}
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
    function main(){
    	if(table_donasi.data().length == 0 && table_agenda.data().length == 0){
    		var search_result =
    		'<div class="w-100 text-center" style="font-size: 75px; color: rgb(255,197,70); padding: 75px 0;">' +
				'<i class="fas fa-search"></i>' +
			'</div>' +
			'<div class="w-100 text-center" style="text-transform: capitalize;"><i>tidak ada hasil</i></div>' +
			'<div class="w-100 text-center" style="text-transform: capitalize;"><i>cobalah kata kunci lain</i></div>';
			$('#search_result').html(search_result);
			swal.close();
			$('#search_result').show();
    	}else{
    		if(table_donasi.data().length != 0 && table_agenda.data().length != 0){
    			swal.close();
    			$('#search_result, #donasi_result, #agenda_result').show();
    		}
    		if(table_donasi.data().length != 0 && table_agenda.data().length == 0){
    			swal.close();
    			$('#search_result, #donasi_result').show();
    		}
    		if(table_donasi.data().length == 0 && table_agenda.data().length != 0){
    			swal.close();
    			$('#search_result, #agenda_result').show();
    		}
    	}
        var column_gambar_items
        $('#column_gambar_items').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            column_gambar_items = true;
        });
        $('#column_gambar_items').on('hide.bs.modal', function(){
            if(column_gambar_items){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                column_gambar_items = false;
                setTimeout(function(){
                    $('#column_gambar_items').modal('hide');
                },500);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#table_search_agenda').on('click', '.detail_artikel', function(){
            var id_agenda = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'log/detail_agenda',
                            data        : {id_agenda : id_agenda},
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                swal.close();
                                $('#image_column_items').attr('src', data.image).hide();
                                $('#description_content').show();
                                $('#title_artikel').html(data.title);
                                $('#description_artikel').html(data.description);
                                $('#column_gambar_items').modal('show');
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_search_agenda').on('click', '.img_artikel', function(){
            var id_agenda = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'log/detail_agenda',
                            data        : {id_agenda : id_agenda},
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                swal.close();
                                $('#image_column_items').attr('src', data.image).show();
                                $('#description_content').hide();
                                $('#title_artikel').html(data.title);
                                $('#description_artikel').html(data.description);
                                $('#column_gambar_items').modal('show');
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
    }
});