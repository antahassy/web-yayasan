$(document).ready(function(){
    var nama_bulan = new Array();
    nama_bulan[1] = 'January';
    nama_bulan[2] = 'February';
    nama_bulan[3] = 'March';
    nama_bulan[4] = 'April';
    nama_bulan[5] = 'May';
    nama_bulan[6] = 'June';
    nama_bulan[7] = 'July';
    nama_bulan[8] = 'August'; 
    nama_bulan[9] = 'September'; 
    nama_bulan[10] = 'October';
    nama_bulan[11] = 'November'; 
    nama_bulan[12] = 'December';
    var table;
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                level_grup();
            },500);
        }
    });
    function level_grup(){
        $.ajax({
            type        : 'ajax',
            method      : 'get',
            url         : site + 'user/level_grup',
            dataType    : "json",
            async       : true,
            success: function(data){
                var s_level = '<option value="">Select Level</option>';
                // for(i = 0; i < data.length; i ++){
                //     s_level += '<option value="' + data[i].id + '">' + data[i].description + '</option>';
                // }
                s_level += '<option value="' + data[0].id + '">' + data[0].description + '</option>';
                $('#s_level').html(s_level);
                setTimeout(function(){
                    s_side_data_table();
                },500);
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
    }
    function s_side_data_table(){
        table = $('#table_data').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            lengthMenu          : [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, 'Semua']],
            initComplete: function(){
                swal.close();
                main();
            },
            ajax            : {
                url         : site + 'user/s_side_data',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
    function main(){
        var modal_form;
        $('#modal_form').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_form = true;
        });
        $('#modal_form').on('hide.bs.modal', function(){
            if(modal_form){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_form = false;
                setTimeout(function(){
                    $('#modal_form').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#btn_add').on('click', function(){
            $('#form_data')[0].reset();
            $('#modal_form').find('.modal-title').text('Add');
            $('#form_data').attr('action', site + 'user/save');
            $('#btn_process').text('Save');
            $('#username').attr('readonly', false).css({'background':'transparent','color':'#3F4254'});
            $('#password').attr('placeholder', '');
            $('#delete_preview_items').css('display','none');
            $('#preview_items').attr('src', '');
            $('#image, #get_image, #delete_image').val('');
            $('#l_password span').show();
            $('#modal_form').modal('show');
            display_image();
            data_process();
        });
        $('#table_data').on('click', '#btn_edit', function(){
            var id = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type           : 'ajax',
                            method         : 'post',
                            url            : site + 'user/edit',
                            data           : {id : id},
                            async          : true,
                            dataType       : 'json',
                            success        : function(data){
                                $('#form_data')[0].reset();
                                $('#id_data').val(data.id);
                                $('#username').val(data.username).attr('readonly', true).css({'background':'#3F4254','color':'#fff'});
                                $('#password').attr('placeholder', 'Leave blank if not filled');
                                $('#nama').val(data.nama);
                                $('#telepon').val(data.phone);
                                $('#email').val(data.email);
                                $('#s_level').val(data.level);

                                if(! data.image){
                                    $('#get_image, #delete_image').val('');
                                    $('#preview_items').attr('src', site + 'assets/metronic/media/users/blank.png');
                                    $('#delete_preview_items').hide();
                                }else{
                                    $('#get_image, #delete_image').val(data.image);
                                    $('#preview_items').attr('src', site + 'assets/user/image/' + data.image);
                                    $('#delete_preview_items').show();
                                }
                                $('#image').val('');
                                display_image(); 
                                $('#l_password span').hide();
                                $('#modal_form').find('.modal-title').text('Edit');
                                $('#form_data').attr('action', site + 'user/update');
                                $('#btn_process').text('Update');
                                swal.close();
                                $('#modal_form').modal('show');
                                data_process();
                            },
                            error     : function(){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_data').on('click', '#btn_activated', function(){
            var id = $(this).attr('data');
            var activ = $(this).attr('alt');
            var question = '', link_url = '';
            if(activ == 0){
                question = 'Active ?';
                link_url = site + 'user/aktifkan';
            }else{
                question = 'Disactive ?';
                link_url = site + 'user/non_aktifkan';
            }
            swal({
                html                : '<pre>' + question + '</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'No',
                confirmButtonText   : 'Yes'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    data        : {id : id},
                                    url         : link_url,
                                    dataType    : "json",
                                    async       : true,
                                    success: function(data){
                                        swal.close();
                                        setTimeout(function(){
                                            table.ajax.reload();
                                        },500);
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
    }
    function display_image(){
        function preview(image){
            if(image.files[0].size > 3000000){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                                  '3 mb</pre>',
                    type        : "warning"
                });
                $("#image").val('');
            }else{
                if(image.files && image.files[0]){
                    var reader      = new FileReader();
                    reader.onload   = function(event){
                        $('#preview_items').attr('src', event.target.result);
                        $('#preview_items').attr('title', image.files[0].name);
                        $('#delete_preview_items').css('display','block');
                    }
                    reader.readAsDataURL(image.files[0]);
                }
            }
        }
        $("#image").on('change', function(){
            preview(this);
            var names = $(this).val();
            var file_names = names.replace(/^.*\\/, "");
        });
        $('#delete_preview_items').on('click', function(){
            $('#delete_preview_items').css('display','none');
            $('#preview_items').attr('src', '');
            $('#image, #get_image').val('');
        });
    }
    function data_process(){
        $('#btn_process').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var url             = $('#form_data').attr('action');
            var form_data       = $('#form_data')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#username').val()){
                errormessage += 'Username required \n';
            }
            if(url == site + 'user/save'){
                if(! $('#password').val()){
                    errormessage += 'Password required \n';
                }
            }
            if(! $('#nama').val()){
                errormessage += 'Name required \n';
            }
            if(! $('#s_level').val()){
                errormessage += 'Level required \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type           : 'ajax',
                                method         : 'post',
                                url            : url,
                                data           : form_data,
                                async          : true,
                                processData    : false,
                                contentType    : false,
                                cache          : false,
                                dataType       : 'json',
                                success        : function(response){
                                    if(response.success){
                                        $('#modal_form').modal('hide');
                                        $('#form_data')[0].reset();
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Data ' + response.type + ' successfully</pre>',
                                            type        : "success"
                                        }).then(function(){
                                            setTimeout(function(){
                                                table.ajax.reload();
                                            },500);
                                        });
                                    }
                                    if(response.account){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Username already exist ' + '<br>' + 
                                                          'Use another username</pre>',
                                            type        : "warning"
                                        });
                                    }
                                },
                                error   : function(){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
            return false;
        });
    }
});