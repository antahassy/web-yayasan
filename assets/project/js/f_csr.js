$(document).ready(function(){
    $('#description').summernote({
        // popover: {
        //     image: [
        //         ['remove', ['removeMedia']]
        //     ],
        // },
        callbacks: {
            onImageUpload: function(image) {
                editor_upload_img(image[0]);
            },
            onMediaDelete : function(target) {
                editor_delete_img(target[0].src);
            }
        },
        // placeholder: 'Set Placeholder',
        tabsize: 2,
        height: 300,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'italic']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']]
        ]
    });
    function editor_upload_img(image){
        if(image.size > 3000000){
            swal({
                background  : 'transparent',
                html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                              '3 mb</pre>',
                type        : "warning"
            });
        }else{
            var data = new FormData();
            data.append("image", image);
            $.ajax({
                url: site + 'f_csr/editor_upload_img',
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: "POST",
                success: function(url) {
                    $('#description').summernote("insertImage", url);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        }
    }
    function editor_delete_img(src) {
        $.ajax({
            data: {src : src},
            type: "POST",
            url: site + 'f_csr/editor_delete_img',
            cache: false,
            success: function(response) {
                console.log(response);
            }
        });
    }
    var nama_bulan = new Array();
    nama_bulan[1] = 'January';
    nama_bulan[2] = 'February';
    nama_bulan[3] = 'March';
    nama_bulan[4] = 'April';
    nama_bulan[5] = 'May';
    nama_bulan[6] = 'June';
    nama_bulan[7] = 'July';
    nama_bulan[8] = 'August'; 
    nama_bulan[9] = 'September'; 
    nama_bulan[10] = 'October';
    nama_bulan[11] = 'November'; 
    nama_bulan[12] = 'December';
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                main();
            },500);
        }
    });
    function main(){
        $.ajax({
            type        : 'ajax',
            method      : 'get',
            url         : site + 'f_csr/main',
            dataType    : "json",
            async       : true,
            success: function(data){
                $('#form_data')[0].reset();
                $('#description').summernote('code', data.description);
                $('#update_content').html(data.last_update);
                $('#form_data').attr('action', site + 'f_csr/update');
                swal.close();
                update();
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
    }
    function update(){
        $('#btn_process').on('click', function(event){
            $('#description').val($('#description').summernote('code'));
            event.preventDefault();
            event.stopImmediatePropagation();
            var url             = $('#form_data').attr('action');
            var form_data       = $('#form_data')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';

            if(! $('#description').val()){
                 errormessage += 'Description required \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    html                : '<pre>Update new description ?</pre>',
                    type                : "question",
                    background          : 'transparent',
                    showCancelButton    : true,
                    cancelButtonText    : 'No',
                    confirmButtonText   : 'Yes'
                }).then((result) => {
                    if(result.value){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                setTimeout(function(){
                                    $.ajax({
                                        type        : 'ajax',
                                        method      : 'post',
                                        url         : url,
                                        data        : form_data,
                                        async       : true,
                                        processData : false,
                                        contentType : false,
                                        cache       : false,
                                        dataType    : 'json',
                                        success     : function(response){
                                            if(response.success){
                                                location.reload(true);
                                            }
                                        },
                                        error: function (){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Connection lost' + '<br>' + 
                                                              'Please try again</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    });
                                },500);
                            }
                        });
                    }
                });
            }
        });
    }
});