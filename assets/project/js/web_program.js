$(document).ready(function(){
    var table_program;
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                list_program();
            },500);
        }
    });
    function list_program(){
        table_program = $('#table_program').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            // scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            info                : false,
            searching           : false,
            lengthChange        : false,
            lengthMenu          : [[5], [5]],
            language: {
                emptyTable: "Program Belum Tersedia"
            },
            initComplete: function(){
                swal.close();
                main();
            },
            ajax            : {
                url         : site + 'log/list_program',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
    function main(){
        var column_gambar_items
        $('#column_gambar_items').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            column_gambar_items = true;
        });
        $('#column_gambar_items').on('hide.bs.modal', function(){
            if(column_gambar_items){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                column_gambar_items = false;
                setTimeout(function(){
                    $('#column_gambar_items').modal('hide');
                },500);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#table_program').on('click', '.detail_artikel', function(){
            var id_agenda = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'log/detail_agenda',
                            data        : {id_agenda : id_agenda},
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                swal.close();
                                $('#image_column_items').attr('src', data.image).hide();
                                $('#description_content').show();
                                $('#title_artikel').html(data.title);
                                $('#description_artikel').html(data.description);
                                $('#column_gambar_items').modal('show');
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_program').on('click', '.img_artikel', function(){
            var id_agenda = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'log/detail_agenda',
                            data        : {id_agenda : id_agenda},
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                swal.close();
                                $('#image_column_items').attr('src', data.image).show();
                                $('#description_content').hide();
                                $('#title_artikel').html(data.title);
                                $('#description_artikel').html(data.description);
                                $('#column_gambar_items').modal('show');
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
    }
});