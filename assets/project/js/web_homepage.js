$(document).ready(function(){
    var table_case;
    list_homepage();
    function list_homepage(){
        table_case = $('#table_case').DataTable({ 
            processing          : false, 
            destroy             : true,
            serverSide          : true, 
            // scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            info                : false,
            searching           : false,
            lengthChange        : false,
            paging              : false,
            lengthMenu          : [[4], [4]],
            language            : {
                emptyTable      : ' '
            },
            initComplete: function(){
                swal.close();
            },
            ajax            : {
                url         : site + 'log/list_homepage',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
});