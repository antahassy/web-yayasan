$(document).ready(function(){
    var table;
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                list_data();
            },500);
        }
    });
    function list_data(){
        table = $('#table_data').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true,  
            // scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            info                : false,
            searching           : false,
            lengthChange        : false,
            lengthMenu          : [[9], [9]],
            initComplete: function(){
                swal.close();
            },
            language: {
                emptyTable: "Daftar Donasi Belum Tersedia"
            },
            ajax            : {
                url         : site + 'log/list_donasi_medis',
                method      : 'post',
                data        : {id_kategori : id_kategori}
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
});