$(document).ready(function(){
    var nama_bulan = new Array();
    nama_bulan[1] = 'January';
    nama_bulan[2] = 'February';
    nama_bulan[3] = 'March';
    nama_bulan[4] = 'April';
    nama_bulan[5] = 'May';
    nama_bulan[6] = 'June';
    nama_bulan[7] = 'July';
    nama_bulan[8] = 'August'; 
    nama_bulan[9] = 'September'; 
    nama_bulan[10] = 'October';
    nama_bulan[11] = 'November'; 
    nama_bulan[12] = 'December';
    var table;
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                s_side_data_table();
            },500);
        }
    });
    function s_side_data_table(){
        table = $('#table_data').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            lengthMenu          : [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, 'Semua']],
            initComplete: function(){
                swal.close();
            },
            ajax            : {
                url         : site + 'k_kesehatan/s_side_data',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
    main();
    function main(){
        var column_gambar_items
        $('#column_gambar_items').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            column_gambar_items = true;
        });
        $('#column_gambar_items').on('hide.bs.modal', function(){
            if(column_gambar_items){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                column_gambar_items = false;
                setTimeout(function(){
                    $('#column_gambar_items').modal('hide');
                },500);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var modal_detail;
        $('#modal_detail').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_detail = true;
        });
        $('#modal_detail').on('hide.bs.modal', function(){
            if(modal_detail){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_detail = false;
                setTimeout(function(){
                    $('#modal_detail').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var modal_kabar_terbaru;
        $('#modal_kabar_terbaru').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_kabar_terbaru = true;
        });
        $('#modal_kabar_terbaru').on('hide.bs.modal', function(){
            if(modal_kabar_terbaru){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_kabar_terbaru = false;
                setTimeout(function(){
                    $('#modal_kabar_terbaru').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var modal_transfer;
        $('#modal_transfer').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_transfer = true;
        });
        $('#modal_transfer').on('hide.bs.modal', function(){
            if(modal_transfer){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_transfer = false;
                setTimeout(function(){
                    $('#modal_transfer').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#table_data').on('click', '#btn_transfer', function(){
            var id_kasus = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'k_kesehatan/get_transfer',
                            data        : {id_kasus : id_kasus},
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                //KTP
                                var ktp_detail = 'KTP belum tersedia';
                                if(data.file_ktp != ''){
                                    ktp_detail = '<img src="' + site + 'assets/project/pencairan_medis/' + data.file_ktp + '" style="width: 300px; cursor: pointer;" class="file_data">';
                                }
                                $('#ktp_content').html(ktp_detail);
                                //KK
                                var kk_detail = 'Kartu Keluarga belum tersedia';
                                if(data.file_kk != ''){
                                    kk_detail = '<img src="' + site + 'assets/project/pencairan_medis/' + data.file_kk + '" style="width: 300px; cursor: pointer;" class="file_data">';
                                }
                                $('#kk_content').html(kk_detail);
                                //Tabungan
                                var tabungan_detail = 'Buku tabungan belum tersedia';
                                if(data.file_rekening != ''){
                                    tabungan_detail = '<img src="' + site + 'assets/project/pencairan_medis/' + data.file_rekening + '" style="width: 300px; cursor: pointer;" class="file_data">';
                                }
                                $('#tabungan_content').html(tabungan_detail);
                                //Donasi
                                $('#donasi_content').html('Rp. ' + data.donasi.replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                                //Transfer
                                var transfer_val = Number(data.donasi - ((7/100) * Number(data.donasi)));
                                transfer_val = transfer_val.toFixed(0);
                                $('#transfer_content').html('Rp. ' + transfer_val.replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                                swal.close();
                                $('#modal_transfer').modal('show');
                                transfer_act();
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_data').on('click', '#btn_detail', function(){
            var id_kasus = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'k_kesehatan/detail',
                            data        : {id_kasus : id_kasus},
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                //Tujuan
                                $('#detail_tujuan_1').text(data.pasien);
                                $('#detail_tujuan_2').text(data.tlp_penggalang.replace(/\B(?=(\d{4})+(?!\d))/g, "-"));
                                $('#detail_tujuan_3').text(data.rekening);
                                //Pasien
                                $('#detail_pasien_1').text(data.nm_pasien);
                                $('#detail_pasien_2').text(data.nm_penyakit);
                                var detail_pasien_3 = '';
                                if(data.dokumen_medis != ''){
                                    detail_pasien_3 = '<img src="' + site + 'assets/project/kasus_medis/' + data.dokumen_medis + '" style="width: 300px; cursor: pointer;" class="file_data">';
                                }else{
                                    detail_pasien_3 =
                                    '<form id="form_dokumen_medis">' +
                                        '<input type="hidden" name="id_data" id="id_data" value="' + id_kasus + '">' +
                                        '<input type="hidden" name="hide_dokumen_medis" id="hide_dokumen_medis" value="">' +
                                        '<input type="hidden" name="delete_dokumen_medis" id="delete_dokumen_medis" value="">' +
                                        '<input type="hidden" name="get_dokumen_medis" id="get_dokumen_medis" value="">' +
                                        '<input type="file" name="dokumen_medis" id="dokumen_medis" accept="image/*" style="width: 100%; display: none;">' +
                                        '<label for="dokumen_medis" class="btn btn-warning" style="background-color: rgb(255,239,212);color: rgb(255,197,70) !important; border-color: transparent; margin-top: 25px;">Upload File</label>' +
                                        '<label id="save_dokumen_medis" data="' + id_kasus + '" class="btn btn-primary" style="margin-top: 25px; margin-left: 25px; display: none;">Save File</label>' +
                                        '<div>' +
                                            '<img id="preview_itemss" src="" title="" style="width: 300px;">' +
                                        '</div>' +
                                    '</form>';
                                }
                                $('#detail_pasien_3').html(detail_pasien_3);
                                //Medis
                                $('#detail_medis_1').text(data.inap);
                                $('#detail_medis_2').text(data.rs_inap);
                                $('#detail_medis_3').text(data.pengobatan);
                                $('#detail_medis_4').text(data.biaya);
                                var detail_medis_5 = '';
                                if(data.hasil_pemeriksaan != ''){
                                    detail_medis_5 = '<img src="' + site + 'assets/project/kasus_medis/' + data.hasil_pemeriksaan + '" style="width: 300px; cursor: pointer;" class="file_data">';
                                }else{
                                    detail_medis_5 =
                                    '<form id="form_hasil_pemeriksaan">' +
                                        '<input type="hidden" name="id_data" id="id_data" value="' + id_kasus + '">' +
                                        '<input type="hidden" name="hide_hasil_pemeriksaan" id="hide_hasil_pemeriksaan" value="">' +
                                        '<input type="hidden" name="delete_hasil_pemeriksaan" id="delete_hasil_pemeriksaan" value="">' +
                                        '<input type="hidden" name="get_hasil_pemeriksaan" id="get_hasil_pemeriksaan" value="">' +
                                        '<input type="file" name="hasil_pemeriksaan" id="hasil_pemeriksaan" accept="image/*" style="width: 100%; display: none;">' +
                                        '<label for="hasil_pemeriksaan" class="btn btn-warning" style="background-color: rgb(255,239,212);color: rgb(255,197,70) !important; border-color: transparent; margin-top: 25px;">Upload File</label>' +
                                        '<label id="save_hasil_pemeriksaan" data="' + id_kasus + '" class="btn btn-primary" style="margin-top: 25px; margin-left: 25px; display: none;">Save File</label>' +
                                        '<div>' +
                                            '<img id="preview_itemsss" src="" title="" style="width: 300px;">' +
                                        '</div>' +
                                    '</form>';
                                }
                                $('#detail_medis_5').html(detail_medis_5);
                                //Donasi
                                $('#detail_donasi_1').text('Rp. ' + data.dana.replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                                $('#detail_donasi_2').text(data.terakhir);
                                
                                var start = new Date(data.now_date);
                                var end = new Date(data.jatuh_tempo);
                                var day_left = Math.abs(end - start);
                                day_left = Math.ceil(day_left / (1000 * 60 * 60 * 24)); 

                                $('#detail_donasi_3').text(day_left + ' Hari');
                                $('#detail_donasi_4').text(data.penggunaan_dana);
                                //Judul
                                $('#detail_judul_1').text(data.judul);
                                $('#detail_judul_2').text(data.link);
                                $('#detail_judul_3').attr('src', site + 'assets/project/kasus_medis/' + data.sampul);
                                //Cerita
                                $('#detail_cerita').html(data.cerita);
                                //Ajakan
                                $('#detail_ajakan').text(data.ajakan);
                                swal.close();
                                $('#modal_detail').modal('show');
                                display_images();
                                display_imagess();
                                form_act();
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_data').on('click', '#btn_homepage', function(){
            var id_kasus = $(this).attr('data');
            var homepage = $(this).attr('alt');
            var question = '', link_url = '';
            if(homepage == '0'){
                question = 'Set Public ?';
                link_url = site + 'k_kesehatan/public_case';
            }else{
                question = 'Set Privat ?';
                link_url = site + 'k_kesehatan/privat_case';
            }
            swal({
                html                : '<pre>' + question + '</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'No',
                confirmButtonText   : 'Yes'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    data        : {id_kasus : id_kasus},
                                    url         : link_url,
                                    dataType    : "json",
                                    async       : true,
                                    success: function(data){
                                        swal.close();
                                        setTimeout(function(){
                                            table.ajax.reload();
                                        },500);
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
        $('#table_data').on('click', '#btn_delete', function(){
            var id_kasus = $(this).attr('data');
            var img_kasus = $(this).attr('alt');
            swal({
                html                : '<pre>Delete this data ?</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'No',
                confirmButtonText   : 'Yes'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    url         : site + 'k_kesehatan/delete',
                                    data        : {
                                        id_kasus   : id_kasus,
                                        img_kasus  : img_kasus
                                    },
                                    dataType    : "json",
                                    async       : true,
                                    success: function(response){
                                        if(response.success){
                                            swal.close();
                                            setTimeout(function(){
                                                table.ajax.reload();
                                            },500);
                                        }
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
        $('#table_data').on('click', '#btn_verify', function(){
            var id_kasus = $(this).attr('data');
            swal({
                html                : '<pre>Verify Data ?</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'No',
                confirmButtonText   : 'Yes'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    url         : site + 'k_kesehatan/verify',
                                    data        : {
                                        id_kasus   : id_kasus
                                    },
                                    dataType    : "json",
                                    async       : true,
                                    success: function(response){
                                        if(response.success){
                                            swal.close();
                                            setTimeout(function(){
                                                table.ajax.reload();
                                            },500);
                                        }
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
        $('#table_data').on('click', '#btn_terbaru', function(){
            var id_kasus = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'k_kesehatan/kabar_terbaru',
                            data        : {
                                id_kasus   : id_kasus
                            },
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                if(data.empty){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Penggalangan belum memiliki' + '<br>' + 
                                                      'Kabar terbaru</pre>'
                                    });
                                }else{
                                    var modal_content = '';
                                    for(i = 0; i < data.length; i ++){
                                        if(data[i].status != '0'){
                                            modal_content += 
                                            '<div style="border-bottom: 3px solid rgb(255,197,70); margin-bottom: 15px;">' +
                                                '<div class="text_title" style="text-transform: capitalize; font-weight: 600;">' + data[i].judul + '</div>' +
                                                '<div style="font-size: 14px;">' + data[i].created + ' yang lalu</div>' +
                                                '<div>' + data[i].kabar_terbaru + '</div>' +
                                                '<div style="padding-bottom: 5px;">' +
                                                    '<i>Terverifikasi pada ' + data[i].verified + '</i>' +
                                                '</div>' +
                                            '</div>';
                                        }else{
                                            modal_content += 
                                            '<div style="border-bottom: 3px solid rgb(255,197,70); margin-bottom: 15px;">' +
                                                '<div class="text_title" style="text-transform: capitalize; font-weight: 600;">' + data[i].judul + '</div>' +
                                                '<div style="font-size: 14px;">' + data[i].created + ' yang lalu</div>' +
                                                '<div>' + data[i].kabar_terbaru + '</div>' +
                                                '<div style="padding-bottom: 5px;">' +
                                                    '<button type="button" class="btn btn-outline-danger btn_news_delete" data="' + data[i].id_kabar_medis + '">Hapus</button>' +
                                                '</div>' +
                                                '<div style="border-bottom: 3px solid rgb(255,197,70); margin-bottom: 15px; padding-bottom: 5px;">' +
                                                    '<button type="button" class="btn btn-outline-primary btn_news_verif" data="' + data[i].id_kabar_medis + '">Verifikasi</button>' +
                                                '</div>' +
                                            '</div>';
                                        }
                                    }
                                    $('#modal_kabar_terbaru').find('.modal-body').html(modal_content);
                                    swal.close();
                                    $('#modal_kabar_terbaru').modal('show');
                                    kabar_terbaru_action();
                                }
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_data').on('click', '.file_data', function(){
            $('#image_column_items').attr('src', $(this).attr('src'));
            $('#column_gambar_items').modal('show');
        });
    }
    function kabar_terbaru_action(){
        $('.btn_news_delete').on('click', function(){
            var id_kabar_medis = $(this).attr('data');
            swal({
                html                : '<pre>Hapus Kabar Terbaru ?</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'No',
                confirmButtonText   : 'Yes'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    url         : site + 'k_kesehatan/delete_kabar_terbaru',
                                    data        : {
                                        id_kabar_medis   : id_kabar_medis
                                    },
                                    dataType    : "json",
                                    async       : true,
                                    success: function(response){
                                        if(response.success){
                                            swal.close();
                                            setTimeout(function(){
                                                $('#modal_kabar_terbaru').modal('hide');
                                                table.ajax.reload();
                                            },500);
                                        }
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
        $('.btn_news_verif').on('click', function(){
            var id_kabar_medis = $(this).attr('data');
            swal({
                html                : '<pre>Verifikasi Kabar Terbaru ?</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'No',
                confirmButtonText   : 'Yes'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    url         : site + 'k_kesehatan/verifikasi_kabar_terbaru',
                                    data        : {
                                        id_kabar_medis   : id_kabar_medis
                                    },
                                    dataType    : "json",
                                    async       : true,
                                    success: function(response){
                                        if(response.success){
                                            swal.close();
                                            setTimeout(function(){
                                                $('#modal_kabar_terbaru').modal('hide');
                                                table.ajax.reload();
                                            },500);
                                        }
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
    }
    function transfer_act(){
        $('.file_data').on('click', function(){
            $('#image_column_items').attr('src', $(this).attr('src'));
            $('#column_gambar_items').modal('show');
        });
    }
    function form_act(){
        $('.file_data').on('click', function(){
            $('#image_column_items').attr('src', $(this).attr('src'));
            $('#column_gambar_items').modal('show');
        });
        $('#save_dokumen_medis').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var url             = site + 'k_kesehatan/update_dokumen_medis';
            var form_data       = $('#form_dokumen_medis')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#hide_dokumen_medis').val()){
                 errormessage += 'Dokumen medis dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    html                : '<pre>Dokumen medis sudah sesuai ?</pre>',
                    type                : "question",
                    background          : 'transparent',
                    showCancelButton    : true,
                    cancelButtonText    : 'No',
                    confirmButtonText   : 'Yes'
                }).then((result) => {
                    if(result.value){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                setTimeout(function(){
                                    $.ajax({
                                        type        : 'ajax',
                                        method      : 'post',
                                        url         : url,
                                        data        : form_data,
                                        async       : true,
                                        processData : false,
                                        contentType : false,
                                        cache       : false,
                                        dataType    : 'json',
                                        success     : function(response){
                                            if(response.success){
                                                $('#modal_detail').modal('hide');
                                                setTimeout(function(){
                                                    swal.close();
                                                    table.ajax.reload();
                                                },500);
                                            }
                                        },
                                        error: function (){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Connection lost' + '<br>' + 
                                                              'Please try again</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    });
                                },500);
                            }
                        });
                    }
                });
            }
        });
        $('#save_hasil_pemeriksaan').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var url             = site + 'k_kesehatan/update_hasil_pemeriksaan';
            var form_data       = $('#form_hasil_pemeriksaan')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#hide_hasil_pemeriksaan').val()){
                 errormessage += 'Hasil pemeriksaan dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    html                : '<pre>Hasil pemeriksaan sudah sesuai ?</pre>',
                    type                : "question",
                    background          : 'transparent',
                    showCancelButton    : true,
                    cancelButtonText    : 'No',
                    confirmButtonText   : 'Yes'
                }).then((result) => {
                    if(result.value){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                setTimeout(function(){
                                    $.ajax({
                                        type        : 'ajax',
                                        method      : 'post',
                                        url         : url,
                                        data        : form_data,
                                        async       : true,
                                        processData : false,
                                        contentType : false,
                                        cache       : false,
                                        dataType    : 'json',
                                        success     : function(response){
                                            if(response.success){
                                                $('#modal_detail').modal('hide');
                                                setTimeout(function(){
                                                    swal.close();
                                                    table.ajax.reload();
                                                },500);
                                            }
                                        },
                                        error: function (){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Connection lost' + '<br>' + 
                                                              'Please try again</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    });
                                },500);
                            }
                        });
                    }
                });
            }
        });
    }
    function display_images(){
        function preview(image){
            if(image.files[0].size > 3000000){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                                  '3 mb</pre>',
                    type        : "warning"
                });
                $("#dokumen_medis").val('');
            }else{
                if(image.files && image.files[0]){
                    var reader      = new FileReader();
                    reader.onload   = function(event){
                        $('#preview_itemss').attr('src', event.target.result);
                        $('#preview_itemss').attr('title', image.files[0].name);
                        $('#hide_dokumen_medis').val(image.files[0].name);
                        $('#delete_preview_itemss').css('display','block');
                        $('#save_dokumen_medis').show();
                    }
                    reader.readAsDataURL(image.files[0]);
                }
            }
        }
        $("#dokumen_medis").on('change', function(){
            preview(this);
            var names = $(this).val();
            var file_names = names.replace(/^.*\\/, "");
        });
        $('#delete_preview_itemss').on('click', function(){
            $('#delete_preview_itemss').css('display','none');
            $('#preview_itemss').attr('src', '');
            $('#dokumen_medis, #get_dokumen_medis, #hide_dokumen_medis').val('');
        });
    }
    function display_imagess(){
        function preview(image){
            if(image.files[0].size > 3000000){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                                  '3 mb</pre>',
                    type        : "warning"
                });
                $("#hasil_pemeriksaan").val('');
            }else{
                if(image.files && image.files[0]){
                    var reader      = new FileReader();
                    reader.onload   = function(event){
                        $('#preview_itemsss').attr('src', event.target.result);
                        $('#preview_itemsss').attr('title', image.files[0].name);
                        $('#hide_hasil_pemeriksaan').val(image.files[0].name);
                        $('#delete_preview_itemsss').css('display','block');
                        $('#save_hasil_pemeriksaan').show();
                    }
                    reader.readAsDataURL(image.files[0]);
                }
            }
        }
        $("#hasil_pemeriksaan").on('change', function(){
            preview(this);
            var names = $(this).val();
            var file_names = names.replace(/^.*\\/, "");
        });
        $('#delete_preview_itemsss').on('click', function(){
            $('#delete_preview_itemsss').css('display','none');
            $('#preview_itemsss').attr('src', '');
            $('#hasil_pemeriksaan, #get_hasil_pemeriksaan, #hide_hasil_pemeriksaan').val('');
        });
    }
});