$(document).ready(function(){
    $('#description').summernote({
        // popover: {
        //     image: [
        //         ['remove', ['removeMedia']]
        //     ],
        // },
        callbacks: {
            onImageUpload: function(image) {
                editor_upload_img(image[0]);
            },
            onMediaDelete : function(target) {
                editor_delete_img(target[0].src);
            }
        },
        // placeholder: 'Set Placeholder',
        tabsize: 2,
        height: 300,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'italic']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']]
        ]
    });
    function editor_upload_img(image){
        if(image.size > 3000000){
            swal({
                background  : 'transparent',
                html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                              '3 mb</pre>',
                type        : "warning"
            });
        }else{
            var data = new FormData();
            data.append("image", image);
            $.ajax({
                url: site + 'b_program/editor_upload_img',
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: "POST",
                success: function(url) {
                    $('#description').summernote("insertImage", url);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        }
    }
    function editor_delete_img(src) {
        $.ajax({
            data: {src : src},
            type: "POST",
            url: site + 'b_program/editor_delete_img',
            cache: false,
            success: function(response) {
                console.log(response);
            }
        });
    }
    var nama_bulan = new Array();
    nama_bulan[1] = 'January';
    nama_bulan[2] = 'February';
    nama_bulan[3] = 'March';
    nama_bulan[4] = 'April';
    nama_bulan[5] = 'May';
    nama_bulan[6] = 'June';
    nama_bulan[7] = 'July';
    nama_bulan[8] = 'August'; 
    nama_bulan[9] = 'September'; 
    nama_bulan[10] = 'October';
    nama_bulan[11] = 'November'; 
    nama_bulan[12] = 'December';
    var table;
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                s_side_data_table();
            },500);
        }
    });
    function s_side_data_table(){
        table = $('#table_data').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            lengthMenu          : [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, 'Semua']],
            initComplete: function(){
                swal.close();
            },
            ajax            : {
                url         : site + 'b_program/s_side_data',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
    main();
    function main(){
        $('#title').on('keyup', function(){
            var chars = /^[a-zA-Z0-9\s]+$/gm;
            if(! chars.test($(this).val())) {
                 $(this).val('');
            }
        });
        var modal_form;
        $('#modal_form').on('show.bs.modal', function(){
            $(this).removeAttr('tabindex');
            $(this).addClass('zoomIn');
            modal_form = true;
        });
        $('#modal_form').on('hide.bs.modal', function(){
            if(modal_form){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_form = false;
                setTimeout(function(){
                    $('#modal_form').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var column_gambar_items
        $('#column_gambar_items').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            column_gambar_items = true;
        });
        $('#column_gambar_items').on('hide.bs.modal', function(){
            if(column_gambar_items){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                column_gambar_items = false;
                setTimeout(function(){
                    $('#column_gambar_items').modal('hide');
                },500);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var modal_description;
        $('#modal_description').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_description = true;
        });
        $('#modal_description').on('hide.bs.modal', function(){
            if(modal_description){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_description = false;
                setTimeout(function(){
                    $('#modal_description').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#table_data').on('click', '#btn_activated', function(){
            var id_agenda = $(this).attr('data');
            var status = $(this).attr('alt');
            var question = '', link_url = '';
            if(status == '0'){
                question = 'Set Public ?';
                link_url = site + 'b_program/aktifkan';
            }else{
                question = 'Set Private ?';
                link_url = site + 'b_program/non_aktifkan';
            }
            swal({
                html                : '<pre>' + question + '</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'No',
                confirmButtonText   : 'Yes'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    data        : {id_agenda : id_agenda},
                                    url         : link_url,
                                    dataType    : "json",
                                    async       : true,
                                    success: function(data){
                                        swal.close();
                                        setTimeout(function(){
                                            table.ajax.reload();
                                        },500);
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
        $('#btn_add').on('click', function(){
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $('#form_data')[0].reset();
                        $('#modal_form').find('.modal-title').text('Add');
                        $('#form_data').attr('action', site + 'b_program/save');
                        $('#btn_process').text('Save');
                        $('#delete_preview_items').css('display','none');
                        $('#preview_items').attr('src', '');
                        $('#hide_program').val('');
                        $('#description').summernote('code', '');
                        display_image();
                        swal.close();
                        $('#modal_form').modal('show');
                    },500);
                }
            });
        });
        $('#table_data').on('click', '#btn_description', function(){
            var id_agenda = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'b_program/edit',
                            data        : {id_agenda : id_agenda},
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                $('#modal_description').find('.modal-title').text(data.title + ' Description');
                                $('#modal_description').find('.modal-body').html(data.description);
                                swal.close();
                                $('#modal_description').modal('show');
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_data').on('click', '#btn_edit', function(){
            var id_agenda = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'b_program/edit',
                            data        : {id_agenda : id_agenda},
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                $('#form_data')[0].reset();
                                $('#id_data').val(data.id_agenda);
                                $('#title').val(data.title);
                                $('#get_program, #delete_program').val(data.image);
                                if(data.image != ''){
                                    $('#preview_items').attr('src', site + 'assets/project/program/' + data.image);
                                }else{
                                    $('#preview_items').attr('src', site + 'assets/project/img.jpg');
                                }
                                $('#description').summernote('code', data.description);
                                $('#hide_program').val(data.image);
                                $('#delete_preview_items').css('display','block');
                                $('#modal_form').find('.modal-title').text('Edit');
                                $('#form_data').attr('action', site + 'b_program/update');
                                $('#btn_process').text('Update');
                                display_image();
                                swal.close();
                                $('#modal_form').modal('show');
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_data').on('click', '#btn_delete', function(){
            var id_agenda = $(this).attr('data');
            var img_agenda = $(this).attr('alt');
            swal({
                html                : '<pre>Delete this data ?</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'No',
                confirmButtonText   : 'Yes'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    url         : site + 'b_program/delete',
                                    data        : {
                                        id_agenda   : id_agenda,
                                        img_agenda  : img_agenda
                                    },
                                    dataType    : "json",
                                    async       : true,
                                    success: function(response){
                                        if(response.success){
                                            swal.close();
                                            setTimeout(function(){
                                                table.ajax.reload();
                                            },500);
                                        }
                                        if(response.last){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Program cannot be empty</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
        $('#btn_process').on('click', function(event){
            $('#description').val($('#description').summernote('code'));
            event.preventDefault();
            event.stopImmediatePropagation();
            var url             = $('#form_data').attr('action');
            var form_data       = $('#form_data')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#title').val()){
                 errormessage += 'Title required \n';
            }
            if(! $('#description').val()){
                 errormessage += 'Description required \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type        : 'ajax',
                                method      : 'post',
                                url         : url,
                                data        : form_data,
                                async       : true,
                                processData : false,
                                contentType : false,
                                cache       : false,
                                dataType    : 'json',
                                success     : function(response){
                                    if(response.success){
                                        $('#modal_form').modal('hide');
                                        setTimeout(function(){
                                            swal.close();
                                            table.ajax.reload();
                                        },500);
                                    }
                                },
                                error: function (){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
        $('#table_data').on('click', '.file_data', function(){
            $('#image_column_items').attr('src', $(this).attr('src'));
            $('#column_gambar_items').modal('show');
        });
    }
    function display_image(){
        function preview(image){
            if(image.files[0].size > 3000000){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                                  '3 mb</pre>',
                    type        : "warning"
                });
                $("#program").val('');
            }else{
                if(image.files && image.files[0]){
                    var reader      = new FileReader();
                    reader.onload   = function(event){
                        $('#preview_items').attr('src', event.target.result);
                        $('#preview_items').attr('title', image.files[0].name);
                        $('#hide_program').val(image.files[0].name);
                        $('#delete_preview_items').css('display','block');
                    }
                    reader.readAsDataURL(image.files[0]);
                }
            }
        }
        $("#program").on('change', function(){
            preview(this);
            var names = $(this).val();
            var file_names = names.replace(/^.*\\/, "");
        });
        $('#delete_preview_items').on('click', function(){
            $('#delete_preview_items').css('display','none');
            $('#preview_items').attr('src', '');
            $('#program, #get_program, #hide_program').val('');
        });
    }
});