$(document).ready(function(){
    password_update();
    function password_update(){
        $('#btn_process').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var email_reg;
            var email = /\@+[a-zA-Z0-9]+\.+[a-zA-Z0-9]/m;
            var errormessage = '';
            if(! $('#recovery_email').val()){
                errormessage += 'Email dibutuhkan \n';
            }
            if(! $('#recovery_password').val()){
                errormessage += 'Password dibutuhkan \n';
            }
            if(! $('#confirm_recovery_password').val()){
                errormessage += 'Konfirmasi password dibutuhkan \n';
            }else{
                if($('#confirm_recovery_password').val() !== $('#recovery_password').val()){
                    errormessage += 'Konfirmasi password tidak cocok \n';
                }
            }
            if(errormessage !== ''){
                swal({
                    html          : '<pre>' + errormessage + '</pre>',
                    background  : 'transparent'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type           : 'ajax',
                                method         : 'post',
                                url            : site + 'recovery/password_recovery',
                                data           : $('form').serialize(),
                                async          : true,
                                dataType       : 'json',
                                success        : function(response){
                                    if(response.success){
                                        swal({
                                            html                : '<pre>Password berhasil diperbaharui' + '<br>' +
                                                                  'Silahkan login</pre>',
                                            type                : "success",
                                            background          : 'transparent',
                                        }).then(function(){
                                            location.href = site;
                                        });
                                    }
                                },
                                error     : function(){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });    
            }
        });
    }
});