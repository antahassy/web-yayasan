$(document).ready(function(){
    var nama_bulan = new Array();
    nama_bulan[1] = 'January';
    nama_bulan[2] = 'February';
    nama_bulan[3] = 'March';
    nama_bulan[4] = 'April';
    nama_bulan[5] = 'May';
    nama_bulan[6] = 'June';
    nama_bulan[7] = 'July';
    nama_bulan[8] = 'August'; 
    nama_bulan[9] = 'September'; 
    nama_bulan[10] = 'October';
    nama_bulan[11] = 'November'; 
    nama_bulan[12] = 'December';
    var table;
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                s_side_data_table();
            },500);
        }
    });
    function s_side_data_table(){
        table = $('#table_data').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            lengthMenu          : [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, 'Semua']],
            initComplete: function(){
                swal.close();
            },
            ajax            : {
                url         : site + 'f_slideshow/s_side_data',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
    main();
    function main(){
        var modal_form;
        $('#modal_form').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_form = true;
        });
        $('#modal_form').on('hide.bs.modal', function(){
            if(modal_form){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_form = false;
                setTimeout(function(){
                    $('#modal_form').modal('hide');
                },350);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var column_gambar_items
        $('#column_gambar_items').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            column_gambar_items = true;
        });
        $('#column_gambar_items').on('hide.bs.modal', function(){
            if(column_gambar_items){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                column_gambar_items = false;
                setTimeout(function(){
                    $('#column_gambar_items').modal('hide');
                },500);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#btn_add').on('click', function(){
            $('#form_data')[0].reset();
            $('#modal_form').find('.modal-title').text('Add');
            $('#form_data').attr('action', site + 'f_slideshow/save');
            $('#btn_process').text('Save');
            $('#delete_preview_items').css('display','none');
            $('#preview_items').attr('src', '');
            $('#hide_slideshow').val('');
            swal.close();
            display_image();
            $('#modal_form').modal('show');
        });
        $('#table_data').on('click', '#btn_edit', function(){
            var id_slideshow = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            url         : site + 'f_slideshow/edit',
                            data        : {id_slideshow : id_slideshow},
                            dataType    : "json",
                            async       : true,
                            success: function(data){
                                $('#form_data')[0].reset();
                                $('#id_data').val(data.id_slideshow);
                                $('#get_slideshow, #delete_slideshow').val(data.image);
                                if(data.image != ''){
                                    $('#preview_items').attr('src', site + 'assets/project/slideshow/' + data.image);
                                }else{
                                    $('#preview_items').attr('src', site + 'assets/project/img.jpg');
                                }
                                $('#hide_slideshow').val(data.image);
                                $('#delete_preview_items').css('display','block');
                                $('#modal_form').find('.modal-title').text('Edit');
                                $('#form_data').attr('action', site + 'f_slideshow/update');
                                $('#btn_process').text('Update');
                                swal.close();
                                display_image();
                                $('#modal_form').modal('show');
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_data').on('click', '#btn_delete', function(){
            var id_slideshow = $(this).attr('data');
            var img_slideshow = $(this).attr('alt');
            swal({
                html                : '<pre>Delete this slideshow ?</pre>',
                type                : "question",
                background          : 'transparent',
                showCancelButton    : true,
                cancelButtonText    : 'No',
                confirmButtonText   : 'Yes'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    url         : site + 'f_slideshow/delete',
                                    data        : {
                                        id_slideshow   : id_slideshow,
                                        img_slideshow  : img_slideshow
                                    },
                                    dataType    : "json",
                                    async       : true,
                                    success: function(response){
                                        if(response.success){
                                            swal.close();
                                            setTimeout(function(){
                                                table.ajax.reload();
                                            },500);
                                        }
                                        if(response.last){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Image cannot be empty</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    },
                                    error: function (){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Connection lost' + '<br>' + 
                                                          'Please try again</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
        $('#btn_process').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var url             = $('#form_data').attr('action');
            var form_data       = $('#form_data')[0];
            form_data           = new FormData(form_data);
            var errormessage    = '';
            if(! $('#hide_slideshow').val()){
                 errormessage += 'Image required \n';
            }
            if(errormessage !== ''){
                swal({
                    background  : 'transparent',
                    html        : '<pre>' + errormessage + '</pre>'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type        : 'ajax',
                                method      : 'post',
                                url         : url,
                                data        : form_data,
                                async       : true,
                                processData : false,
                                contentType : false,
                                cache       : false,
                                dataType    : 'json',
                                success     : function(response){
                                    if(response.success){
                                        $('#modal_form').modal('hide');
                                        setTimeout(function(){
                                            swal.close();
                                            table.ajax.reload();
                                        },500);
                                    }
                                    if(response.slideshow){
                                        swal({
                                            html        : '<pre>Image required</pre>',
                                            background  : 'transparent'
                                        });
                                    }
                                },
                                error: function (){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });
            }
        });
        $('#table_data').on('click', '.file_data', function(){
            $('#image_column_items').attr('src', $(this).attr('src'));
            $('#column_gambar_items').modal('show');
        });
    }
    function display_image(){
        function preview(image){
            if(image.files[0].size > 3000000){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Ukuran gambar maksimal' + '<br>' + 
                                  '3 mb</pre>',
                    type        : "warning"
                });
                $("#slideshow").val('');
            }else{
                if(image.files && image.files[0]){
                    var reader      = new FileReader();
                    reader.onload   = function(event){
                        $('#preview_items').attr('src', event.target.result);
                        $('#preview_items').attr('title', image.files[0].name);
                        $('#hide_slideshow').val(image.files[0].name);
                        $('#delete_preview_items').css('display','block');
                    }
                    reader.readAsDataURL(image.files[0]);
                }
            }
        }
        $("#slideshow").on('change', function(){
            preview(this);
            var names = $(this).val();
            var file_names = names.replace(/^.*\\/, "");
        });
        $('#delete_preview_items').on('click', function(){
            $('#delete_preview_items').css('display','none');
            $('#preview_items').attr('src', '');
            $('#slideshow, #get_slideshow, #hide_slideshow').val('');
        });
    }
});