var sum = (index, value) => index + value;
function unique(list) {
    var result = [];
    $.each(list, function(i, e) {
        if($.inArray(e, result) == -1){
            result.push(e);
        }
    });
    return result;
}
var map = L.map('map', {
    center              : [-2, 118],
    zoom                : 5,
    doubleClickZoom     : false,
    minZoom             : 5
});
 //Map style
var map_style = {
    color           : 'rgba(0,0,0,0.15)',
    weight          : 1,
    fillOpacity     : 1,
}
$(document).ready(function(){
    request_count = 0;
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                list_provinsi();
            },500);
        }
    });
    function list_provinsi(){
        $.ajax({
            type        : 'ajax',
            method      : 'get',
            url         : site + 'koordinat/list_provinsi',
            dataType    : "json",
            async       : true,
            success: function(data){
                var s_provinsi = '<option value="">Pilih Provinsi</option>';
                for(i = 0; i < data.length; i ++){
                    s_provinsi += '<option value="' + data[i].id_provinsi + '" data="' + data[i].lat_provinsi + '_' + data[i].lng_provinsi + '" title="' + data[i].nama_provinsi + '">' + data[i].nama_provinsi + '</option>';
                }
                $('#s_provinsi').html(s_provinsi);
                swal.close();
                var area_val = '';
                var area_koordinat = '_';
                map_data(area_val, area_koordinat);
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
    }
    function list_kota(provinsi_val){
        $.ajax({
            type        : 'ajax',
            method      : 'post',
            data        : {id_provinsi : provinsi_val},
            url         : site + 'koordinat/list_kota',
            dataType    : "json",
            async       : true,
            success: function(data){
                var s_kota = '<option value="">Pilih Kota/Kabupaten</option>';
                for(i = 0; i < data.length; i ++){
                    s_kota += '<option value="' + data[i].id_kota + '" data="' + data[i].lat_kota + '_' + data[i].lng_kota + '" title="' + data[i].nama_kota + '">' + data[i].nama_kota + '</option>';
                }
                $('#s_kota').html(s_kota);
                request_count ++;
                var area_val = $('#s_provinsi').find("option:selected").attr('title');
                var area_koordinat = $('#s_provinsi').find("option:selected").attr('data');
                map_data(area_val, area_koordinat);
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
    }
    function list_kecamatan(kota_val){
        $.ajax({
            type        : 'ajax',
            method      : 'post',
            data        : {id_kota : kota_val},
            url         : site + 'koordinat/list_kecamatan',
            dataType    : "json",
            async       : true,
            success: function(data){
                var s_kecamatan = '<option value="">Pilih Kecamatan</option>';
                for(i = 0; i < data.length; i ++){
                    s_kecamatan += '<option value="' + data[i].id_kecamatan + '" data="' + data[i].lat_kecamatan + '_' + data[i].lng_kecamatan + '" title="' + data[i].nama_kecamatan + '">' + data[i].nama_kecamatan + '</option>';
                }
                $('#s_kecamatan').html(s_kecamatan);
                request_count ++;
                var area_val = $('#s_kota').find("option:selected").attr('title') + ', ' + $('#s_provinsi').find("option:selected").attr('title');
                var area_koordinat = $('#s_kota').find("option:selected").attr('data');
                map_data(area_val, area_koordinat);
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
    }
    function list_kelurahan(kecamatan_val){
        $.ajax({
            type        : 'ajax',
            method      : 'post',
            data        : {id_kecamatan : kecamatan_val},
            url         : site + 'koordinat/list_kelurahan',
            dataType    : "json",
            async       : true,
            success: function(data){
                var s_kelurahan = '<option value="">Pilih Desa/Kelurahan</option>';
                for(i = 0; i < data.length; i ++){
                    s_kelurahan += '<option value="' + data[i].id_kelurahan + '" data="' + data[i].lat_kelurahan + '_' + data[i].lng_kelurahan + '" title="' + data[i].nama_kelurahan + '">' + data[i].nama_kelurahan + '</option>';
                }
                $('#s_kelurahan').html(s_kelurahan);
                request_count ++;
                var area_val = $('#s_kecamatan').find("option:selected").attr('title') + ', ' + $('#s_kota').find("option:selected").attr('title') + ', ' + $('#s_provinsi').find("option:selected").attr('title');
                var area_koordinat = $('#s_kecamatan').find("option:selected").attr('data');
                map_data(area_val, area_koordinat);
            },
            error: function (){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Connection lost' + '<br>' + 
                                  'Please try again</pre>',
                    type        : "warning"
                });
            }
        });
    }
    main();
    function main(){
        $('#s_provinsi').on('change', function(){
            var provinsi_val = $(this).val();
            if(provinsi_val != ''){
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            list_kota(provinsi_val);
                        },500);
                    }
                });
            }
        });
        $('#s_kota').on('change', function(){
            var kota_val = $(this).val();
            if(kota_val != ''){
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            list_kecamatan(kota_val);
                        },500);
                    }
                });
            }
        });
        $('#s_kecamatan').on('change', function(){
            var kecamatan_val = $(this).val();
            if(kecamatan_val != ''){
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            list_kelurahan(kecamatan_val);
                        },500);
                    }
                });
            }
        });
        $('#s_kelurahan').on('change', function(){
            var kelurahan_val = $(this).val();
            if(kelurahan_val != ''){
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            request_count ++;
                            var area_val = $('#s_kelurahan').find("option:selected").attr('title') + ', ' + $('#s_kecamatan').find("option:selected").attr('title') + ', ' + $('#s_kota').find("option:selected").attr('title') + ', ' + $('#s_provinsi').find("option:selected").attr('title');
                            var area_koordinat = $('#s_kelurahan').find("option:selected").attr('data');
                            map_data(area_val, area_koordinat);
                        },500);
                    }
                });
            }
        });
    }
    new_map = '', pin = '', map_search = ''; 
    var layerGroup = new L.layerGroup().addTo(map);
    function map_data(area_val, area_koordinat){
        if(area_koordinat == '_' && request_count > 0){
            swal({
                background  : 'transparent',
                html        : '<pre>Koordinat belum tersedia</pre>'
            }).then(function(){
                map.setView(new L.LatLng(-2, 118), 5);
                if(pin != ''){
                    map.removeLayer(pin);
                }
            });
            return;
        }
        layerGroup.clearLayers();
        var custom_icon = L.icon({
            iconUrl        : site + 'assets/map/css/images/red_marker.png',
            iconSize       : [25, 40],
            iconAnchor     : [14, 35],
            popupAnchor    : [-1, -30]
        });
        L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png').addTo(map);
        new_map = new L.GeoJSON.AJAX([site + 'assets/map/provinsi_indonesia.geojson'],{
            onEachFeature  : layer_control,
            style          : map_style
        }).addTo(map);

        if(map_search == ''){
            map_search = L.esri.Geocoding.geosearch({
                position        : 'topright',
                placeholder     : 'Cari Kota, Kecamatan, Kelurahan',
                useMapBounds    : false,
                providers       : [L.esri.Geocoding.arcgisOnlineProvider({
                    apikey      : 'AAPK742192433a95482899311445d5fcc9a6oPaR1oFT3ZK56MvUcoHXcR3t6nESnoD9iCL7mBBBgJP1OTMnRu8ddSIn050xW5MI',
                    nearby      : {
                        lat: -2,
                        lng: 118
                    }
                })]
            }).addTo(map);
        }
	  	map_search.on('results', function (data) {
	    	layerGroup.clearLayers();
            if(pin){
                map.removeLayer(pin);
            }
            var keterangan = '';
            var data_area = '';
            var id_area = '';
            if($('#s_kelurahan').val() != ''){
                if(keterangan != ''){
                    keterangan += ', '; 
                }
                keterangan += $('#s_kelurahan').find("option:selected").attr('title');
                id_area = $('#s_kelurahan').val();
                data_area = 'kelurahan';
            }else if($('#s_kecamatan').val() != ''){
                if(keterangan != ''){
                    keterangan += ', '; 
                }
                keterangan += $('#s_kecamatan').find("option:selected").attr('title');
                id_area = $('#s_kecamatan').val();
                data_area = 'kecamatan';
            }else if($('#s_kota').val() != ''){
                if(keterangan != ''){
                    keterangan += ', '; 
                }
                keterangan += $('#s_kota').find("option:selected").attr('title');
                id_area = $('#s_kota').val();
                data_area = 'kota';
            }else{
                if(keterangan != ''){
                    keterangan += ', '; 
                }
                keterangan += $('#s_provinsi').find("option:selected").attr('title');
                id_area = $('#s_provinsi').val();
                data_area = 'provinsi';
            }
	    	for (var i = data.results.length - 1; i >= 0; i--) { 
                pin = new L.Marker([data.results[i].latlng.lat, data.results[i].latlng.lng], {icon: custom_icon}).bindPopup(keterangan + '<br>' + data.results[i].latlng.lat + ', ' + data.results[i].latlng.lng + '<br>' + '<div class="koordinat_update" data="' + data_area + '" id="' + id_area + '" alt="' + data.results[i].latlng.lat + '_' + data.results[i].latlng.lng + '" style="color: rgb(255,197,70); cursor: pointer; font-weight: 600; text-align: center;">Update</div>');
	    	    pin.addTo(map).openPopup();
            }
            update_coordinate();
	  	});

        if(area_koordinat != '_'){
            if(pin != ''){
                map.removeLayer(pin);
            }
            if(new_map != ''){
                map.removeLayer(new_map);
            }
            map.setView(new L.LatLng(Number(area_koordinat.split('_')[0]), Number(area_koordinat.split('_')[1])));
            pin = new L.marker([Number(area_koordinat.split('_')[0]), Number(area_koordinat.split('_')[1])], {icon: custom_icon}).bindPopup(area_val + '<br>' + Number(area_koordinat.split('_')[0]) + ', ' + Number(area_koordinat.split('_')[1]));
            pin.addTo(map).openPopup();
            swal.close();
        }
        function layer_control(feature,layer){
            if(pin != ''){
                pin.on('mouseover', function () {
                    layer.setStyle({ 
                        color           : 'transparent',
                        weight          : 3,
                        fillOpacity     : 1,
                    });
                });
            }
            layer.on('mouseover', function () {
                layer.setStyle({
                    color           : 'transparent',
                    weight          : 3,
                    fillOpacity     : 1,
                });
            });
            layer.on('mouseout', function () {
                layer.setStyle({
                    color           : 'rgba(0,0,0,0.15)',
                    weight          : 3,
                    fillOpacity     : 1,
                });
            });
            layer.on('click', function(event) {
                map.setView(new L.LatLng(event.latlng.lat, event.latlng.lng)); 
                if(pin){
                    map.removeLayer(pin);
                }
                var keterangan = '';
                var data_area = '';
                var id_area = '';
                if($('#s_kelurahan').val() != ''){
                    if(keterangan != ''){
                        keterangan += ', '; 
                    }
                    keterangan += $('#s_kelurahan').find("option:selected").attr('title');
                    id_area = $('#s_kelurahan').val();
                    data_area = 'kelurahan';
                }else if($('#s_kecamatan').val() != ''){
                    if(keterangan != ''){
                        keterangan += ', '; 
                    }
                    keterangan += $('#s_kecamatan').find("option:selected").attr('title');
                    id_area = $('#s_kecamatan').val();
                    data_area = 'kecamatan';
                }else if($('#s_kota').val() != ''){
                    if(keterangan != ''){
                        keterangan += ', '; 
                    }
                    keterangan += $('#s_kota').find("option:selected").attr('title');
                    id_area = $('#s_kota').val();
                    data_area = 'kota';
                }else{
                    if(keterangan != ''){
                        keterangan += ', '; 
                    }
                    keterangan += $('#s_provinsi').find("option:selected").attr('title');
                    id_area = $('#s_provinsi').val();
                    data_area = 'provinsi';
                }
                if(keterangan == ''){
                    pin = new L.Marker([event.latlng.lat, event.latlng.lng], {icon: custom_icon}).bindPopup(event.latlng.lat + ', ' + event.latlng.lng);
                }else{
                    pin = new L.Marker([event.latlng.lat, event.latlng.lng], {icon: custom_icon}).bindPopup(keterangan + '<br>' + event.latlng.lat + ', ' + event.latlng.lng + '<br>' + '<div class="koordinat_update" data="' + data_area + '" id="' + id_area + '" alt="' + event.latlng.lat + '_' + event.latlng.lng + '" style="color: rgb(255,197,70); cursor: pointer; font-weight: 600; text-align: center;">Update</div>');
                }
                pin.addTo(map).openPopup();
                update_coordinate();
            });
        }
    }
    function update_coordinate(){
        $('.koordinat_update').on('click', function(){
            var data = $(this).attr('data');
            var id = $(this).attr('id');
            var lat = $(this).attr('alt').split('_')[0];
            var lng = $(this).attr('alt').split('_')[1];
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type        : 'ajax',
                            method      : 'post',
                            data        : {
                                id      : id,
                                data    : data,
                                lat     : lat,
                                lng     : lng
                            },
                            url         : site + 'koordinat/update_koordinat',
                            dataType    : "json",
                            async       : true,
                            success: function(response){
                                if(response.success){
                                    swal.close();
                                    map.setView(new L.LatLng(-2, 118), 5);
                                    map.removeLayer(pin);
                                }
                            },
                            error: function (){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Connection lost' + '<br>' + 
                                                  'Please try again</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        })
    }
});