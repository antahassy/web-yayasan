<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_model extends CI_Model {

   public function ref_agama()
   {
      $arr = $this->db->get('ref_agama');
      return $arr;
   }

   public function ref_setting()
   {
      $arr = $this->db->get('setting');
      return $arr;
   }

   public function ref_golongan($kategori_pegawai)
   {
      $where = array('kategori' => $kategori_pegawai);
      $this->db->where($where);
      $arr = $this->db->get('ref_golongan');
      return $arr;
   }

   public function ref_kepakaran()
   {
      $this->db->where('is_trash', 0);
      $arr = $this->db->get('ref_kepakaran');
      return $arr;
   }

   public function ref_unit_kerja()
   {
      $this->db->where('is_trash', 0);
      $arr = $this->db->get('ref_unit_kerja');
      return $arr;
   }

   public function ref_pendidikan()
   {
      $arr = $this->db->get('ref_pendidikan');
      return $arr;
   }

   public function get_pegawai_by_id($id_pegawai)
   {
      $select = array('pegawai.nama','pegawai.gelar_depan','pegawai.gelar_belakang','pegawai.jenis_kelamin','pegawai.tempat_lahir','pegawai.kd_agama','pegawai.status_perkawinan','pegawai.alamat','pegawai.kd_kepakaran','pegawai.pendidikan_terakhir','pegawai.foto','pegawai.status_pegawai','ref_agama.nama_agama','ref_kepakaran.nama as kepakaran','ref_pendidikan.nama_pendidikan','pegawai.tgl_lahir','ref_unit_kerja.nama as unit_kerja');

      $where = array('id_pegawai' => $id_pegawai);
      $this->db->select($select);
      $this->db->join('ref_agama','ref_agama.id_ref_agama = pegawai.kd_agama','left');
      $this->db->join('ref_kepakaran','ref_kepakaran.id_kepakaran = pegawai.kd_kepakaran','left');
      $this->db->join('ref_pendidikan','ref_pendidikan.id_pendidikan = pegawai.pendidikan_terakhir','left');
      $this->db->join('ref_unit_kerja','ref_unit_kerja.id_unit_kerja = pegawai.kd_unit_kerja','left');
      $kueri = $this->db->get_where('pegawai', $where);
      return $kueri->row();
   }

   public function itung_pegawai($status_pegawai)
   {
      $where = array('status_pegawai' => $status_pegawai, 'is_trash' => 0);
      $this->db->where($where);
      $data = $this->db->get('pegawai');
      return $data->num_rows();
   }
}

/* End of file Ref_model.php */
/* Location: ./application/models/Ref_model.php */
