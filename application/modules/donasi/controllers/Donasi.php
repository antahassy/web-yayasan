<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Donasi extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Donasi_model', 'model');
        require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
        date_default_timezone_set('Asia/Jakarta');
    }
    
    // public $mail_host = 'kaizenpeduli.com';
    // public $mail_username = 'noreply@kaizenpeduli.com';
    public $mail_host = 'peduliindonesia.org';
    public $mail_username = 'noreply@peduliindonesia.org';
    public $mail_password = '#5(9~-)ZV6j~';
    public $mail_dns = '103.147.154.41';

    public function medis($link){
        $data_medis = $this->model->data_medis($link);
        if(! $data_medis){
            redirect(site_url());
        }else{
            $month_format = array (1 => 
                'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );

            if($data_medis->kategori == '1'){
                $title = 'Anak Sakit';
            }else{
                $title = 'Kesehatan';
            }
            $data_medis->sampul = site_url('assets/project/kasus_medis/' . $data_medis->sampul);
            if($data_medis->image != ''){
                $data_medis->image = site_url('assets/project/user/image/' . $data_medis->image);
            }else{
                $data_medis->image = site_url('assets/project/blank.png');
            }

            $s_tempo = explode('-', $data_medis->jatuh_tempo);
            $data_medis->batas_akhir = $s_tempo[2] .'/'. $month_format[(int)$s_tempo[1]] .'/'. $s_tempo[0];

            $tgl1 = strtotime(date('Y-m-d')); 
            $tgl2 = strtotime($data_medis->jatuh_tempo); 
            $selisih = $tgl2 - $tgl1;
            $selisih = $selisih / 60 / 60 / 24;
            $data_medis->jatuh_tempo = number_format($selisih , 0, ',', '.');

            if($data_medis->rekening == '1'){
                $data_medis->rekening = $data_medis->nm_pasien;
            }
            if($data_medis->rekening == '2'){
                $data_medis->rekening = 'Keluarga Pasien';
            }
            if($data_medis->rekening == '3'){
                $data_medis->rekening = 'Keluarga Pasien';
            }
            if($data_medis->rekening == '4'){
                $data_medis->rekening = 'Rumah Sakit';
            }

            $total_donasi = $this->model->total_donasi_medis($data_medis->id_kasus_medis);
            if($total_donasi->donasi == null){
                $data_medis->bantuan = '0';
            }else{
                $data_medis->bantuan = number_format($total_donasi->donasi , 0, ',', '.');
            }

            $donatur = $this->model->total_donatur_medis($data_medis->id_kasus_medis);
            $data_medis->donatur = number_format($donatur , 0, ',', '.');
            
            $terbaru = $this->model->terbaru_medis($data_medis->id_kasus_medis);
            if($terbaru){
                $data['terbaru'] = $terbaru;
            }else{
                $data['terbaru'] = '';
            }
            $doa = $this->model->doa_medis($data_medis->id_kasus_medis);
            if($doa){
                for($i = 0; $i < count($doa); $i ++){
                    if($doa[$i]->hide_name == '1'){
                        $doa[$i]->nama = 'Orang Baik';
                        $doa[$i]->image = site_url('assets/project/blank.png');
                    }else{
                        if($doa[$i]->nama == ''){
                            $doa[$i]->nama = $doa[$i]->donatur;
                            $doa[$i]->image = site_url('assets/project/blank.png');
                        }else{
                            if($doa[$i]->image == ''){
                                $doa[$i]->image = site_url('assets/project/blank.png');
                            }else{
                                $doa[$i]->image = site_url('assets/project/user/image/' . $doa[$i]->image);
                            }
                        }
                    }
                    $tgl1 = strtotime(date('Y-m-d H:i:s')); 
                    $tgl2 = strtotime($doa[$i]->created_at); 
                    $detik = $tgl1 - $tgl2;
                    $last_created = number_format($detik , 0, ',', '.') . ' detik';
                    if((int)$detik > 60){
                        $menit = $detik / 60;//menit
                        $last_created = number_format($menit , 0, ',', '.') . ' menit';
                        if((int)$menit > 60){
                            $jam = $menit / 60;//jam
                            $last_created = number_format($jam , 0, ',', '.') . ' jam';
                            if((int)$jam > 24){
                                $hari = $jam / 24;//hari
                                $last_created = number_format($hari , 0, ',', '.') . ' hari';
                            }
                        }
                    }
                    $doa[$i]->createds = $last_created;
                }
                $data['doa'] = $doa;
            }else{
                $data['doa'] = '';
            }

            $data['title'] = $data_medis->judul;
            $data['dd'] = $data_medis;
            $data['slideshow'] = $this->model->all_slideshow();
            $this->load->view('header_front', $data);
            $this->load->view('medis');
            $this->load->view('footer_front');
        }
    }

    public function non_medis($link){
        $data_non_medis = $this->model->data_non_medis($link);
        if(! $data_non_medis){
            redirect(site_url());
        }else{
            $month_format = array (1 => 
                'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );

            if($data_non_medis->kategori == '1'){
                $title = 'Kemanusiaan';
            }else{
                $title = 'Bencana';
            }
            $data_non_medis->sampul = site_url('assets/project/kasus_non_medis/' . $data_non_medis->sampul);
            if($data_non_medis->image != ''){
                $data_non_medis->image = site_url('assets/project/user/image/' . $data_non_medis->image);
            }else{
                $data_non_medis->image = site_url('assets/project/blank.png');
            }

            $s_tempo = explode('-', $data_non_medis->jatuh_tempo);
            $data_non_medis->batas_akhir = $s_tempo[2] .'/'. $month_format[(int)$s_tempo[1]] .'/'. $s_tempo[0];

            $tgl1 = strtotime(date('Y-m-d')); 
            $tgl2 = strtotime($data_non_medis->jatuh_tempo); 
            $selisih = $tgl2 - $tgl1;
            $selisih = $selisih / 60 / 60 / 24;
            $data_non_medis->jatuh_tempo = number_format($selisih , 0, ',', '.');

            $total_donasi = $this->model->total_donasi_non_medis($data_non_medis->id_kasus_non_medis);
            if($total_donasi->donasi == null){
                $data_non_medis->bantuan = '0';
            }else{
                $data_non_medis->bantuan = number_format($total_donasi->donasi , 0, ',', '.');
            }

            $donatur = $this->model->total_donatur_non_medis($data_non_medis->id_kasus_non_medis);
            $data_non_medis->donatur = number_format($donatur , 0, ',', '.');
            
            $terbaru = $this->model->terbaru_non_medis($data_non_medis->id_kasus_non_medis);
            if($terbaru){
                $data['terbaru'] = $terbaru;
            }else{
                $data['terbaru'] = '';
            }
            $doa = $this->model->doa_non_medis($data_non_medis->id_kasus_non_medis);
            if($doa){
                for($i = 0; $i < count($doa); $i ++){
                    if($doa[$i]->hide_name == '1'){
                        $doa[$i]->nama = 'Orang Baik';
                        $doa[$i]->image = site_url('assets/project/blank.png');
                    }else{
                        if($doa[$i]->nama == ''){
                            $doa[$i]->nama = $doa[$i]->donatur;
                            $doa[$i]->image = site_url('assets/project/blank.png');
                        }else{
                            if($doa[$i]->image == ''){
                                $doa[$i]->image = site_url('assets/project/blank.png');
                            }else{
                                $doa[$i]->image = site_url('assets/project/user/image/' . $doa[$i]->image);
                            }
                        }
                    }
                    $tgl1 = strtotime(date('Y-m-d H:i:s')); 
                    $tgl2 = strtotime($doa[$i]->created_at); 
                    $detik = $tgl1 - $tgl2;
                    $last_created = number_format($detik , 0, ',', '.') . ' detik';
                    if((int)$detik > 60){
                        $menit = $detik / 60;//menit
                        $last_created = number_format($menit , 0, ',', '.') . ' menit';
                        if((int)$menit > 60){
                            $jam = $menit / 60;//jam
                            $last_created = number_format($jam , 0, ',', '.') . ' jam';
                            if((int)$jam > 24){
                                $hari = $jam / 24;//hari
                                $last_created = number_format($hari , 0, ',', '.') . ' hari';
                            }
                        }
                    }
                    $doa[$i]->createds = $last_created;
                }
                $data['doa'] = $doa;
            }else{
                $data['doa'] = '';
            }

            $data['title'] = $data_non_medis->judul;
            $data['dd'] = $data_non_medis;
            $data['slideshow'] = $this->model->all_slideshow();
            $this->load->view('header_front', $data);
            $this->load->view('non_medis');
            $this->load->view('footer_front');
        }
    }

    public function medis_help($link){
        // if (! $this->ion_auth->logged_in()){
        //     redirect(site_url());
        // }else{
            $data_medis = $this->model->data_medis($link);
            if(! $data_medis){
                redirect(site_url());
            }else{
                if($data_medis->status == '2'){
                    redirect(site_url());
                }
                if($data_medis->kategori == '1'){
                    $title = 'Anak Sakit';
                }else{
                    $title = 'Kesehatan';
                }
                $data['title'] = $data_medis->judul;
                $data['sampul'] = site_url('assets/project/kasus_medis/' . $data_medis->sampul);
                $data['id_data'] = $data_medis->id_kasus_medis;
                $data['slideshow'] = $this->model->all_slideshow();
                $this->load->view('header_front', $data);
                $this->load->view('medis_help');
                $this->load->view('footer_front');
            }
        // }
    }

    public function non_medis_help($link){
        // if (! $this->ion_auth->logged_in()){
        //     redirect(site_url());
        // }else{
            $data_non_medis = $this->model->data_non_medis($link);
            if(! $data_non_medis){
                redirect(site_url());
            }else{
                if($data_non_medis->status == '2'){
                    redirect(site_url());
                }
                if($data_non_medis->kategori == '1'){
                    $title = 'Kemanusiaan';
                }else{
                    $title = 'Bencana';
                }
                $data['title'] = $data_non_medis->judul;
                $data['sampul'] = site_url('assets/project/kasus_non_medis/' . $data_non_medis->sampul);
                $data['id_data'] = $data_non_medis->id_kasus_non_medis;
                $data['slideshow'] = $this->model->all_slideshow();
                $this->load->view('header_front', $data);
                $this->load->view('non_medis_help');
                $this->load->view('footer_front');
            }
        // }
    }
    
    // public function list_bank(){
    //     $metode = $this->input->post('metode');
    //     $data = $this->model->list_bank();
    //     if($data){
    //         foreach ($data as $row) {
    //             if($metode == '1'){
    //                 $row->name = 'Virtual Account ' . $row->name;
    //             }else{
    //                 $row->name = 'Transfer Bank ' . $row->name;
    //             }
    //         }
    //         echo json_encode($data);
    //     }
    // }
    
    // public function save_donasi_medis(){
    //     $by = $this->ion_auth->user()->row()->nama;
    //     $id_user = $this->ion_auth->user()->row()->id;
    //     $id_kasus = $this->input->post('donasi_main_id');
    //     $jml = $this->input->post('hide_donasi');
    //     $metode = $this->input->post('metode');
    //     $judul_kasus = $this->input->post('judul_kasus');
    //     $user_email = $this->input->post('user_email');
    //     $id_bank = $this->input->post('bank');
    //     $d_bank = $this->model->banks($id_bank);
    //     if($metode == '2'){
    //         $uniq_code = mt_rand(100,500);
    //         $donasi = (int)$jml + (int)$uniq_code;
    //         $va_number = '-';
    //         $va_name = '-';
    //         $bank_metode = 'Transfer Bank';
    //         $bank_image = $d_bank->link;
    //         $bank_name = $d_bank->owner;
    //         $bank_number = number_format($d_bank->number , 0, ',', '-');
    //     }else{
    //         $main_name = explode('.', $_SERVER['HTTP_HOST']);
    //         $donasi = (int)$jml;
    //         $va_number = 'KODE' . substr($this->ion_auth->user()->row()->phone, 1);
    //         $va_name = strtoupper($main_name[0]) . ' a/n ' . $this->ion_auth->user()->row()->nama;
    //         $bank_metode = 'Virtual Account';
    //         $bank_image = $d_bank->link;
    //         $bank_name = $va_name;
    //         $bank_number = $va_number;
    //     }
    //     $email = 'Email';
    //     $d_email = $this->model->base_contact($email);
    //     $email = $d_email->detail;

    //     $telepon = 'Telepon';
    //     $d_telepon = $this->model->base_contact($telepon);
    //     $telepon = $d_telepon->detail;

    //     $data = $this->model->save_donasi_medis($id_kasus, $by, $id_user, $donasi, $va_number, $va_name);
    //     if($data){
    //         $last = $this->model->last_medis_donasi($id_user);
    //         $mail   = new PHPMailer(); 
    //         $mail->IsSMTP(); 
    //         $mail->SMTPAuth     = true; 
    //         $mail->SMTPSecure   = "ssl";   
    //         $mail->Host         = $this->mail_host;      
    //         $mail->Port         = 465;                   
    //         $mail->Username     = $this->mail_username;  
    //         $mail->Password     = $this->mail_password;            
    //         $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
    //         $mail->isHTML(true);
    //         $mail->SMTPAutoTLS  = false;
    //         $mail->Hostname     = $this->mail_dns;
    //         $mail->Subject      = "Donasi";
    //         $mail->Body         = 
    //         '<!DOCTYPE html>
    //         <html>
    //             <head>
    //                  <title></title>
    //             </head>
    //             <body>
    //                 <div> 
    //                   <div style="width: 100%;">
    //                       <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
    //                   </div>
    //                   <br>
    //                   <br>
    //                   <div style="width: 100%; text-align: center;">Hi ' . $by . ',</div>
    //                   <div style="width: 100%; text-align: center;">Selesaikan donasi kamu melalui :</div>
    //                   <br>
    //                   <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">' . $bank_metode . '</div>
    //                   <div style="width: 100%; text-align: center;">
    //                         <img src="' . $bank_image . '" style="height: 50px;">
    //                   </div>
    //                   <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">' . $bank_name . '</div>
    //                   <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">' . $bank_number . '</div>
    //                   <br>
    //                   <div style="width: 100%; text-align: center; padding: 25px 0; font-weight: 600; border-radius: 15px; background: rgba(0,0,0,0.1);">
    //                       <div style="font-size: 20px;">' . $judul_kasus . '</div>
    //                       <br>
    //                       <div>Total Donasi</div>
    //                       <div>Rp. ' . number_format($donasi , 0, ',', '.') . '</div>
    //                   </div>
    //                   <br>
    //                   <br>
    //                   <div style="width: 100%; text-align: center;"><a href="'.site_url('donasi/medis_pay/'.$last->id_donasi_medis).'" class="btn btn-warning" target="_blank" style="border-radius: 10px; background: rgb(255,197,70); padding: 10px; color: #fff; text-decoration: none; font-weight: 600;">Cek Status Donasi</a></div>
    //                   <br>
    //                   <br>
    //                   <div style="width: 100%; text-align: center;">
    //                         <div>Salam,</div>
    //                         <div style="font-weight: 600;">Peduli Indonesia</div>
    //                   </div>
    //                   <br>
    //                   <div style="width: 100%; text-align: center;">
    //                         <div style="width: auto; padding: 0 10px; display: inline-block;">
    //                             <a href="https://www.facebook.com/kaizenpeduliindonesia" target="_blank">
    //                                 <img src="https://imgur.com/TiFD2VP.png" style="height: 30px; cursor: pointer;">
    //                             </a>
    //                         </div>
    //                         <div style="width: auto; padding: 0 10px; display: inline-block;">
    //                             <a href="https://www.instagram.com/kaizenpeduli.ind" target="_blank">
    //                                 <img src="https://imgur.com/aCbCLu1.png" style="height: 30px; cursor: pointer;">
    //                             </a>
    //                         </div>
    //                         <div style="width: auto; padding: 0 10px; display: inline-block;">
    //                             <a href="https://web.whatsapp.com/send?phone=' . preg_replace('/0/', '62', $telepon, 1) . '" target="_blank">
    //                                 <img src="https://imgur.com/ClkBQ1z.png" style="height: 30px; cursor: pointer;">
    //                             </a>
    //                         </div>
    //                   </div>
    //                   <br>
    //                   <div style="width: 100%; text-align: center;">
    //                         <div>Email : ' . $email . '</div>
    //                         <div>Whatsapp : ' . preg_replace('/0/', '+62 ', $telepon, 1) . '</div>
    //                   </div>
    //                 </div>
    //             </body>
    //         </html>';
    //         $mail->AddAddress($user_email);
    //         $mail->Send();
    //         $message['success'] = $last->id_donasi_medis;
    //         echo json_encode($message);
    //     }
    // }

    // public function save_donasi_non_medis(){
    //     $by = $this->ion_auth->user()->row()->nama;
    //     $id_user = $this->ion_auth->user()->row()->id;
    //     $id_kasus = $this->input->post('donasi_main_id');
    //     $jml = $this->input->post('hide_donasi');
    //     $metode = $this->input->post('metode');
    //     $judul_kasus = $this->input->post('judul_kasus');
    //     $user_email = $this->input->post('user_email');
    //     $id_bank = $this->input->post('bank');
    //     $d_bank = $this->model->banks($id_bank);
    //     if($metode == '2'){
    //         $uniq_code = mt_rand(100,500);
    //         $donasi = (int)$jml + (int)$uniq_code;
    //         $va_number = '-';
    //         $va_name = '-';
    //         $bank_metode = 'Transfer Bank';
    //         $bank_image = $d_bank->link;
    //         $bank_name = $d_bank->owner;
    //         $bank_number = number_format($d_bank->number , 0, ',', '-');
    //     }else{
    //         $main_name = explode('.', $_SERVER['HTTP_HOST']);
    //         $donasi = (int)$jml;
    //         $va_number = 'KODE' . substr($this->ion_auth->user()->row()->phone, 1);
    //         $va_name = strtoupper($main_name[0]) . ' a/n ' . $this->ion_auth->user()->row()->nama;
    //         $bank_metode = 'Virtual Account';
    //         $bank_image = $d_bank->link;
    //         $bank_name = $va_name;
    //         $bank_number = $va_number;
    //     }
    //     $email = 'Email';
    //     $d_email = $this->model->base_contact($email);
    //     $email = $d_email->detail;

    //     $telepon = 'Telepon';
    //     $d_telepon = $this->model->base_contact($telepon);
    //     $telepon = $d_telepon->detail;

    //     $data = $this->model->save_donasi_non_medis($id_kasus, $by, $id_user, $donasi, $va_number, $va_name);
    //     if($data){
    //         $last = $this->model->last_non_medis_donasi($id_user);
    //         $mail   = new PHPMailer(); 
    //         $mail->IsSMTP(); 
    //         $mail->SMTPAuth     = true; 
    //         $mail->SMTPSecure   = "ssl";   
    //         $mail->Host         = $this->mail_host;      
    //         $mail->Port         = 465;                   
    //         $mail->Username     = $this->mail_username;  
    //         $mail->Password     = $this->mail_password;            
    //         $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
    //         $mail->isHTML(true);
    //         $mail->SMTPAutoTLS  = false;
    //         $mail->Hostname     = $this->mail_dns;
    //         $mail->Subject      = "Donasi";
    //         $mail->Body         = 
    //         '<!DOCTYPE html>
    //         <html>
    //             <head>
    //                  <title></title>
    //             </head>
    //             <body>
    //                 <div> 
    //                   <div style="width: 100%;">
    //                       <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
    //                   </div>
    //                   <br>
    //                   <br>
    //                   <div style="width: 100%; text-align: center;">Hi ' . $by . ',</div>
    //                   <div style="width: 100%; text-align: center;">Selesaikan donasi kamu melalui :</div>
    //                   <br>
    //                   <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">' . $bank_metode . '</div>
    //                   <div style="width: 100%; text-align: center;">
    //                         <img src="' . $bank_image . '" style="height: 50px;">
    //                   </div>
    //                   <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">' . $bank_name . '</div>
    //                   <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">' . $bank_number . '</div>
    //                   <br>
    //                   <div style="width: 100%; text-align: center; padding: 25px 0; font-weight: 600; border-radius: 15px; background: rgba(0,0,0,0.1);">
    //                       <div style="font-size: 20px;">' . $judul_kasus . '</div>
    //                       <br>
    //                       <div>Total Donasi</div>
    //                       <div>Rp. ' . number_format($donasi , 0, ',', '.') . '</div>
    //                   </div>
    //                   <br>
    //                   <br>
    //                   <div style="width: 100%; text-align: center;"><a href="'.site_url('donasi/non_medis_pay/'.$last->id_donasi_non_medis).'" class="btn btn-warning" target="_blank" style="border-radius: 10px; background: rgb(255,197,70); padding: 10px; color: #fff; text-decoration: none; font-weight: 600;">Cek Status Donasi</a></div>
    //                   <br>
    //                   <br>
    //                   <div style="width: 100%; text-align: center;">
    //                         <div>Salam,</div>
    //                         <div style="font-weight: 600;">Peduli Indonesia</div>
    //                   </div>
    //                   <br>
    //                   <div style="width: 100%; text-align: center;">
    //                         <div style="width: auto; padding: 0 10px; display: inline-block;">
    //                             <a href="https://www.facebook.com/kaizenpeduliindonesia" target="_blank">
    //                                 <img src="https://imgur.com/TiFD2VP.png" style="height: 30px; cursor: pointer;">
    //                             </a>
    //                         </div>
    //                         <div style="width: auto; padding: 0 10px; display: inline-block;">
    //                             <a href="https://www.instagram.com/kaizenpeduli.ind" target="_blank">
    //                                 <img src="https://imgur.com/aCbCLu1.png" style="height: 30px; cursor: pointer;">
    //                             </a>
    //                         </div>
    //                         <div style="width: auto; padding: 0 10px; display: inline-block;">
    //                             <a href="https://web.whatsapp.com/send?phone=' . preg_replace('/0/', '62', $telepon, 1) . '" target="_blank">
    //                                 <img src="https://imgur.com/ClkBQ1z.png" style="height: 30px; cursor: pointer;">
    //                             </a>
    //                         </div>
    //                   </div>
    //                   <br>
    //                   <div style="width: 100%; text-align: center;">
    //                         <div>Email : ' . $email . '</div>
    //                         <div>Whatsapp : ' . preg_replace('/0/', '+62 ', $telepon, 1) . '</div>
    //                   </div>
    //                 </div>
    //             </body>
    //         </html>';
    //         $mail->AddAddress($user_email);
    //         $mail->Send();
    //         $message['success'] = $last->id_donasi_non_medis;
    //         echo json_encode($message);
    //     }
    // }
    
    public function medis_pay($id_donasi){
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        // if (! $this->ion_auth->logged_in()){
        //     redirect(site_url());
        // }else{
            if (! $this->ion_auth->logged_in()){
                $id_user = '0';
            }else{
                $id_user = $this->ion_auth->user()->row()->id;
            }
            $data_donasi = $this->model->data_donasi_medis($id_donasi, $id_user);
            if(! $data_donasi){
                redirect(site_url());
            }else{
                $expired = date('Y-m-d H:i:s', strtotime($data_donasi->created_at . ' + 90 minutes'));
                $s_expired = explode(' ', $expired);
                $s_expired_date = explode('-', $s_expired[0]);
                $data_donasi->expired = $s_expired_date[2] .' '. $month_format[(int)$s_expired_date[1]] .' '. $s_expired_date[0] . '<br>' . $s_expired[1];
                $data_donasi->time_expired = $expired;
                $data['title'] = $data_donasi->judul;
                $data['id_data'] = $id_donasi;
                $data['dd'] = $data_donasi;
                $data['slideshow'] = $this->model->all_slideshow();
                $this->load->view('header_front', $data);
                $this->load->view('medis_pay');
                $this->load->view('footer_front');
            }
        // }
    }

    public function non_medis_pay($id_donasi){
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        // if (! $this->ion_auth->logged_in()){
        //     redirect(site_url());
        // }else{
            if (! $this->ion_auth->logged_in()){
                $id_user = '0';
            }else{
                $id_user = $this->ion_auth->user()->row()->id;
            }
            $data_donasi = $this->model->data_donasi_non_medis($id_donasi, $id_user);
            if(! $data_donasi){
                redirect(site_url());
            }else{
                $expired = date('Y-m-d H:i:s', strtotime($data_donasi->created_at . ' + 90 minutes'));
                $s_expired = explode(' ', $expired);
                $s_expired_date = explode('-', $s_expired[0]);
                $data_donasi->expired = $s_expired_date[2] .' '. $month_format[(int)$s_expired_date[1]] .' '. $s_expired_date[0] . '<br>' . $s_expired[1];
                $data_donasi->time_expired = $expired;
                $data['title'] = $data_donasi->judul;
                $data['id_data'] = $id_donasi;
                $data['dd'] = $data_donasi;
                $data['slideshow'] = $this->model->all_slideshow();
                $this->load->view('header_front', $data);
                $this->load->view('non_medis_pay');
                $this->load->view('footer_front');
            }
        // }
    }
}