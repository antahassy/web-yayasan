<style type="text/css">
	.name_side_content{
		padding: 35px 0;
	}
	#cerita_content img{ 
		margin-left: -25px;
	}
	#cerita_content b{ 
		font-weight: 600 !important;
	}
	@media screen and (max-width: 1000px){
		#cerita_content img{ 
			width: 100% !important;
		}
	}
	@media screen and (max-width: 767px){
		.name_side_content{
			padding: 0 15px;
		}
	}
	@media screen and (max-width: 500px){
		#donasi_content{
			padding-right: 0 !important;
			padding-left: 0 !important;
		}
	}
</style>
<div class="row" style="padding: 15px 0; min-height: 500px;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row" style="padding-bottom: 25px;">
			<div class="col-md-12" style="margin-bottom: 25px;" id="donasi_content">
				<div class="w-100">
					<div style="padding: 0 20px;" id="limit_scroll">
						<center>
							<img src="<?php echo $dd->sampul ?>" class="donasi_sampul">
						</center>
						<div class="text_title text-center" style="text-transform: capitalize; margin: 5px 0; font-weight: 600; padding: 20px 0;"><?php echo $dd->judul ?></div>
						<div style="margin: 5px 0;">Rp. <span style="font-weight: 600;"><?php echo $dd->bantuan ?></span> Terkumpul Dari Rp. <span style="font-weight: 600;"><?php echo number_format($dd->dana , 0, ',', '.') ?></span></div>
						<div style="margin: 5px 0;"><span style="font-weight: 600;"><?php echo $dd->donatur ?></span> Donatur</div>
						<?php
							if($dd->status == '2'){
								echo '<div style="margin: 5px 0;">Berakhir pada <span style="font-weight: 600;">' . $dd->batas_akhir . '</span></div>';
								echo '<div class="row" style="margin: 10px 0;">';
									echo '<div class="col-md-3" style="padding: 5px 15px 5px 0;">';
										echo '<button type="button" class="btn btn-warning w-100 btn_share" style="background: rgb(255,239,212); border-color: rgb(255,239,212); color: rgb(255,197,70) !important;"><i class="fas fa-share-alt" style="margin-right: 5px;"></i> Bagikan</button>';
									echo '</div>';
								echo '</div>';
							}else{
								echo '<div style="margin: 5px 0;"><span style="font-weight: 600;">' . $dd->jatuh_tempo . '</span> Hari lagi</div>';
								echo '<div class="row" style="margin: 10px 0;">';
									echo '<div class="col-md-3" style="padding: 5px 15px 5px 0;">';
										echo '<button type="button" class="btn btn-warning w-100 btn_share" style="background: rgb(255,239,212); border-color: rgb(255,239,212); color: rgb(255,197,70) !important;"><i class="fas fa-share-alt" style="margin-right: 5px;"></i> Bagikan</button>';
									echo '</div>';
									echo '<div class="col-md-6" style="padding: 5px 15px 5px 0;">';
										if(! $this->ion_auth->logged_in()){
											echo '<a href="' . site_url('donasi/medis_help/' . $dd->link) . '" class="btn btn-warning w-100 sess_logged_in">Donasi Sekarang</a>';
										}else{
											echo '<a href="' . site_url('donasi/medis_help/' . $dd->link) . '" class="btn btn-warning w-100">Donasi Sekarang</a>';
										}
									echo '</div>';
								echo '</div>';
							}
						?>
					</div>
					<ul class="list-group">
						<li class="list-group-item" style="border: none; border-top: 4px solid rgb(255,239,212); padding: 15px 20px;">
							<div style="font-size: 18px; font-weight: 600;">Informasi Cerita</div>
							<div class="row" style="margin-top: 10px;">
								<div class="col-md-12" style="margin: 5px 0;" id="cerita_content">
									<?php echo $dd->cerita ?>
								</div>
							</div>
						</li>
						<li class="list-group-item" style="border: none; border-top: 4px solid rgb(255,239,212); padding: 15px 20px;">
							<div style="font-size: 18px; font-weight: 600;">Informasi Kabar Terbaru</div>
							<?php
								if($terbaru == ''){
									echo '<div class="w-100" style="padding: 20px; margin-top: 20px; border-radius: 5px;">';
										echo 'Belum ada kabar terbaru untuk penggalangan dana ini';
									echo '</div>';
								}else{
									foreach ($terbaru as $row) {
							            $tgl1 = strtotime(date('Y-m-d H:i:s')); 
							            $tgl2 = strtotime($row->created_at); 
							            $detik = $tgl1 - $tgl2;
							            $last_created = number_format($detik , 0, ',', '.') . ' detik';
							            if((int)$detik > 60){
							            	$menit = $detik / 60;//menit
							            	$last_created = number_format($menit , 0, ',', '.') . ' menit';
							            	if((int)$menit > 60){
								            	$jam = $menit / 60;//jam
								            	$last_created = number_format($jam , 0, ',', '.') . ' jam';
								            	if((int)$jam > 24){
									            	$hari = $jam / 24;//hari
									            	$last_created = number_format($hari , 0, ',', '.') . ' hari';
									            }
								            }
							            }
										echo '<div class="w-100" style="padding: 20px; margin-top: 20px; border-radius: 5px;">';
											echo '<div>';
												echo '<div><i>' . $last_created . '</i></div>';
												echo '<div class="text_title" style="text-transform: capitalize; margin-top:15px;">';
													echo $row->judul;
												echo '</div>';
												echo '<div class="kbr_terbaru">';
													echo $row->kabar_terbaru;
												echo '</div>';
											echo '</div>';
										echo '</div>';
									}
								}
							?>
						</li>
						<li class="list-group-item" style="border: none; border-top: 4px solid rgb(255,239,212); padding: 15px 20px;">
							<div style="font-size: 18px; font-weight: 600;">Informasi Penggalang Dana</div>
							<div class="row">
								<div class="col-md-3">
									<img src="<?php echo $dd->image ?>" style="width: 100px; height: 100px; border-radius: 100%; margin: 10px 0;">
								</div>
								<div class="col-md-9 name_side_content">
									<?php echo $dd->nama ?> <br> <div style="font-size: 15px; font-family: 'Apercu' !important; font-style: italic !important; font-weight: 300 !important; src: local('Apercu'), url('https://fonts.cdnfonts.com/s/22258/Apercu Light Italic.woff') format('woff'); color: rgba(0,0,0,0.5); margin-top: 7.5px;">Terverifikasi <i style="color: orange; font-size: 15px; margin-left: 5px;" class="fas fa-check"></i></div>
								</div>
							</div>
						</li>
						<li class="list-group-item" style="border: none; border-top: 4px solid rgb(255,239,212); padding: 15px 20px;">
							<div style="font-size: 18px; font-weight: 600;">Informasi Pasien</div>
							<div class="row" style="margin-top: 10px;">
								<div class="col-md-12" style="margin: 10px 0;">
									<i style="color: orange; font-size: 20px;" class="fas fa-user"></i> &nbsp<?php echo $dd->nm_pasien ?> 
									<?php if($dd->dokumen_medis != ''){ ?> 
										<div style="margin-top: 7.5px;">
											<i style="font-size: 15px; font-family: 'Apercu' !important; font-style: italic !important; font-weight: 300 !important; src: local('Apercu'), url('https://fonts.cdnfonts.com/s/22258/Apercu Light Italic.woff') format('woff'); color: rgba(0,0,0,0.5);">Sesuai dokumen medis</i> <i style="color: orange; font-size: 15px; margin-left: 5px;" class="fas fa-check"></i>
										</div>
									<?php } ?> 
								</div>
								<div class="col-md-12" style="margin: 10px 0;">
									<i style="color: orange; font-size: 20px;" class="fas fa-file-invoice"></i> &nbsp<?php echo $dd->nm_penyakit ?> 
									<?php if($dd->hasil_pemeriksaan != ''){ ?> 
										<div style="margin-top: 7.5px;">
											<i style="font-size: 15px; font-family: 'Apercu' !important; font-style: italic !important; font-weight: 300 !important; src: local('Apercu'), url('https://fonts.cdnfonts.com/s/22258/Apercu Light Italic.woff') format('woff'); color: rgba(0,0,0,0.5);">Disertai dokumen medis</i> <i style="color: orange; font-size: 15px; margin-left: 5px;" class="fas fa-check"></i>
										</div>
									<?php } ?> 
								</div>
							</div>
						</li>
						<li class="list-group-item" style="border: none; border-top: 4px solid rgb(255,239,212); padding: 15px 20px;">
							<div style="font-size: 18px; font-weight: 600;">Informasi Penerima Dana</div>
							<div class="row" style="margin-top: 10px;">
								<div class="col-md-12" style="margin: 10px 0;">
										<i style="color: orange; font-size: 20px;" class="fas fa-file-invoice"></i> &nbsp<?php echo $dd->rekening ?> 
									<?php if($dd->file_kk != ''){ ?> 
										<div style="margin-top: 7.5px;">
											<i style="font-size: 15px; font-family: 'Apercu' !important; font-style: italic !important; font-weight: 300 !important; src: local('Apercu'), url('https://fonts.cdnfonts.com/s/22258/Apercu Light Italic.woff') format('woff'); color: rgba(0,0,0,0.5);">Kartu Keluarga (KK) Terverifikasi</i> <i style="color: orange; font-size: 15px; margin-left: 5px;" class="fas fa-check"></i>
										</div>
									<?php } ?> 
									<?php if($dd->file_rekening != ''){ ?> 
										<div style="margin-top: 7.5px;">
											<i style="font-size: 15px; font-family: 'Apercu' !important; font-style: italic !important; font-weight: 300 !important; src: local('Apercu'), url('https://fonts.cdnfonts.com/s/22258/Apercu Light Italic.woff') format('woff'); color: rgba(0,0,0,0.5);">Rekening Bank Terverifikasi</i> <i style="color: orange; font-size: 15px; margin-left: 5px;" class="fas fa-check"></i>
										</div>
									<?php } ?> 
								</div>
							</div>
						</li>
						<li class="list-group-item" style="border: none; border-top: 4px solid rgb(255,239,212); padding: 15px 20px;">
							<div style="font-size: 18px; font-weight: 600;">Informasi Penggunaan Dana</div>
							<div class="row" style="margin-top: 10px;">
								<div class="col-md-12" style="margin: 5px 0;">
									<?php echo $dd->penggunaan_dana ?>
								</div>
							</div>
						</li>
						<?php if($doa != ''){ ?>
							<li class="list-group-item" style="border: none; border-top: 4px solid rgb(255,239,212); padding: 15px 20px;">
								<div style="font-size: 18px; font-weight: 600;">Doa Orang Baik</div>
								<div class="row" style="margin-top: 10px;">
									<div class="col-md-12" style="margin: 5px 0;">
										<?php foreach($doa as $row){ ?>
											<div class="row">
												<div class="col-md-3">
													<img src="<?php echo $row->image ?>" style="width: 100px; height: 100px; border-radius: 100%; margin: 10px 0;">
												</div>
												<div class="col-md-9 name_side_content">
													<div style="font-size: 12px; margin-bottom: 2.5px; font-style: italic;"><?php echo $row->createds ?> yang lalu</div>
													<?php echo $row->nama ?> <br> <div style="font-size: 15px; font-family: 'Apercu' !important; font-style: italic !important; font-weight: 300 !important; src: local('Apercu'), url('https://fonts.cdnfonts.com/s/22258/Apercu Light Italic.woff') format('woff'); color: rgba(0,0,0,0.5); margin-top: 2.5px;"><?php echo $row->doa ?></div>
												</div>
											</div>
										<?php } ?>
									</div>
								</div>
							</li>
						<?php } ?>
					</ul>
					<div style="position: sticky; bottom: 15px; z-index: 1; display: none;" id="fix_donasi">
						<?php if($dd->status == '1'){
							if(! $this->ion_auth->logged_in()){
								echo '<a href="' . site_url('donasi/medis_help/' . $dd->link) . '" class="btn btn-warning w-100 sess_logged_in">Donasi Sekarang</a>';
							}else{
								echo '<a href="' . site_url('donasi/medis_help/' . $dd->link) . '" class="btn btn-warning w-100">Donasi Sekarang</a>';
							}
						}else{
							echo '<button class="btn btn-warning w-100 btn_share" style="background: rgb(255,239,212); border-color: rgb(255,239,212); color: rgb(255,197,70) !important;"><i class="fas fa-share-alt" style="margin-right: 5px;"></i> Bagikan</button>';
						} ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<script type="text/javascript">
    $(window).scroll(function(){
		var scroll = $(window).scrollTop();
	    if (scroll > ($('header').height() + $('#limit_scroll').height())){
	    	$('#fix_donasi').fadeIn(500);
        }else{
        	$('#fix_donasi').fadeOut(500);
        }
	});
	$(document).ready(function(){
		$('.btn_share').on('click', function(){
			var temporary = $("<input>");
		    $("body").append(temporary);
		    temporary.val(window.location.href).select();
		    document.execCommand("copy");
		    temporary.remove();
		    swal({
                background  : 'transparent',
                html        : '<pre>Link tersalin</pre>',
            });
		});
	});
</script>