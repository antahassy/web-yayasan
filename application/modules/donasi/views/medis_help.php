<style type="text/css">
	.toggle-group .btn-default{
		color: #fff !important;
		background-color: rgba(0,0,0,0.75) !important;
		border-color: rgba(0,0,0,0.75) !important;
	}
	.toggle-group .btn-primary{
		color: rgba(0,0,0,0.75) !important;
		background-color: #fff !important;
		border-color: rgba(0,0,0,0.75) !important;
	}
	.toggle{
		margin: 5px 0;
	}
	form .form-control{
		box-shadow: none !important;
		background-color: #fff !important;
		border-color: rgba(0,0,0,0.3) !important;
		color: #495057 !important;
		font-family: 'Geliat' !important;
	    font-style: normal !important;
	    font-weight: 200 !important;
	    src: local('Geliat'), url('https://fonts.cdnfonts.com/s/65524/Geliat-ExtraLight.woff') format('woff');
	}
	form .form-control::placeholder{
		color: #495057 !important;
	}
	input[type="radio"], input[type="checkbox"] {
	    display:none;
	}
	.radio_icon{
		padding-top: 13px; 
		font-weight: 400; 
		cursor: pointer;
	}
	.svg-inline--fa {
	    vertical-align: -10px;
	}
	.form-check{
		background-color: rgba(0,0,0,0.1);
		margin: 15px 0;
		border-radius: 5px;
	}
	.i_icon{
		font-size: 25px;
		margin-top: -2px;
		color: rgba(0,0,0,0.3);
		float: right;
	}
	.ver_data{
		display: none;
	}
</style>
<!-- <script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-kGejpJI7lVyKYeVS"></script> -->
<script type="text/javascript" src="https://app.midtrans.com/snap/snap.js" data-client-key="Mid-client-zMkyQ1q4-Sqj9oFY"></script>
<div class="row" style="padding: 15px 0; min-height: 500px;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row" style="padding-bottom: 25px;">
			<div class="col-md-12" style="margin-bottom: 25px;">
				<div class="w-100">
					<center>
						<img src="<?php echo $sampul ?>" class="donasi_sampul" style="border-top: 10px solid rgb(255,197,70);">
					</center>
					<div class="text_title text-center" style="text-transform: uppercase; margin: 15px 0; padding: 15px 0; font-weight: 600;"><?php echo $title ?></div>
					<form id="form_donasi">
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8" style="padding-bottom: 15px;">
								<input type="hidden" name="result_type" id="result_type" value="">
      							<input type="hidden" name="result_data" id="result_data" value="">
      							<input type="hidden" name="kategori" id="kategori" value="medis">
								<div style="padding: 2.5px 0; font-weight: 600; text-transform: capitalize;">
									<?php if (! $this->ion_auth->logged_in()){ ?>
										<label style="font-weight: 600;">Email</label>
										<input type="text" name="user_email" id="user_email" class="form-control" placeholder="Email Anda">
										<label style="font-weight: 600;">Nama</label>
										<input type="text" name="user_nama" id="user_nama" class="form-control" placeholder="Nama Anda">
										<label style="font-weight: 600;">No. Ponsel</label>
										<input type="text" name="user_phone" id="user_phone" class="form-control" placeholder="No. Ponsel Anda">
									<?php }else{ ?>
										<input type="hidden" name="user_email" id="user_email" value="<?php echo $this->ion_auth->user()->row()->username ?>">
										<input type="hidden" name="user_nama" id="user_nama" value="<?php echo $this->ion_auth->user()->row()->nama ?>">
										<input type="hidden" name="user_phone" id="user_phone" value="<?php echo $this->ion_auth->user()->row()->phone ?>">
										<?php echo $this->ion_auth->user()->row()->nama?> <span style="font-weight: 400;">(<?php echo $this->ion_auth->user()->row()->phone?>)</span></div>
									<?php } ?>
								<div class="">
									<input type="hidden" name="donatur_name" id="donatur_name" value="0">
									<input type="hidden" name="judul_kasus" id="judul_kasus" value="<?php echo $title ?>">
								  	<input class="form-check-input" type="checkbox" value="1" name="hide_name[]" id="hide_name1">
								  	Sembunyikan nama saya ?
								  	<label class="radio_icon check_hide_name" for="hide_name1" data="1">
								  		<i style="font-size: 35px; color: rgb(255,197,70); margin-left: 10px;" class="fas fa-toggle-off"></i>
								  	</label>
								</div>

								<input type="hidden" name="donasi_main_id" value="<?php echo $id_data ?>">
								<input type="hidden" name="hide_donasi" id="hide_donasi">
								<div class="form-check s_donasi" style="padding: 5px 1.25rem;">
								  	<input class="form-check-input" type="radio" name="donasi" id="donasi1" value="10000" style="cursor: pointer;">
								  	<label class="radio_icon radio_donasi w-100" for="donasi1" data="10000" style="font-weight: 600;">
								    	Rp. 10.000
								    	<i class="fas fa-angle-right i_icon"></i>
								    	<div style="font-size: 12px; font-weight: 400;">Nominal Minimum Donasi</div>
								  	</label>
								</div>
								<div class="form-check s_donasi" style="padding: 5px 1.25rem;">
								  	<input class="form-check-input" type="radio" name="donasi" id="donasi2" value="50000" style="cursor: pointer;">
								  	<label class="radio_icon radio_donasi w-100" for="donasi2" data="50000" style="font-weight: 600;">
								    	Rp. 50.000
								    	<i class="fas fa-angle-right i_icon"></i>
								  	</label>
								</div>
								<div class="form-check s_donasi" style="padding: 5px 1.25rem;">
								  	<input class="form-check-input" type="radio" name="donasi" id="donasi3" value="100000" style="cursor: pointer;">
								  	<label class="radio_icon radio_donasi w-100" for="donasi3" data="100000" style="font-weight: 600;">
								    	Rp. 100.000
								    	<i class="fas fa-angle-right i_icon"></i>
								  	</label>
								</div>
								<div class="form-check s_donasi" style="padding: 5px 1.25rem;">
								  	<input class="form-check-input" type="radio" name="donasi" id="donasi4" value="" style="cursor: pointer;">
								  	<label class="radio_icon radio_donasi w-100" for="donasi4" data="" style="padding-top: 0;">
								    	<div style="font-size: 12px; font-weight: 400;">Nominal Donasi Lainnya</div>
								    	<div class="ket_nominal" style="margin-top: 5px;">
								    		<div class="input-group">
									        	<div class="input-group-prepend">
									          		<div class="input-group-text" style="background-color: fff; font-weight: 600; border-color: transparent; background-color: #fff; border-top: 1px solid rgba(0,0,0,0.3); border-left: 1px solid rgba(0,0,0,0.3); border-bottom: 1px solid rgba(0,0,0,0.3);">Rp.</div>
									        	</div>
									        	<input type="text" name="jml_nominal" id="jml_nominal" class="form-control" style="font-weight: 600 !important; border-left: none;">
									      	</div>
								    	</div>
								  	</label>
								</div>
								
								<label style="font-weight: 600;">Bank</label>
								<input type="hidden" name="hide_bank" id="hide_bank">
								<div class="form-check s_bank" style="padding: 5px 1.25rem;">
								  	<input class="form-check-input" type="radio" name="bank" id="bank1" value="1" style="cursor: pointer;">
								  	<label class="radio_icon radio_bank w-100" for="bank1" data="1" style="font-weight: 600; padding-top: 0;">
								    	<img src="<?php echo site_url('assets/project/web/bca.png')?>" style="height: 50px;">
								    	<i class="fas fa-angle-right i_icon" style="margin-top: 15px;"></i>
								  	</label>
								  	<div class="ver_data" style="font-weight: 500; color: black !important;">Verifikasi Manual 1 x 24 Jam</div>
								</div>
								<div class="form-check s_bank" style="padding: 5px 1.25rem;">
								  	<input class="form-check-input" type="radio" name="bank" id="bank2" value="2" style="cursor: pointer;">
								  	<label class="radio_icon radio_bank w-100" for="bank2" data="2" style="font-weight: 600; padding-top: 0;">
								  		<div style="padding: 2.5px 0;">
								  			<img src="<?php echo site_url('assets/project/web/mandiri.png')?>" style="height: 35px;">
								  		</div>
								  		<div style="padding: 2.5px 0;">
								  			<img src="<?php echo site_url('assets/project/web/bni.png')?>" style="height: 35px; margin-left: -4px;">
								  		</div>
								    	<div style="padding: 2.5px 0;">
								    		<img src="<?php echo site_url('assets/project/web/bri.png')?>" style="height: 35px;">
								    	</div>
								    	<i class="fas fa-angle-right i_icon" style="margin-top: 0; position: absolute; right: 20px; top: 60px;"></i>
								  	</label>
								  	<div class="ver_data" style="font-weight: 500; color: black !important;">Verifikasi Otomatis</div>
								</div>

								<label>Doa Orang Baik (Opsional)</label>
								<textarea name="doa_donasi" id="doa_donasi" class="form-control" rows="5"></textarea>

								<div class="text-center" style="margin-top: 25px;">
									<button type="button" class="btn btn-warning" id="btn_donasi">Donasi</button>
								</div>
							</div>
							<div class="col-md-2"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<script type="text/javascript">
	<?php if (! $this->ion_auth->logged_in()){ ?>
		$('#user_email, #user_nama, #user_phone, #doa_donasi').val('');
	<?php } ?>
</script>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/web_donasi_medis.js?t=').mt_rand()?>"></script>