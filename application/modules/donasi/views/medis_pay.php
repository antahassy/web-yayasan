<style type="text/css">
	.radio_icon{
		margin-top: 0; 
		font-weight: 400; 
		cursor: pointer;
		padding-top: 7px;
	}
</style>
<div class="row" style="padding: 15px 0; min-height: 500px;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row" style="padding-bottom: 25px;">
			<div class="col-md-12" style="margin-bottom: 25px;">
				<div class="w-100">
					<div class="text_title text-center" style="text-transform: uppercase; margin: 15px 0; padding: 15px 0;">Donasi<br><b><?php echo $title ?></b></div>
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<div class="w-100" style="padding: 15px 30px; margin-bottom: 15px; background-color: rgb(255,239,212);">
								<label class="radio_icon">
									<?php if($dd->status == '0'){
										if($dd->metode == '1'){
											if($dd->time_expired > date('Y-m-d H:i:s')){ ?>
												<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="fas fa-circle"></i>
								    			Menunggu Donasi
											<?php }else{ ?>
												<i style="font-size: 20px; color: rgb(207,16,32); margin-right: 10px;" class="fas fa-circle"></i>
									    		Donasi Kadaluarsa
											<?php } 
										}else{ ?>
											<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="fas fa-circle"></i>
							    			Menunggu Donasi
										<?php } ?>
									<?php }if($dd->status == '1'){?>
										<i style="font-size: 20px; color: rgb(55,125,61); margin-right: 10px;" class="fas fa-circle"></i>
							    		Donasi Berhasil
								    <?php } ?>
							  	</label>
							</div>
							<div class="w-100" style="padding: 15px 30px; background-color: rgb(255,239,212);">
								<?php if($dd->metode == '2'){ ?>
									<div style="padding: 5px 0;">
										<b style="text-transform: uppercase; font-weight: 600;">Transfer Bank <?php echo $dd->name ?></b>
									</div>
									<div>
										<img src="<?php echo site_url('assets/project/web/' . $dd->image)?>" style="height: 50px;">
									</div>
									<div style="padding: 2.5px 0;">
										<?php echo $dd->owner ?>
									</div>
									<div style="padding: 2.5px 0;">
										<?php echo number_format($dd->number , 0, ',', '-') ?>
									</div>
								<?php }else{ ?>
									<div style="padding: 5px 0;">
										<b style="text-transform: uppercase; font-weight: 600;">Virtual Account <?php echo $dd->name ?></b>
									</div>
									<div>
										<img src="<?php echo site_url('assets/project/web/' . $dd->image)?>" style="height: 50px;">
									</div>
									<?php if($dd->va_code != ''){?>
										<div style="padding: 2.5px 0;">
											Midtrans a/n <?php echo $dd->nama ?>
										</div>
										<div style="padding: 2.5px 0;">
											Kode Perusahaan <?php echo $dd->va_code ?>
										</div>
										<div style="padding: 2.5px 0;">
											Kode Pembayaran <?php echo $dd->va_number ?>
										</div>
									<?php }else{?>
										<div style="padding: 2.5px 0;">
											Midtrans a/n <?php echo $dd->nama ?>
										</div>
										<div style="padding: 2.5px 0;">
											Nomor VA <?php echo $dd->va_number ?>
										</div>
									<?php }?>
								<?php } ?>
							</div>
							<?php if($dd->metode == '1'){ ?>
							<div>
								<div class="w-100" style="padding: 15px 30px;">
									<div style="padding: 2.5px 0;">
										Batas Akhir Donasi
									</div>
									<div style="padding: 2.5px 0; font-weight: 600;">
										<?php echo $dd->expired ?> WIB
									</div>
								</div>
								<div class="w-100" style="padding: 15px 30px; background-color: rgb(255,239,212);">
									<?php if($dd->status == '0'){
										if($dd->time_expired > date('Y-m-d H:i:s')){ ?>
											<div style="padding: 2.5px 0;">
												Waktu Tersisa
											</div>
											<div style="padding: 2.5px 0; font-weight: 600;" id="count_expired">
												
											</div>
											<?php if($dd->bukti != ''){?>
												Lihat panduan donasi <a href="<?php echo $dd->bukti ?>" target="_blank" style="font-weight: 600;">Disini</a>
											<?php } ?>
										<?php }else{ ?>
											<div style="padding: 2.5px 0; font-weight: 600;">
												Donasi Kadaluarsa
											</div>
										<?php } ?>
									<?php }if($dd->status == '1'){?>
										<div style="padding: 2.5px 0; font-weight: 600;">
											Donasi Berhasil
										</div>
								    <?php } ?>
								</div>
							</div>
							<?php } ?>
							<div class="w-100" style="padding: 15px 30px;">
								<div style="padding: 2.5px 0;">
									Total Donasi
								</div>
								<div style="padding: 2.5px 0; font-weight: 600;">
									Rp. <?php echo number_format($dd->donasi , 0, ',', '.') ?>
									<?php if($dd->metode == '2'){ ?>
										<div style="font-size: 14px; text-align: center; margin: 15px 0; font-family: 'Apercu' !important; font-style: italic !important; font-weight: 300 !important; src: local('Apercu'), url('https://fonts.cdnfonts.com/s/22258/Apercu Light Italic.woff') format('woff'); color: rgba(0,0,0,0.3) !important;">
											<i>*Mohon masukkan total donasi akhir untuk memudahkan proses verifikasi</i>
										</div>
									<?php } ?>
									<div class="text-center" style="margin-top: 25px;">
										<button type="button" class="btn btn-warning" id="btn_status_donasi" data="<?php echo $id_data ?>">Cek Status Donasi</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-2"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<script type="text/javascript">
	var donasi_metode = '<?php echo $dd->metode ?>';
	var donasi_expired = '<?php echo $dd->time_expired ?>';
	var donasi_status = '<?php echo $dd->status ?>';
	var now_time = '<?php echo date('Y-m-d H:i:s') ?>';

	var waktu, expired_content;
	var expired = new Date(donasi_expired).getTime();

	if(donasi_metode == '1'){
		setInterval(function(){
	        var now = new Date().getTime();
		    var distance = expired - now;
		    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		    var hours = (24 * days) + Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		    if(distance < 0){
		    	if(donasi_status == '0'){
		    		expired_content = 
			        '<i style="font-size: 20px; color: rgb(207,16,32); margin-right: 10px;" class="fas fa-circle"></i>' +
					'Donasi Kadaluarsa';
		    	}
		    	if(donasi_status == '1'){
		    		expired_content = 
			        '<i style="font-size: 20px; color: rgb(55,125,61); margin-right: 10px;" class="fas fa-circle"></i>' +
					'Donasi Berhasil';
		    	}
		        waktu = 'Kadaluarsa';
		    }else{
		    	if(hours.toString().length == 1){
		    		hours = '0' + hours;
		    	}
		    	if(minutes.toString().length == 1){
		    		minutes = '0' + minutes;
		    	}
		    	if(seconds.toString().length == 1){
		    		seconds = '0' + seconds;
		    	}
		        waktu =  hours + ':'+ minutes + ':'+ seconds;
		    } 
		    $('#count_expired').text(waktu);
		    $('.radio_icon').html(expired_content);
	    }, 1000);
	}
    $('#btn_status_donasi').on('click', function(){
    	location.reload(true);
    });
</script>