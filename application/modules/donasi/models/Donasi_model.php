<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Donasi_model extends CI_Model {

    public function __construct(){
        parent::__construct(); 
        $this->db = $this->load->database('default', TRUE);
        date_default_timezone_set('Asia/Jakarta');
    }
    
    // public function list_bank(){
    //     $this->db->order_by('tb_bank.id_bank', 'asc');
    //     $this->db->select('id_bank, image, name, number, owner');
    //     $query = $this->db->get_where('tb_bank', array(
    //         'deleted_at'    => ''
    //     ));
    //     if($query->num_rows() > 0){
    //         return $query->result();
    //     }else{ 
    //         return false;
    //     }
    // }

    public function all_slideshow(){
        $this->db->order_by('tb_slideshow.id_slideshow', 'desc');
        $this->db->select('image');
        $query = $this->db->get_where('tb_slideshow', array(
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }
    
    public function doa_medis($id_kasus){
        $this->db->limit(10);
        $this->db->order_by('tb_donasi_medis.id_donasi_medis', 'desc');
        $this->db->select('
            tb_donasi_medis.nama as donatur,
            tb_donasi_medis.hide_name,
            tb_donasi_medis.doa,
            tb_donasi_medis.created_at,
            users.nama,
            users.image
        ');
        $this->db->where('tb_donasi_medis.id_kasus_medis', $id_kasus);
        $this->db->where('tb_donasi_medis.deleted_at', '');
        $this->db->where('tb_donasi_medis.status', '1');
        $this->db->where('tb_donasi_medis.doa !=', '');
        $this->db->from('tb_donasi_medis');
        $this->db->join('users', 'tb_donasi_medis.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function data_medis($link){
        $this->db->select('
            tb_kasus_medis.*,
            users.nama,
            users.image
        ');
        $this->db->where('tb_kasus_medis.status >','0');
        $this->db->where('tb_kasus_medis.admin_verify','1');
        $this->db->where('tb_kasus_medis.link',$link);
        $this->db->where('tb_kasus_medis.deleted_at','');
        $this->db->from('tb_kasus_medis');
        $this->db->join('users', 'tb_kasus_medis.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function total_donasi_medis($id_kasus_medis){
        $this->db->select_sum('donasi');
        $query = $this->db->get_where('tb_donasi_medis', array(
            'id_kasus_medis'    => $id_kasus_medis,
            'status'            => '1',
            'deleted_at'            => '',
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function total_donatur_medis($id_kasus_medis){
        $this->db->where('tb_donasi_medis.status','1');
        $this->db->where('tb_donasi_medis.deleted_at','');
        $this->db->where('tb_donasi_medis.id_kasus_medis', $id_kasus_medis);
        $this->db->from('tb_donasi_medis');
        return $this->db->count_all_results();
    }
    
    public function doa_non_medis($id_kasus){
        $this->db->limit(10);
        $this->db->order_by('tb_donasi_non_medis.id_donasi_non_medis', 'desc');
        $this->db->select('
            tb_donasi_non_medis.nama as donatur,
            tb_donasi_non_medis.hide_name,
            tb_donasi_non_medis.doa,
            tb_donasi_non_medis.created_at,
            users.nama,
            users.image
        ');
        $this->db->where('tb_donasi_non_medis.id_kasus_non_medis', $id_kasus);
        $this->db->where('tb_donasi_non_medis.deleted_at', '');
        $this->db->where('tb_donasi_non_medis.status', '1');
        $this->db->where('tb_donasi_non_medis.doa !=', '');
        $this->db->from('tb_donasi_non_medis');
        $this->db->join('users', 'tb_donasi_non_medis.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function data_non_medis($link){
        $this->db->select('
            tb_kasus_non_medis.*,
            users.nama,
            users.image
        ');
        $this->db->where('tb_kasus_non_medis.status >','0');
        $this->db->where('tb_kasus_non_medis.admin_verify','1');
        $this->db->where('tb_kasus_non_medis.link',$link);
        $this->db->where('tb_kasus_non_medis.deleted_at','');
        $this->db->from('tb_kasus_non_medis');
        $this->db->join('users', 'tb_kasus_non_medis.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function total_donasi_non_medis($id_kasus_non_medis){
        $this->db->select_sum('donasi');
        $query = $this->db->get_where('tb_donasi_non_medis', array(
            'id_kasus_non_medis'    => $id_kasus_non_medis,
            'status'                => '1',
            'deleted_at'            => '',
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function total_donatur_non_medis($id_kasus_non_medis){
        $this->db->where('tb_donasi_non_medis.status','1');
        $this->db->where('tb_donasi_non_medis.deleted_at','');
        $this->db->where('tb_donasi_non_medis.id_kasus_non_medis', $id_kasus_non_medis);
        $this->db->from('tb_donasi_non_medis');
        return $this->db->count_all_results();
    }
    
    public function terbaru_medis($id_kasus_medis){
        $this->db->order_by('id_kabar_medis', 'asc');
        $this->db->select('judul, kabar_terbaru, created_at');
        $query = $this->db->get_where('tb_kabar_medis', array(
            'id_kasus_medis'    => $id_kasus_medis,
            'deleted_at'        => '',
            'status'            => '1',
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function terbaru_non_medis($id_kasus_non_medis){
        $this->db->order_by('id_kabar_non_medis', 'asc');
        $this->db->select('judul, kabar_terbaru, created_at');
        $query = $this->db->get_where('tb_kabar_non_medis', array(
            'id_kasus_non_medis'    => $id_kasus_non_medis,
            'deleted_at'        => '',
            'status'            => '1',
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }
    
    public function save_donasi_medis($id_kasus, $by, $id_user, $donasi, $va_number, $va_name){
        $data = array(
            'id_kasus_medis'    => $id_kasus,
            'id_user'           => $id_user,
            'id_bank'           => $this->input->post('bank'),
            'metode'            => $this->input->post('metode'),
            'va_number'         => $va_number,
            'va_name'           => $va_name,
            'doa'               => $this->input->post('doa_donasi'),
            'hide_name'         => $this->input->post('donatur_name'),
            'donasi'            => $donasi,
            'status'            => '0',
            'created_at'        => date('Y-m-d H:i:s'),
            'created_by'        => $by
        );
        $query = $this->db->insert('tb_donasi_medis', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function save_donasi_non_medis($id_kasus, $by, $id_user, $donasi, $va_number, $va_name){
        $data = array(
            'id_kasus_medis'    => $id_kasus,
            'id_user'           => $id_user,
            'id_bank'           => $this->input->post('bank'),
            'metode'            => $this->input->post('metode'),
            'va_number'         => $va_number,
            'va_name'           => $va_name,
            'doa'               => $this->input->post('doa_donasi'),
            'hide_name'         => $this->input->post('donatur_name'),
            'donasi'            => $donasi,
            'status'            => '0',
            'created_at'        => date('Y-m-d H:i:s'),
            'created_by'        => $by
        );
        $query = $this->db->insert('tb_donasi_non_medis', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function last_medis_donasi($id_user){
        $this->db->order_by('id_donasi_medis', 'desc');
        $this->db->select('id_donasi_medis');
        $query = $this->db->get_where('tb_donasi_medis', array('id_user' => $id_user));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function last_non_medis_donasi($id_user){
        $this->db->order_by('id_donasi_non_medis', 'desc');
        $this->db->select('id_donasi_non_medis');
        $query = $this->db->get_where('tb_donasi_non_medis', array('id_user' => $id_user));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }
    
    public function data_donasi_medis($id_donasi, $id_user){
        $this->db->select('
            tb_donasi_medis.nama,
            tb_donasi_medis.va_number,
            tb_donasi_medis.va_name,
            tb_donasi_medis.va_code,
            tb_donasi_medis.bukti,
            tb_donasi_medis.metode,
            tb_donasi_medis.donasi,
            tb_donasi_medis.status,
            tb_donasi_medis.created_at,
            tb_kasus_medis.judul,
            tb_bank.name,
            tb_bank.image,
            tb_bank.number,
            tb_bank.owner
        ');
        $this->db->where('tb_donasi_medis.id_donasi_medis',$id_donasi);
        $this->db->where('tb_donasi_medis.id_user',$id_user);
        $this->db->where('tb_donasi_medis.deleted_at','');
        $this->db->from('tb_donasi_medis');
        $this->db->join('tb_kasus_medis', 'tb_donasi_medis.id_kasus_medis = tb_kasus_medis.id_kasus_medis', 'left');
        $this->db->join('tb_bank', 'tb_donasi_medis.id_bank = tb_bank.id_bank', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function data_donasi_non_medis($id_donasi, $id_user){
        $this->db->select('
            tb_donasi_non_medis.nama,
            tb_donasi_non_medis.va_number,
            tb_donasi_non_medis.va_name,
            tb_donasi_non_medis.va_code,
            tb_donasi_non_medis.bukti,
            tb_donasi_non_medis.metode,
            tb_donasi_non_medis.donasi,
            tb_donasi_non_medis.status,
            tb_donasi_non_medis.created_at,
            tb_kasus_non_medis.judul,
            tb_bank.name,
            tb_bank.image,
            tb_bank.number,
            tb_bank.owner
        ');
        $this->db->where('tb_donasi_non_medis.id_donasi_non_medis',$id_donasi);
        $this->db->where('tb_donasi_non_medis.id_user',$id_user);
        $this->db->where('tb_donasi_non_medis.deleted_at','');
        $this->db->from('tb_donasi_non_medis');
        $this->db->join('tb_kasus_non_medis', 'tb_donasi_non_medis.id_kasus_non_medis = tb_kasus_non_medis.id_kasus_non_medis', 'left');
        $this->db->join('tb_bank', 'tb_donasi_non_medis.id_bank = tb_bank.id_bank', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
    
    // public function banks($id_bank){
    //     $this->db->select('image, number, owner, link');
    //     $query = $this->db->get_where('tb_bank', array(
    //         'id_bank'    => $id_bank
    //     ));
    //     if($query->num_rows() > 0){
    //         return $query->row();
    //     }else{ 
    //         return false;
    //     }
    // }

    public function base_contact($name){
        $this->db->select('detail');
        $query = $this->db->get_where('tb_contact', array(
            'name'    => $name
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
}