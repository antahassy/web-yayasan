<style>
    .thead-dark tr th{
        color: #fff !important;
    }
    form label{
        margin-top: 5px;
        font-weight: 600 !important;
    }
    div i{
        font-size: 25px;
        color: #000041;
    }
    #va_content{
        display: none;
    }
</style>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <?php if (in_array('1', $akses_temp)){ ?>
                    <h5 class="font-weight-bold mt-2 mb-2 mr-5" id="menu_title"><?php echo $title ?></h5>
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                    <button class="btn btn-light-success font-weight-bolder btn-sm" id="btn_add">Add Data</button>
                <?php }else{ ?>
                    <h5 class="font-weight-bold mt-2 mb-2 mr-5" id="menu_title"><?php echo $title ?></h5>
                <?php } ?>
            </div>
            <div class="d-flex align-items-center">
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row" style="min-height: 68vh;">
                <div class="col-lg-12 col-xxl-4 order-1 order-xxl-2">
                    <div class="card card-custom card-stretch gutter-b">
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bolder text-dark"><?php echo $title ?> List</h3>
                        </div>
                        <div class="card-body pt-0">
                            <table id="table_data" class="table table-striped table-vcenter" style="width: 100%;">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Order_ID</th>
                                        <th>Session</th>
                                        <th>Donatur</th>
                                        <th>Donasi</th>
                                        <th>Metode</th>
                                        <th>Kategori</th>
                                        <th>Judul</th>
                                        <th>Tindakan</th>
                                        <th>Created</th>
                                        <th>Updated</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
            </div>
            <div class="modal-body"> 
                <form id="form_data">
                    <input type="hidden" name="id_data" id="id_data">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Judul Kasus</label>
                            <input type="hidden" name="id_kasus" id="id_kasus">
                            <input type="text" class="form-control" name="judul_kasus" id="judul_kasus" placeholder="Autocomplete">

                            <label>Metode</label>
                            <select class="form-control" name="s_metode" id="s_metode">
                                <option value="">Pilih Metode</option>
                                <option value="1">Virtual Account</option>
                                <option value="2">Transfer Manual</option>
                            </select>

                            <label>Bank</label>
                            <select class="form-control" name="s_bank" id="s_bank">
                                <option value="">Pilih Bank</option>
                            </select>

                            <div id="va_content">
                                <label>Nama Virtual Account</label>
                                <input type="text" name="va_name" id="va_name" class="form-control">

                                <label>Nomor Virtual Account</label>
                                <input type="text" name="va_number" id="va_number" class="form-control">
                            </div>

                            <label>Donasi</label>
                            <input type="text" name="donasi" id="donasi" class="form-control">
                            
                            <label>Tanggal</label>
                            <input type="text" name="tgl_donasi" id="tgl_donasi" class="form-control" style="cursor: pointer;">

                            <label>Status Donasi</label>
                            <select class="form-control" name="s_status" id="s_status">
                                <option value="">Pilih Status Donasi</option>
                                <option value="0">Pending</option>
                                <option value="1">Sukses</option>
                            </select>

                            <label>Doa (Opsional)</label>
                            <textarea name="doa" id="doa" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-primary" id="btn_process"></button>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="column_gambar_items" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content" style="background-color: transparent; border-color: transparent;">
            <div class="modal-header" style="display: contents;">
                <img id="image_column_items" src="" title="" style="background-color: #fff;">
            </div>
            <div style="text-align: center; width: 100%; position: fixed; bottom: 0;">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/b_donasi_non_medis.js?t=').mt_rand()?>"></script>
