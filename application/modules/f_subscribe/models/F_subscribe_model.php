<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_subscribe_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

    // Datatable server side processing
    public function s_side_datatables_query(){
        $this->db->select('
            tb_subscribe.id_subscribe,
            tb_subscribe.email,
            tb_subscribe.created_by,
            tb_subscribe.created_at,
            tb_subscribe.updated_by,
            tb_subscribe.updated_at
        ');
        $column_order = array(null, 
            'tb_subscribe.id_subscribe',
            'tb_subscribe.email',
            'tb_subscribe.created_by',
            'tb_subscribe.created_at',
            'tb_subscribe.updated_by',
            'tb_subscribe.updated_at'
        );
        $column_search = array(
            'tb_subscribe.id_subscribe',
            'tb_subscribe.email',
            'tb_subscribe.created_by',
            'tb_subscribe.created_at',
            'tb_subscribe.updated_by',
            'tb_subscribe.updated_at'
        );
        $i = 0;
        $this->db->where('tb_subscribe.deleted_at','');
        $this->db->from('tb_subscribe');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_subscribe.id_subscribe', 'asc');
        }
    }

    public function s_side_data(){
        $this->s_side_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered(){
        $this->s_side_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->where('tb_subscribe.deleted_at','');
        $this->db->from('tb_subscribe');
        return $this->db->count_all_results();
    }
    // End datatable server side processing
    public function subscriber(){
        $this->db->select('email');
        $query = $this->db->get_where('tb_subscribe', array(
            'deleted_at' => ''
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }
}
