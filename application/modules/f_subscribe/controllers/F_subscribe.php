<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_subscribe extends CI_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->helper('directory');
        date_default_timezone_set('Asia/Jakarta');
		$this->load->model('F_subscribe_model','model');
		require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
		if (! $this->ion_auth->logged_in()){
            redirect('auth', 'refresh');
        }
	}

    public $id_menu = '24';
    // public $mail_host = 'kaizenpeduli.com';
    // public $mail_username = 'noreply@kaizenpeduli.com';
    public $mail_host = 'peduliindonesia.org';
    public $mail_username = 'noreply@peduliindonesia.org';
    public $mail_password = '#5(9~-)ZV6j~';
    public $mail_dns = '103.147.154.41';

	public function index(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
            if (in_array('3', $akses_temp)){
                $data['akses_temp'] = $akses_temp;
                $data['title'] = 'Subscriber';
                $this->load->view('header', $data);
                $this->load->view('index');
                $this->load->view('footer');
            }else{
                echo "Access Denied";
            }
        }else{
            echo "Access Denied";
        }
	}

	public function s_side_data(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
        }

        $month_format = array (1 => 
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        );
        $list = $this->model->s_side_data();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->email;

            if($field->created_at != ''){
                $s_created = explode(' ', $field->created_at);
                $s_created_date = explode('-', $s_created[0]);
                $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<br>' . $s_created[1];
            }else{
                $row[] = '';
            }

            if($field->updated_at != ''){
                $s_updated = explode(' ', $field->updated_at);
                $s_updated_date = explode('-', $s_updated[0]);
                $row[] = $s_updated_date[2] .'/'. $month_format[(int)$s_updated_date[1]] .'/'. $s_updated_date[0] . '<br>' . $s_updated[1];
            }else{
                $row[] = '';
            }
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    public function info(){
        $data = $this->model->subscriber();
        $info = $this->input->post('info');
        for ($i = 0; $i < count($data); $i ++) {
            $mail   = new PHPMailer(); 
            $mail->IsSMTP(); 
            $mail->SMTPAuth     = true; 
            $mail->SMTPSecure   = "ssl";   
            $mail->Host         = $this->mail_host;      
            $mail->Port         = 465;                   
            $mail->Username     = $this->mail_username;  
            $mail->Password     = $this->mail_password;            
            $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
            $mail->isHTML(true);
            $mail->SMTPAutoTLS  = false;
            $mail->Hostname     = $this->mail_dns;
            $mail->Subject      = "Info";
            // $mail     = new PHPMailer(); 
            // $mail->IsSMTP(); 
            // $mail->SMTPAuth   = true; 
            // $mail->SMTPSecure = "ssl";   
            // $mail->Host       = "smtp.gmail.com";      
            // $mail->Port       = 465;                   
            // $mail->Username   = "ascoglobalindo@gmail.com";  
            // $mail->Password   = "Ascoglobal2021";            
            // $mail->SetFrom('ascoglobalindo@gmail.com', 'Kaizen Peduli Indonesia'); 
            // $mail->isHTML(true);
            // $mail->Subject    = "Pesan Anda";
            $mail->Body         = 
            '<!DOCTYPE html>
            <html> 
                <head>
                    <title></title>
                </head>
                <body>
                    <div style="text-align: justify;">' . $info . '</div>
                </body>
            </html>';
            $mail->AddAddress($data[$i]->email);
            $send = $mail->Send();
            if($i == (count($data) - 1)){
                $message['success'] = true;
                echo json_encode($message);
            }
        }
    }
}
