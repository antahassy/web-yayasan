<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	// Datatable server side processing
    public function s_side_datatables_query(){
        $this->db->select('
            menu.id_menu,
            menu.nama_menu,
            menu.url,
            menu.rel, 
            menu.rel2,
            menu.urutan, 
            menu.created_by,
            menu.created_at,
            menu.updated_by,
            menu.updated_at
        ');
        $column_order = array(null, 
            'menu.id_menu',
            'menu.nama_menu',
            'menu.url',
            'menu.rel',
            'menu.rel2',
            'menu.urutan',
            'menu.created_by',
            'menu.created_at',
            'menu.updated_by',
            'menu.updated_at'
        );
        $column_search = array(
            'menu.id_menu',
            'menu.nama_menu',
            'menu.url',
            'menu.rel',
            'menu.rel2',
            'menu.urutan',
            'menu.created_by',
            'menu.created_at',
            'menu.updated_by',
            'menu.updated_at'
        ); 
        $this->db->where('menu.is_trash','0');
        $this->db->where('menu.rel','0');
        $this->db->from('menu');
        $i = 0;
        foreach ($column_search as $item){
            if($_POST['search']['value']  != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('menu.urutan', 'asc');
        }
    }

    public function s_side_data(){
        $this->s_side_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered(){
        $this->s_side_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->where('menu.is_trash','0');
        $this->db->from('menu');
        return $this->db->count_all_results();
    }
    // End datatable server side processing

    public function rel_menu(){
    	$this->db->order_by('urutan','asc');
    	$query = $this->db->get_where('menu', array(
            'is_trash'  => '0',
            'rel'  => '0',
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

	public function get_dropdown_sub($rel){
		$this->db->order_by('urutan','asc');
    	$query = $this->db->get_where('menu', array(
            'is_trash'  => '0',
            'rel'  => $rel,
            'rel2'  => '0'
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
	}

	public function get_dropdown_sub2($rel,$rel2){
		$this->db->order_by('urutan','asc');
    	$query = $this->db->get_where('menu', array(
            'is_trash'  => '0',
            'rel'  => $rel,
            'rel2'  => $rel2
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
	}

	public function save($by){
        $data = array(
            'nama_menu'     => $this->input->post('nama'),
            'url'   		=> $this->input->post('url'),
            'rel' 			=> $this->input->post('s_rel'),
            'urutan' 		=> $this->input->post('urutan'),
            'is_trash' 		=> '0',
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => $by
        );
        $query = $this->db->insert('menu', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function edit($id){
        $this->db->select('
            id_menu,
            nama_menu,
            url,
            rel,
            urutan
        ');
        $this->db->where('id_menu', $id);
        $query = $this->db->get('menu');
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function update($id, $by){
        $data = array(
            'nama_menu'     => $this->input->post('nama'),
            'url'   		=> $this->input->post('url'),
            'rel' 			=> $this->input->post('s_rel'),
            'urutan' 		=> $this->input->post('urutan'),
            'is_trash' 		=> '0',
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by
        );
        $this->db->where('id_menu', $id);
        $this->db->update('menu', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function delete($id, $by){
        $data = array(
            'is_trash'    => '1',
            'deleted_by'    => $by
        );
        $this->db->where('id_menu', $id);
        $this->db->update('menu', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function delete_menu($id){
        $this->db->where('id_menu', $id);
        $this->db->delete('rel_group');
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false; 
        }
    }
}
