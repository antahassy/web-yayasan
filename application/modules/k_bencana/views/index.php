<style>
    .thead-dark tr th{
        color: #fff !important;
    }
    form label{
        margin-top: 5px;
        font-weight: 600 !important;
    }
    .table_detail tr td{
        padding: 2.5px 10px;
    }
    .modal-body{
        font-size: 15px !important;
    }
</style>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <?php if (in_array('1', $akses_temp)){ ?>
                    <h5 class="font-weight-bold mt-2 mb-2 mr-5" id="menu_title"><?php echo $title ?></h5>
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                    <button class="btn btn-light-success font-weight-bolder btn-sm" id="btn_add">Add Data</button>
                <?php }else{ ?>
                    <h5 class="font-weight-bold mt-2 mb-2 mr-5" id="menu_title"><?php echo $title ?></h5>
                <?php } ?>
            </div>
            <div class="d-flex align-items-center">
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row" style="min-height: 68vh;">
                <div class="col-lg-12 col-xxl-4 order-1 order-xxl-2">
                    <div class="card card-custom card-stretch gutter-b">
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bolder text-dark"><?php echo $title ?> List</h3>
                        </div>
                        <div class="card-body pt-0">
                            <table id="table_data" class="table table-striped table-vcenter" style="width: 100%;">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>No</th>
                                        <th>Penggalang</th>
                                        <th>Dana</th>
                                        <th>Kategori</th>
                                        <th>Judul</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                        <th>Hompage</th>
                                        <th>Created</th>
                                        <th>Updated</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_detail" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: justify;"> 
                <ol>
                    <li style="font-weight: 600; font-size: 18px; margin-top: 10px;">Identitas</li>
                    <table class="table_detail" border="1">
                        <tr>
                            <td>Pekerjaan</td>
                            <td id="detail_identitas_1"></td>
                        </tr>
                        <tr>
                            <td>Perusahaan/Sekolah/Lembaga</td>
                            <td id="detail_identitas_2"></td>
                        </tr>
                        <tr>
                            <td>Kontak Penggalang Dana</td>
                            <td id="detail_identitas_3"></td>
                        </tr>
                        <tr>
                            <td>Media Sosial</td>
                            <td id="detail_identitas_4"></td>
                        </tr>
                        <tr>
                            <td>Akun Media Sosial</td>
                            <td id="detail_identitas_5"></td>
                        </tr>
                        <tr>
                            <td>Domisili</td>
                            <td id="detail_identitas_6"></td>
                        </tr>
                        <tr>
                            <td>Tentang Penggalang</td>
                            <td id="detail_identitas_7"></td>
                        </tr>
                    </table>
                    <li style="font-weight: 600; font-size: 18px; margin-top: 10px;">Target Donasi</li>
                    <table class="table_detail" border="1">
                        <tr>
                            <td>Dana Yang Dibutuhkan</td>
                            <td id="detail_donasi_1"></td>
                        </tr>
                        <tr>
                            <td>Batas Akhir Galang Dana</td>
                            <td id="detail_donasi_2"></td>
                        </tr>
                        <tr>
                            <td>Sisa Waktu Galang Dana</td>
                            <td id="detail_donasi_3"></td>
                        </tr>
                        <tr>
                            <td>Keperluan Galang Dana</td>
                            <td id="detail_donasi_4"></td>
                        </tr>
                        <tr>
                            <td>Penerima Dana</td>
                            <td id="detail_donasi_5"></td>
                        </tr>
                        <tr>
                            <td>Rincian Penggunaan Dana</td>
                            <td id="detail_donasi_6"></td>
                        </tr>
                    </table>
                    <li style="font-weight: 600; font-size: 18px; margin-top: 10px;">Detail</li>
                    <table class="table_detail" border="1">
                        <tr>
                            <td>Kategori</td>
                            <td id="detail_detail_1"></td>
                        </tr>
                        <tr>
                            <td>Judul</td>
                            <td id="detail_detail_2"></td>
                        </tr>
                        <tr>
                            <td>Link</td>
                            <td id="detail_detail_3"></td>
                        </tr>
                        <tr>
                            <td>Tujuan</td>
                            <td id="detail_detail_4"></td>
                        </tr>
                        <tr>
                            <td>Foto Utama</td>
                            <td>
                                <img src="" id="detail_detail_5" style="width: 250px; cursor: pointer;" class="file_data">
                            </td>
                        </tr>
                    </table>
                    <li style="font-weight: 600; font-size: 18px; margin-top: 10px;">Cerita</li>
                    <div id="detail_cerita"></div>
                    <li style="font-weight: 600; font-size: 18px; margin-top: 10px;">Ajakan</li>
                    <div id="detail_ajakan"></div>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="column_gambar_items" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content" style="background-color: transparent; border-color: transparent;">
            <div class="modal-header" style="display: contents;">
                <img id="image_column_items" src="" title="" style="background-color: #fff;">
            </div>
            <div style="text-align: center; width: 100%; position: fixed; bottom: 0;">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_kabar_terbaru" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: justify;"> 
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_transfer" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: justify;"> 
                <ol>
                    <li style="font-weight: 600; font-size: 18px; margin-top: 10px;">KTP</li>
                    <div id="ktp_content"></div>
                    <li style="font-weight: 600; font-size: 18px; margin-top: 10px;">Kartu Keluarga</li>
                    <div id="kk_content"></div>
                    <li style="font-weight: 600; font-size: 18px; margin-top: 10px;">Buku Tabungan</li>
                    <div id="tabungan_content"></div>
                    <li style="font-weight: 600; font-size: 18px; margin-top: 10px;">Jumlah Donasi</li>
                    <div id="donasi_content"></div>
                    <li style="font-weight: 600; font-size: 18px; margin-top: 10px;">Jumlah Transfer</li>
                    <div id="transfer_content"></div>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/k_bencana.js?t=').mt_rand()?>"></script>
