<?php 
$setting = $this->Ref_model->ref_setting()->result_array();
$logo = $setting[2]['value'];
?>
<style type="text/css">
	.marker-cluster-small{
	  	background-color: transparent !important;
	}
	.marker-cluster-small div{
		background-color: rgba(0, 0, 255, 0.75) !important;
	}
	.legend{
		background-color: #fff;
		padding: 5px;
		border-radius: 5px;
	}
	/*.leaflet-marker-icon{
  		background: #fff;
  		border-radius: 100%;
  		padding: 5px;
  	}*/
  	.search_btn{
  		background-color: rgb(255,239,212) !important;
  		color: rgb(255,197,70) !important;
  		border-color: transparent !important;
  	}
  	#map{
  		width: 100%; 
  		height: 93vh; 
  		cursor: default;
  		z-index: 0;
  	}
  	.leaflet-control-attribution{
  		display: none;
  	}
  	@media screen and (max-width: 767px){
  		#map{
	  		height: 87vh; 
	  	}
  	}
</style>
<script type="text/javascript">
	var marker_logo = '<?php echo site_url('assets/project/'.$logo); ?>';
</script>

<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/map/css/leaflet.css?t=').mt_rand()?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/map/css/marker-cluster.css?t=').mt_rand()?>" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/map/css/marker-cluster-default.css?t=').mt_rand()?>" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/map/css/esri-leaflet-geocoder.css?t=').mt_rand()?>" />

<script src="<?php echo site_url('assets/map/js/leaflet.js?t=').mt_rand()?>"></script>
<script src="<?php echo site_url('assets/map/js/leaflet-ajax.js?t=').mt_rand()?>"></script>
<script src="<?php echo site_url('assets/map/js/marker-cluster.js?t=').mt_rand()?>"></script>
<script src="<?php echo site_url('assets/map/js/leaflet-button.js?t=').mt_rand()?>"></script>
<script src="<?php echo site_url('assets/map/js/esri-leaflet.js?t=').mt_rand()?>"></script>
<script src="<?php echo site_url('assets/map/js/esri-leaflet-geocoder.js?t=').mt_rand()?>"></script>

<div id="map"></div>
<div class="modal animated" id="modal_search" tabindex="-1" role="dialog" aria-hidden="true">
   	<div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
             <div class="modal-body">
                <div class="row">
                    <div class="col-md-6" style="margin: 5px 0;">
                        <select name="kategori_pencarian" id="kategori_pencarian" class="form-control">
                            <option value="">Pilih Kategori</option>
                            <option value="">Tanpa Kategori</option>
                            <option value="2">Kesehatan</option>
                            <option value="1">Anak Sakit</option>
                            <option value="3">Kemanusiaan</option>
                            <option value="4">Bencana</option>
                        </select>
                    </div>
                    <div class="col-md-6" style="margin: 5px 0;">
                        <select name="s_penggalangan" id="s_penggalangan" class="form-control" style="display: none;">
                            <option value="">Pilih Judul</option>
                        </select>
                        <input type="text" name="input_pencarian" id="input_pencarian" class="form-control" placeholder="Kata kunci" style="background-color: #fff; display: none;">
                        <input type="hidden" name="hidden_input_pencarian" id="hidden_input_pencarian" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal" style="background-color: rgb(255,239,212) !important; color: rgb(255,197,70) !important; border-color: transparent !important;">Tutup</button>
                <button type="button" class="btn btn-warning" id="btn_map_search">Cari</button>
            </div>
        </div>
   	</div>
</div>
<script type="text/javascript">
    var sum = (index, value) => index + value;
    function unique(list) {
        var result = [];
        $.each(list, function(i, e) {
            if($.inArray(e, result) == -1){
                result.push(e);
            }
        });
        return result;
    }
    var map = L.map('map', {
        center              : [-1.7, 120],
        zoom                : 5,
        doubleClickZoom     : false,
        minZoom             : 5,
        zoomControl			: false
    });
     //Map style
    var map_style = {
        color           : 'rgba(0,0,0,0.15)',
        weight          : 1,
        fillOpacity     : 1,
    }
    $(document).ready(function(){
        var map_kategori = '';
        var map_keyword = '';

        var request_count = 0;
        swal({
            showConfirmButton   : false,
            allowOutsideClick   : false,
            allowEscapeKey      : false,
            background          : 'transparent',
            onOpen  : function(){
                swal.showLoading();
                setTimeout(function(){
                    main_data(map_kategori, map_keyword);
                },500);
            }
        });
        map_btn();
        function map_btn(){
            var modal_search;
            $('#modal_search').on('show.bs.modal', function(){
                $(this).addClass('zoomIn');
                modal_search = true;
            });
            $('#modal_search').on('hide.bs.modal', function(){
                if(modal_search){
                    $(this).removeClass('zoomIn').addClass('zoomOut');
                    modal_search = false;
                    setTimeout(function(){
                        $('#modal_search').modal('hide');
                    },350);
                    return false;
                }
                $(this).removeClass('zoomOut');
            });
        }
        searching();
        function searching(){
            $('#kategori_pencarian').on('change', function(){
                if($(this).val() == '1'){//anak sakit
                    
                }else if($(this).val() == '2'){//kesehatan
                         
                }else if($(this).val() == '3'){//kemanusiaan
                         
                }else if($(this).val() == '4'){//bencana
                        
                }else{//tanpa kategori

                }
            });
           	$('#btn_map_search').on('click', function(){
                $('#modal_search').modal('hide');
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            map_kategori = $('#kategori_pencarian').val();
                            map_keyword = $('#hidden_input_pencarian').val();
                            main_data(map_kategori, map_keyword);
                        },500);
                    }
                });
           	});
        }
        function main_data(map_kategori, map_keyword){
           	$.ajax({
                type        : 'ajax',
                method      : 'get',
                url         : site + 'map/main_data',
                data        : {
                    kategori  : map_kategori,
                    keyword   : map_keyword,
                },
                async       : true,
                dataType    : 'json',
                success     : function(data){
                	//fungsi map data cek array lokasi provinsi
                	var lokasi_provinsi = [];
                	for(i = 0; i < data.length; i ++){
                		lokasi_provinsi.push({
                            id_provinsi   : data[i].id_provinsi,
                            id_kasus      : data[i].id_kasus,
                            total         : 1
                        });
                	}
                	var arr_max = [];
                	var list_judul = '<option value="">Pilih Judul</option>';
                	for(i = 0; i < data.length; i ++){
                		var max_data = [];
                        lokasi_provinsi.filter(function(item){
                             if(item.id_provinsi === data[i].id_provinsi){
                                  max_data.push(1);
                             }
                        });
                        if(max_data.length == 0){
                             max_data = 0;
                        }else{
                             max_data = max_data.reduce(sum);
                        }
                        arr_max.push(max_data);
                        list_judul += '<option value="' + data[i].id_kasus + '">' + data[i].judul + '</option>';
                    }
                    $('#s_penggalangan').html(list_judul);
                   	var max_data = Math.max(...unique(arr_max)); 
                   	request_count ++;

                   	var koordinat_kasus = '';
                   	// if(kd_penggalangan != ''){
                    //     masjid.filter(function(item){
                    //         if(item.id_masjid === kd_masjid){
                    //             if(typeof item.total === 'undefined'){
                    //                 koordinat_kasus = {
                    //                     lat     : Number(item.lat),
                    //                     lng     : Number(item.lng),
                    //                     total   : 0,
                    //                     alamat  : item.alamat,
                    //                     nama    : item.nama
                    //                 };
                    //             }else{
                    //                 koordinat_kasus = {
                    //                     lat     : Number(item.lat),
                    //                     lng     : Number(item.lng),
                    //                     total   : item.total,
                    //                     alamat  : item.alamat,
                    //                     nama    : item.nama
                    //                 };
                    //             }
                    //         }
                    //     });
                   	// }else{
                    //     koordinat_kasus = '';
                   	// }
                   	var filtered = false;
                    swal.close();
                    map_data(lokasi_provinsi, data, max_data, koordinat_kasus, filtered);
                },
                error       : function(){
                     swal({
                          background  : 'transparent',
                          html        : '<pre>Koneksi terputus</pre>',
                          type        : "warning"
                     });
                }
           });
        }
        var legend = '', search_btn = '', new_map = '', marker_kasus = '', link_kasus = ''; 
        var layerGroup = new L.layerGroup().addTo(map);
        function map_data(lokasi_provinsi, data, max_data, koordinat_kasus, filtered){
            if(request_count > 1){
                map.removeLayer(new_map);
                map.removeControl(legend);
                map.removeControl(search_btn);
            }
            if(marker_kasus != ''){
                map.removeLayer(marker_kasus);
            }
            layerGroup.clearLayers();
            var group_icon = L.icon({
                iconUrl        : marker_logo,
                iconSize       : [50, 50],
                iconAnchor     : [0, 50],
                popupAnchor    : [25, -45]
            });
            L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png').addTo(map);
            new_map = new L.GeoJSON.AJAX([site + 'assets/map/provinsi_indonesia.geojson'],{
                onEachFeature  : layer_control,
                style          : map_style
            }).addTo(map);

            var total_data = [];
            lokasi_provinsi.forEach(function(element){
                total_data.push(element.total);
            });
            if(total_data.length > 1){
                total_data = total_data.reduce(sum);
            }

            var searchControl = L.esri.Geocoding.geosearch({
			    position 		: 'topright',
			    placeholder 	: 'Cari Kota, Kecamatan, Kelurahan',
			    useMapBounds 	: false,
			    providers 		: [L.esri.Geocoding.arcgisOnlineProvider({
			      	apikey 		: 'AAPK742192433a95482899311445d5fcc9a6oPaR1oFT3ZK56MvUcoHXcR3t6nESnoD9iCL7mBBBgJP1OTMnRu8ddSIn050xW5MI',
			      	nearby 		: {
			        	lat: -1.7,
			        	lng: 120
			      	}
			    })]
		  	}).addTo(map);
		  	var results = L.layerGroup().addTo(map);
		  	searchControl.on('results', function (data) {
		    	results.clearLayers();
		    	for (var i = data.results.length - 1; i >= 0; i--) {
		      		results.addLayer(L.marker(data.results[i].latlng));
		    	}
		  	});

            if(koordinat_kasus != ''){
                // map.setView(new L.LatLng(koordinat_masjid.lat, koordinat_masjid.lng), 17);
                // marker_kasus = new L.marker([koordinat_masjid.lat, koordinat_masjid.lng], {
                //     icon: group_icon
                // }).bindPopup(koordinat_masjid.nama + '<br>' + koordinat_masjid.alamat + '<br><br>' + 'Jumlah Kajian : ' + Number(koordinat_masjid.total).toLocaleString('de') + ' (' + (Number(koordinat_masjid.total / total_data) * 100).toFixed(2) + '% dari total kajian)');
                //     marker_kasus.addTo(map).openPopup();
            }else{
                map.setView(new L.LatLng(-1.7, 120), 5);
            }

            var arr_legend = [], legend_row = '';
            if(max_data == 1){
                legend_row = 1;
                var max_each = max_data / legend_row;
                max_each = Number(max_each.toFixed(0));
                for(i = 0; i <= legend_row; i ++){
                    if(i == legend_row){
                        arr_legend.push({
                            min     : 1,
                            max     : 1,
                            color   : 'rgba(255,197,70, 0.5)'
                        });
                    }else{
                        arr_legend.push({
                            min     : 0,
                            max     : 0,
                            color   : 'rgba(0,0,0,0.15)'
                        });
                    }
                }
            }else if(max_data == 2){
                legend_row = 2;
                var max_each = max_data / legend_row;
                max_each = Number(max_each.toFixed(0));
                for(i = 0; i <= legend_row; i ++){
                    if(i == 0){
                        arr_legend.push({
	                        min     : 0,
	                        max     : 0,
	                        color   : 'rgba(0,0,0,0.15)'
        	            });
                    }else if(i == legend_row){
                      	arr_legend.push({
                           	min     : ((i - 1) * max_each) + 1,
                           	max     : max_data,
                           	color   : 'rgba(255,197,70, 1)'
                      	});
                    }else{
                        arr_legend.push({
                            min     : 1,
                            max     : max_each,
                            color   : 'rgba(255,197,70, 0.' + (i * 5) + ')'
                        });
                    }
                }
       		}else if(max_data == 3){
            	legend_row = 3;
            	var max_each = max_data / legend_row;
            	max_each = Number(max_each.toFixed(0));
            	for(i = 0; i <= legend_row; i ++){
                 	if(i == 0){
                      	arr_legend.push({
                           	min     : 0,
                           	max     : 0,
                           	color   : 'rgba(0,0,0,0.15)'
                      	});
                 	}else if(i == legend_row){
                      	arr_legend.push({
                           	min     : ((i - 1) * max_each) + 1,
                           	max     : max_data,
                           	color   : 'rgba(255,197,70, 1)'
                      	});
                 	}else if(i == 1){
                      	arr_legend.push({
                           	min     : 1,
                           	max     : max_each,
                           	color   : 'rgba(255,197,70, 0.' + (i * 33) + ')'
                      	});
                 	}else{
                      	arr_legend.push({
                           	min     : (i - 1) * max_each,
                           	max     : i * max_each,
                           	color   : 'rgba(255,197,70, 0.' + (i * 33) + ')'
                      	});
                 	}
            	}
            }else if(max_data == 4){
	            legend_row = 4;
	            var max_each = max_data / legend_row;
	            max_each = Number(max_each.toFixed(0));
	            for(i = 0; i <= legend_row; i ++){
                 	if(i == 0){
                      	arr_legend.push({
                           	min     : 0,
                           	max     : 0,
                           	color   : 'rgba(0,0,0,0.15)'
                      	});
                 	}else if(i == legend_row){
                      	arr_legend.push({
                           	min     : ((i - 1) * max_each) + 1,
                           	max     : max_data,
                           	color   : 'rgba(255,197,70, 1)'
                      	});
                 	}else if(i == 1){
                      	arr_legend.push({
                           	min     : 1,
                           	max     : max_each,
                           	color   : 'rgba(255,197,70, 0.' + (i * 25) + ')'
                      	});
                 	}else{
                      	arr_legend.push({
                           	min     : ((i - 1) * max_each) + 1,
                           	max     : i * max_each,
                           	color   : 'rgba(255,197,70, 0.' + (i * 25) + ')'
                      	});
                 	}
            	}
            }else if(max_data >= 5){
	            legend_row = 5;
	            var max_each = max_data / legend_row;
	            max_each = Number(max_each.toFixed(0));
	            for(i = 0; i <= legend_row; i ++){
                 	if(i == 0){
                      	arr_legend.push({
                           	min     : 0,
                           	max     : 0,
                           	color   : 'rgba(0,0,0,0.15)'
                      	});
                 	}else if(i == legend_row){
                      	arr_legend.push({
                           	min     : ((i - 1) * max_each) + 1,
                           	max     : max_data,
                           	color   : 'rgba(255,197,70, 1)'
                      	});
                 	}else if(i == 1){
                      	arr_legend.push({
                           	min     : 1,
                           	max     : max_each,
                           	color   : 'rgba(255,197,70, 0.' + (i * 2) + ')'
                      	});
                 	}else{
                      	arr_legend.push({
                           	min     : ((i - 1) * max_each) + 1,
                           	max     : i * max_each,
                           	color   : 'rgba(255,197,70, 0.' + (i * 2) + ')'
                      	});
                 	}
            	}
       		}
           	legend = new L.control({ position: "topleft" });
           	legend.onAdd = function(map) {
                var div = L.DomUtil.create("div", "legend");
                div.innerHTML += '<span>Total Penggalangan : ' + total_data + '</span><br>';
                for(i = 0; i < arr_legend.length; i ++){
                    if(arr_legend[i].min == arr_legend[i].max){
                        div.innerHTML += '<span style="background: ' + arr_legend[i].color + '; color: transparent;">__</span> <span>' + arr_legend[i].min + '</span><br>';
                    }else{
                        div.innerHTML += '<span style="background: ' + arr_legend[i].color + '; color: transparent;">__</span> <span>' + Number(arr_legend[i].min).toLocaleString('de') + ' - ' + Number(arr_legend[i].max).toLocaleString('de') + '</span><br>';
                    }
                }
                return div;
           	};
            legend.addTo(map);
            search_btn = new L.Control({
                position    : 'topright'
            });
            L.control.zoom({
			    position: 'topright'
			}).addTo(map);
            search_btn.onAdd = function(map) {
                var div = L.DomUtil.create("button", "search_btn btn btn-danger");
                // div.innerHTML += '<i class="fas fa-search"></i>';
                div.innerHTML += 'Cari';
                return div;
            };
            search_btn.addTo(map);
            $('.search_btn').on('click', function () {
                $('#modal_search').modal('show');
            });
            function layer_control(feature,layer){
                var data_provinsi = [];
                lokasi_provinsi.filter(function(item){
                    if(item.id_provinsi === feature.properties['id_provinsi'].toString()){
                        data_provinsi.push(item.total);
                    }
                });
                if(data_provinsi.length == 0){
                    data_provinsi = 0;
                }else{
                    data_provinsi = data_provinsi.reduce(sum);
                }

                var percent_provinsi = (data_provinsi / total_data) * 100;
                percent_provinsi = Number(percent_provinsi.toFixed(2));

                var popup_provinsi = L.DomUtil.create('div', 'content');
                popup_provinsi.innerHTML += 'Provinsi ' + feature.properties['Provinsi'] + '<br>';
                popup_provinsi.innerHTML += Number(data_provinsi).toLocaleString('de') + ' Penggalangan<br>';
                popup_provinsi.innerHTML += percent_provinsi + '% dari total penggalangan' + '<br>';
                popup_provinsi.innerHTML += '<div class="provinsi_detail" style="color: rgb(255,197,70); cursor: pointer; font-weight: 600; text-align: center;">Detail</div>';
                layer.bindPopup(popup_provinsi);
                if(marker_kasus != ''){
                    marker_kasus.on('mouseover', function () {
                        layer.setStyle({ 
                            color           : 'rgba(0, 255, 0, 0.15)',
                            weight          : 3,
                            fillOpacity     : 1,
                        });
                    });
                }
                layer.on('mouseover', function () {
                    layer.setStyle({
                        color           : 'rgba(0, 255, 0, 0.15)',
                        weight          : 3,
                        fillOpacity     : 1,
                    });
                });
                layer.on('mouseout', function () {
                    layer.setStyle({
                        color           : 'rgba(0,0,0,0.15)',
                        weight          : 3,
                        fillOpacity     : 1,
                    });
                });
                for(i = 0; i < arr_legend.length; i ++){
                    if(data_provinsi >= arr_legend[i].min && data_provinsi <= arr_legend[i].max){
                        var bg_layer = arr_legend[i].color;
                        layer.setStyle({
                            color           : bg_layer,
                            weight          : 3,
                            fillOpacity     : 1,
                        });
                        layer.on('mouseover', function () {
                            layer.setStyle({
                                color           : 'rgba(0, 255, 0, 0.15)',
                                weight          : 3,
                                fillOpacity     : 1,
                            });
                        });
                        layer.on('mouseout', function () {
                            layer.setStyle({
                                color           : bg_layer,
                                weight          : 3,
                                fillOpacity     : 1,
                            });
                        });
                    }
                }
                L.DomEvent.addListener(popup_provinsi, 'click', function(event){
                    var bounds = layer.getBounds();
                    var clicked_coordinat = bounds.getCenter();
                    map.setView(new L.LatLng(clicked_coordinat.lat, clicked_coordinat.lng), 8);
                    layer.setStyle({
                        color           : 'rgba(0, 255, 0, 0.15)',
                        weight          : 3,
                        fillOpacity     : 1,
                    });
                    layer.closePopup();
                    layerGroup.clearLayers();
                    var koordinat_provinsi = [];
                    data.filter(function(item){
                        if(item.id_provinsi === feature.properties['id_provinsi'].toString()){
                            koordinat_provinsi.push(item);
                        }
                    });
                    var markers = L.markerClusterGroup();
                    markers.on('clustermouseover', function(event) {
                        layer.setStyle({
                            color           : 'rgba(0, 255, 0, 0.15)',
                            weight          : 3,
                            fillOpacity     : 1,
                        });
                    });
                    for(i = 0; i < koordinat_provinsi.length; i ++){
                        var jumlah_data;
                        if(typeof koordinat_provinsi[i].total === 'undefined'){
                            jumlah_data = 0;
                        }else{
                            jumlah_data = koordinat_provinsi[i].total;
                        }
                        var percent_data = (jumlah_data / total_data) * 100;
                        percent_data = Number(percent_data.toFixed(2));

                        var popup_kasus = L.DomUtil.create('div', koordinat_provinsi[i].links);
                        popup_kasus.innerHTML += '<img src="' + koordinat_provinsi[i].sampul + '" style="width: 100%; margin-bottom: 5px;">' + '<br>';
                        popup_kasus.innerHTML += '<div style="text-align: center;"><b>' + koordinat_provinsi[i].judul + '</b></div>';
                        popup_kasus.innerHTML += koordinat_provinsi[i].terkumpul + '<br>';
                        popup_kasus.innerHTML += koordinat_provinsi[i].donatur + '<br>';
                        popup_kasus.innerHTML += koordinat_provinsi[i].sisa_waktu + '<br>';
                        popup_kasus.innerHTML += '<div class="' + koordinat_provinsi[i].links_donasi + '" style="background: rgb(255,197,70); color: #fff; font-weight: 600; cursor: pointer; text-align: center; width: 100%; padding: 5px 0;">Donasi</div>';

                        var marker = new L.marker([koordinat_provinsi[i].lat, koordinat_provinsi[i].lng], {
                            icon: group_icon
                        }).bindPopup(popup_kasus);
                        marker.on('dblclick', function(location){
                            map.setView(new L.LatLng(location.latlng.lat, location.latlng.lng), 17);
                        });
                        marker.on('mouseover', function () {
                            layer.setStyle({
                                color           : 'rgba(0, 255, 0, 0.15)',
                                weight          : 3,
                                fillOpacity     : 1,
                            });
                        });
                        markers.addLayer(marker);
                        L.DomEvent.addListener(popup_kasus, 'click', function(event){
                            link_kasus = $(event.target).attr('class');
                            event.preventDefault();
                            event.stopImmediatePropagation();
                            swal({
                                showConfirmButton   : false,
                                allowOutsideClick   : false,
                                allowEscapeKey      : false,
                                background          : 'transparent',
                                onOpen  : function(){
                                    swal.showLoading();
                                    setTimeout(function(){
                                        window.open(link_kasus, '_blank').focus();
                                        swal.close();
                                    },500);
                                }
                            });
                        });
                    }
                    markers.addTo(layerGroup);
                });
                layer.on('dblclick', function(event) {
                    var bounds = layer.getBounds();
                    var clicked_coordinat = bounds.getCenter();
                    map.setView(new L.LatLng(clicked_coordinat.lat, clicked_coordinat.lng), 8); 
                    layer.setStyle({
                        color           : 'rgba(0, 255, 0, 0.15)',
                        weight          : 3,
                        fillOpacity     : 1,
                    });
                    this.closePopup();
                    layerGroup.clearLayers();
                    var koordinat_provinsi = [];
                    data.filter(function(item){
                        if(item.id_provinsi === feature.properties['id_provinsi'].toString()){
                            koordinat_provinsi.push(item);
                        }
                    });
                    var markers = L.markerClusterGroup();
                    markers.on('clustermouseover', function(event) {
                        layer.setStyle({
                            color           : 'rgba(0, 255, 0, 0.15)',
                            weight          : 3,
                            fillOpacity     : 1,
                        });
                    });
                    for(i = 0; i < koordinat_provinsi.length; i ++){
                        var jumlah_data;
                        if(typeof koordinat_provinsi[i].total === 'undefined'){
                            jumlah_data = 0;
                        }else{
                            jumlah_data = koordinat_provinsi[i].total;
                        }
                        var percent_data = (jumlah_data / total_data) * 100;
                        percent_data = Number(percent_data.toFixed(2));

                        var popup_kasus = L.DomUtil.create('div', koordinat_provinsi[i].links);
                        popup_kasus.innerHTML += '<img src="' + koordinat_provinsi[i].sampul + '" style="width: 100%; margin-bottom: 5px;">' + '<br>';
                        popup_kasus.innerHTML += '<div style="text-align: center;"><b>' + koordinat_provinsi[i].judul + '</b></div>';
                        popup_kasus.innerHTML += koordinat_provinsi[i].terkumpul + '<br>';
                        popup_kasus.innerHTML += koordinat_provinsi[i].donatur + '<br>';
                        popup_kasus.innerHTML += koordinat_provinsi[i].sisa_waktu + '<br>';
                        popup_kasus.innerHTML += '<div class="' + koordinat_provinsi[i].links_donasi + '" style="background: rgb(255,197,70); color: #fff; font-weight: 600; cursor: pointer; text-align: center; width: 100%; padding: 5px 0;">Donasi</div>';

                        var marker = new L.marker([koordinat_provinsi[i].lat, koordinat_provinsi[i].lng], {
                            icon: group_icon
                        }).bindPopup(popup_kasus);
                        marker.on('dblclick', function(location){
                            map.setView(new L.LatLng(location.latlng.lat, location.latlng.lng), 17);
                        });
                        marker.on('mouseover', function () {
                            layer.setStyle({
                                color           : 'rgba(0, 255, 0, 0.15)',
                                weight          : 3,
                                fillOpacity     : 1,
                            });
                        });
                        markers.addLayer(marker);
                        L.DomEvent.addListener(popup_kasus, 'click', function(event){
                            link_kasus = $(event.target).attr('class');
                            event.preventDefault();
                            event.stopImmediatePropagation();
                            swal({
                                showConfirmButton   : false,
                                allowOutsideClick   : false,
                                allowEscapeKey      : false,
                                background          : 'transparent',
                                onOpen  : function(){
                                    swal.showLoading();
                                    setTimeout(function(){
                                        window.open(link_kasus, '_blank').focus();
                                        swal.close();
                                    },500);
                                }
                            });
                        });
                    }
                    markers.addTo(layerGroup);
                });
            }
        }
    });
</script>