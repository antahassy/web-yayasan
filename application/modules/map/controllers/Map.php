<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Map_model', 'model');
        require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
        date_default_timezone_set('Asia/Jakarta');
    }

    // public $mail_host = 'kaizenpeduli.com';
    // public $mail_username = 'noreply@kaizenpeduli.com';
    public $mail_host = 'peduliindonesia.org';
    public $mail_username = 'noreply@peduliindonesia.org';
    public $mail_password = '#5(9~-)ZV6j~';
    public $mail_dns = '103.147.154.41';

    public function index(){
        $data['title'] = 'Peta Sebaran';
        $data['slideshow'] = $this->model->all_slideshow();
        $this->load->view('header_front', $data);
        $this->load->view('index');
        $this->load->view('footer_front');
    }

    public function main_data(){
        $kategori = $this->input->get('kategori');
        $keyword = $this->input->get('keyword');
        // $kategori = '';
        // $keyword = '';
        if($kategori == ''){
            $arr_data = array();
            $medis = $this->model->map_medis($kategori, $keyword); 
            if($medis){
                foreach ($medis as $row) {
                    $row->sampul = site_url('assets/project/kasus_medis/' . $row->sampul);
                    $row->links = site_url('donasi/medis/' . $row->link);
                    $row->links_donasi = site_url('donasi/medis_help/' . $row->link);

                    $tgl1 = strtotime(date('Y-m-d')); 
                    $tgl2 = strtotime($row->jatuh_tempo); 
                    $selisih = $tgl2 - $tgl1;
                    $selisih = $selisih / 60 / 60 / 24;
                    $sisa_waktu = number_format($selisih , 0, ',', '.');
                    if((int)$sisa_waktu == 0){
                        $row->sisa_waktu = 'Hari terakhir';
                    }else{
                        $row->sisa_waktu = $sisa_waktu . ' Hari lagi';
                    }

                    $total_donasi = $this->model->total_donasi_medis($row->id_kasus);
                    if($total_donasi->donasi == null){
                        $row->terkumpul = 'Rp. 0';
                    }else{
                        $row->terkumpul = 'Rp. ' . number_format($total_donasi->donasi , 0, ',', '.');
                    }
                    $donatur = $this->model->total_donatur_medis($row->id_kasus);
                    $row->donatur = number_format($donatur , 0, ',', '.') . ' Donatur';
                    $row->id_kasus = 'm_' . $row->id_kasus;

                    array_push($arr_data, $row);
                }
            }
            $non_medis = $this->model->map_non_medis($kategori, $keyword); 
            if($non_medis){
                foreach ($non_medis as $row) {
                    $row->sampul = site_url('assets/project/kasus_non_medis/' . $row->sampul);
                    $row->links = site_url('donasi/non_medis/' . $row->link);
                    $row->links_donasi = site_url('donasi/non_medis_help/' . $row->link);

                    $tgl1 = strtotime(date('Y-m-d')); 
                    $tgl2 = strtotime($row->jatuh_tempo); 
                    $selisih = $tgl2 - $tgl1;
                    $selisih = $selisih / 60 / 60 / 24;
                    $sisa_waktu = number_format($selisih , 0, ',', '.');
                    if((int)$sisa_waktu == 0){
                        $row->sisa_waktu = 'Hari terakhir';
                    }else{
                        $row->sisa_waktu = $sisa_waktu . ' Hari lagi';
                    }

                    $total_donasi = $this->model->total_donasi_non_medis($row->id_kasus);
                    if($total_donasi->donasi == null){
                        $row->terkumpul = 'Rp. 0';
                    }else{
                        $row->terkumpul = 'Rp. ' . number_format($total_donasi->donasi , 0, ',', '.');
                    }
                    $donatur = $this->model->total_donatur_non_medis($row->id_kasus);
                    $row->donatur = number_format($donatur , 0, ',', '.') . ' Donatur';
                    $row->id_kasus = 'nm_' . $row->id_kasus;

                    array_push($arr_data, $row);
                }
            }
            if(count($arr_data) == 0){
                $data = false;
            }else{
                $data = $arr_data;
            }
        }else if($kategori == '1'){//anak sakit
            $data = $this->model->map_medis($kategori, $keyword); 
            foreach ($data as $row) {
                $row->sampul = site_url('assets/project/kasus_medis/' . $row->sampul);
                $row->links = site_url('donasi/medis/' . $row->link);
                $row->links_donasi = site_url('donasi/medis_help/' . $row->link);

                $tgl1 = strtotime(date('Y-m-d')); 
                $tgl2 = strtotime($row->jatuh_tempo); 
                $selisih = $tgl2 - $tgl1;
                $selisih = $selisih / 60 / 60 / 24;
                $sisa_waktu = number_format($selisih , 0, ',', '.');
                if((int)$sisa_waktu == 0){
                    $row->sisa_waktu = 'Hari terakhir';
                }else{
                    $row->sisa_waktu = $sisa_waktu . ' Hari lagi';
                }

                $total_donasi = $this->model->total_donasi_medis($row->id_kasus);
                if($total_donasi->donasi == null){
                    $row->terkumpul = 'Rp. 0';
                }else{
                    $row->terkumpul = 'Rp. ' . number_format($total_donasi->donasi , 0, ',', '.');
                }
                $donatur = $this->model->total_donatur_medis($row->id_kasus);
                $row->donatur = number_format($donatur , 0, ',', '.') . ' Donatur';
                $row->id_kasus = 'm_' . $row->id_kasus;
            }
        }else if($kategori == '2'){//kesehatan
            $data = $this->model->map_medis($kategori, $keyword); 
            foreach ($data as $row) {
                $row->sampul = site_url('assets/project/kasus_medis/' . $row->sampul);
                $row->links = site_url('donasi/medis/' . $row->link);
                $row->links_donasi = site_url('donasi/medis_help/' . $row->link);

                $tgl1 = strtotime(date('Y-m-d')); 
                $tgl2 = strtotime($row->jatuh_tempo); 
                $selisih = $tgl2 - $tgl1;
                $selisih = $selisih / 60 / 60 / 24;
                $sisa_waktu = number_format($selisih , 0, ',', '.');
                if((int)$sisa_waktu == 0){
                    $row->sisa_waktu = 'Hari terakhir';
                }else{
                    $row->sisa_waktu = $sisa_waktu . ' Hari lagi';
                }

                $total_donasi = $this->model->total_donasi_medis($row->id_kasus);
                if($total_donasi->donasi == null){
                    $row->terkumpul = 'Rp. 0';
                }else{
                    $row->terkumpul = 'Rp. ' . number_format($total_donasi->donasi , 0, ',', '.');
                }
                $donatur = $this->model->total_donatur_medis($row->id_kasus);
                $row->donatur = number_format($donatur , 0, ',', '.') . ' Donatur';
                $row->id_kasus = 'm_' . $row->id_kasus;
            }
        }else if($kategori == '3'){//kemanusiaan
            $data = $this->model->map_non_medis($kategori, $keyword); 
            foreach ($data as $row) {
                $row->sampul = site_url('assets/project/kasus_non_medis/' . $row->sampul);
                $row->links = site_url('donasi/non_medis/' . $row->link);
                $row->links_donasi = site_url('donasi/non_medis_help/' . $row->link);

                $tgl1 = strtotime(date('Y-m-d')); 
                $tgl2 = strtotime($row->jatuh_tempo); 
                $selisih = $tgl2 - $tgl1;
                $selisih = $selisih / 60 / 60 / 24;
                $sisa_waktu = number_format($selisih , 0, ',', '.');
                if((int)$sisa_waktu == 0){
                    $row->sisa_waktu = 'Hari terakhir';
                }else{
                    $row->sisa_waktu = $sisa_waktu . ' Hari lagi';
                }

                $total_donasi = $this->model->total_donasi_non_medis($row->id_kasus);
                if($total_donasi->donasi == null){
                    $row->terkumpul = 'Rp. 0';
                }else{
                    $row->terkumpul = 'Rp. ' . number_format($total_donasi->donasi , 0, ',', '.');
                }
                $donatur = $this->model->total_donatur_non_medis($row->id_kasus);
                $row->donatur = number_format($donatur , 0, ',', '.') . ' Donatur';
                $row->id_kasus = 'nm_' . $row->id_kasus;
            }
        }else if($kategori == '4'){//bencana
            $data = $this->model->map_non_medis($kategori, $keyword); 
            foreach ($data as $row) {
                $row->sampul = site_url('assets/project/kasus_non_medis/' . $row->sampul);
                $row->links = site_url('donasi/non_medis/' . $row->link);
                $row->links_donasi = site_url('donasi/non_medis_help/' . $row->link);

                $tgl1 = strtotime(date('Y-m-d')); 
                $tgl2 = strtotime($row->jatuh_tempo); 
                $selisih = $tgl2 - $tgl1;
                $selisih = $selisih / 60 / 60 / 24;
                $sisa_waktu = number_format($selisih , 0, ',', '.');
                if((int)$sisa_waktu == 0){
                    $row->sisa_waktu = 'Hari terakhir';
                }else{
                    $row->sisa_waktu = $sisa_waktu . ' Hari lagi';
                }

                $total_donasi = $this->model->total_donasi_non_medis($row->id_kasus);
                if($total_donasi->donasi == null){
                    $row->terkumpul = 'Rp. 0';
                }else{
                    $row->terkumpul = 'Rp. ' . number_format($total_donasi->donasi , 0, ',', '.');
                }
                $donatur = $this->model->total_donatur_non_medis($row->id_kasus);
                $row->donatur = number_format($donatur , 0, ',', '.') . ' Donatur';
                $row->id_kasus = 'nm_' . $row->id_kasus;
            }
        }

        if($data){
            echo json_encode($data);
        }else{
            $empty[] = array();
            echo json_encode($empty);
        }
    }
}