<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map_model extends CI_Model {

    public function __construct(){
        parent::__construct(); 
        $this->db = $this->load->database('default', TRUE);
        date_default_timezone_set('Asia/Jakarta');
    }

    public function all_slideshow(){
        $this->db->order_by('tb_slideshow.id_slideshow', 'desc');
        $this->db->select('image');
        $query = $this->db->get_where('tb_slideshow', array(
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function map_medis($kategori, $keyword){
        $this->db->order_by('tb_kasus_medis.id_kasus_medis', 'asc');
        $this->db->select(' 
            tb_kasus_medis.id_kasus_medis as id_kasus,
            tb_kasus_medis.alamat_lokasi,
            tb_kasus_medis.lat,
            tb_kasus_medis.lng,
            tb_kasus_medis.dana as donasi,
            tb_kasus_medis.judul,
            tb_kasus_medis.link,
            tb_kasus_medis.sampul,
            tb_kasus_medis.jatuh_tempo,
            tb_kasus_medis.id_provinsi,
            tb_provinsi.nama as provinsi,
            tb_kota.nama as kota,
            tb_kecamatan.nama as kecamatan,
            tb_kelurahan.nama as kelurahan,
        ');
        $this->db->where('tb_kasus_medis.status', '1');
        $this->db->where('tb_kasus_medis.admin_verify', '1');
        if($kategori != ''){
            $this->db->where('tb_kasus_medis.kategori', $keyword);
        }
        if($keyword != ''){
            $this->db->like('tb_kasus_medis.judul', $keyword, 'both');
            $this->db->or_like('tb_kasus_medis.cerita', $keyword, 'both');
        }
        $this->db->from('tb_kasus_medis');
        $this->db->join('tb_provinsi', 'tb_kasus_medis.id_provinsi = tb_provinsi.id_provinsi', 'left');
        $this->db->join('tb_kota', 'tb_kasus_medis.id_kota = tb_kota.id_kota', 'left');
        $this->db->join('tb_kecamatan', 'tb_kasus_medis.id_kecamatan = tb_kecamatan.id_kecamatan', 'left');
        $this->db->join('tb_kelurahan', 'tb_kasus_medis.id_kelurahan = tb_kelurahan.id_kelurahan', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function map_non_medis($kategori, $keyword){
        $this->db->order_by('tb_kasus_non_medis.id_kasus_non_medis', 'asc');
        $this->db->select(' 
            tb_kasus_non_medis.id_kasus_non_medis as id_kasus,
            tb_kasus_non_medis.alamat_lokasi,
            tb_kasus_non_medis.lat,
            tb_kasus_non_medis.lng,
            tb_kasus_non_medis.donasi,
            tb_kasus_non_medis.judul,
            tb_kasus_non_medis.link,
            tb_kasus_non_medis.sampul,
            tb_kasus_non_medis.jatuh_tempo,
            tb_kasus_non_medis.id_provinsi,
            tb_provinsi.nama as provinsi,
            tb_kota.nama as kota,
            tb_kecamatan.nama as kecamatan,
            tb_kelurahan.nama as kelurahan,
        ');
        $this->db->where('tb_kasus_non_medis.status', '1');
        $this->db->where('tb_kasus_non_medis.admin_verify', '1');
        if($kategori == '3'){
            $this->db->where('tb_kasus_non_medis.kategori', '1');
        }
        if($kategori == '4'){
            $this->db->where('tb_kasus_non_medis.kategori', '2');
        }
        if($keyword != ''){
            $this->db->like('tb_kasus_non_medis.judul', $keyword, 'both');
            $this->db->or_like('tb_kasus_non_medis.cerita', $keyword, 'both');
        }
        $this->db->from('tb_kasus_non_medis');
        $this->db->join('tb_provinsi', 'tb_kasus_non_medis.id_provinsi = tb_provinsi.id_provinsi', 'left');
        $this->db->join('tb_kota', 'tb_kasus_non_medis.id_kota = tb_kota.id_kota', 'left');
        $this->db->join('tb_kecamatan', 'tb_kasus_non_medis.id_kecamatan = tb_kecamatan.id_kecamatan', 'left');
        $this->db->join('tb_kelurahan', 'tb_kasus_non_medis.id_kelurahan = tb_kelurahan.id_kelurahan', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function total_donatur_medis($id_kasus_medis){
        $this->db->where('tb_donasi_medis.status','1');
        $this->db->where('tb_donasi_medis.deleted_at','');
        $this->db->where('tb_donasi_medis.id_kasus_medis', $id_kasus_medis);
        $this->db->from('tb_donasi_medis');
        return $this->db->count_all_results();
    }

    public function total_donasi_medis($id_kasus_medis){
        $this->db->select_sum('donasi');
        $query = $this->db->get_where('tb_donasi_medis', array(
            'id_kasus_medis'    => $id_kasus_medis,
            'status'            => '1',
            'deleted_at'            => '',
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function total_donatur_non_medis($id_kasus_non_medis){
        $this->db->where('tb_donasi_non_medis.status','1');
        $this->db->where('tb_donasi_non_medis.deleted_at','');
        $this->db->where('tb_donasi_non_medis.id_kasus_non_medis', $id_kasus_non_medis);
        $this->db->from('tb_donasi_non_medis');
        return $this->db->count_all_results();
    }

    public function total_donasi_non_medis($id_kasus_non_medis){
        $this->db->select_sum('donasi');
        $query = $this->db->get_where('tb_donasi_non_medis', array(
            'id_kasus_non_medis'    => $id_kasus_non_medis,
            'status'                => '1',
            'deleted_at'            => '',
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
}