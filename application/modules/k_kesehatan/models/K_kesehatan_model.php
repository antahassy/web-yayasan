<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class K_kesehatan_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

    // Datatable server side processing
    public function s_side_datatables_query(){
        $this->db->select('
            tb_kasus_medis.id_kasus_medis,
            tb_kasus_medis.dana,
            tb_kasus_medis.judul,
            tb_kasus_medis.sampul,
            tb_kasus_medis.status,
            tb_kasus_medis.admin_verify,
            tb_kasus_medis.homepage,
            tb_kasus_medis.created_by,
            tb_kasus_medis.created_at,
            tb_kasus_medis.updated_by,
            tb_kasus_medis.updated_at,
            users.username,
            users.nama
        ');
        $column_order = array(null, 
            'tb_kasus_medis.id_kasus_medis',
            'tb_kasus_medis.dana',
            'tb_kasus_medis.judul',
            'tb_kasus_medis.sampul',
            'tb_kasus_medis.status',
            'tb_kasus_medis.admin_verify',
            'tb_kasus_medis.homepage',
            'tb_kasus_medis.created_by',
            'tb_kasus_medis.created_at',
            'tb_kasus_medis.updated_by',
            'tb_kasus_medis.updated_at',
            'users.username',
            'users.nama'
        );
        $column_search = array(
            'tb_kasus_medis.id_kasus_medis',
            'tb_kasus_medis.dana',
            'tb_kasus_medis.judul',
            'tb_kasus_medis.sampul',
            'tb_kasus_medis.status',
            'tb_kasus_medis.admin_verify',
            'tb_kasus_medis.homepage',
            'tb_kasus_medis.created_by',
            'tb_kasus_medis.created_at',
            'tb_kasus_medis.updated_by',
            'tb_kasus_medis.updated_at',
            'users.username',
            'users.nama'
        );
        $i = 0;
        $this->db->where('tb_kasus_medis.deleted_at','');
        $this->db->where('tb_kasus_medis.kategori','2');
        $this->db->from('tb_kasus_medis');
        $this->db->join('users', 'tb_kasus_medis.id_user = users.id', 'left');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_kasus_medis.id_kasus_medis', 'desc');
        }
    }

    public function s_side_data(){
        $this->s_side_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered(){
        $this->s_side_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->where('tb_kasus_medis.deleted_at','');
        $this->db->where('tb_kasus_medis.kategori','2');
        $this->db->from('tb_kasus_medis');
        return $this->db->count_all_results();
    }
    // End datatable server side processing

    public function detail($id_kasus){
        $this->db->select('
            tb_kasus_medis.*,
            users.username as user_email,
            users.nama as user_nama
        ');
        $this->db->where('tb_kasus_medis.id_kasus_medis', $id_kasus);
        $this->db->from('tb_kasus_medis');
        $this->db->join('users', 'tb_kasus_medis.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function delete($id, $by){
        $data = array(
            'deleted_at'    => date('Y-m-d H:i:s'),
            'deleted_by'    => $by
        );
        $this->db->where('id_kasus_medis', $id);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function verify($id, $by){
        $data = array(
            'admin_verify'  => '1',
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by
        );
        $this->db->where('id_kasus_medis', $id);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function public_case($id_kasus){
        $data = array('homepage' => '1');
        $this->db->where('id_kasus_medis', $id_kasus);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function privat_case($id_kasus){
        $data = array('homepage' => '0');
        $this->db->where('id_kasus_medis', $id_kasus);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function unverif_news($id_kasus){
        $this->db->where('tb_kabar_medis.id_kasus_medis',$id_kasus);
        $this->db->where('tb_kabar_medis.deleted_at','');
        $this->db->where('tb_kabar_medis.status','0');
        $this->db->from('tb_kabar_medis');
        return $this->db->count_all_results();
    }

    public function kabar_terbaru($id_kasus){
        $this->db->order_by('id_kabar_medis', 'asc');
        $this->db->select('
            id_kabar_medis,
            judul,
            status,
            kabar_terbaru,
            created_at,
            updated_at
        ');
        $query = $this->db->get_where('tb_kabar_medis', array(
            'id_kasus_medis' => $id_kasus,
            'deleted_at' => ''
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function verifikasi_kabar_terbaru($id_kabar, $by){
        $data = array(
            'status'        => '1',
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by
        );
        $this->db->where('id_kabar_medis', $id_kabar);
        $this->db->update('tb_kabar_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function delete_kabar_terbaru($id_kabar, $by){
        $data = array(
            'deleted_at'    => date('Y-m-d H:i:s'),
            'deleted_by'    => $by
        );
        $this->db->where('id_kabar_medis', $id_kabar);
        $this->db->update('tb_kabar_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
    
    public function update_dokumen_medis($id, $by, $image){
        $data = array(
            'dokumen_medis'     => $image,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_medis', $id);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function update_hasil_pemeriksaan($id, $by, $image){
        $data = array(
            'hasil_pemeriksaan' => $image,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_medis', $id);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
    
    public function base_contact($name){
        $this->db->select('detail');
        $query = $this->db->get_where('tb_contact', array(
            'name'    => $name
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
    
    public function total_donasi_medis($id_kasus_medis){
        $this->db->select_sum('donasi');
        $query = $this->db->get_where('tb_donasi_medis', array(
            'id_kasus_medis'    => $id_kasus_medis,
            'status'            => '1',
            'deleted_at'            => '',
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
    
    public function get_transfer($id_kasus){
        $this->db->select('file_rekening, file_ktp, file_kk');
        $query = $this->db->get_where('tb_kasus_medis', array(
            'id_kasus_medis'    => $id_kasus,
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
}
