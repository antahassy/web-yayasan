<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_model extends CI_Model {

    public function __construct(){
        parent::__construct(); 
        $this->db = $this->load->database('default', TRUE);
        date_default_timezone_set('Asia/Jakarta');
    }

    public function check_email($reg_email){
        $this->db->select('id, email, nama, password');
        $query = $this->db->get_where('users', array('email' => $reg_email));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function delete_user($id_user){
        $this->db->where('id', $id_user);
        $this->db->delete('users');
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function delete_user_group($id_user){
        $this->db->where('user_id', $id_user);
        $this->db->delete('users_groups');
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function verif($id_user){
        $query = $this->db->get_where('users', array('id' => $id_user));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function verification($id_user, $username){
        $data = array(
            'active'        => 1,
            'logged_in'     => date('Y-m-d H:i:s'),
            'updated_by'    => $username,
            'updated_at'    => date('Y-m-d H:i:s'),
        );
        $this->db->where('id', $id_user);
        $this->db->update('users', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function register($email){
        $phone_val = str_replace('-', '', $this->input->post('phone'));
        $data = array(
            'nama'          => $this->input->post('name'),
            'email'         => $email,
            'phone'         => $phone_val,
            'username'      => $email,
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => $email,
            'active'        => 0
        );
        $query = $this->db->insert('users', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function save_user_grup($id_user, $s_level){
        $data = array(
            'user_id'                   => $id_user,
            'group_id'                  => $s_level
        );
        $query = $this->db->insert('users_groups', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function check_account($identity){
    	$query = $this->db->get_where('users', array('username' => $identity));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
    }

    public function update_ip($username, $ip_address){
        $data = array(
            'ip_address'  => $ip_address,
            'logged_in'  => date('Y-m-d H:i:s'),
        );
        $this->db->where('username', $username);
        $this->db->update('users', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
    
    public function check_subscribe($email){
        $this->db->select('email');
        $query = $this->db->get_where('tb_subscribe', array('email' => $email));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function subscribe($email){
        $data = array(
            'email'         => $email,
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => $email
        );
        $query = $this->db->insert('tb_subscribe', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }
    
    public function list_medis(){
        $this->db->select('
            tb_kasus_medis.id_kasus_medis,
            tb_kasus_medis.jatuh_tempo,
            tb_kasus_medis.judul,
            users.username,
            users.nama
        ');
        $this->db->where('tb_kasus_medis.status', '1');
        $this->db->where('tb_kasus_medis.deleted_at', '');
        $this->db->from('tb_kasus_medis');
        $this->db->join('users', 'tb_kasus_medis.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function list_non_medis(){
        $this->db->select('
            tb_kasus_non_medis.id_kasus_non_medis,
            tb_kasus_non_medis.jatuh_tempo,
            tb_kasus_non_medis.judul,
            users.username,
            users.nama
        ');
        $this->db->where('tb_kasus_non_medis.status', '1');
        $this->db->where('tb_kasus_non_medis.deleted_at', '');
        $this->db->from('tb_kasus_non_medis');
        $this->db->join('users', 'tb_kasus_non_medis.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function medis_expired($id_kasus){
        $data = array(
            'status'        => '2',
            'updated_by'    => 'System',
            'updated_at'    => date('Y-m-d H:i:s'),
        );
        $this->db->where('id_kasus_medis', $id_kasus);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function non_medis_expired($id_kasus){
        $data = array(
            'status'        => '2',
            'updated_by'    => 'System',
            'updated_at'    => date('Y-m-d H:i:s'),
        );
        $this->db->where('id_kasus_non_medis', $id_kasus);
        $this->db->update('tb_kasus_non_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
    
    public function base_contact($name){
        $this->db->select('detail');
        $query = $this->db->get_where('tb_contact', array(
            'name'    => $name
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function total_donasi_medis($id_kasus_medis){
        $this->db->select_sum('donasi');
        $query = $this->db->get_where('tb_donasi_medis', array(
            'id_kasus_medis'    => $id_kasus_medis,
            'status'            => '1',
            'deleted_at'            => '',
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function total_donasi_non_medis($id_kasus_non_medis){
        $this->db->select_sum('donasi');
        $query = $this->db->get_where('tb_donasi_non_medis', array(
            'id_kasus_non_medis'    => $id_kasus_non_medis,
            'status'                => '1',
            'deleted_at'            => '',
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
}