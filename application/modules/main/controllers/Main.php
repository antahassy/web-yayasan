<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct(){
		parent::__construct();
      	$this->load->model('Main_model', 'model');
      	require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
      	date_default_timezone_set('Asia/Jakarta');
	}
	
	// public $mail_host = 'kaizenpeduli.com';
    // public $mail_username = 'noreply@kaizenpeduli.com';
    public $mail_host = 'peduliindonesia.org';
    public $mail_username = 'noreply@peduliindonesia.org';
    public $mail_password = '#5(9~-)ZV6j~';
    public $mail_dns = '103.147.154.41';

    public function register(){
        $email = $this->input->post('email');
        $checking = $this->model->check_email($email);
        if($checking){
            $message['email'] = true;
            echo json_encode($message);
        }else{
            $password = $this->input->post('password');
            $s_level = '2';
            $data   = $this->model->register($email); 
            if($data){
                $user = $this->model->check_email($email);
                $this->ion_auth->reset_password($email, $password);
                $level_grup = $this->model->save_user_grup($user->id, $s_level);
                if($level_grup){
                    $ids = $this->model->check_email($email);
                    
                    $base_email = 'Email';
                    $d_email = $this->model->base_contact($base_email);
                    $base_email = $d_email->detail;

                    $base_telepon = 'Telepon';
                    $d_telepon = $this->model->base_contact($base_telepon);
                    $base_telepon = $d_telepon->detail;
                    
                    $mail   = new PHPMailer(); 
                    $mail->IsSMTP(); 
                    $mail->SMTPAuth     = true; 
                    $mail->SMTPSecure   = "ssl";   
                    $mail->Host         = $this->mail_host;      
                    $mail->Port         = 465;                   
                    $mail->Username     = $this->mail_username;  
                    $mail->Password     = $this->mail_password;            
                    $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
                    $mail->isHTML(true);
                    $mail->SMTPAutoTLS  = false;
                    $mail->Hostname     = $this->mail_dns;
                    $mail->Subject      = "Verifikasi";
                    // $mail       = new PHPMailer(); 
                    // $mail->IsSMTP(); 
                    // $mail->SMTPAuth   = true; 
                    // $mail->SMTPSecure = "ssl";   
                    // $mail->Host       = "smtp.gmail.com";      
                    // $mail->Port       = 465;                   
                    // $mail->Username   = "ascoglobalindo@gmail.com";  
                    // $mail->Password   = "Ascoglobal2021";            
                    // $mail->SetFrom('ascoglobalindo@gmail.com', 'Kaizen Peduli Indonesia'); 
                    // $mail->isHTML(true);
                    // $mail->Subject    = "Verifikasi";
                    $mail->Body       = 
                    '<!DOCTYPE html>
                    <html>
                        <head>
                             <title></title>
                        </head>
                        <body>
                            <div> 
                              <div style="width: 100%;">
                                   <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
                              </div>
                              <br>
                              <br>
                              <div>Hallo Sahabat Peduli Indonesia,</div>
                              <div>Akun kamu akan segera aktif, klik link dibawah ini untuk verifikasi email sekarang</div>
                              <a href="'.site_url('main/verification/'.$ids->id).'" target="_blank"><b>' . $ids->password . '</b></a>
                              <br>
                              <div>Dengan melakukan verifikasi email, kamu bisa mulai membuat penggalangan dana, melakukan donasi, dan fitur-fitur lainnya di website Peduli Indonesia</div>
                              <br>
                              <div>Selamat bergabung dan terimakasih karena telah ikut peduli kepada sesama</div>
                              <br>
                              <div style="width: 100%; text-align: center;">
                                <div>Salam,</div>
                                <div style="font-weight: 600;">Peduli Indonesia</div>
                              </div>
                              <br>
                              <div style="width: 100%; text-align: center;">
                                    <div style="width: auto; padding: 0 10px; display: inline-block;">
                                        <a href="https://www.facebook.com/kaizenpeduliindonesia" target="_blank">
                                            <img src="https://imgur.com/TiFD2VP.png" style="height: 30px; cursor: pointer;">
                                        </a>
                                    </div>
                                    <div style="width: auto; padding: 0 10px; display: inline-block;">
                                        <a href="https://www.instagram.com/peduliindo" target="_blank">
                                            <img src="https://imgur.com/aCbCLu1.png" style="height: 30px; cursor: pointer;">
                                        </a>
                                    </div>
                                    <div style="width: auto; padding: 0 10px; display: inline-block;">
                                        <a href="https://web.whatsapp.com/send?phone=' . preg_replace('/0/', '62', $base_telepon, 1) . '" target="_blank">
                                            <img src="https://imgur.com/ClkBQ1z.png" style="height: 30px; cursor: pointer;">
                                        </a>
                                    </div>
                              </div>
                              <br>
                              <div style="width: 100%; text-align: center;">
                                    <div>Email : ' . $base_email . '</div>
                                    <div>Whatsapp : ' . preg_replace('/0/', '+62 ', $base_telepon, 1) . '</div>
                              </div>
                            </div>
                        </body>
                    </html>';
                    $mail->AddAddress($email);
                    $sent = $mail->Send();
                    if($sent){
                        $message['success'] = true;
                        echo json_encode($message);
                    }else{
                        $this->model->delete_user($ids->id);
                        $this->model->delete_user_group($ids->id);
                        $message['unsent'] = true;
                        echo json_encode($message);
                    }
                }
            }
        }
    }

    public function verification($code){
        $verif   = $this->model->verif($code);
        if($verif){
            $verification = $this->model->verification($code, $verif->username);
            $login = $this->ion_auth->set_session($verif);
            redirect(site_url());
        }
    }

    public function search_email(){
        $email = $this->input->post('email');
        $checking = $this->model->check_email($email);
        if($checking){
            echo json_encode($checking);
        }else{
            $message['empty'] = true;
            echo json_encode($message);
        }
    }

    public function recovery(){
        $email = $this->input->post('email');
        $checking = $this->model->check_email($email);
        if($checking){
            $pw_user = $checking->password;
            $nm_user = $checking->nama;
            $id_user = $checking->id;
            $session = array(
                'sess_hash'      => $pw_user
            );
            $this->session->set_userdata($session);
            
            $base_email = 'Email';
            $d_email = $this->model->base_contact($base_email);
            $base_email = $d_email->detail;

            $base_telepon = 'Telepon';
            $d_telepon = $this->model->base_contact($base_telepon);
            $base_telepon = $d_telepon->detail;
            
            $mail   = new PHPMailer(); 
            $mail->IsSMTP(); 
            $mail->SMTPAuth     = true; 
            $mail->SMTPSecure   = "ssl";   
            $mail->Host         = $this->mail_host;      
            $mail->Port         = 465;                   
            $mail->Username     = $this->mail_username;  
            $mail->Password     = $this->mail_password;            
            $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
            $mail->isHTML(true);
            $mail->SMTPAutoTLS  = false;
            $mail->Hostname     = $this->mail_dns;
            $mail->Subject      = "Pemulihan Password";
            // $mail     = new PHPMailer(); 
            // $mail->IsSMTP(); 
            // $mail->SMTPAuth   = true; 
            // $mail->SMTPSecure = "ssl";   
            // $mail->Host       = "smtp.gmail.com";      
            // $mail->Port       = 465;                   
            // $mail->Username   = "ascoglobalindo@gmail.com";  
            // $mail->Password   = "Ascoglobal2021";            
            // $mail->SetFrom('ascoglobalindo@gmail.com', 'Kaizen Peduli Indonesia'); 
            // $mail->isHTML(true);
            // $mail->Subject    = "Pemulihan Password";
            $mail->Body       = 
            '<!DOCTYPE html>
            <html>
                <head>
                     <title></title>
                </head>
                <body>
                    <div> 
                        <div style="width: 100%;">
                            <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
                        </div>
                        <br>
                        <br>
                        <div>Hai <b>' . $nm_user . '</b></div>
                        <div>Jika kamu lupa password, kamu dapat me-reset ulang password kamu dengan klik link dibawah ini</div>
                        <a href="'.site_url('recovery').'" target="_blank"><b>' . $pw_user . '</b></a>
                        <br>
                        <div>Abaikan pesan ini jika kamu tidak pernah meminta untuk reset password</div>
                        <br>
                        <div style="width: 100%; text-align: center;">
                            <div>Salam,</div>
                            <div style="font-weight: 600;">Peduli Indonesia</div>
                          </div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://www.facebook.com/kaizenpeduliindonesia" target="_blank">
                                        <img src="https://imgur.com/TiFD2VP.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://www.instagram.com/peduliindo" target="_blank">
                                        <img src="https://imgur.com/aCbCLu1.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://web.whatsapp.com/send?phone=' . preg_replace('/0/', '62', $base_telepon, 1) . '" target="_blank">
                                        <img src="https://imgur.com/ClkBQ1z.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                          </div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div>Email : ' . $base_email . '</div>
                                <div>Whatsapp : ' . preg_replace('/0/', '+62 ', $base_telepon, 1) . '</div>
                          </div>
                    </div>
                </body>
            </html>';
            $mail->AddAddress($email);
            $sent = $mail->Send();
            if($sent){
                $message['success'] = true;
                echo json_encode($message);
            }else{
                $message['unsent'] = true;
                echo json_encode($message);
            }
        }
    }

	public function login_captcha(){
    	$path = './assets/captcha/';
        $text = array_merge(range('A', 'Z'), range('0', '9'), range('a', 'z'));
        $random = shuffle($text);
        $random_str = substr(implode($text), 0, 5);
        $this->data['random_str'] = $random_str;
        $captcha_sess = array('captcha_sess' => $random_str);
        $this->session->set_userdata($captcha_sess);
        $vals = array(
            'word'          => $random_str,
            'img_path'      => $path,
            'img_url'       => base_url() . 'assets/captcha/',
            'img_width'     => '150',
            'img_height'    => 50,
            'expiration'    => 7200, 
            'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(0, 0, 0),
                'text' => array(0, 0, 0),
                'grid' => array(170, 240, 20)
            )
        );
        $cap = create_captcha($vals);
        $data['captcha'] = $cap['image'];
        echo json_encode($data);
    }

	public function login_process(){
		$captcha = $this->input->post('g-recaptcha-response');
		// $captcha = $this->input->post('captcha');
		$username = $this->input->post('login_email');
		$password = $this->input->post('login_password');
		$ip_address = $this->input->post('ip_address');

		// if($captcha != $this->session->userdata('captcha_sess')){
		//     $message['captcha_false'] = true; 
		// 	echo json_encode($message);
		// }
        if(! $captcha){
            $message['captcha_false'] = true; 
            echo json_encode($message);
        }else{
            $secret_key = "6Ld3FLodAAAAAGfb29WRT8OkW50dHwa4ajDElIHQ";
            $google_link_validation = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secret_key) .  '&response=' . urlencode($captcha);
            $google_response_validation = file_get_contents($google_link_validation);
            $google_response_key = json_decode($google_response_validation,true);
            if($google_response_key["success"]){
                $check_account = $this->model->check_account($username);
                if($check_account){
                    $status = $check_account->active;
                    if($status == 1){
                        $process = $this->ion_auth->login($username, $password);
                        if($process){
                            $this->model->update_ip($username, $ip_address);
                            $message['success'] = true; 
                            echo json_encode($message);
                        }else{
                            $message['match'] = true;
                            echo json_encode($message); 
                        }   
                    }else{
                        $message['inactive'] = true;
                        echo json_encode($message);
                    }
                }else{
                    $message['username'] = true;
                    echo json_encode($message);
                }
            }
		}
	}

	public function logout_process(){
		$process = $this->ion_auth->logout();
		if($process){
			$message['success'] = true;
            echo json_encode($message);
		}
	}

    public function web_captcha(){
        $path = './assets/captcha/';
        $text = array_merge(range('A', 'Z'), range('0', '9'), range('a', 'z'));
        $random = shuffle($text);
        $random_str = substr(implode($text), 0, 5);
        $this->data['random_str'] = $random_str;
        $captcha_web = array('captcha_web' => $random_str);
        $this->session->set_userdata($captcha_web);
        $vals = array(
            'word'          => $random_str,
            'img_path'      => $path,
            'img_url'       => base_url() . 'assets/captcha/',
            'img_width'     => '150',
            'img_height'    => 50,
            'expiration'    => 7200, 
            'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(0, 0, 0),
                'text' => array(0, 0, 0),
                'grid' => array(170, 240, 20)
            )
        );
        $cap = create_captcha($vals);
        $data['captcha'] = $cap['image'];
        echo json_encode($data);
    }
    
    public function subscribe(){
        $email = $this->input->post('email');
        $check = $this->model->check_subscribe($email);
        if($check){
            $message['subscriber'] = true;
            echo json_encode($message);
        }else{
            $data = $this->model->subscribe($email);
            if($data){
                $message['success'] = true;
                echo json_encode($message);
            }
        }
    }
    
    public function medis_expired(){
        ini_set('memory_limit', '1024M');
        $email = 'Email';
        $d_email = $this->model->base_contact($email);
        $email = $d_email->detail;

        $telepon = 'Telepon';
        $d_telepon = $this->model->base_contact($telepon);
        $telepon = $d_telepon->detail;

        $tanggal = date('Y-m-d');
        $list = $this->model->list_medis();
        if($list){
            foreach($list as $row){
                $tgl1 = strtotime($tanggal); 
                $tgl2 = strtotime($row->jatuh_tempo); 
                $selisih = $tgl2 - $tgl1;
                $selisih = $selisih / 60 / 60 / 24;
                if($selisih < 0){
                    $expired = $this->model->medis_expired($row->id_kasus_medis);
                    $total_d = $this->model->total_donasi_medis($row->id_kasus_medis);
                    if($total_d->donasi == null){
                        $donation = '0';
                    }else{
                        $donation = number_format($total_d->donasi , 0, ',', '.');
                    }
                    $mail   = new PHPMailer(); 
                    $mail->IsSMTP(); 
                    $mail->SMTPAuth     = true; 
                    $mail->SMTPSecure   = "ssl";   
                    $mail->Host         = $this->mail_host;      
                    $mail->Port         = 465;                   
                    $mail->Username     = $this->mail_username;  
                    $mail->Password     = $this->mail_password;            
                    $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
                    $mail->isHTML(true);
                    $mail->SMTPAutoTLS  = false;
                    $mail->Hostname     = $this->mail_dns;
                    $mail->Subject      = "Galang Dana";
                    $mail->Body         = 
                    '<!DOCTYPE html>
                    <html>
                        <head>
                             <title></title>
                        </head>
                        <body>
                            <div> 
                              <div style="width: 100%;">
                                   <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
                              </div>
                              <br>
                              <br>
                              <div style="width: 100%; text-align: center; text-transform: capitalize;">Hi ' . $row->nama . ',</div>
                              <br>
                              <div style="width: 100%; text-align: center;">Penggalangan dana kamu dengan judul</div>
                              <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize; font-size: 18px;">' . $row->judul . '</div>
                              <div style="width: 100%; text-align: center;">Sudah berakhir</div>
                              <div style="width: 100%; text-align: center;">Dana yang berhasil dikumpulkan sejumlah :</div>
                              <br>
                              <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize; font-size: 18px;">Rp. ' . $donation . '</div>
                              <br><div style="width: 100%; text-align: center;">
                                    <div>Harap segera lengkapi KTP, KK, No Rekening</div>
                                    <div>Pada halaman riwayat galang dana untuk memproses pencairan donasi</div>
                              </div>
                              <br>
                              <br>
                              <div style="width: 100%; text-align: center;"><a href="'.site_url('home/riwayat_galang_dana').'" class="btn btn-warning" target="_blank" style="border-radius: 10px; background: rgb(255,197,70); padding: 10px; color: #fff; text-decoration: none; font-weight: 600;">Lihat Riwayat</a></div>
                              <br>
                              <br>
                              <div style="width: 100%; text-align: center;">
                                    <div>Salam,</div>
                                    <div style="font-weight: 600;">Peduli Indonesia</div>
                              </div>
                              <br>
                              <div style="width: 100%; text-align: center;">
                                    <div style="width: auto; padding: 0 10px; display: inline-block;">
                                        <a href="https://www.facebook.com/kaizenpeduliindonesia" target="_blank">
                                            <img src="https://imgur.com/TiFD2VP.png" style="height: 30px; cursor: pointer;">
                                        </a>
                                    </div>
                                    <div style="width: auto; padding: 0 10px; display: inline-block;">
                                        <a href="https://www.instagram.com/peduliindo" target="_blank">
                                            <img src="https://imgur.com/aCbCLu1.png" style="height: 30px; cursor: pointer;">
                                        </a>
                                    </div>
                                    <div style="width: auto; padding: 0 10px; display: inline-block;">
                                        <a href="https://web.whatsapp.com/send?phone=' . preg_replace('/0/', '62', $telepon, 1) . '" target="_blank">
                                            <img src="https://imgur.com/ClkBQ1z.png" style="height: 30px; cursor: pointer;">
                                        </a>
                                    </div>
                              </div>
                              <br>
                              <div style="width: 100%; text-align: center;">
                                    <div>Email : ' . $email . '</div>
                                    <div>Whatsapp : ' . preg_replace('/0/', '+62 ', $telepon, 1) . '</div>
                              </div>
                            </div>
                        </body>
                    </html>';
                    $mail->AddAddress($row->username);
                    $mail->Send();
                }
            }
        }
    }

    public function non_medis_expired(){
        ini_set('memory_limit', '1024M');
        $email = 'Email';
        $d_email = $this->model->base_contact($email);
        $email = $d_email->detail;

        $telepon = 'Telepon';
        $d_telepon = $this->model->base_contact($telepon);
        $telepon = $d_telepon->detail;

        $tanggal = date('Y-m-d');
        $list = $this->model->list_non_medis();
        if($list){
            foreach($list as $row){
                $tgl1 = strtotime($tanggal); 
                $tgl2 = strtotime($row->jatuh_tempo); 
                $selisih = $tgl2 - $tgl1;
                $selisih = $selisih / 60 / 60 / 24;
                if($selisih < 0){
                    $expired = $this->model->non_medis_expired($row->id_kasus_non_medis);
                    $total_d = $this->model->total_donasi_non_medis($row->id_kasus_non_medis);
                    if($total_d->donasi == null){
                        $donation = '0';
                    }else{
                        $donation = number_format($total_d->donasi , 0, ',', '.');
                    }
                    $mail   = new PHPMailer(); 
                    $mail->IsSMTP(); 
                    $mail->SMTPAuth     = true; 
                    $mail->SMTPSecure   = "ssl";   
                    $mail->Host         = $this->mail_host;      
                    $mail->Port         = 465;                   
                    $mail->Username     = $this->mail_username;  
                    $mail->Password     = $this->mail_password;            
                    $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
                    $mail->isHTML(true);
                    $mail->SMTPAutoTLS  = false;
                    $mail->Hostname     = $this->mail_dns;
                    $mail->Subject      = "Galang Dana";
                    $mail->Body         = 
                    '<!DOCTYPE html>
                    <html>
                        <head>
                             <title></title>
                        </head>
                        <body>
                            <div> 
                              <div style="width: 100%;">
                                   <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
                              </div>
                              <br>
                              <br>
                              <div style="width: 100%; text-align: center; text-transform: capitalize;">Hi ' . $row->nama . ',</div>
                              <br>
                              <div style="width: 100%; text-align: center;">Penggalangan dana kamu dengan judul</div>
                              <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize; font-size: 18px;">' . $row->judul . '</div>
                              <div style="width: 100%; text-align: center;">Sudah berakhir</div>
                              <div style="width: 100%; text-align: center;">Dana yang berhasil dikumpulkan sejumlah :</div>
                              <br>
                              <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize; font-size: 18px;">Rp. ' . $donation . '</div>
                              <br><div style="width: 100%; text-align: center;">
                                    <div>Harap segera lengkapi KTP, KK, No Rekening</div>
                                    <div>Pada halaman riwayat galang dana untuk memproses pencairan donasi</div>
                              </div>
                              <br>
                              <br>
                              <div style="width: 100%; text-align: center;"><a href="'.site_url('home/riwayat_galang_dana').'" class="btn btn-warning" target="_blank" style="border-radius: 10px; background: rgb(255,197,70); padding: 10px; color: #fff; text-decoration: none; font-weight: 600;">Lihat Riwayat</a></div>
                              <br>
                              <br>
                              <div style="width: 100%; text-align: center;">
                                    <div>Salam,</div>
                                    <div style="font-weight: 600;">Peduli Indonesia</div>
                              </div>
                              <br>
                              <div style="width: 100%; text-align: center;">
                                    <div style="width: auto; padding: 0 10px; display: inline-block;">
                                        <a href="https://www.facebook.com/kaizenpeduliindonesia" target="_blank">
                                            <img src="https://imgur.com/TiFD2VP.png" style="height: 30px; cursor: pointer;">
                                        </a>
                                    </div>
                                    <div style="width: auto; padding: 0 10px; display: inline-block;">
                                        <a href="https://www.instagram.com/peduliindo" target="_blank">
                                            <img src="https://imgur.com/aCbCLu1.png" style="height: 30px; cursor: pointer;">
                                        </a>
                                    </div>
                                    <div style="width: auto; padding: 0 10px; display: inline-block;">
                                        <a href="https://web.whatsapp.com/send?phone=' . preg_replace('/0/', '62', $telepon, 1) . '" target="_blank">
                                            <img src="https://imgur.com/ClkBQ1z.png" style="height: 30px; cursor: pointer;">
                                        </a>
                                    </div>
                              </div>
                              <br>
                              <div style="width: 100%; text-align: center;">
                                    <div>Email : ' . $email . '</div>
                                    <div>Whatsapp : ' . preg_replace('/0/', '+62 ', $telepon, 1) . '</div>
                              </div>
                            </div>
                        </body>
                    </html>';
                    $mail->AddAddress($row->username);
                    $mail->Send();
                }
            }
        }
    }
}