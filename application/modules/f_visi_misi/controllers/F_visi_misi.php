<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_visi_misi extends CI_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->helper('directory');
        date_default_timezone_set('Asia/Jakarta');
		$this->load->model('F_visi_misi_model','model');
		if (! $this->ion_auth->logged_in()){
            redirect('auth', 'refresh');
        }
	}

    public $id_rule = '5';
    public $id_menu = '23';

	public function index(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
            if (in_array('3', $akses_temp)){
                $data['akses_temp'] = $akses_temp;
                $data['title'] = 'Visi Misi';
                $this->load->view('header', $data);
                $this->load->view('index');
                $this->load->view('footer');
            }else{
                echo "Access Denied";
            }
        }else{
            echo "Access Denied";
        }
	}

    public function main(){
        $month_format = array (1 => 
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        );
        $data = $this->model->main($this->id_rule);
        if($data->updated_at == ''){
            $s_created = explode(' ', $data->created_at);
            $s_created_date = explode('-', $s_created[0]);
            $data->last_update = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . ' ' . $s_created[1];
        }else{
            $s_updated = explode(' ', $data->updated_at);
            $s_updated_date = explode('-', $s_updated[0]);
            $data->last_update = $s_updated_date[2] .'/'. $month_format[(int)$s_updated_date[1]] .'/'. $s_updated_date[0] . ' ' . $s_updated[1];
        }
        unset($data->created_at);
        unset($data->updated_at);
        echo json_encode($data);
    }

    public function update(){
        $by = $this->ion_auth->user()->row()->username;
        $data = $this->model->update($this->id_rule, $by);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function editor_upload_img(){
        $config['upload_path']   = './assets/project/visi_misi';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image')){
            $file   = array('image' => $this->upload->data());
            $image  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/visi_misi/'.$image;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['new_image']        = './assets/project/visi_misi/'.$image;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            echo site_url('assets/project/visi_misi/'.$image);
        }
    }

    public function editor_delete_img(){
        $src = $this->input->post('src');
        $file_name = str_replace(base_url(), '', $src);
        if(unlink($file_name))
        {
            echo 'File Delete Successfully';
        }
    }
}
