<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_kontak extends CI_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->helper('directory');
        date_default_timezone_set('Asia/Jakarta');
		$this->load->model('F_kontak_model','model');
		if (! $this->ion_auth->logged_in()){
            redirect('auth', 'refresh');
        }
	}

    public $id_menu = '7';

	public function index(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
            if (in_array('3', $akses_temp)){
                $data['akses_temp'] = $akses_temp;
                $data['title'] = 'Kontak';
                $this->load->view('header', $data);
                $this->load->view('index');
                $this->load->view('footer');
            }else{
                echo "Access Denied";
            }
        }else{
            echo "Access Denied";
        }
	}

	public function s_side_data(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
        }

        $month_format = array (1 => 
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        );
        $list = $this->model->s_side_data();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<div><i class="' . $field->icon . '" style="font-size: 35px; color: #000041"></i></div>';
            $row[] = $field->name;
            $row[] = $field->detail;

            if (in_array('2', $akses_temp) && in_array('4', $akses_temp)){
                $row[] = '<button data="' . $field->id_contact . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_edit" title="Edit" style="margin: 2.5px;">Edit</button> <button data="' . $field->id_contact . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" title="Delete" style="margin: 2.5px;">Delete</button>';
            }
            if (! in_array('2', $akses_temp) && in_array('4', $akses_temp)){
                $row[] = '<button data="' . $field->id_contact . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" title="Delete" style="margin: 2.5px;">Delete</button>';
            }
            if (in_array('2', $akses_temp) && ! in_array('4', $akses_temp)){
                $row[] = '<button data="' . $field->id_contact . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_edit" title="Edit" style="margin: 2.5px;">Edit</button>';
            }
            if (! in_array('2', $akses_temp) && ! in_array('4', $akses_temp)){
                $row[] = '';
            }

            if($field->created_at != ''){
                $s_created = explode(' ', $field->created_at);
                $s_created_date = explode('-', $s_created[0]);
                $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<br>' . $s_created[1];
            }else{
                $row[] = '';
            }

            if($field->updated_at != ''){
                $s_updated = explode(' ', $field->updated_at);
                $s_updated_date = explode('-', $s_updated[0]);
                $row[] = $s_updated_date[2] .'/'. $month_format[(int)$s_updated_date[1]] .'/'. $s_updated_date[0] . '<br>' . $s_updated[1];
            }else{
                $row[] = '';
            }
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function save(){
        $by = $this->ion_auth->user()->row()->username;
        $data   = $this->model->save($by); 
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function edit(){
        $id_contact = $this->input->post('id_contact');
        $data = $this->model->edit($id_contact);
        echo json_encode($data);
    }

    public function update(){
        $by = $this->ion_auth->user()->row()->username;
        $id_contact = $this->input->post('id_data');
        $data   = $this->model->update($id_contact, $by);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function delete(){
        $jumlah = $this->model->count_all();
        if($jumlah > 1){
            $id_contact = $this->input->post('id_contact');
            $by = $this->ion_auth->user()->row()->username;
            $data = $this->model->delete($id_contact, $by); 
            if($data){
                $message['success'] = true;
                echo json_encode($message);
            }
        }else{
            $message['last'] = true;
            echo json_encode($message);
        }
    }
}
