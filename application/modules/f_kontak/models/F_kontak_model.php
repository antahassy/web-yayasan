<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_kontak_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

    // Datatable server side processing
    public function s_side_datatables_query(){
        $this->db->select('
            tb_contact.id_contact,
            tb_contact.name,
            tb_contact.detail,
            tb_contact.icon,
            tb_contact.created_by,
            tb_contact.created_at,
            tb_contact.updated_by,
            tb_contact.updated_at
        ');
        $column_order = array(null, 
            'tb_contact.id_contact',
            'tb_contact.name',
            'tb_contact.detail',
            'tb_contact.icon',
            'tb_contact.created_by',
            'tb_contact.created_at',
            'tb_contact.updated_by',
            'tb_contact.updated_at'
        );
        $column_search = array(
            'tb_contact.id_contact',
            'tb_contact.name',
            'tb_contact.detail',
            'tb_contact.icon',
            'tb_contact.created_by',
            'tb_contact.created_at',
            'tb_contact.updated_by',
            'tb_contact.updated_at'
        );
        $i = 0;
        $this->db->where('tb_contact.deleted_at','');
        $this->db->from('tb_contact');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_contact.id_contact', 'asc');
        }
    }

    public function s_side_data(){
        $this->s_side_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered(){
        $this->s_side_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->where('tb_contact.deleted_at','');
        $this->db->from('tb_contact');
        return $this->db->count_all_results();
    }
    // End datatable server side processing

    public function save($by){
        $data = array(
            'name'          => $this->input->post('contact_name'),
            'detail'        => $this->input->post('contact_detail'),
            // 'icon'          => $this->input->post('icon'),
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => $by
        );
        $query = $this->db->insert('tb_contact', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function edit($id_contact){
        $this->db->select('id_contact, name, detail, icon');
        $query = $this->db->get_where('tb_contact', array(
            'id_contact' => $id_contact
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function update($id_contact, $by){
        $data           = array(
            'name'          => $this->input->post('contact_name'),
            'detail'        => $this->input->post('contact_detail'),
            // 'icon'          => $this->input->post('icon'),
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by
        );
        $this->db->where('id_contact', $id_contact);
        $this->db->update('tb_contact', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function delete($id, $by){
        $data = array(
            'deleted_at'    => date('Y-m-d H:i:s'),
            'deleted_by'    => $by
        );
        $this->db->where('id_contact', $id);
        $this->db->update('tb_contact', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
}
