<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_syarat_ketentuan_model extends CI_Model {

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function main($id_rule){
        $this->db->select('description, created_at, updated_at');
        $query = $this->db->get_where('tb_rule', array(
            'id_rule' => $id_rule
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function update($id_rule, $by){
        $data           = array(
            'description'   => $this->input->post('description'),
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by
        );
        $this->db->where('id_rule', $id_rule);
        $this->db->update('tb_rule', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
}
