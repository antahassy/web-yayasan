<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kegiatan_model extends CI_Model {

    public function __construct(){
        parent::__construct(); 
        $this->db = $this->load->database('default', TRUE);
        date_default_timezone_set('Asia/Jakarta');
    }

    public function all_slideshow(){
        $this->db->order_by('tb_slideshow.id_slideshow', 'desc');
        $this->db->select('image');
        $query = $this->db->get_where('tb_slideshow', array(
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function data_kegiatan($link){
        $this->db->select('
            title,
            image,
            description,
            created_at
        ');
        $query = $this->db->get_where('tb_agenda', array(
            'title'             => $link,
            'type'              => '1',
            'status'            => '1',
            'deleted_at'        => ''
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
}