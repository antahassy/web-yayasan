<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kegiatan extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Kegiatan_model', 'model');
        require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
        date_default_timezone_set('Asia/Jakarta');
    }

    // public $mail_host = 'kaizenpeduli.com';
    // public $mail_username = 'noreply@kaizenpeduli.com';
    public $mail_host = 'peduliindonesia.org';
    public $mail_username = 'noreply@peduliindonesia.org';
    public $mail_password = '#5(9~-)ZV6j~';
    public $mail_dns = '103.147.154.41';

    public function _remap($link){
        if(is_numeric($link)){
            redirect(base_url());
        }else{
            $this->index($link);
        }
    }

    public function index($link){
        $s_link = explode('_', $link);
        $links = '';
        for($i = 0; $i < count($s_link); $i ++){
            if($i == (count($s_link) - 1)){
                $links .= $s_link[$i];
            }else{
                $links .= $s_link[$i] . ' ';
            }
        }
        $main_data = $this->model->data_kegiatan($links);
        if(! $main_data){
            redirect(site_url());
        }else{
            $month_format = array (1 => 
                'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            if($main_data->image != ''){
                $main_data->image = site_url('assets/project/kegiatan/' . $main_data->image);
            }

            $s_created = explode(' ', $main_data->created_at);
            $s_date = explode('-', $s_created[0]);
            $main_data->created_at = $s_date[2] .'/'. $month_format[(int)$s_date[1]] .'/'. $s_date[0] .' '. $s_created[1];

            $data['title'] = $main_data->title;
            $data['dd'] = $main_data;
            $data['slideshow'] = $this->model->all_slideshow();
            $this->load->view('header_front', $data);
            $this->load->view('index');
            $this->load->view('footer_front');
        }
    }
}