<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model {

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function update_1($id, $value, $by){
        $data = array(
            'value'         => $value,
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by,
        );
        $this->db->where('id_setting', $id);
        $this->db->update('setting', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function update_2($id, $value, $by){
        $data = array(
            'value'         => $value,
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by,
        );
        $this->db->where('id_setting', $id);
        $this->db->update('setting', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function update_3($id, $value, $by){
        $data = array(
            'value'         => $value,
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by,
        );
        $this->db->where('id_setting', $id);
        $this->db->update('setting', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function update_4($id, $value, $by){
        $data = array(
            'value'         => $value,
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by,
        );
        $this->db->where('id_setting', $id);
        $this->db->update('setting', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
}
