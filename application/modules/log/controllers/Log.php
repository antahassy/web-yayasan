<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Log_model', 'model');
        require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
        date_default_timezone_set('Asia/Jakarta');
    }
    
    // public $mail_host = 'kaizenpeduli.com';
    // public $mail_username = 'noreply@kaizenpeduli.com';
    public $mail_host = 'peduliindonesia.org';
    public $mail_username = 'noreply@peduliindonesia.org';
    public $mail_password = '#5(9~-)ZV6j~';
    public $mail_dns = '103.147.154.41';
    //Start Homepage
    public function list_homepage(){
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = array();
        $list_medis = $this->model->list_homepage_medis();
        if(count($list_medis) != 0){
            foreach($list_medis as $row){
                $row->kat = 'medis';
                array_push($list, $row);
            }
        }
        $list_non_medis = $this->model->list_homepage_non_medis();
        if(count($list_non_medis) != 0){
            foreach($list_non_medis as $row){
                $row->kat = 'nonmedis';
                array_push($list, $row);
            }
        }

        $total_medis = $this->model->total_list_homepage_medis();
        $total_non_medis = $this->model->total_list_homepage_non_medis();
        $totals = (int)$total_medis + (int)$total_non_medis;

        $filter_medis = $this->model->filter_list_homepage_medis();
        $filter_non_medis = $this->model->filter_list_homepage_non_medis();
        $filters = (int)$filter_medis + (int)$filter_non_medis;

        $data = array();
        for($i = 0; $i < count($list); $i ++) {
            $row = array();
            $doc = new DOMDocument();
            $doc->loadHTML($list[$i]->cerita);
            $items = array();
            foreach ($doc->getElementsByTagName('p') as $p) {
                $items[] = $p->nodeValue;
            }
            if(count($items) == 0){
                foreach ($doc->getElementsByTagName('div') as $p) {
                    $items[] = $p->nodeValue;
                }
            }
            $list[$i]->cerita = substr($items[0], 0, 200);

            $tgl1 = strtotime(date('Y-m-d')); 
            $tgl2 = strtotime($list[$i]->jatuh_tempo); 
            $selisih = $tgl2 - $tgl1;
            $selisih = $selisih / 60 / 60 / 24;
            $sisa_waktu = number_format($selisih , 0, ',', '.');
            if((int)$sisa_waktu == 0){
                $sisa_waktu = 'Hari terakhir';
            }else{
                $sisa_waktu = $sisa_waktu . ' Hari lagi';
            }
            if($list[$i]->kat == 'medis'){
                $path_img = site_url('assets/project/kasus_medis/' . $list[$i]->sampul);
                $link_case = site_url('donasi/medis/' . $list[$i]->link);
                $link_help = site_url('donasi/medis_help/' . $list[$i]->link);
                $s_donasi = $this->model->total_donasi_medis($list[$i]->id_kasus);
            }else{
                $path_img = site_url('assets/project/kasus_non_medis/' . $list[$i]->sampul);
                $link_case = site_url('donasi/non_medis/' . $list[$i]->link);
                $link_help = site_url('donasi/non_medis_help/' . $list[$i]->link);
                $s_donasi = $this->model->total_donasi_non_medis($list[$i]->id_kasus);
            }
            if($s_donasi->donasi == null){
                $tot_donasi = '0';
            }else{
                $tot_donasi = number_format($s_donasi->donasi , 0, ',', '.');
            }
            if(! $this->ion_auth->logged_in()){
                $row[] ='<div class="row">
                            <div class="col-md-12" style="padding: 10px;">
                                <div style="padding: 10px; background: rgb(255,239,212);">
                                    <div class="row" style="margin: 0;">
                                        <div class="col-lg-6" style="padding: 0;">
                                            <a href="' . $link_case . '" style="width: 100%;">
                                                <img src="' . $path_img . '" class="w-100">
                                            </a>
                                        </div>
                                        <div class="col-lg-6 text_col_search">
                                            <a href="' . $link_case . '" style="color: rgba(0,0,0,0.75); font-weight: 600;">' . $list[$i]->judul . '</a>
                                            <div style="margin-top: 15px;">' . $list[$i]->cerita . '</div><br>
                                            <div>Rp. ' . $tot_donasi . '</div>
                                            <div>' . $sisa_waktu . '</div>
                                            <a href="' . $link_help . '" class="btn btn-warning sess_logged_in" style="margin-top: 5px;">Donasi Sekarang</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
            }else{
                $row[] ='<div class="row">
                            <div class="col-md-12" style="padding: 10px;">
                                <div style="padding: 10px; background: rgb(255,239,212);">
                                    <div class="row" style="margin: 0;">
                                        <div class="col-lg-6" style="padding: 0;">
                                            <a href="' . $link_case . '" style="width: 100%;">
                                                <img src="' . $path_img . '" class="w-100">
                                            </a>
                                        </div>
                                        <div class="col-lg-6 text_col_search">
                                            <a href="' . $link_case . '" style="color: rgba(0,0,0,0.75); font-weight: 600;">' . $list[$i]->judul . '</a>
                                            <div style="margin-top: 15px;">' . $list[$i]->cerita . '</div><br>
                                            <div>Rp. ' . $tot_donasi . '</div>
                                            <div>' . $sisa_waktu . '</div>
                                            <a href="' . $link_help . '" class="btn btn-warning" style="margin-top: 5px;">Donasi Sekarang</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $totals,
            "recordsFiltered" => $filters,
            "data" => $data,
        );
        echo json_encode($output);
    }
    //End Homepage
    public function profil_update(){
        $id = $this->ion_auth->user()->row()->id;
        $by = $this->ion_auth->user()->row()->username;
        $password = $this->input->post('user_password');
        $data = $this->model->profil_update($id, $by);
        if($data){
            if($password != ''){
                $this->ion_auth->reset_password($by, $password);
                $this->ion_auth->logout();
                $message['chpass'] = true;
                echo json_encode($message);
            }else{
                $message['success'] = true;
                echo json_encode($message);
            }
        }
    }

    public function profil_update_image(){
        $id = $this->ion_auth->user()->row()->id;
        $by = $this->ion_auth->user()->row()->username;
        $config['upload_path']   = './assets/project/user/image';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('foto_profil')){
            $file   = array('image' => $this->upload->data());
            $image  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/user/image/'.$image;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['width']            = 800;
            $config['height']           = 800;
            $config['new_image']        = './assets/project/user/image/'.$image;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $data   = $this->model->profil_update_image($id, $by, $image); 
            if($data){
                $last_image = $this->input->post('get_foto_profil');
                $delete_image = $this->input->post('delete_foto_profil');
                if($last_image != ''){
                    unlink('./assets/project/user/image/'.$last_image);
                    $message['success'] = $image;
                    echo json_encode($message);
                }else if($delete_image != ''){
                    unlink('./assets/project/user/image/'.$delete_image);
                    $message['success'] = $image;
                    echo json_encode($message);
                }else{
                    $message['success'] = $image;
                    echo json_encode($message);
                }
            }
        }else{
            $last_image = $this->input->post('get_foto_profil');
            $delete_image = $this->input->post('delete_foto_profil');
            if($last_image == '' && $delete_image != ''){
                unlink('./assets/project/user/image/'.$delete_image);
                $image  = '';
                $data   = $this->model->profil_update_image($id, $by, $image);
                if($data){
                    $message['success'] = $image;
                    echo json_encode($message);
                }
            }else if($last_image == '' && $delete_image == ''){
                $image  = '';
                $data   = $this->model->profil_update_image($id, $by, $image);
                if($data){
                    $message['success'] = $image;
                    echo json_encode($message);
                }
            }else{
                $image  = $last_image;
                $data   = $this->model->profil_update_image($id, $by, $image);
                if($data){
                    $message['success'] = $image;
                    echo json_encode($message);
                }
            }
        }
    }

    public function riwayat_donasi_medis(){
        $id_user = $this->ion_auth->user()->row()->id;
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = $this->model->riwayat_donasi_medis($id_user);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = 'm_' . $field->id_donasi_medis;

            if($field->created_at != ''){
                $s_created = explode(' ', $field->created_at);
                $s_created_date = explode('-', $s_created[0]);
                $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<br>' . $s_created[1];
            }else{
                $row[] = '';
            }

            $row[] = 'Rp.' . number_format($field->donasi , 0, ',', '.');

            $row[] = '<a href="' . site_url('donasi/medis/' . $field->link) . '" target="_blank" style="text-transform: capitalize; font-weight: 600; color: rgb(255,197,70);">' . $field->judul . '</a>';
            
            if($field->status == '0'){
                $row[] = '<div>Pending</div><a style="margin-top: 5px;" href="#" class="btn btn-warning btn_status" id="' . $field->id_donasi_medis . '">Cek Status</a>';
            }
            if($field->status == '1'){
                $row[] = '<div>Sukses</div><a style="margin-top: 5px;" href="#" class="btn btn-warning btn_status" id="' . $field->id_donasi_medis . '">Cek Status</a>';
            }
            if($field->status == '2'){
                $row[] = '<div>Kadaluarsa</div><a style="margin-top: 5px;" href="#" class="btn btn-warning btn_status" id="' . $field->id_donasi_medis . '">Cek Status</a>';
            }
            
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->total_riwayat_donasi_medis($id_user),
            "recordsFiltered" => $this->model->filter_riwayat_donasi_medis($id_user),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function riwayat_donasi_non_medis(){
        $id_user = $this->ion_auth->user()->row()->id;
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = $this->model->riwayat_donasi_non_medis($id_user);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = 'nm_' . $field->id_donasi_non_medis;

            if($field->created_at != ''){
                $s_created = explode(' ', $field->created_at);
                $s_created_date = explode('-', $s_created[0]);
                $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<br>' . $s_created[1];
            }else{
                $row[] = '';
            }

            $row[] = 'Rp.' . number_format($field->donasi , 0, ',', '.');

            $row[] = '<a href="' . site_url('donasi/non_medis/' . $field->link) . '" target="_blank" style="text-transform: capitalize; font-weight: 600; color: rgb(255,197,70);">' . $field->judul . '</a>';
            
            if($field->status == '0'){
                $row[] = '<div>Pending</div><a style="margin-top: 5px;" href="#" class="btn btn-warning btn_status" id="' . $field->id_donasi_non_medis . '">Cek Status</a>';
            }
            if($field->status == '1'){
                $row[] = '<div>Sukses</div><a style="margin-top: 5px;" href="#" class="btn btn-warning btn_status" id="' . $field->id_donasi_non_medis . '">Cek Status</a>';
            }
            if($field->status == '2'){
                $row[] = '<div>Kadaluarsa</div><a style="margin-top: 5px;" href="#" class="btn btn-warning btn_status" id="' . $field->id_donasi_non_medis . '">Cek Status</a>';
            }
            
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->total_riwayat_donasi_non_medis($id_user),
            "recordsFiltered" => $this->model->filter_riwayat_donasi_non_medis($id_user),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function riwayat_galang_dana_medis(){
        $id_user = $this->ion_auth->user()->row()->id;
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = $this->model->riwayat_galang_dana_medis($id_user);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;

            if($field->admin_verify == '0'){
                if($field->status == '0'){
                    if($field->kategori == '1'){
                        $row[] = 'Draft<br><a href="' . site_url('home/galang_dana_anak_sakit/' . $field->id_kasus_medis) . '" class="btn btn-primary" target="_blank">Edit</a>';
                    }else{
                        $row[] = 'Draft<br><a href="' . site_url('home/galang_dana_kesehatan/' . $field->id_kasus_medis) . '" class="btn btn-primary" target="_blank">Edit</a>';
                    }
                }
                if($field->status == '1'){
                    $row[] = 'Menunggu Verifikasi Peduli Indonesia';
                }
                if($field->status == '2'){
                    $row[] = 'Galang Dana Berakhir';
                }
            }else{
                if($field->status == '0'){
                    $row[] = 'Publish<br><button type="button" class="btn btn-warning btn_terbaru" id="' . $field->id_kasus_medis . '" alt="' . $field->judul . '">Kabar Terbaru</button>';
                }
                if($field->status == '1'){
                    $row[] = 'Publish<br><button type="button" class="btn btn-warning btn_terbaru" id="' . $field->id_kasus_medis . '" alt="' . $field->judul . '">Kabar Terbaru</button>';
                }
                if($field->status == '2'){
                    if($field->file_rekening == '' && $field->file_ktp == '' && $field->file_kk == ''){
                        $row[] = 'Galang Dana Berakhir<br><button type="button" class="btn btn-warning btn_dokumen" id="' . $field->id_kasus_medis . '">Lengkapi Dokumen</button>';
                    }else{
                        $row[] = 'Galang Dana Berakhir<br><br>Kami akan segera menghubungi anda';
                    }
                }
            }

            if($field->created_at != ''){
                $s_created = explode(' ', $field->created_at);
                $s_created_date = explode('-', $s_created[0]);
                if($field->jatuh_tempo == ''){
                    $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0];
                }else{
                    $tgl1 = strtotime(date('Y-m-d')); 
                    $tgl2 = strtotime($field->jatuh_tempo); 
                    $selisih = $tgl2 - $tgl1;
                    if($selisih < 0){
                        $s_tempo = explode('-', $field->jatuh_tempo);
                        $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<div style="text-align: center;">s/d</div>' . $s_tempo[2] .'/'. $month_format[(int)$s_tempo[1]] .'/'. $s_tempo[0];
                    }else{
                        $selisih = $selisih / 60 / 60 / 24;
                        $sisa_waktu = number_format($selisih , 0, ',', '.');
                        if((int)$sisa_waktu == 0){
                            $sisa_waktu = 'Hari terakhir';
                        }else{
                            $sisa_waktu = $sisa_waktu . ' Hari lagi';
                        }
                        $s_tempo = explode('-', $field->jatuh_tempo);
                        $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<div style="text-align: center;">s/d</div>' . $s_tempo[2] .'/'. $month_format[(int)$s_tempo[1]] .'/'. $s_tempo[0] .'<div style="text-align: center;">'. $sisa_waktu . '</div>';
                    }
                }
            }else{
                $row[] = '';
            }

            if($field->kategori == '1'){
                $row[] = 'Anak Sakit';
            }else{
                $row[] = 'Kesehatan';
            }

            $row[] = '<a href="' . site_url('donasi/medis/' . $field->link) . '" target="_blank" style="text-transform: capitalize; font-weight: 600; color: rgb(255,197,70);">' . $field->judul . '</a>';

            $total = $this->model->total_donasi_medis($field->id_kasus_medis);
            if($total->donasi == null){
                $row[] = 'Rp.0 <div style="text-align: center;">Dari</div> Rp.' . number_format($field->dana , 0, ',', '.');
            }else{
                $row[] = 'Rp.' . number_format($total->donasi , 0, ',', '.') . ' <div style="text-align: center;">Dari</div> Rp.' . number_format($field->dana , 0, ',', '.');
            }

            $donatur = $this->model->total_donatur_medis($field->id_kasus_medis);
            $row[] = number_format($donatur , 0, ',', '.') . ' Donatur';
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->total_riwayat_galang_dana_medis($id_user),
            "recordsFiltered" => $this->model->filter_riwayat_galang_dana_medis($id_user),
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    public function save_kabar_terbaru_medis(){
        $by = $this->ion_auth->user()->row()->nama;
        $data   = $this->model->save_kabar_terbaru_medis($by);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function kabar_terbaru_medis(){
        $id_kasus = $this->input->post('id_kasus');
        $data = $this->model->kabar_terbaru_medis($id_kasus);
        if($data){
            foreach($data as $row){
                $tgl1 = strtotime(date('Y-m-d H:i:s')); 
                $tgl2 = strtotime($row->created_at); 
                $detik = $tgl1 - $tgl2;
                $row->created = number_format($detik , 0, ',', '.') . ' detik';
                if((int)$detik > 60){
                    $menit = $detik / 60;//menit
                    $row->created = number_format($menit , 0, ',', '.') . ' menit';
                    if((int)$menit > 60){
                        $jam = $menit / 60;//jam
                        $row->created = number_format($jam , 0, ',', '.') . ' jam';
                        if((int)$jam > 24){
                            $hari = $jam / 24;//hari
                            $row->created = number_format($hari , 0, ',', '.') . ' hari';
                        }
                    }
                }
            }
            echo json_encode($data);
        }else{
            $message['empty'] = true;
            echo json_encode($message);
        }
    }

    public function medis_kabar_terbaru_upload_img(){
        $config['upload_path']   = './assets/project/kabar_medis';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image')){
            $file   = array('image' => $this->upload->data());
            $image  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/kabar_medis/'.$image;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['new_image']        = './assets/project/kabar_medis/'.$image;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            echo site_url('assets/project/kabar_medis/'.$image);
        }
    }

    public function medis_kabar_terbaru_delete_img(){
        $src = $this->input->post('src');
        $file_name = str_replace(base_url(), '', $src);
        if(unlink($file_name))
        {
            echo 'File Delete Successfully';
        }
    }
    
    public function dokumen_pencairan_medis(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->input->post('id_data');
        $config['upload_path']   = './assets/project/pencairan_medis';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('rekening')){
            $file   = array('image' => $this->upload->data());
            $rekening  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/pencairan_medis/'.$rekening;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['width']            = 1200;
            $config['height']           = 900;
            $config['new_image']        = './assets/project/pencairan_medis/'.$rekening;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $data   = $this->model->save_rekening_medis($id, $by, $rekening); 
        }
        if($this->upload->do_upload('ktp')){
            $file   = array('image' => $this->upload->data());
            $ktp  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/pencairan_medis/'.$ktp;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['width']            = 1200;
            $config['height']           = 900;
            $config['new_image']        = './assets/project/pencairan_medis/'.$ktp;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $data   = $this->model->save_ktp_medis($id, $by, $ktp); 
        }
        if($this->upload->do_upload('kk')){
            $file   = array('image' => $this->upload->data());
            $kk  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/pencairan_medis/'.$kk;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['width']            = 1200;
            $config['height']           = 900;
            $config['new_image']        = './assets/project/pencairan_medis/'.$kk;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $data   = $this->model->save_kk_medis($id, $by, $kk); 
        }
        $last_dokumen = $this->model->last_dokumen_medis($id);
        if($last_dokumen){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function riwayat_galang_dana_non_medis(){
        $id_user = $this->ion_auth->user()->row()->id;
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = $this->model->riwayat_galang_dana_non_medis($id_user);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;

            if($field->admin_verify == '0'){
                if($field->status == '0'){
                    if($field->kategori == '1'){
                        $row[] = 'Draft<br><a href="' . site_url('home/galang_dana_kemanusiaan/' . $field->id_kasus_non_medis) . '" class="btn btn-primary" target="_blank">Edit</a>';
                    }else{
                        $row[] = 'Draft<br><a href="' . site_url('home/galang_dana_bencana/' . $field->id_kasus_non_medis) . '" class="btn btn-primary" target="_blank">Edit</a>';
                    }
                }
                if($field->status == '1'){
                    $row[] = 'Menunggu Verifikasi Peduli Indonesia';
                }
                if($field->status == '2'){
                    $row[] = 'Galang Dana Berakhir';
                }
            }else{
                if($field->status == '0'){
                    $row[] = 'Publish<br><button type="button" class="btn btn-warning btn_terbaru" id="' . $field->id_kasus_non_medis . '" alt="' . $field->judul . '">Kabar Terbaru</button>';
                }
                if($field->status == '1'){
                    $row[] = 'Publish<br><button type="button" class="btn btn-warning btn_terbaru" id="' . $field->id_kasus_non_medis . '" alt="' . $field->judul . '">Kabar Terbaru</button>';
                }
                if($field->status == '2'){
                    if($field->file_rekening == '' && $field->file_ktp == '' && $field->file_kk == ''){
                        $row[] = 'Galang Dana Berakhir<br><button type="button" class="btn btn-warning btn_dokumen" id="' . $field->id_kasus_non_medis . '">Lengkapi Dokumen</button>';
                    }else{
                        $row[] = 'Galang Dana Berakhir<br><br>Kami akan segera menghubungi anda';
                    }
                }
            }

            if($field->created_at != ''){
                $s_created = explode(' ', $field->created_at);
                $s_created_date = explode('-', $s_created[0]);
                if($field->jatuh_tempo == ''){
                    $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0];
                }else{
                    $tgl1 = strtotime(date('Y-m-d')); 
                    $tgl2 = strtotime($field->jatuh_tempo); 
                    $selisih = $tgl2 - $tgl1;
                    if($selisih < 0){
                        $s_tempo = explode('-', $field->jatuh_tempo);
                        $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<div style="text-align: center;">s/d</div>' . $s_tempo[2] .'/'. $month_format[(int)$s_tempo[1]] .'/'. $s_tempo[0];
                    }else{
                        $selisih = $selisih / 60 / 60 / 24;
                        $sisa_waktu = number_format($selisih , 0, ',', '.');
                        if((int)$sisa_waktu == 0){
                            $sisa_waktu = 'Hari terakhir';
                        }else{
                            $sisa_waktu = $sisa_waktu . ' Hari lagi';
                        }
                        $s_tempo = explode('-', $field->jatuh_tempo);
                        $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<div style="text-align: center;">s/d</div>' . $s_tempo[2] .'/'. $month_format[(int)$s_tempo[1]] .'/'. $s_tempo[0] .'<div style="text-align: center;">'. $sisa_waktu . '</div>';
                    }
                }
            }else{
                $row[] = '';
            }

            if($field->kategori == '1'){
                $row[] = 'Kemanusiaan';
            }else{
                $row[] = 'Bencana';
            }

            $row[] = '<a href="' . site_url('donasi/non_medis/' . $field->link) . '" target="_blank" style="text-transform: capitalize; font-weight: 600; color: rgb(255,197,70);">' . $field->judul . '</a>';

            $total = $this->model->total_donasi_non_medis($field->id_kasus_non_medis);
            if($total->donasi == null){
                $row[] = 'Rp.0 <div style="text-align: center;">Dari</div> Rp.' . number_format($field->dana , 0, ',', '.');
            }else{
                $row[] = 'Rp.' . number_format($total->donasi , 0, ',', '.') . ' <div style="text-align: center;">Dari</div> Rp.' . number_format($field->dana , 0, ',', '.');
            }

            $donatur = $this->model->total_donatur_non_medis($field->id_kasus_non_medis);
            $row[] = number_format($donatur , 0, ',', '.') . ' Donatur';
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->total_riwayat_galang_dana_non_medis($id_user),
            "recordsFiltered" => $this->model->filter_riwayat_galang_dana_non_medis($id_user),
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    public function save_kabar_terbaru_non_medis(){
        $by = $this->ion_auth->user()->row()->nama;
        $data   = $this->model->save_kabar_terbaru_non_medis($by);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function kabar_terbaru_non_medis(){
        $id_kasus = $this->input->post('id_kasus');
        $data = $this->model->kabar_terbaru_non_medis($id_kasus);
        if($data){
            foreach($data as $row){
                $tgl1 = strtotime(date('Y-m-d H:i:s')); 
                $tgl2 = strtotime($row->created_at); 
                $detik = $tgl1 - $tgl2;
                $row->created = number_format($detik , 0, ',', '.') . ' detik';
                if((int)$detik > 60){
                    $menit = $detik / 60;//menit
                    $row->created = number_format($menit , 0, ',', '.') . ' menit';
                    if((int)$menit > 60){
                        $jam = $menit / 60;//jam
                        $row->created = number_format($jam , 0, ',', '.') . ' jam';
                        if((int)$jam > 24){
                            $hari = $jam / 24;//hari
                            $row->created = number_format($hari , 0, ',', '.') . ' hari';
                        }
                    }
                }
            }
            echo json_encode($data);
        }else{
            $message['empty'] = true;
            echo json_encode($message);
        }
    }

    public function non_medis_kabar_terbaru_upload_img(){
        $config['upload_path']   = './assets/project/kabar_non_medis';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image')){
            $file   = array('image' => $this->upload->data());
            $image  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/kabar_non_medis/'.$image;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['new_image']        = './assets/project/kabar_non_medis/'.$image;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            echo site_url('assets/project/kabar_non_medis/'.$image);
        }
    }

    public function non_medis_kabar_terbaru_delete_img(){
        $src = $this->input->post('src');
        $file_name = str_replace(base_url(), '', $src);
        if(unlink($file_name))
        {
            echo 'File Delete Successfully';
        }
    }
    
    public function dokumen_pencairan_non_medis(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->input->post('id_data');
        $config['upload_path']   = './assets/project/pencairan_non_medis';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('rekening')){
            $file   = array('image' => $this->upload->data());
            $rekening  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/pencairan_non_medis/'.$rekening;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['width']            = 1200;
            $config['height']           = 900;
            $config['new_image']        = './assets/project/pencairan_non_medis/'.$rekening;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $data   = $this->model->save_rekening_non_medis($id, $by, $rekening); 
        }
        if($this->upload->do_upload('ktp')){
            $file   = array('image' => $this->upload->data());
            $ktp  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/pencairan_non_medis/'.$ktp;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['width']            = 1200;
            $config['height']           = 900;
            $config['new_image']        = './assets/project/pencairan_non_medis/'.$ktp;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $data   = $this->model->save_ktp_non_medis($id, $by, $ktp); 
        }
        if($this->upload->do_upload('kk')){
            $file   = array('image' => $this->upload->data());
            $kk  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/pencairan_non_medis/'.$kk;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['width']            = 1200;
            $config['height']           = 900;
            $config['new_image']        = './assets/project/pencairan_non_medis/'.$kk;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $data   = $this->model->save_kk_non_medis($id, $by, $kk); 
        }
        $last_dokumen = $this->model->last_dokumen_non_medis($id);
        if($last_dokumen){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function message_process(){
        $data   = $this->model->message_process();
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function detail_agenda(){
        $id_agenda = $this->input->post('id_agenda');
        $data   = $this->model->detail_agenda($id_agenda);
        if($data->type == '0'){
            $data->image = site_url('assets/project/program/' . $data->image);
        }else{
            $data->image = site_url('assets/project/kegiatan/' . $data->image);
        }
        unset($data->type);
        echo json_encode($data);
    }
    //Start Penggalangan Medis
    public function medis_registration(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->ion_auth->user()->row()->id;
        $medis = $this->input->post('medis');
        if($medis == 'kesehatan'){
            $kategori = '2';
        }else{
            $kategori = '1';
        }
        $data   = $this->model->medis_registration($id, $by, $kategori); 
        if($data){
            $last_kasus_medis = $this->model->last_kasus_medis($id); 
            $message['id'] = $last_kasus_medis->id_kasus_medis;
            echo json_encode($message);
        }
    }

    public function medis_cerita_upload_img(){
        $config['upload_path']   = './assets/project/kasus_medis';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image')){
            $file   = array('image' => $this->upload->data());
            $image  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/kasus_medis/'.$image;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['new_image']        = './assets/project/kasus_medis/'.$image;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            echo site_url('assets/project/kasus_medis/'.$image);
        }
    }

    public function medis_cerita_delete_img(){
        $src = $this->input->post('src');
        $file_name = str_replace(base_url(), '', $src);
        if(unlink($file_name))
        {
            echo 'File Delete Successfully';
        }
    }

    public function medis_tujuan(){
        $id_kasus = $this->input->post('id_kasus');
        $data   = $this->model->medis_tujuan($id_kasus); 
        echo json_encode($data);
    }

    public function medis_tujuan_update(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->input->post('tujuan_main_id');
        $data   = $this->model->medis_tujuan_update($id, $by); 
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function medis_pasien(){
        $id_kasus = $this->input->post('id_kasus');
        $data   = $this->model->medis_pasien($id_kasus); 
        echo json_encode($data);
    }

    public function medis_pasien_update(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->input->post('pasien_main_id');
        $config['upload_path']   = './assets/project/kasus_medis';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('dokumen_medis')){
            $file   = array('image' => $this->upload->data());
            $image  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/kasus_medis/'.$image;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['width']            = 1200;
            $config['height']           = 900;
            $config['new_image']        = './assets/project/kasus_medis/'.$image;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $data   = $this->model->medis_pasien_update($id, $by, $image); 
            if($data){
                $last_image = $this->input->post('get_dokumen_medis');
                $delete_image = $this->input->post('delete_dokumen_medis');
                if($last_image != ''){
                    unlink('./assets/project/kasus_medis/'.$last_image);
                    $message['success'] = true;
                    echo json_encode($message);
                }else if($delete_image != ''){
                    unlink('./assets/project/kasus_medis/'.$delete_image);
                    $message['success'] = true;
                    echo json_encode($message);
                }else{
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }
        }else{
            $last_image = $this->input->post('get_dokumen_medis');
            $delete_image = $this->input->post('delete_dokumen_medis');
            if($last_image == '' && $delete_image != ''){
                unlink('./assets/project/kasus_medis/'.$delete_image);
                $image  = '';
                $data   = $this->model->medis_pasien_update($id, $by, $image);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }else if($last_image == '' && $delete_image == ''){
                $image  = '';
                $data   = $this->model->medis_pasien_update($id, $by, $image);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }else{
                $image  = $last_image;
                $data   = $this->model->medis_pasien_update($id, $by, $image);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }
        }
    }

    public function medis_medis(){
        $id_kasus = $this->input->post('id_kasus');
        $data   = $this->model->medis_medis($id_kasus); 
        echo json_encode($data);
    }

    public function medis_medis_update(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->input->post('medis_main_id');
        $config['upload_path']   = './assets/project/kasus_medis';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('hasil_pemeriksaan')){
            $file   = array('image' => $this->upload->data());
            $image  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/kasus_medis/'.$image;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['width']            = 1200;
            $config['height']           = 900;
            $config['new_image']        = './assets/project/kasus_medis/'.$image;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $data   = $this->model->medis_medis_update($id, $by, $image); 
            if($data){
                $last_image = $this->input->post('get_hasil_pemeriksaan');
                $delete_image = $this->input->post('delete_hasil_pemeriksaan');
                if($last_image != ''){
                    unlink('./assets/project/kasus_medis/'.$last_image);
                    $message['success'] = true;
                    echo json_encode($message);
                }else if($delete_image != ''){
                    unlink('./assets/project/kasus_medis/'.$delete_image);
                    $message['success'] = true;
                    echo json_encode($message);
                }else{
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }
        }else{
            $last_image = $this->input->post('get_hasil_pemeriksaan');
            $delete_image = $this->input->post('delete_hasil_pemeriksaan');
            if($last_image == '' && $delete_image != ''){
                unlink('./assets/project/kasus_medis/'.$delete_image);
                $image  = '';
                $data   = $this->model->medis_medis_update($id, $by, $image);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }else if($last_image == '' && $delete_image == ''){
                $image  = '';
                $data   = $this->model->medis_medis_update($id, $by, $image);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }else{
                $image  = $last_image;
                $data   = $this->model->medis_medis_update($id, $by, $image);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }
        }
    }

    public function medis_donasi(){
        $id_kasus = $this->input->post('id_kasus');
        $data   = $this->model->medis_donasi($id_kasus); 
        $data->now_date = date('Y-m-d');
        echo json_encode($data);
    }

    public function medis_donasi_update(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->input->post('donasi_main_id');
        $data   = $this->model->medis_donasi_update($id, $by); 
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function medis_judul(){
        $id_kasus = $this->input->post('id_kasus');
        $data   = $this->model->medis_judul($id_kasus); 
        echo json_encode($data);
    }

    public function medis_judul_update(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->input->post('judul_main_id');
        $links = $this->input->post('link_penggalangan');
        $check_link = $this->model->check_link_medis($id, $links);
        if($check_link){
            $message['link'] = true;
            echo json_encode($message);
        }else{
            $config['upload_path']   = './assets/project/kasus_medis';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size']      = 0;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if($this->upload->do_upload('sampul')){
                $file   = array('image' => $this->upload->data());
                $image  = $file['image']['file_name'];
                $config['image_library']    = 'gd2';
                $config['source_image']     = './assets/project/kasus_medis/'.$image;
                $config['create_thumb']     = FALSE;
                $config['maintain_ratio']   = FALSE;
                $config['quality']          = '75%';
                $config['width']            = 1200;
                $config['height']           = 900;
                $config['new_image']        = './assets/project/kasus_medis/'.$image;
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $data   = $this->model->medis_judul_update($id, $by, $image); 
                if($data){
                    $last_image = $this->input->post('get_sampul');
                    $delete_image = $this->input->post('delete_sampul');
                    if($last_image != ''){
                        unlink('./assets/project/kasus_medis/'.$last_image);
                        $message['success'] = $image;
                        echo json_encode($message);
                    }else if($delete_image != ''){
                        unlink('./assets/project/kasus_medis/'.$delete_image);
                        $message['success'] = $image;
                        echo json_encode($message);
                    }else{
                        $message['success'] = $image;
                        echo json_encode($message);
                    }
                }
            }else{
                $last_image = $this->input->post('get_sampul');
                $delete_image = $this->input->post('delete_sampul');
                if($last_image == '' && $delete_image != ''){
                    unlink('./assets/project/kasus_medis/'.$delete_image);
                    $image  = '';
                    $data   = $this->model->medis_judul_update($id, $by, $image);
                    if($data){
                        $message['success'] = $image;
                        echo json_encode($message);
                    }
                }else if($last_image == '' && $delete_image == ''){
                    $image  = '';
                    $data   = $this->model->medis_judul_update($id, $by, $image);
                    if($data){
                        $message['success'] = $image;
                        echo json_encode($message);
                    }
                }else{
                    $image  = $last_image;
                    $data   = $this->model->medis_judul_update($id, $by, $image);
                    if($data){
                        $message['success'] = $image;
                        echo json_encode($message);
                    }
                }
            }
        }
    }

    public function medis_cerita(){
        $id_kasus = $this->input->post('id_kasus');
        $data   = $this->model->medis_cerita($id_kasus); 
        echo json_encode($data);
    }

    public function medis_cerita_update(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->input->post('cerita_main_id');
        $data   = $this->model->medis_cerita_update($id, $by); 
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function medis_ajakan(){
        $id_kasus = $this->input->post('id_kasus');
        $data   = $this->model->medis_ajakan($id_kasus); 
        echo json_encode($data);
    }

    public function medis_ajakan_update(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->input->post('ajakan_main_id');
        $email = $this->input->post('deskripsi_email');
        $otp = mt_rand(100000,1000000);
        $data   = $this->model->medis_ajakan_update($id, $by, $otp); 
        if($data){
            $message['success'] = true;
            echo json_encode($message);
            // $kategori = $this->model->kategori_penggalangan_medis($id);
            // if($kategori->kategori == '1'){
            //     $penggalangan = 'anak sakit';
            // }else{
            //     $penggalangan = 'kesehatan';
            // }
            // $mail   = new PHPMailer(); 
            // $mail->IsSMTP(); 
            // $mail->SMTPAuth     = true; 
            // $mail->SMTPSecure   = "ssl";   
            // $mail->Host         = $this->mail_host;      
            // $mail->Port         = 465;                   
            // $mail->Username     = $this->mail_username;  
            // $mail->Password     = $this->mail_password;            
            // $mail->SetFrom($this->mail_username, 'Kaizen Peduli');
            // $mail->isHTML(true);
            // $mail->SMTPAutoTLS  = false;
            // $mail->Hostname     = $this->mail_dns;
            // $mail->Subject      = "Galang Dana";
            // // $mail       = new PHPMailer(); 
            // // $mail->IsSMTP(); 
            // // $mail->SMTPAuth   = true; 
            // // $mail->SMTPSecure = "ssl";   
            // // $mail->Host       = "smtp.gmail.com";      
            // // $mail->Port       = 465;                   
            // // $mail->Username   = "ascoglobalindo@gmail.com";  
            // // $mail->Password   = "Ascoglobal2021";            
            // // $mail->SetFrom('ascoglobalindo@gmail.com', 'Kaizen Peduli Indonesia'); 
            // // $mail->isHTML(true);
            // // $mail->Subject    = "Galang Dana";
            // $mail->Body       = 
            // '<!DOCTYPE html>
            // <html>
            //     <head>
            //          <title></title>
            //     </head>
            //     <body>
            //          <div> 
            //               <div style="width: 100%;">
            //                   <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
            //               </div>
            //               <br>
            //               <br>
            //               <h3>Masukkan Kode OTP penggalangan dana ' . $penggalangan . ' dibawah ini :</h3>
            //               <h1>' . $otp . '</h1>
            //               <br>
            //               <div>Kesulitan login ?</div>
            //               <div>Tim Kaizen Peduli akan dengan senang hati membantu</div>
            //               <br>
            //               <div>Cheers</div>
            //               <div>Yayasan Kaizen Peduli Indonesia</div>
            //          </div>
            //     </body>
            // </html>';
            // $mail->AddAddress($email);
            // $sent = $mail->Send();
            // if($sent){
            //     $message['success'] = true;
            //     echo json_encode($message);
            // }else{
            //     $message['unsent'] = true;
            //     echo json_encode($message);
            // }
        }
    }
    //End Penggalangan Medis
    //Start Penggalangan Non Medis
    public function non_medis_registration(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->ion_auth->user()->row()->id;
        $non_medis = $this->input->post('non_medis');
        if($non_medis == 'kemanusiaan'){
            $kategori = '1';
        }else{
            $kategori = '2';
        }
        $data   = $this->model->non_medis_registration($id, $by, $kategori); 
        if($data){
            $last_kasus_non_medis = $this->model->last_kasus_non_medis($id); 
            $message['id'] = $last_kasus_non_medis->id_kasus_non_medis;
            echo json_encode($message);
        }
    }

    public function non_medis_cerita_upload_img(){
        $config['upload_path']   = './assets/project/kasus_non_medis';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image')){
            $file   = array('image' => $this->upload->data());
            $image  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/kasus_non_medis/'.$image;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['new_image']        = './assets/project/kasus_non_medis/'.$image;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            echo site_url('assets/project/kasus_non_medis/'.$image);
        }
    }

    public function non_medis_cerita_delete_img(){
        $src = $this->input->post('src');
        $file_name = str_replace(base_url(), '', $src);
        if(unlink($file_name))
        {
            echo 'File Delete Successfully';
        }
    }

    public function non_medis_identitas(){
        $id_kasus = $this->input->post('id_kasus');
        $data   = $this->model->non_medis_identitas($id_kasus); 
        echo json_encode($data);
    }

    public function non_medis_identitas_update(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->input->post('identitas_main_id');
        $data   = $this->model->non_medis_identitas_update($id, $by); 
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function non_medis_donasi(){
        $id_kasus = $this->input->post('id_kasus');
        $data   = $this->model->non_medis_donasi($id_kasus); 
        $data->now_date = date('Y-m-d');
        echo json_encode($data);
    }

    public function non_medis_donasi_update(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->input->post('donasi_main_id');
        $data   = $this->model->non_medis_donasi_update($id, $by); 
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function non_medis_detail(){
        $id_kasus = $this->input->post('id_kasus');
        $data   = $this->model->non_medis_detail($id_kasus); 
        echo json_encode($data);
    }

    public function non_medis_detail_update(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->input->post('detail_main_id');
        $links = $this->input->post('detail_link');
        $check_link = $this->model->check_link_non_medis($id, $links);
        if($check_link){
            $message['link'] = true;
            echo json_encode($message);
        }else{
            $data   = $this->model->non_medis_detail_update($id, $by); 
            if($data){
                $message['success'] = true;
                echo json_encode($message);
            }
        }
    }

    public function non_medis_foto(){
        $id_kasus = $this->input->post('id_kasus');
        $data   = $this->model->non_medis_foto($id_kasus); 
        echo json_encode($data);
    }

    public function non_medis_foto_update(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->input->post('foto_main_id');
        $config['upload_path']   = './assets/project/kasus_non_medis';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('sampul')){
            $file   = array('image' => $this->upload->data());
            $image  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/kasus_non_medis/'.$image;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['width']            = 1200;
            $config['height']           = 900;
            $config['new_image']        = './assets/project/kasus_non_medis/'.$image;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $data   = $this->model->non_medis_foto_update($id, $by, $image); 
            if($data){
                $last_image = $this->input->post('get_sampul');
                $delete_image = $this->input->post('delete_sampul');
                if($last_image != ''){
                    unlink('./assets/project/kasus_non_medis/'.$last_image);
                    $message['success'] = $image;
                    echo json_encode($message);
                }else if($delete_image != ''){
                    unlink('./assets/project/kasus_non_medis/'.$delete_image);
                    $message['success'] = $image;
                    echo json_encode($message);
                }else{
                    $message['success'] = $image;
                    echo json_encode($message);
                }
            }
        }else{
            $last_image = $this->input->post('get_sampul');
            $delete_image = $this->input->post('delete_sampul');
            if($last_image == '' && $delete_image != ''){
                unlink('./assets/project/kasus_non_medis/'.$delete_image);
                $image  = '';
                $data   = $this->model->non_medis_foto_update($id, $by, $image);
                if($data){
                    $message['success'] = $image;
                    echo json_encode($message);
                }
            }else if($last_image == '' && $delete_image == ''){
                $image  = '';
                $data   = $this->model->non_medis_foto_update($id, $by, $image);
                if($data){
                    $message['success'] = $image;
                    echo json_encode($message);
                }
            }else{
                $image  = $last_image;
                $data   = $this->model->non_medis_foto_update($id, $by, $image);
                if($data){
                    $message['success'] = $image;
                    echo json_encode($message);
                }
            }
        }
    }

    public function non_medis_deskripsi(){
        $id_kasus = $this->input->post('id_kasus');
        $data   = $this->model->non_medis_deskripsi($id_kasus); 
        echo json_encode($data);
    }

    public function non_medis_deskripsi_update(){
        $by = $this->ion_auth->user()->row()->nama;
        $id = $this->input->post('deskripsi_main_id');
        $data   = $this->model->non_medis_deskripsi_update($id, $by); 
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function non_medis_konfirmasi(){
        $id_kasus = $this->input->post('id_kasus');
        $data   = $this->model->non_medis_konfirmasi($id_kasus); 
        echo json_encode($data);
    }

    public function non_medis_konfirmasi_update(){
        $by = $this->ion_auth->user()->row()->nama;
        $phone = $this->ion_auth->user()->row()->phone;
        $id = $this->input->post('konfirmasi_main_id');
        $email = $this->input->post('konfirmasi_email');
        $otp = mt_rand(100000,1000000);
        $data   = $this->model->non_medis_konfirmasi_update($id, $by, $otp, $phone); 
        if($data){
            $message['success'] = true;
            echo json_encode($message);
            // $kategori = $this->model->kategori_penggalangan_non_medis($id);
            // if($kategori->kategori == '1'){
            //     $penggalangan = 'kemanusiaan';
            // }else{
            //     $penggalangan = 'bencana';
            // }
            // $mail   = new PHPMailer(); 
            // $mail->IsSMTP(); 
            // $mail->SMTPAuth     = true; 
            // $mail->SMTPSecure   = "ssl";   
            // $mail->Host         = $this->mail_host;      
            // $mail->Port         = 465;                   
            // $mail->Username     = $this->mail_username;  
            // $mail->Password     = $this->mail_password;            
            // $mail->SetFrom($this->mail_username, 'Kaizen Peduli');
            // $mail->isHTML(true);
            // $mail->SMTPAutoTLS  = false;
            // $mail->Hostname     = $this->mail_dns;
            // $mail->Subject      = "Galang Dana";
            // // $mail       = new PHPMailer(); 
            // // $mail->IsSMTP(); 
            // // $mail->SMTPAuth   = true; 
            // // $mail->SMTPSecure = "ssl";   
            // // $mail->Host       = "smtp.gmail.com";      
            // // $mail->Port       = 465;                   
            // // $mail->Username   = "ascoglobalindo@gmail.com";  
            // // $mail->Password   = "Ascoglobal2021";            
            // // $mail->SetFrom('ascoglobalindo@gmail.com', 'Kaizen Peduli Indonesia'); 
            // // $mail->isHTML(true);
            // // $mail->Subject    = "Galang Dana";
            // $mail->Body       = 
            // '<!DOCTYPE html>
            // <html>
            //     <head>
            //          <title></title>
            //     </head>
            //     <body>
            //          <div> 
            //               <div style="width: 100%;">
            //                   <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
            //               </div>
            //               <br>
            //               <br>
            //               <h3>Masukkan Kode OTP penggalangan dana ' . $penggalangan . ' dibawah ini :</h3>
            //               <h1>' . $otp . '</h1>
            //               <br>
            //               <div>Kesulitan login ?</div>
            //               <div>Tim Kaizen Peduli akan dengan senang hati membantu</div>
            //               <br>
            //               <div>Cheers</div>
            //               <div>Yayasan Kaizen Peduli Indonesia</div>
            //          </div>
            //     </body>
            // </html>';
            // $mail->AddAddress($email);
            // $sent = $mail->Send();
            // if($sent){
            //     $message['success'] = true;
            //     echo json_encode($message);
            // }else{
            //     $message['unsent'] = true;
            //     echo json_encode($message);
            // }
        }
    }
    //End Penggalangan Non Medis
    //Start OTP Medis
    public function otp_medis_code(){
        $id_user = $this->ion_auth->user()->row()->id;
        $by = $this->ion_auth->user()->row()->nama;
        $id_kasus_medis = $this->input->post('id_kasus');
        $otp_code = $this->input->post('otp_code');
        $medis_id = $this->model->otp_medis_code($id_kasus_medis, $otp_code, $id_user);
        if($medis_id){
            $publish = $this->model->otp_medis_publish($id_kasus_medis, $by);
            if($publish){
                $message['success'] = true;
                echo json_encode($message);
            }
        }else{
            $message['match'] = true;
            echo json_encode($message);
        }
    }

    public function otp_medis_resend(){
        $id = $this->input->post('id_kasus');
        $id_user = $this->ion_auth->user()->row()->id;
        $by = $this->ion_auth->user()->row()->nama;
        $otp = mt_rand(100000,1000000);
        $data   = $this->model->otp_medis_resend($id, $by, $otp); 
        if($data){
            $kategori = $this->model->otp_medis_kategori($id, $id_user);
            if($kategori->kategori == '1'){
                $penggalangan = 'anak sakit';
            }else{
                $penggalangan = 'kesehatan';
            }
            $email = 'Email';
            $d_email = $this->model->base_contact($email);
            $email = $d_email->detail;

            $telepon = 'Telepon';
            $d_telepon = $this->model->base_contact($telepon);
            $telepon = $d_telepon->detail;
            
            $mail   = new PHPMailer(); 
            $mail->IsSMTP(); 
            $mail->SMTPAuth     = true; 
            $mail->SMTPSecure   = "ssl";   
            $mail->Host         = $this->mail_host;      
            $mail->Port         = 465;                   
            $mail->Username     = $this->mail_username;  
            $mail->Password     = $this->mail_password;            
            $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
            $mail->isHTML(true);
            $mail->SMTPAutoTLS  = false;
            $mail->Hostname     = $this->mail_dns;
            $mail->Subject      = "Galang Dana";
            // $mail       = new PHPMailer(); 
            // $mail->IsSMTP(); 
            // $mail->SMTPAuth   = true; 
            // $mail->SMTPSecure = "ssl";   
            // $mail->Host       = "smtp.gmail.com";      
            // $mail->Port       = 465;                   
            // $mail->Username   = "ascoglobalindo@gmail.com";  
            // $mail->Password   = "Ascoglobal2021";            
            // $mail->SetFrom('ascoglobalindo@gmail.com', 'Kaizen Peduli Indonesia'); 
            // $mail->isHTML(true);
            // $mail->Subject    = "Galang Dana";
            $mail->Body       = 
            '<!DOCTYPE html>
            <html>
                <head>
                     <title></title>
                </head>
                <body>
                     <div> 
                          <div style="width: 100%;">
                               <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
                          </div>
                          <br>
                          <br>
                           <h3>Masukkan Kode OTP penggalangan dana ' . $penggalangan . ' dibawah ini :</h3>
                           <h1>' . $otp . '</h1>
                          <br>
                          <div>Kesulitan login ?</div>
                          <div>Tim Peduli akan dengan senang hati membantu</div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                            <div>Salam,</div>
                            <div style="font-weight: 600;">Peduli Indonesia</div>
                          </div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://www.facebook.com/kaizenpeduliindonesia" target="_blank">
                                        <img src="https://imgur.com/TiFD2VP.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://www.instagram.com/peduliindo" target="_blank">
                                        <img src="https://imgur.com/aCbCLu1.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://web.whatsapp.com/send?phone=' . preg_replace('/0/', '62', $telepon, 1) . '" target="_blank">
                                        <img src="https://imgur.com/ClkBQ1z.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                          </div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div>Email : ' . $email . '</div>
                                <div>Whatsapp : ' . preg_replace('/0/', '+62 ', $telepon, 1) . '</div>
                          </div>
                     </div>
                </body>
            </html>';
            $mail->AddAddress($kategori->email);
            $sent = $mail->Send();
            if($sent){
                $message['success'] = $kategori->email;
                echo json_encode($message);
            }else{
                $message['unsent'] = true;
                echo json_encode($message);
            }
        }
    }
    //End OTP Medis
    //Start OTP Non Medis
    public function otp_non_medis_code(){
        $id_user = $this->ion_auth->user()->row()->id;
        $by = $this->ion_auth->user()->row()->nama;
        $id_kasus_non_medis = $this->input->post('id_kasus');
        $otp_code = $this->input->post('otp_code');
        $medis_id = $this->model->otp_non_medis_code($id_kasus_non_medis, $otp_code, $id_user);
        if($medis_id){
            $publish = $this->model->otp_non_medis_publish($id_kasus_non_medis, $by);
            if($publish){
                $message['success'] = true;
                echo json_encode($message);
            }
        }else{
            $message['match'] = true;
            echo json_encode($message);
        }
    }

    public function otp_non_medis_resend(){
        $id = $this->input->post('id_kasus');
        $id_user = $this->ion_auth->user()->row()->id;
        $by = $this->ion_auth->user()->row()->nama;
        $otp = mt_rand(100000,1000000);
        $data   = $this->model->otp_non_medis_resend($id, $by, $otp); 
        if($data){
            $kategori = $this->model->otp_non_medis_kategori($id, $id_user);
            if($kategori->kategori == '1'){
                $penggalangan = 'kemanusiaan';
            }else{
                $penggalangan = 'bencana';
            }
            $email = 'Email';
            $d_email = $this->model->base_contact($email);
            $email = $d_email->detail;

            $telepon = 'Telepon';
            $d_telepon = $this->model->base_contact($telepon);
            $telepon = $d_telepon->detail;
            
            $mail   = new PHPMailer(); 
            $mail->IsSMTP(); 
            $mail->SMTPAuth     = true; 
            $mail->SMTPSecure   = "ssl";   
            $mail->Host         = $this->mail_host;      
            $mail->Port         = 465;                   
            $mail->Username     = $this->mail_username;  
            $mail->Password     = $this->mail_password;            
            $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
            $mail->isHTML(true);
            $mail->SMTPAutoTLS  = false;
            $mail->Hostname     = $this->mail_dns;
            $mail->Subject      = "Galang Dana";
            // $mail       = new PHPMailer(); 
            // $mail->IsSMTP(); 
            // $mail->SMTPAuth   = true; 
            // $mail->SMTPSecure = "ssl";   
            // $mail->Host       = "smtp.gmail.com";      
            // $mail->Port       = 465;                   
            // $mail->Username   = "ascoglobalindo@gmail.com";  
            // $mail->Password   = "Ascoglobal2021";            
            // $mail->SetFrom('ascoglobalindo@gmail.com', 'Kaizen Peduli Indonesia'); 
            // $mail->isHTML(true);
            // $mail->Subject    = "Galang Dana";
            $mail->Body       = 
            '<!DOCTYPE html>
            <html>
                <head>
                     <title></title>
                </head>
                <body>
                     <div> 
                          <div style="width: 100%;">
                               <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
                          </div>
                          <br>
                           <h3>Masukkan Kode OTP penggalangan dana ' . $penggalangan . ' dibawah ini :</h3>
                           <h1>' . $otp . '</h1>
                          <br>
                          <div>Kesulitan login ?</div>
                          <div>Tim Peduli akan dengan senang hati membantu</div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                            <div>Salam,</div>
                            <div style="font-weight: 600;">Peduli Indonesia</div>
                          </div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://www.facebook.com/kaizenpeduliindonesia" target="_blank">
                                        <img src="https://imgur.com/TiFD2VP.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://www.instagram.com/peduliindo" target="_blank">
                                        <img src="https://imgur.com/aCbCLu1.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://web.whatsapp.com/send?phone=' . preg_replace('/0/', '62', $telepon, 1) . '" target="_blank">
                                        <img src="https://imgur.com/ClkBQ1z.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                          </div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div>Email : ' . $email . '</div>
                                <div>Whatsapp : ' . preg_replace('/0/', '+62 ', $telepon, 1) . '</div>
                          </div>
                     </div>
                </body>
            </html>';
            $mail->AddAddress($kategori->email);
            $sent = $mail->Send();
            if($sent){
                $message['success'] = $kategori->email;
                echo json_encode($message);
            }else{
                $message['unsent'] = true;
                echo json_encode($message);
            }
        }
    }
    //End OTP Non Medis
    //Start Pencarian
    public function search_donasi(){
        $keysearch = $this->input->post('keysearch');
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = array();
        $list_medis = $this->model->search_donasi_medis($keysearch);
        if(count($list_medis) != 0){
            foreach($list_medis as $row){
                $row->kat = 'medis';
                array_push($list, $row);
            }
        }
        $list_non_medis = $this->model->search_donasi_non_medis($keysearch);
        if(count($list_non_medis) != 0){
            foreach($list_non_medis as $row){
                $row->kat = 'nonmedis';
                array_push($list, $row);
            }
        }

        $total_medis = $this->model->total_search_donasi_medis($keysearch);
        $total_non_medis = $this->model->total_search_donasi_non_medis($keysearch);
        $totals = (int)$total_medis + (int)$total_non_medis;

        $filter_medis = $this->model->filter_search_donasi_medis($keysearch);
        $filter_non_medis = $this->model->filter_search_donasi_non_medis($keysearch);
        $filters = (int)$filter_medis + (int)$filter_non_medis;

        $data = array();
        for($i = 0; $i < count($list); $i += 2) {
            $row = array();
            if($i + 1 == count($list)){
                $tgl1 = strtotime(date('Y-m-d')); 
                $tgl2 = strtotime($list[$i]->jatuh_tempo); 
                $selisih = $tgl2 - $tgl1;
                $selisih = $selisih / 60 / 60 / 24;
                $sisa_waktu = number_format($selisih , 0, ',', '.');
                if((int)$sisa_waktu == 0){
                    $sisa_waktu = 'Hari terakhir';
                }else{
                    $sisa_waktu = $sisa_waktu . ' Hari lagi';
                }
                if($list[$i]->kat == 'medis'){
                    $path_img = site_url('assets/project/kasus_medis/' . $list[$i]->sampul);
                    $link_case = site_url('donasi/medis/' . $list[$i]->link);
                    $link_help = site_url('donasi/medis_help/' . $list[$i]->link);
                    $s_donasi = $this->model->total_donasi_medis($list[$i]->id_kasus);
                }else{
                    $path_img = site_url('assets/project/kasus_non_medis/' . $list[$i]->sampul);
                    $link_case = site_url('donasi/non_medis/' . $list[$i]->link);
                    $link_help = site_url('donasi/non_medis_help/' . $list[$i]->link);
                    $s_donasi = $this->model->total_donasi_non_medis($list[$i]->id_kasus);
                }
                if($s_donasi->donasi == null){
                    $tot_donasi = '0';
                }else{
                    $tot_donasi = number_format($s_donasi->donasi , 0, ',', '.');
                }
                if(! $this->ion_auth->logged_in()){
                    $row[] ='<div class="row">
                                <div class="col-md-6" style="padding: 10px;">
                                    <div style="padding: 10px; background: rgb(255,239,212);">
                                        <div class="row" style="margin: 0;">
                                            <div class="col-lg-6" style="padding: 0;">
                                                <a href="' . $link_case . '" style="width: 100%;">
                                                    <img src="' . $path_img . '" class="w-100">
                                                </a>
                                            </div>
                                            <div class="col-lg-6 text_col_search">
                                                <div style="font-weight: 600;">' . $list[$i]->judul . '</div><br>
                                                <div>Rp. ' . $tot_donasi . '</div>
                                                <div>' . $sisa_waktu . '</div>
                                                <a href="' . $link_help . '" class="btn btn-warning sess_logged_in" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>';
                }else{
                    $row[] ='<div class="row">
                                <div class="col-md-6" style="padding: 10px;">
                                    <div style="padding: 10px; background: rgb(255,239,212);">
                                        <div class="row" style="margin: 0;">
                                            <div class="col-lg-6" style="padding: 0;">
                                                <a href="' . $link_case . '" style="width: 100%;">
                                                    <img src="' . $path_img . '" class="w-100">
                                                </a>
                                            </div>
                                            <div class="col-lg-6 text_col_search">
                                                <div style="font-weight: 600;">' . $list[$i]->judul . '</div><br>
                                                <div>Rp. ' . $tot_donasi . '</div>
                                                <div>' . $sisa_waktu . '</div>
                                                <a href="' . $link_help . '" class="btn btn-warning" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>';
                }
            }else{
                $tgl1 = strtotime(date('Y-m-d')); 
                $tgl2 = strtotime($list[$i]->jatuh_tempo); 
                $selisih = $tgl2 - $tgl1;
                $selisih = $selisih / 60 / 60 / 24;
                $sisa_waktu = number_format($selisih , 0, ',', '.');
                if((int)$sisa_waktu == 0){
                    $sisa_waktu = 'Hari terakhir';
                }else{
                    $sisa_waktu = $sisa_waktu . ' Hari lagi';
                }
                if($list[$i]->kat == 'medis'){
                    $path_img = site_url('assets/project/kasus_medis/' . $list[$i]->sampul);
                    $link_case = site_url('donasi/medis/' . $list[$i]->link);
                    $link_help = site_url('donasi/medis_help/' . $list[$i]->link);
                    $s_donasi = $this->model->total_donasi_medis($list[$i]->id_kasus);
                }else{
                    $path_img = site_url('assets/project/kasus_non_medis/' . $list[$i]->sampul);
                    $link_case = site_url('donasi/non_medis/' . $list[$i]->link);
                    $link_help = site_url('donasi/non_medis_help/' . $list[$i]->link);
                    $s_donasi = $this->model->total_donasi_non_medis($list[$i]->id_kasus);
                }
                if($s_donasi->donasi == null){
                    $tot_donasi = '0';
                }else{
                    $tot_donasi = number_format($s_donasi->donasi , 0, ',', '.');
                }
                ////////////////////////////////////////////
                $tgls1 = strtotime(date('Y-m-d')); 
                $tgls2 = strtotime($list[$i + 1]->jatuh_tempo); 
                $selisihs = $tgls2 - $tgls1;
                $selisihs = $selisihs / 60 / 60 / 24;
                $sisa_waktus = number_format($selisihs , 0, ',', '.');
                if((int)$sisa_waktus == 0){
                    $sisa_waktus = 'Hari terakhir';
                }else{
                    $sisa_waktus = $sisa_waktus . ' Hari lagi';
                }
                if($list[$i + 1]->kat == 'medis'){
                    $path_imgs = site_url('assets/project/kasus_medis/' . $list[$i + 1]->sampul);
                    $link_cases = site_url('donasi/medis/' . $list[$i + 1]->link);
                    $link_helps = site_url('donasi/medis_help/' . $list[$i + 1]->link);
                    $s_donasis = $this->model->total_donasi_medis($list[$i + 1]->id_kasus);
                }else{
                    $path_imgs = site_url('assets/project/kasus_non_medis/' . $list[$i + 1]->sampul);
                    $link_cases = site_url('donasi/non_medis/' . $list[$i + 1]->link);
                    $link_helps = site_url('donasi/non_medis_help/' . $list[$i + 1]->link);
                    $s_donasis = $this->model->total_donasi_non_medis($list[$i + 1]->id_kasus);
                }
                if($s_donasis->donasi == null){
                    $tot_donasis = '0';
                }else{
                    $tot_donasis = number_format($s_donasis->donasi , 0, ',', '.');
                }
                if(! $this->ion_auth->logged_in()){
                    $row[] ='<div class="row">
                                <div class="col-md-6" style="padding: 10px;">
                                    <div style="padding: 10px; background: rgb(255,239,212);">
                                        <div class="row" style="margin: 0;">
                                            <div class="col-lg-6" style="padding: 0;">
                                                <a href="' . $link_case . '" style="width: 100%;">
                                                    <img src="' . $path_img . '" class="w-100">
                                                </a>
                                            </div>
                                            <div class="col-lg-6 text_col_search">
                                                <div style="font-weight: 600;">' . $list[$i]->judul . '</div><br>
                                                <div>Rp. ' . $tot_donasi . '</div>
                                                <div>' . $sisa_waktu . '</div>
                                                <a href="' . $link_help . '" class="btn btn-warning sess_logged_in" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6" style="padding: 10px;">
                                    <div style="padding: 10px; background: rgb(255,239,212);">
                                        <div class="row" style="margin: 0;">
                                            <div class="col-lg-6" style="padding: 0;">
                                                <a href="' . $link_cases . '" style="width: 100%;">
                                                    <img src="' . $path_imgs . '" class="w-100">
                                                </a>
                                            </div>
                                            <div class="col-lg-6 text_col_search">
                                                <div style="font-weight: 600;">' . $list[$i + 1]->judul . '</div><br>
                                                <div>Rp. ' . $tot_donasis . '</div>
                                                <div>' . $sisa_waktus . '</div>
                                                <a href="' . $link_helps . '" class="btn btn-warning sess_logged_in" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>';
                }else{
                    $row[] ='<div class="row">
                                <div class="col-md-6" style="padding: 10px;">
                                    <div style="padding: 10px; background: rgb(255,239,212);">
                                        <div class="row" style="margin: 0;">
                                            <div class="col-lg-6" style="padding: 0;">
                                                <a href="' . $link_case . '" style="width: 100%;">
                                                    <img src="' . $path_img . '" class="w-100">
                                                </a>
                                            </div>
                                            <div class="col-lg-6 text_col_search">
                                                <div style="font-weight: 600;">' . $list[$i]->judul . '</div><br>
                                                <div>Rp. ' . $tot_donasi . '</div>
                                                <div>' . $sisa_waktu . '</div>
                                                <a href="' . $link_help . '" class="btn btn-warning" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6" style="padding: 10px;">
                                    <div style="padding: 10px; background: rgb(255,239,212);">
                                        <div class="row" style="margin: 0;">
                                            <div class="col-lg-6" style="padding: 0;">
                                                <a href="' . $link_cases . '" style="width: 100%;">
                                                    <img src="' . $path_imgs . '" class="w-100">
                                                </a>
                                            </div>
                                            <div class="col-lg-6 text_col_search">
                                                <div style="font-weight: 600;">' . $list[$i + 1]->judul . '</div><br>
                                                <div>Rp. ' . $tot_donasis . '</div>
                                                <div>' . $sisa_waktus . '</div>
                                                <a href="' . $link_helps . '" class="btn btn-warning" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>';
                }
            }
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $totals,
            "recordsFiltered" => $filters,
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function search_agenda(){
        $keysearch = $this->input->post('keysearch');
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = $this->model->search_agenda($keysearch);
        $data = array();
        for($i = 0; $i < count($list); $i += 2) {
            $row = array();
            if($i + 1 == count($list)){
                if($list[$i]->type == '0'){//program
                    $path_img = site_url('assets/project/program/' . $list[$i]->image);
                    $site_link = site_url('program/' . str_replace(" ", "_", $list[$i]->title));
                }else{
                    $path_img = site_url('assets/project/kegiatan/' . $list[$i]->image);
                    $site_link = site_url('kegiatan/' . str_replace(" ", "_", $list[$i]->title));
                }
                $s_created = explode(' ', $list[$i]->created_at);
                $s_created_date = explode('-', $s_created[0]);
                $dates = $s_created_date[2] .'/'. $s_created_date[1] .'/'. $s_created_date[0]; 
                $row[] ='<div class="row">
                            <div class="col-md-6" style="padding: 10px;">
                                <div style="padding: 10px; background: rgb(255,239,212);">
                                    <div class="row" style="margin: 0;">
                                        <div class="col-lg-6" style="padding: 0;">
                                            <img src="' . $path_img . '" class="w-100 img_artikel" data="' . $list[$i]->id_agenda . '" style="cursor: pointer;">
                                        </div>
                                        <div class="col-lg-6 text_col_search">
                                            <div style="font-weight: 600;">' . $list[$i]->title . '</div>
                                            <div style="text-align: justify; font-size: 14px;">' . $dates . '</div>
                                            <a href="' . $site_link . '" style="color: rgb(255,197,70);">Selengkapnya</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
            }else{
                if($list[$i]->type == '0'){//program
                    $path_img = site_url('assets/project/program/' . $list[$i]->image);
                    $site_link = site_url('program/' . str_replace(" ", "_", $list[$i]->title));
                }else{
                    $path_img = site_url('assets/project/kegiatan/' . $list[$i]->image);
                    $site_link = site_url('kegiatan/' . str_replace(" ", "_", $list[$i]->title));
                }
                $s_created = explode(' ', $list[$i]->created_at);
                $s_created_date = explode('-', $s_created[0]);
                $dates = $s_created_date[2] .'/'. $s_created_date[1] .'/'. $s_created_date[0];
                ////////////////////////////////////////////
                if($list[$i + 1]->type == '0'){//program
                    $path_imgs = site_url('assets/project/program/' . $list[$i + 1]->image);
                    $site_links = site_url('program/' . str_replace(" ", "_", $list[$i + 1]->title));
                }else{
                    $path_imgs = site_url('assets/project/kegiatan/' . $list[$i + 1]->image);
                    $site_links = site_url('kegiatan/' . str_replace(" ", "_", $list[$i + 1]->title));
                }
                $s_createds = explode(' ', $list[$i + 1]->created_at);
                $s_created_dates = explode('-', $s_createds[0]);
                $datess = $s_created_dates[2] .'/'. $s_created_dates[1] .'/'. $s_created_dates[0];
                $row[] ='<div class="row">
                            <div class="col-md-6" style="padding: 10px;">
                                <div style="padding: 10px; background: rgb(255,239,212);">
                                    <div class="row" style="margin: 0;">
                                        <div class="col-lg-6" style="padding: 0;">
                                            <img src="' . $path_img . '" class="w-100 img_artikel" data="' . $list[$i]->id_agenda . '" style="cursor: pointer;">
                                        </div>
                                        <div class="col-lg-6 text_col_search">
                                            <div style="font-weight: 600;">' . $list[$i]->title . '</div>
                                            <div style="text-align: justify; font-size: 14px;">' . $dates . '</div>
                                            <a href="' . $site_link . '" style="color: rgb(255,197,70);">Selengkapnya</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" style="padding: 10px;">
                                <div style="padding: 10px; background: rgb(255,239,212);">
                                    <div class="row" style="margin: 0;">
                                        <div class="col-lg-6" style="padding: 0;">
                                            <img src="' . $path_imgs . '" class="w-100 img_artikel" data="' . $list[$i + 1]->id_agenda . '" style="cursor: pointer;">
                                        </div>
                                        <div class="col-lg-6 text_col_search">
                                            <div style="font-weight: 600;">' . $list[$i + 1]->title . '</div>
                                            <div style="text-align: justify; font-size: 14px;">' . $datess . '</div>
                                            <a href="' . $site_links . '" style="color: rgb(255,197,70);">Selengkapnya</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
            }
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->total_search_agenda($keysearch),
            "recordsFiltered" => $this->model->filter_search_agenda($keysearch),
            "data" => $data,
        );
        echo json_encode($output);
    }
    //End Pencarian
    //Start List Agenda
    public function list_kegiatan(){
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = $this->model->list_kegiatan();
        $data = array();
        for($i = 0; $i < count($list); $i += 3) {
            $row = array();
            if($i + 1 == count($list)){
                $path_img = site_url('assets/project/kegiatan/' . $list[$i]->image);
                // $s_created = explode(' ', $list[$i]->created_at);
                // $s_created_date = explode('-', $s_created[0]);
                // $dates = $s_created_date[2] .'/'. $s_created_date[1] .'/'. $s_created_date[0]; 
                $doc = new DOMDocument();
                $doc->loadHTML($list[$i]->description);
                $items = array();
                foreach ($doc->getElementsByTagName('p') as $p) {
                    $items[] = $p->nodeValue;
                }
                if(count($items) == 0){
                    foreach ($doc->getElementsByTagName('div') as $p) {
                        $items[] = $p->nodeValue;
                    }
                }
                $list[$i]->description = substr($items[0], 0, 50);
                $row[] ='<div class="row">
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 10px; background: rgb(255,239,212);">
                                    <div class="row" style="margin: 0;">
                                        <div class="col-md-12" style="padding: 0;">
                                            <img src="' . $path_img . '" class="w-100 img_artikel" data="' . $list[$i]->id_agenda . '" style="cursor: pointer;">
                                            <div style="font-weight: 600;">' . $list[$i]->title . '</div>
                                            <div style="text-align: justify;">' . $list[$i]->description . '</div>
                                            <a href="' . site_url('kegiatan/' . str_replace(" ", "_", $list[$i]->title)) . '" style="color: rgb(255,197,70);">Selengkapnya</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
            }elseif($i + 2 == count($list)){
                $path_img = site_url('assets/project/kegiatan/' . $list[$i]->image);
                // $s_created = explode(' ', $list[$i]->created_at);
                // $s_created_date = explode('-', $s_created[0]);
                // $dates = $s_created_date[2] .'/'. $s_created_date[1] .'/'. $s_created_date[0];
                $doc = new DOMDocument();
                $doc->loadHTML($list[$i]->description);
                $items = array();
                foreach ($doc->getElementsByTagName('p') as $p) {
                    $items[] = $p->nodeValue;
                }
                if(count($items) == 0){
                    foreach ($doc->getElementsByTagName('div') as $p) {
                        $items[] = $p->nodeValue;
                    }
                }
                $list[$i]->description = substr($items[0], 0, 50);
                ////////////////////////////////////////////
                $path_imgs = site_url('assets/project/kegiatan/' . $list[$i + 1]->image);
                // $s_createds = explode(' ', $list[$i + 1]->created_at);
                // $s_created_dates = explode('-', $s_createds[0]);
                // $datess = $s_created_dates[2] .'/'. $s_created_dates[1] .'/'. $s_created_dates[0];
                $docs = new DOMDocument();
                $docs->loadHTML($list[$i + 1]->description);
                $itemss = array();
                foreach ($docs->getElementsByTagName('p') as $p) {
                    $itemss[] = $p->nodeValue;
                }
                if(count($itemss) == 0){
                    foreach ($docs->getElementsByTagName('div') as $p) {
                        $itemss[] = $p->nodeValue;
                    }
                }
                $list[$i + 1]->description = substr($itemss[0], 0, 50);
                $row[] ='<div class="row">
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 10px; background: rgb(255,239,212);">
                                    <div class="row" style="margin: 0;">
                                        <div class="col-md-12" style="padding: 0;">
                                            <img src="' . $path_img . '" class="w-100 img_artikel" data="' . $list[$i]->id_agenda . '" style="cursor: pointer;">
                                            <div style="font-weight: 600;">' . $list[$i]->title . '</div>
                                            <div style="text-align: justify;">' . $list[$i]->description . '</div>
                                            <a href="' . site_url('kegiatan/' . str_replace(" ", "_", $list[$i]->title)) . '" style="color: rgb(255,197,70);">Selengkapnya</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 10px; background: rgb(255,239,212);">
                                    <div class="row" style="margin: 0;">
                                        <div class="col-md-12" style="padding: 0;">
                                            <img src="' . $path_imgs . '" class="w-100 img_artikel" data="' . $list[$i + 1]->id_agenda . '" style="cursor: pointer;">
                                            <div style="font-weight: 600;">' . $list[$i + 1]->title . '</div>
                                            <div style="text-align: justify;">' . $list[$i + 1]->description . '</div>
                                            <a href="' . site_url('kegiatan/' . str_replace(" ", "_", $list[$i + 1]->title)) . '" style="color: rgb(255,197,70);">Selengkapnya</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
            }else{
                $path_img = site_url('assets/project/kegiatan/' . $list[$i]->image);
                // $s_created = explode(' ', $list[$i]->created_at);
                // $s_created_date = explode('-', $s_created[0]);
                // $dates = $s_created_date[2] .'/'. $s_created_date[1] .'/'. $s_created_date[0];
                $doc = new DOMDocument();
                $doc->loadHTML($list[$i]->description);
                $items = array();
                foreach ($doc->getElementsByTagName('p') as $p) {
                    $items[] = $p->nodeValue;
                }
                if(count($items) == 0){
                    foreach ($doc->getElementsByTagName('div') as $p) {
                        $items[] = $p->nodeValue;
                    }
                }
                $list[$i]->description = substr($items[0], 0, 50);
                ////////////////////////////////////////////
                $path_imgs = site_url('assets/project/kegiatan/' . $list[$i + 1]->image);
                // $s_createds = explode(' ', $list[$i + 1]->created_at);
                // $s_created_dates = explode('-', $s_createds[0]);
                // $datess = $s_created_dates[2] .'/'. $s_created_dates[1] .'/'. $s_created_dates[0];
                $docs = new DOMDocument();
                $docs->loadHTML($list[$i + 1]->description);
                $itemss = array();
                foreach ($docs->getElementsByTagName('p') as $p) {
                    $itemss[] = $p->nodeValue;
                }
                if(count($itemss) == 0){
                    foreach ($docs->getElementsByTagName('div') as $p) {
                        $itemss[] = $p->nodeValue;
                    }
                }
                $list[$i + 1]->description = substr($itemss[0], 0, 50);
                ////////////////////////////////////////////
                $path_imgss = site_url('assets/project/kegiatan/' . $list[$i + 2]->image);
                // $s_createdss = explode(' ', $list[$i + 2]->created_at);
                // $s_created_datess = explode('-', $s_createdss[0]);
                // $datesss = $s_created_datess[2] .'/'. $s_created_datess[1] .'/'. $s_created_datess[0];
                $docss = new DOMDocument();
                $docss->loadHTML($list[$i + 2]->description);
                $itemsss = array();
                foreach ($docss->getElementsByTagName('p') as $p) {
                    $itemsss[] = $p->nodeValue;
                }
                if(count($itemsss) == 0){
                    foreach ($docss->getElementsByTagName('div') as $p) {
                        $itemsss[] = $p->nodeValue;
                    }
                }
                $list[$i + 2]->description = substr($itemsss[0], 0, 50);
                $row[] ='<div class="row">
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 10px; background: rgb(255,239,212);">
                                    <div class="row" style="margin: 0;">
                                        <div class="col-md-12" style="padding: 0;">
                                            <img src="' . $path_img . '" class="w-100 img_artikel" data="' . $list[$i]->id_agenda . '" style="cursor: pointer;">
                                            <div style="font-weight: 600;">' . $list[$i]->title . '</div>
                                            <div style="text-align: justify;">' . $list[$i]->description . '</div>
                                            <a href="' . site_url('kegiatan/' . str_replace(" ", "_", $list[$i]->title)) . '" style="color: rgb(255,197,70);">Selengkapnya</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 10px; background: rgb(255,239,212);">
                                    <div class="row" style="margin: 0;">
                                        <div class="col-md-12" style="padding: 0;">
                                            <img src="' . $path_imgs . '" class="w-100 img_artikel" data="' . $list[$i + 1]->id_agenda . '" style="cursor: pointer;">
                                            <div style="font-weight: 600;">' . $list[$i + 1]->title . '</div>
                                            <div style="text-align: justify;">' . $list[$i + 1]->description . '</div>
                                            <a href="' . site_url('kegiatan/' . str_replace(" ", "_", $list[$i + 1]->title)) . '" style="color: rgb(255,197,70);">Selengkapnya</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 10px; background: rgb(255,239,212);">
                                    <div class="row" style="margin: 0;">
                                        <div class="col-md-12" style="padding: 0;">
                                            <img src="' . $path_imgss . '" class="w-100 img_artikel" data="' . $list[$i + 2]->id_agenda . '" style="cursor: pointer;">
                                            <div style="font-weight: 600;">' . $list[$i + 2]->title . '</div>
                                            <div style="text-align: justify;">' . $list[$i + 2]->description . '</div>
                                            <a href="' . site_url('kegiatan/' . str_replace(" ", "_", $list[$i + 2]->title)) . '" style="color: rgb(255,197,70);">Selengkapnya</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
            }
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->total_list_kegiatan(),
            "recordsFiltered" => $this->model->filter_list_kegiatan(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function list_program(){
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = $this->model->list_program();
        $data = array();
        for($i = 0; $i < count($list); $i ++) {
            $row = array();
            $path_img = site_url('assets/project/program/' . $list[$i]->image);
            // $s_created = explode(' ', $list[$i]->created_at);
            // $s_created_date = explode('-', $s_created[0]);
            // $dates = $s_created_date[2] .'/'. $s_created_date[1] .'/'. $s_created_date[0]; 
            $doc = new DOMDocument();
            $doc->loadHTML($list[$i]->description);
            $items = array();
            foreach ($doc->getElementsByTagName('p') as $p) {
                $items[] = $p->nodeValue;
            }
            if(count($items) == 0){
                foreach ($doc->getElementsByTagName('div') as $p) {
                    $items[] = $p->nodeValue;
                }
            }
            $list[$i]->description = substr($items[0], 0, 300);
            $row[] ='<div class="row">
                        <div class="col-md-12" style="padding: 10px;">
                            <div style="padding: 25px; background: rgb(255,239,212);">
                                <div class="row" style="margin: 0;">
                                    <div class="col-lg-6" style="padding: 0;">
                                        <img src="' . $path_img . '" class="w-100 img_artikel" data="' . $list[$i]->id_agenda . '" style="cursor: pointer;">
                                    </div>
                                    <div class="col-lg-6 text_col_search">
                                        <div style="font-weight: 600; margin-bottom: 15px;">' . $list[$i]->title . '</div>
                                        <div style="text-align: justify;">' . $list[$i]->description . '</div>
                                        <a href="' . site_url('program/' . str_replace(" ", "_", $list[$i]->title)) . '" style="color: rgb(255,197,70);">Selengkapnya</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->total_list_program(),
            "recordsFiltered" => $this->model->filter_list_program(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    //End List Agenda
    //Start List Donasi Medis
    public function list_donasi_medis(){
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $id_kategori = $this->input->post('id_kategori');
        $list = $this->model->list_donasi_medis($id_kategori);
        $data = array();
        for($i = 0; $i < count($list); $i += 3) {
            $row = array();
            if($i + 1 == count($list)){
                $tgl1 = strtotime(date('Y-m-d')); 
                $tgl2 = strtotime($list[$i]->jatuh_tempo); 
                $selisih = $tgl2 - $tgl1;
                $selisih = $selisih / 60 / 60 / 24;
                $list[$i]->jatuh_tempo = number_format($selisih , 0, ',', '.');

                $total_donasi = $this->model->total_donasi_medis($list[$i]->id_kasus_medis);
                if($total_donasi->donasi == null){
                    $list[$i]->donasi = '0';
                }else{
                    $list[$i]->donasi = number_format($total_donasi->donasi , 0, ',', '.');
                }
                $donatur = $this->model->total_donatur_medis($list[$i]->id_kasus_medis);
                $list[$i]->donatur = number_format($donatur , 0, ',', '.');
                if(strlen($list[$i]->judul) > 90){
                    $list[$i]->judul = substr($list[$i]->judul, 0, 90);
                }else{
                    $sisa_string = 75 - (int)strlen($list[$i]->judul);
                    $left_string = '';
                    for($x = 0; $x < $sisa_string; $x ++){
                        if ($x % 2 == 0){
                            $left_string .= ' ';
                        }else {
                            $left_string .= '_';
                        }
                    }
                    $list[$i]->judul = $list[$i]->judul . '<span style="color: transparent">' . $left_string . '</span>';
                }
                if(! $this->ion_auth->logged_in()){
                    $logged = '<a href="' . site_url('donasi/medis_help/' . $list[$i]->link) . '" class="btn btn-warning sess_logged_in" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                }else{
                    $logged = '<a href="' . site_url('donasi/medis_help/' . $list[$i]->link) . '" class="btn btn-warning" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                }
                $row[] ='<div class="row">
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 15px; background: rgba(0,0,0,0.1);">
                                    <div style="text-align: left; font-weight: 500; text-transform: capitalize; font-size: 16px; margin-bottom: 5px;" class="donasi_title">' . $list[$i]->judul . '</div>
                                    <a href="' . site_url('donasi/medis/' . $list[$i]->link) . '" style="width: 100%;"><img src="' . site_url('assets/project/kasus_medis/' . $list[$i]->sampul) . '" class="w-100 img_donasi" style="cursor: pointer; border-radius: 5px; background: #fff; margin-bottom: 5px;"></a>
                                    <div>Rp. <span style="font-weight: 600;">' . $list[$i]->donasi . '</span></div>
                                    <div><span style="font-weight: 600;">' . $list[$i]->donatur . '</span> Donatur</div>
                                    <div style="margin-top: 5px;"><span style="font-weight: 600;">' . $list[$i]->jatuh_tempo . '</span> Hari Lagi</div>' . $logged . '
                                </div>
                            </div>
                        </div>';
            }elseif($i + 2 == count($list)){
                $tgl1 = strtotime(date('Y-m-d')); 
                $tgl2 = strtotime($list[$i]->jatuh_tempo); 
                $selisih = $tgl2 - $tgl1;
                $selisih = $selisih / 60 / 60 / 24;
                $list[$i]->jatuh_tempo = number_format($selisih , 0, ',', '.');

                $total_donasi = $this->model->total_donasi_medis($list[$i]->id_kasus_medis);
                if($total_donasi->donasi == null){
                    $list[$i]->donasi = '0';
                }else{
                    $list[$i]->donasi = number_format($total_donasi->donasi , 0, ',', '.');
                }
                $donatur = $this->model->total_donatur_medis($list[$i]->id_kasus_medis);
                $list[$i]->donatur = number_format($donatur , 0, ',', '.');
                if(strlen($list[$i]->judul) > 90){
                    $list[$i]->judul = substr($list[$i]->judul, 0, 90);
                }else{
                    $sisa_string = 75 - (int)strlen($list[$i]->judul);
                    $left_string = '';
                    for($x = 0; $x < $sisa_string; $x ++){
                        if ($x % 2 == 0){
                            $left_string .= ' ';
                        }else {
                            $left_string .= '_';
                        }
                    }
                    $list[$i]->judul = $list[$i]->judul . '<span style="color: transparent">' . $left_string . '</span>';
                }
                ////////////////////////////////////////////
                $tgls1 = strtotime(date('Y-m-d')); 
                $tgls2 = strtotime($list[$i + 1]->jatuh_tempo); 
                $selisihs = $tgls2 - $tgls1;
                $selisihs = $selisihs / 60 / 60 / 24;
                $list[$i + 1]->jatuh_tempo = number_format($selisihs , 0, ',', '.');

                $total_donasis = $this->model->total_donasi_medis($list[$i + 1]->id_kasus_medis);
                if($total_donasis->donasi == null){
                    $list[$i + 1]->donasi = '0';
                }else{
                    $list[$i + 1]->donasi = number_format($total_donasis->donasi , 0, ',', '.');
                }
                $donaturs = $this->model->total_donatur_medis($list[$i + 1]->id_kasus_medis);
                $list[$i + 1]->donatur = number_format($donaturs , 0, ',', '.');
                if(strlen($list[$i + 1]->judul) > 90){
                    $list[$i + 1]->judul = substr($list[$i + 1]->judul, 0, 90);
                }else{
                    $sisa_strings = 75 - (int)strlen($list[$i + 1]->judul);
                    $left_strings = '';
                    for($x = 0; $x < $sisa_strings; $x ++){
                        if ($x % 2 == 0){
                            $left_strings .= ' ';
                        }else {
                            $left_strings .= '_';
                        }
                    }
                    $list[$i + 1]->judul = $list[$i + 1]->judul . '<span style="color: transparent">' . $left_strings . '</span>';
                }

                if(! $this->ion_auth->logged_in()){
                    $logged = '<a href="' . site_url('donasi/medis_help/' . $list[$i]->link) . '" class="btn btn-warning sess_logged_in" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                    $loggeds = '<a href="' . site_url('donasi/medis_help/' . $list[$i + 1]->link) . '" class="btn btn-warning sess_logged_in" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                }else{
                    $logged = '<a href="' . site_url('donasi/medis_help/' . $list[$i]->link) . '" class="btn btn-warning" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                    $loggeds = '<a href="' . site_url('donasi/medis_help/' . $list[$i + 1]->link) . '" class="btn btn-warning" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                }
                $row[] ='<div class="row">
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 15px; background: rgba(0,0,0,0.1);">
                                    <div style="text-align: left; font-weight: 500; text-transform: capitalize; font-size: 16px; margin-bottom: 5px;" class="donasi_title">' . $list[$i]->judul . '</div>
                                    <a href="' . site_url('donasi/medis/' . $list[$i]->link) . '" style="width: 100%;"><img src="' . site_url('assets/project/kasus_medis/' . $list[$i]->sampul) . '" class="w-100 img_donasi" style="cursor: pointer; border-radius: 5px; background: #fff; margin-bottom: 5px;"></a>
                                    <div>Rp. <span style="font-weight: 600;">' . $list[$i]->donasi . '</span></div>
                                    <div><span style="font-weight: 600;">' . $list[$i]->donatur . '</span> Donatur</div>
                                    <div style="margin-top: 5px;"><span style="font-weight: 600;">' . $list[$i]->jatuh_tempo . '</span> Hari Lagi</div>' . $logged . '
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 15px; background: rgba(0,0,0,0.1);">
                                    <div style="text-align: left; font-weight: 500; text-transform: capitalize; font-size: 16px; margin-bottom: 5px;" class="donasi_title">' . $list[$i + 1]->judul . '</div>
                                    <a href="' . site_url('donasi/medis/' . $list[$i + 1]->link) . '" style="width: 100%;"><img src="' . site_url('assets/project/kasus_medis/' . $list[$i + 1]->sampul) . '" class="w-100 img_donasi" style="cursor: pointer; border-radius: 5px; background: #fff; margin-bottom: 5px;"></a>
                                    <div>Rp. <span style="font-weight: 600;">' . $list[$i + 1]->donasi . '</span></div>
                                    <div><span style="font-weight: 600;">' . $list[$i + 1]->donatur . '</span> Donatur</div>
                                    <div style="margin-top: 5px;"><span style="font-weight: 600;">' . $list[$i + 1]->jatuh_tempo . '</span> Hari Lagi</div>' . $loggeds . '
                                </div>
                            </div>
                        </div>';
            }else{
                $tgl1 = strtotime(date('Y-m-d')); 
                $tgl2 = strtotime($list[$i]->jatuh_tempo); 
                $selisih = $tgl2 - $tgl1;
                $selisih = $selisih / 60 / 60 / 24;
                $list[$i]->jatuh_tempo = number_format($selisih , 0, ',', '.');

                $total_donasi = $this->model->total_donasi_medis($list[$i]->id_kasus_medis);
                if($total_donasi->donasi == null){
                    $list[$i]->donasi = '0';
                }else{
                    $list[$i]->donasi = number_format($total_donasi->donasi , 0, ',', '.');
                }
                $donatur = $this->model->total_donatur_medis($list[$i]->id_kasus_medis);
                $list[$i]->donatur = number_format($donatur , 0, ',', '.');
                if(strlen($list[$i]->judul) > 90){
                    $list[$i]->judul = substr($list[$i]->judul, 0, 90);
                }else{
                    $sisa_string = 75 - (int)strlen($list[$i]->judul);
                    $left_string = '';
                    for($x = 0; $x < $sisa_string; $x ++){
                        if ($x % 2 == 0){
                            $left_string .= ' ';
                        }else {
                            $left_string .= '_';
                        }
                    }
                    $list[$i]->judul = $list[$i]->judul . '<span style="color: transparent">' . $left_string . '</span>';
                }
                ////////////////////////////////////////////
                $tgls1 = strtotime(date('Y-m-d')); 
                $tgls2 = strtotime($list[$i + 1]->jatuh_tempo); 
                $selisihs = $tgls2 - $tgls1;
                $selisihs = $selisihs / 60 / 60 / 24;
                $list[$i + 1]->jatuh_tempo = number_format($selisihs , 0, ',', '.');

                $total_donasis = $this->model->total_donasi_medis($list[$i + 1]->id_kasus_medis);
                if($total_donasis->donasi == null){
                    $list[$i + 1]->donasi = '0';
                }else{
                    $list[$i + 1]->donasi = number_format($total_donasis->donasi , 0, ',', '.');
                }
                $donaturs = $this->model->total_donatur_medis($list[$i + 1]->id_kasus_medis);
                $list[$i + 1]->donatur = number_format($donaturs , 0, ',', '.');
                if(strlen($list[$i + 1]->judul) > 90){
                    $list[$i + 1]->judul = substr($list[$i + 1]->judul, 0, 90);
                }else{
                    $sisa_strings = 75 - (int)strlen($list[$i + 1]->judul);
                    $left_strings = '';
                    for($x = 0; $x < $sisa_strings; $x ++){
                        if ($x % 2 == 0){
                            $left_strings .= ' ';
                        }else {
                            $left_strings .= '_';
                        }
                    }
                    $list[$i + 1]->judul = $list[$i + 1]->judul . '<span style="color: transparent">' . $left_strings . '</span>';
                }
                ////////////////////////////////////////////
                $tglss1 = strtotime(date('Y-m-d')); 
                $tglss2 = strtotime($list[$i + 2]->jatuh_tempo); 
                $selisihss = $tglss2 - $tglss1;
                $selisihss = $selisihss / 60 / 60 / 24;
                $list[$i + 2]->jatuh_tempo = number_format($selisihss , 0, ',', '.');

                $total_donasiss = $this->model->total_donasi_medis($list[$i + 2]->id_kasus_medis);
                if($total_donasiss->donasi == null){
                    $list[$i + 2]->donasi = '0';
                }else{
                    $list[$i + 2]->donasi = number_format($total_donasiss->donasi , 0, ',', '.');
                }
                $donaturss = $this->model->total_donatur_medis($list[$i + 2]->id_kasus_medis);
                $list[$i + 2]->donatur = number_format($donaturss , 0, ',', '.');
                if(strlen($list[$i + 2]->judul) > 90){
                    $list[$i + 2]->judul = substr($list[$i + 2]->judul, 0, 90);
                }else{
                    $sisa_stringss = 75 - (int)strlen($list[$i + 2]->judul);
                    $left_stringss = '';
                    for($x = 0; $x < $sisa_stringss; $x ++){
                        if ($x % 2 == 0){
                            $left_stringss .= ' ';
                        }else {
                            $left_stringss .= '_';
                        }
                    }
                    $list[$i + 2]->judul = $list[$i + 2]->judul . '<span style="color: transparent">' . $left_stringss . '</span>';
                }
                
                if(! $this->ion_auth->logged_in()){
                    $logged = '<a href="' . site_url('donasi/medis_help/' . $list[$i]->link) . '" class="btn btn-warning sess_logged_in" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                    $loggeds = '<a href="' . site_url('donasi/medis_help/' . $list[$i + 1]->link) . '" class="btn btn-warning sess_logged_in" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                    $loggedss = '<a href="' . site_url('donasi/medis_help/' . $list[$i + 2]->link) . '" class="btn btn-warning sess_logged_in" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                }else{
                    $logged = '<a href="' . site_url('donasi/medis_help/' . $list[$i]->link) . '" class="btn btn-warning" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                    $loggeds = '<a href="' . site_url('donasi/medis_help/' . $list[$i + 1]->link) . '" class="btn btn-warning" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                    $loggedss = '<a href="' . site_url('donasi/medis_help/' . $list[$i + 2]->link) . '" class="btn btn-warning" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                }
                $row[] ='<div class="row">
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 15px; background: rgba(0,0,0,0.1);">
                                    <div style="text-align: left; font-weight: 500; text-transform: capitalize; font-size: 16px; margin-bottom: 5px;" class="donasi_title">' . $list[$i]->judul . '</div>
                                    <a href="' . site_url('donasi/medis/' . $list[$i]->link) . '" style="width: 100%;"><img src="' . site_url('assets/project/kasus_medis/' . $list[$i]->sampul) . '" class="w-100 img_donasi" style="cursor: pointer; border-radius: 5px; background: #fff; margin-bottom: 5px;"></a>
                                    <div>Rp. <span style="font-weight: 600;">' . $list[$i]->donasi . '</span></div>
                                    <div><span style="font-weight: 600;">' . $list[$i]->donatur . '</span> Donatur</div>
                                    <div style="margin-top: 5px;"><span style="font-weight: 600;">' . $list[$i]->jatuh_tempo . '</span> Hari Lagi</div>' . $logged . '
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 15px; background: rgba(0,0,0,0.1);">
                                    <div style="text-align: left; font-weight: 500; text-transform: capitalize; font-size: 16px; margin-bottom: 5px;" class="donasi_title">' . $list[$i + 1]->judul . '</div>
                                    <a href="' . site_url('donasi/medis/' . $list[$i + 1]->link) . '" style="width: 100%;"><img src="' . site_url('assets/project/kasus_medis/' . $list[$i + 1]->sampul) . '" class="w-100 img_donasi" style="cursor: pointer; border-radius: 5px; background: #fff; margin-bottom: 5px;"></a>
                                    <div>Rp. <span style="font-weight: 600;">' . $list[$i + 1]->donasi . '</span></div>
                                    <div><span style="font-weight: 600;">' . $list[$i + 1]->donatur . '</span> Donatur</div>
                                    <div style="margin-top: 5px;"><span style="font-weight: 600;">' . $list[$i + 1]->jatuh_tempo . '</span> Hari Lagi</div>' . $loggeds . '
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 15px; background: rgba(0,0,0,0.1);">
                                    <div style="text-align: left; font-weight: 500; text-transform: capitalize; font-size: 16px; margin-bottom: 5px;" class="donasi_title">' . $list[$i + 2]->judul . '</div>
                                    <a href="' . site_url('donasi/medis/' . $list[$i + 2]->link) . '" style="width: 100%;"><img src="' . site_url('assets/project/kasus_medis/' . $list[$i + 2]->sampul) . '" class="w-100 img_donasi" style="cursor: pointer; border-radius: 5px; background: #fff; margin-bottom: 5px;"></a>
                                    <div>Rp. <span style="font-weight: 600;">' . $list[$i + 2]->donasi . '</span></div>
                                    <div><span style="font-weight: 600;">' . $list[$i + 2]->donatur . '</span> Donatur</div>
                                    <div style="margin-top: 5px;"><span style="font-weight: 600;">' . $list[$i + 2]->jatuh_tempo . '</span> Hari Lagi</div>' . $loggedss . '
                                </div>
                            </div>
                        </div>';
            }
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->total_list_donasi_medis($id_kategori),
            "recordsFiltered" => $this->model->filter_list_donasi_medis($id_kategori),
            "data" => $data,
        );
        echo json_encode($output);
    }
    //End List Donasi Medis
    //Start List Donasi Non Medis
    public function list_donasi_non_medis(){
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $id_kategori = $this->input->post('id_kategori');
        $list = $this->model->list_donasi_non_medis($id_kategori);
        $data = array();
        for($i = 0; $i < count($list); $i += 3) {
            $row = array();
            if($i + 1 == count($list)){
                $tgl1 = strtotime(date('Y-m-d')); 
                $tgl2 = strtotime($list[$i]->jatuh_tempo); 
                $selisih = $tgl2 - $tgl1;
                $selisih = $selisih / 60 / 60 / 24;
                $list[$i]->jatuh_tempo = number_format($selisih , 0, ',', '.');

                $total_donasi = $this->model->total_donasi_non_medis($list[$i]->id_kasus_non_medis);
                if($total_donasi->donasi == null){
                    $list[$i]->donasi = '0';
                }else{
                    $list[$i]->donasi = number_format($total_donasi->donasi , 0, ',', '.');
                }
                $donatur = $this->model->total_donatur_non_medis($list[$i]->id_kasus_non_medis);
                $list[$i]->donatur = number_format($donatur , 0, ',', '.');
                if(strlen($list[$i]->judul) > 90){
                    $list[$i]->judul = substr($list[$i]->judul, 0, 90);
                }else{
                    $sisa_string = 75 - (int)strlen($list[$i]->judul);
                    $left_string = '';
                    for($x = 0; $x < $sisa_string; $x ++){
                        if ($x % 2 == 0){
                            $left_string .= ' ';
                        }else {
                            $left_string .= '_';
                        }
                    }
                    $list[$i]->judul = $list[$i]->judul . '<span style="color: transparent">' . $left_string . '</span>';
                }
                if(! $this->ion_auth->logged_in()){
                    $logged = '<a href="' . site_url('donasi/non_medis_help/' . $list[$i]->link) . '" class="btn btn-warning sess_logged_in" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                }else{
                    $logged = '<a href="' . site_url('donasi/non_medis_help/' . $list[$i]->link) . '" class="btn btn-warning" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                }
                $row[] ='<div class="row">
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 15px; background: rgba(0,0,0,0.1);">
                                    <div style="text-align: left; font-weight: 500; text-transform: capitalize; font-size: 16px; margin-bottom: 5px;" class="donasi_title">' . $list[$i]->judul . '</div>
                                    <a href="' . site_url('donasi/non_medis/' . $list[$i]->link) . '" style="width: 100%;"><img src="' . site_url('assets/project/kasus_non_medis/' . $list[$i]->sampul) . '" class="w-100 img_donasi" style="cursor: pointer; border-radius: 5px; background: #fff; margin-bottom: 5px;"></a>
                                    <div>Rp. <span style="font-weight: 600;">' . $list[$i]->donasi . '</span></div>
                                    <div><span style="font-weight: 600;">' . $list[$i]->donatur . '</span> Donatur</div>
                                    <div style="margin-top: 5px;"><span style="font-weight: 600;">' . $list[$i]->jatuh_tempo . '</span> Hari Lagi</div>' . $logged . '
                                </div>
                            </div>
                        </div>';
            }elseif($i + 2 == count($list)){
                $tgl1 = strtotime(date('Y-m-d')); 
                $tgl2 = strtotime($list[$i]->jatuh_tempo); 
                $selisih = $tgl2 - $tgl1;
                $selisih = $selisih / 60 / 60 / 24;
                $list[$i]->jatuh_tempo = number_format($selisih , 0, ',', '.');

                $total_donasi = $this->model->total_donasi_non_medis($list[$i]->id_kasus_non_medis);
                if($total_donasi->donasi == null){
                    $list[$i]->donasi = '0';
                }else{
                    $list[$i]->donasi = number_format($total_donasi->donasi , 0, ',', '.');
                }
                $donatur = $this->model->total_donatur_non_medis($list[$i]->id_kasus_non_medis);
                $list[$i]->donatur = number_format($donatur , 0, ',', '.');
                if(strlen($list[$i]->judul) > 90){
                    $list[$i]->judul = substr($list[$i]->judul, 0, 90);
                }else{
                    $sisa_string = 75 - (int)strlen($list[$i]->judul);
                    $left_string = '';
                    for($x = 0; $x < $sisa_string; $x ++){
                        if ($x % 2 == 0){
                            $left_string .= ' ';
                        }else {
                            $left_string .= '_';
                        }
                    }
                    $list[$i]->judul = $list[$i]->judul . '<span style="color: transparent">' . $left_string . '</span>';
                }
                ////////////////////////////////////////////
                $tgls1 = strtotime(date('Y-m-d')); 
                $tgls2 = strtotime($list[$i + 1]->jatuh_tempo); 
                $selisihs = $tgls2 - $tgls1;
                $selisihs = $selisihs / 60 / 60 / 24;
                $list[$i + 1]->jatuh_tempo = number_format($selisihs , 0, ',', '.');

                $total_donasis = $this->model->total_donasi_non_medis($list[$i + 1]->id_kasus_non_medis);
                if($total_donasis->donasi == null){
                    $list[$i + 1]->donasi = '0';
                }else{
                    $list[$i + 1]->donasi = number_format($total_donasis->donasi , 0, ',', '.');
                }
                $donaturs = $this->model->total_donatur_non_medis($list[$i + 1]->id_kasus_non_medis);
                $list[$i + 1]->donatur = number_format($donaturs , 0, ',', '.');
                if(strlen($list[$i + 1]->judul) > 90){
                    $list[$i + 1]->judul = substr($list[$i + 1]->judul, 0, 90);
                }else{
                    $sisa_strings = 75 - (int)strlen($list[$i + 1]->judul);
                    $left_strings = '';
                    for($x = 0; $x < $sisa_strings; $x ++){
                        if ($x % 2 == 0){
                            $left_strings .= ' ';
                        }else {
                            $left_strings .= '_';
                        }
                    }
                    $list[$i + 1]->judul = $list[$i + 1]->judul . '<span style="color: transparent">' . $left_strings . '</span>';
                }

                if(! $this->ion_auth->logged_in()){
                    $logged = '<a href="' . site_url('donasi/non_medis_help/' . $list[$i]->link) . '" class="btn btn-warning sess_logged_in" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                    $loggeds = '<a href="' . site_url('donasi/non_medis_help/' . $list[$i + 1]->link) . '" class="btn btn-warning sess_logged_in" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                }else{
                    $logged = '<a href="' . site_url('donasi/non_medis_help/' . $list[$i]->link) . '" class="btn btn-warning" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                    $loggeds = '<a href="' . site_url('donasi/non_medis_help/' . $list[$i + 1]->link) . '" class="btn btn-warning" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                }
                $row[] ='<div class="row">
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 15px; background: rgba(0,0,0,0.1);">
                                    <div style="text-align: left; font-weight: 500; text-transform: capitalize; font-size: 16px; margin-bottom: 5px;" class="donasi_title">' . $list[$i]->judul . '</div>
                                    <a href="' . site_url('donasi/non_medis/' . $list[$i]->link) . '" style="width: 100%;"><img src="' . site_url('assets/project/kasus_non_medis/' . $list[$i]->sampul) . '" class="w-100 img_donasi" style="cursor: pointer; border-radius: 5px; background: #fff; margin-bottom: 5px;"></a>
                                    <div>Rp. <span style="font-weight: 600;">' . $list[$i]->donasi . '</span></div>
                                    <div><span style="font-weight: 600;">' . $list[$i]->donatur . '</span> Donatur</div>
                                    <div style="margin-top: 5px;"><span style="font-weight: 600;">' . $list[$i]->jatuh_tempo . '</span> Hari Lagi</div>' . $logged . '
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 15px; background: rgba(0,0,0,0.1);">
                                    <div style="text-align: left; font-weight: 500; text-transform: capitalize; font-size: 16px; margin-bottom: 5px;" class="donasi_title">' . $list[$i + 1]->judul . '</div>
                                    <a href="' . site_url('donasi/non_medis/' . $list[$i + 1]->link) . '" style="width: 100%;"><img src="' . site_url('assets/project/kasus_non_medis/' . $list[$i + 1]->sampul) . '" class="w-100 img_donasi" style="cursor: pointer; border-radius: 5px; background: #fff; margin-bottom: 5px;"></a>
                                    <div>Rp. <span style="font-weight: 600;">' . $list[$i + 1]->donasi . '</span></div>
                                    <div><span style="font-weight: 600;">' . $list[$i + 1]->donatur . '</span> Donatur</div>
                                    <div style="margin-top: 5px;"><span style="font-weight: 600;">' . $list[$i + 1]->jatuh_tempo . '</span> Hari Lagi</div>' . $loggeds . '
                                </div>
                            </div>
                        </div>';
            }else{
                $tgl1 = strtotime(date('Y-m-d')); 
                $tgl2 = strtotime($list[$i]->jatuh_tempo); 
                $selisih = $tgl2 - $tgl1;
                $selisih = $selisih / 60 / 60 / 24;
                $list[$i]->jatuh_tempo = number_format($selisih , 0, ',', '.');

                $total_donasi = $this->model->total_donasi_non_medis($list[$i]->id_kasus_non_medis);
                if($total_donasi->donasi == null){
                    $list[$i]->donasi = '0';
                }else{
                    $list[$i]->donasi = number_format($total_donasi->donasi , 0, ',', '.');
                }
                $donatur = $this->model->total_donatur_non_medis($list[$i]->id_kasus_non_medis);
                $list[$i]->donatur = number_format($donatur , 0, ',', '.');
                if(strlen($list[$i]->judul) > 90){
                    $list[$i]->judul = substr($list[$i]->judul, 0, 90);
                }else{
                    $sisa_string = 75 - (int)strlen($list[$i]->judul);
                    $left_string = '';
                    for($x = 0; $x < $sisa_string; $x ++){
                        if ($x % 2 == 0){
                            $left_string .= ' ';
                        }else {
                            $left_string .= '_';
                        }
                    }
                    $list[$i]->judul = $list[$i]->judul . '<span style="color: transparent">' . $left_string . '</span>';
                }
                ////////////////////////////////////////////
                $tgls1 = strtotime(date('Y-m-d')); 
                $tgls2 = strtotime($list[$i + 1]->jatuh_tempo); 
                $selisihs = $tgls2 - $tgls1;
                $selisihs = $selisihs / 60 / 60 / 24;
                $list[$i + 1]->jatuh_tempo = number_format($selisihs , 0, ',', '.');

                $total_donasis = $this->model->total_donasi_non_medis($list[$i + 1]->id_kasus_non_medis);
                if($total_donasis->donasi == null){
                    $list[$i + 1]->donasi = '0';
                }else{
                    $list[$i + 1]->donasi = number_format($total_donasis->donasi , 0, ',', '.');
                }
                $donaturs = $this->model->total_donatur_non_medis($list[$i + 1]->id_kasus_non_medis);
                $list[$i + 1]->donatur = number_format($donaturs , 0, ',', '.');
                if(strlen($list[$i + 1]->judul) > 90){
                    $list[$i + 1]->judul = substr($list[$i + 1]->judul, 0, 90);
                }else{
                    $sisa_strings = 75 - (int)strlen($list[$i + 1]->judul);
                    $left_strings = '';
                    for($x = 0; $x < $sisa_strings; $x ++){
                        if ($x % 2 == 0){
                            $left_strings .= ' ';
                        }else {
                            $left_strings .= '_';
                        }
                    }
                    $list[$i + 1]->judul = $list[$i + 1]->judul . '<span style="color: transparent">' . $left_strings . '</span>';
                }
                ////////////////////////////////////////////
                $tglss1 = strtotime(date('Y-m-d')); 
                $tglss2 = strtotime($list[$i + 2]->jatuh_tempo); 
                $selisihss = $tglss2 - $tglss1;
                $selisihss = $selisihss / 60 / 60 / 24;
                $list[$i + 2]->jatuh_tempo = number_format($selisihss , 0, ',', '.');

                $total_donasiss = $this->model->total_donasi_non_medis($list[$i + 2]->id_kasus_non_medis);
                if($total_donasiss->donasi == null){
                    $list[$i + 2]->donasi = '0';
                }else{
                    $list[$i + 2]->donasi = number_format($total_donasiss->donasi , 0, ',', '.');
                }
                $donaturss = $this->model->total_donatur_non_medis($list[$i + 2]->id_kasus_non_medis);
                $list[$i + 2]->donatur = number_format($donaturss , 0, ',', '.');
                if(strlen($list[$i + 2]->judul) > 90){
                    $list[$i + 2]->judul = substr($list[$i + 2]->judul, 0, 90);
                }else{
                    $sisa_stringss = 75 - (int)strlen($list[$i + 2]->judul);
                    $left_stringss = '';
                    for($x = 0; $x < $sisa_stringss; $x ++){
                        if ($x % 2 == 0){
                            $left_stringss .= ' ';
                        }else {
                            $left_stringss .= '_';
                        }
                    }
                    $list[$i + 2]->judul = $list[$i + 2]->judul . '<span style="color: transparent">' . $left_stringss . '</span>';
                }
                
                if(! $this->ion_auth->logged_in()){
                    $logged = '<a href="' . site_url('donasi/non_medis_help/' . $list[$i]->link) . '" class="btn btn-warning sess_logged_in" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                    $loggeds = '<a href="' . site_url('donasi/non_medis_help/' . $list[$i + 1]->link) . '" class="btn btn-warning sess_logged_in" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                    $loggedss = '<a href="' . site_url('donasi/non_medis_help/' . $list[$i + 2]->link) . '" class="btn btn-warning sess_logged_in" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                }else{
                    $logged = '<a href="' . site_url('donasi/non_medis_help/' . $list[$i]->link) . '" class="btn btn-warning" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                    $loggeds = '<a href="' . site_url('donasi/non_medis_help/' . $list[$i + 1]->link) . '" class="btn btn-warning" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                    $loggedss = '<a href="' . site_url('donasi/non_medis_help/' . $list[$i + 2]->link) . '" class="btn btn-warning" style="width: 100%; margin-top: 5px;">Donasi Sekarang</a>';
                }
                $row[] ='<div class="row">
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 15px; background: rgba(0,0,0,0.1);">
                                    <div style="text-align: left; font-weight: 500; text-transform: capitalize; font-size: 16px; margin-bottom: 5px;" class="donasi_title">' . $list[$i]->judul . '</div>
                                    <a href="' . site_url('donasi/non_medis/' . $list[$i]->link) . '" style="width: 100%;"><img src="' . site_url('assets/project/kasus_non_medis/' . $list[$i]->sampul) . '" class="w-100 img_donasi" style="cursor: pointer; border-radius: 5px; background: #fff; margin-bottom: 5px;"></a>
                                    <div>Rp. <span style="font-weight: 600;">' . $list[$i]->donasi . '</span></div>
                                    <div><span style="font-weight: 600;">' . $list[$i]->donatur . '</span> Donatur</div>
                                    <div style="margin-top: 5px;"><span style="font-weight: 600;">' . $list[$i]->jatuh_tempo . '</span> Hari Lagi</div>' . $logged . '
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 15px; background: rgba(0,0,0,0.1);">
                                    <div style="text-align: left; font-weight: 500; text-transform: capitalize; font-size: 16px; margin-bottom: 5px;" class="donasi_title">' . $list[$i + 1]->judul . '</div>
                                    <a href="' . site_url('donasi/non_medis/' . $list[$i + 1]->link) . '" style="width: 100%;"><img src="' . site_url('assets/project/kasus_non_medis/' . $list[$i + 1]->sampul) . '" class="w-100 img_donasi" style="cursor: pointer; border-radius: 5px; background: #fff; margin-bottom: 5px;"></a>
                                    <div>Rp. <span style="font-weight: 600;">' . $list[$i + 1]->donasi . '</span></div>
                                    <div><span style="font-weight: 600;">' . $list[$i + 1]->donatur . '</span> Donatur</div>
                                    <div style="margin-top: 5px;"><span style="font-weight: 600;">' . $list[$i + 1]->jatuh_tempo . '</span> Hari Lagi</div>' . $loggeds . '
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 10px;">
                                <div style="padding: 15px; background: rgba(0,0,0,0.1);">
                                    <div style="text-align: left; font-weight: 500; text-transform: capitalize; font-size: 16px; margin-bottom: 5px;" class="donasi_title">' . $list[$i + 2]->judul . '</div>
                                    <a href="' . site_url('donasi/non_medis/' . $list[$i + 2]->link) . '" style="width: 100%;"><img src="' . site_url('assets/project/kasus_non_medis/' . $list[$i + 2]->sampul) . '" class="w-100 img_donasi" style="cursor: pointer; border-radius: 5px; background: #fff; margin-bottom: 5px;"></a>
                                    <div>Rp. <span style="font-weight: 600;">' . $list[$i + 2]->donasi . '</span></div>
                                    <div><span style="font-weight: 600;">' . $list[$i + 2]->donatur . '</span> Donatur</div>
                                    <div style="margin-top: 5px;"><span style="font-weight: 600;">' . $list[$i + 2]->jatuh_tempo . '</span> Hari Lagi</div>' . $loggedss . '
                                </div>
                            </div>
                        </div>';
            }
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->total_list_donasi_non_medis($id_kategori),
            "recordsFiltered" => $this->model->filter_list_donasi_non_medis($id_kategori),
            "data" => $data,
        );
        echo json_encode($output);
    }
    //End List Donasi Non Medis
    public function tester(){
        for($i = 0; $i < 6; $i += 3){
            echo $i;
            echo '<br>';
        } 
    }
}