 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_model extends CI_Model {

    public function __construct(){
        parent::__construct(); 
        $this->db = $this->load->database('default', TRUE);
        date_default_timezone_set('Asia/Jakarta');
    }
    //Start Homepage
    public function datatable_list_homepage_medis(){
        $this->db->select('
            tb_kasus_medis.id_kasus_medis as id_kasus,
            tb_kasus_medis.judul,
            tb_kasus_medis.cerita,
            tb_kasus_medis.link,
            tb_kasus_medis.jatuh_tempo,
            tb_kasus_medis.sampul
        ');
        $column_order = array(null, 
            'tb_kasus_medis.id_kasus_medis',
            'tb_kasus_medis.judul',
            'tb_kasus_medis.cerita',
            'tb_kasus_medis.link',
            'tb_kasus_medis.jatuh_tempo',
            'tb_kasus_medis.sampul'
        );
        $column_search = array(
            'tb_kasus_medis.id_kasus_medis',
            'tb_kasus_medis.judul',
            'tb_kasus_medis.cerita',
            'tb_kasus_medis.link',
            'tb_kasus_medis.jatuh_tempo',
            'tb_kasus_medis.sampul'
        );
        $i = 0;
        $this->db->where('tb_kasus_medis.deleted_at','');
        $this->db->where('tb_kasus_medis.status','1');
        $this->db->where('tb_kasus_medis.admin_verify','1');
        $this->db->where('tb_kasus_medis.homepage','1');
        $this->db->from('tb_kasus_medis');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_kasus_medis.id_kasus_medis', 'desc');
        }
    }

    public function list_homepage_medis(){
        $this->datatable_list_homepage_medis();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function filter_list_homepage_medis(){
        $this->datatable_list_homepage_medis();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function total_list_homepage_medis(){
        $this->db->where('tb_kasus_medis.deleted_at','');
        $this->db->where('tb_kasus_medis.status','1');
        $this->db->where('tb_kasus_medis.admin_verify','1');
        $this->db->where('tb_kasus_medis.homepage','1');
        $this->db->from('tb_kasus_medis');
        return $this->db->count_all_results();
    }

    public function datatable_list_homepage_non_medis(){
        $this->db->select('
            tb_kasus_non_medis.id_kasus_non_medis as id_kasus,
            tb_kasus_non_medis.judul,
            tb_kasus_non_medis.cerita,
            tb_kasus_non_medis.link,
            tb_kasus_non_medis.jatuh_tempo,
            tb_kasus_non_medis.sampul
        ');
        $column_order = array(null, 
            'tb_kasus_non_medis.id_kasus_non_medis',
            'tb_kasus_non_medis.judul',
            'tb_kasus_non_medis.cerita',
            'tb_kasus_non_medis.link',
            'tb_kasus_non_medis.jatuh_tempo',
            'tb_kasus_non_medis.sampul'
        );
        $column_search = array(
            'tb_kasus_non_medis.id_kasus_non_medis',
            'tb_kasus_non_medis.judul',
            'tb_kasus_non_medis.cerita',
            'tb_kasus_non_medis.link',
            'tb_kasus_non_medis.jatuh_tempo',
            'tb_kasus_non_medis.sampul'
        );
        $i = 0;
        $this->db->where('tb_kasus_non_medis.deleted_at','');
        $this->db->where('tb_kasus_non_medis.status','1');
        $this->db->where('tb_kasus_non_medis.admin_verify','1');
        $this->db->where('tb_kasus_non_medis.homepage','1');
        $this->db->from('tb_kasus_non_medis');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_kasus_non_medis.id_kasus_non_medis', 'desc');
        }
    }

    public function list_homepage_non_medis(){
        $this->datatable_list_homepage_non_medis();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function filter_list_homepage_non_medis(){
        $this->datatable_list_homepage_non_medis();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function total_list_homepage_non_medis(){
        $this->db->where('tb_kasus_non_medis.deleted_at','');
        $this->db->where('tb_kasus_non_medis.status','1');
        $this->db->where('tb_kasus_non_medis.admin_verify','1');
        $this->db->where('tb_kasus_non_medis.homepage','1');
        $this->db->from('tb_kasus_non_medis');
        return $this->db->count_all_results();
    }
    //End Homepage
    public function profil_update($id, $by){
        $phone_val = str_replace('-', '', $this->input->post('user_phone'));
        $data = array(
            'nama'              => $this->input->post('user_name'),
            'phone'             => $phone_val,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function profil_update_image($id, $by, $image){
        $data = array(
            'image'             => $image,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function datatable_donasi_medis($id_user){
        $this->db->select('
            tb_donasi_medis.id_donasi_medis,
            tb_donasi_medis.donasi,
            tb_donasi_medis.metode,
            tb_donasi_medis.bukti,
            tb_donasi_medis.status,
            tb_donasi_medis.created_by,
            tb_donasi_medis.created_at,
            tb_donasi_medis.updated_by,
            tb_donasi_medis.updated_at,
            tb_kasus_medis.judul,
            tb_kasus_medis.link,
            tb_kasus_medis.kategori,
            tb_bank.name,
            tb_bank.image,
            tb_bank.number,
            tb_bank.owner
        ');
        $column_order = array(null, 
            'tb_donasi_medis.id_donasi_medis',
            'tb_donasi_medis.donasi',
            'tb_donasi_medis.metode',
            'tb_donasi_medis.bukti',
            'tb_donasi_medis.status',
            'tb_donasi_medis.created_by',
            'tb_donasi_medis.created_at',
            'tb_donasi_medis.updated_by',
            'tb_donasi_medis.updated_at',
            'tb_kasus_medis.judul',
            'tb_kasus_medis.link',
            'tb_kasus_medis.kategori',
            'tb_bank.name',
            'tb_bank.image',
            'tb_bank.number',
            'tb_bank.owner'
        );
        $column_search = array(
            'tb_donasi_medis.id_donasi_medis',
            'tb_donasi_medis.donasi',
            'tb_donasi_medis.metode',
            'tb_donasi_medis.bukti',
            'tb_donasi_medis.status',
            'tb_donasi_medis.created_by',
            'tb_donasi_medis.created_at',
            'tb_donasi_medis.updated_by',
            'tb_donasi_medis.updated_at',
            'tb_kasus_medis.judul',
            'tb_kasus_medis.link',
            'tb_kasus_medis.kategori',
            'tb_bank.name',
            'tb_bank.image',
            'tb_bank.number',
            'tb_bank.owner'
        );
        $i = 0;
        $this->db->where('tb_donasi_medis.deleted_at','');
        $this->db->where('tb_donasi_medis.id_user', $id_user);
        $this->db->from('tb_donasi_medis');
        $this->db->join('tb_kasus_medis', 'tb_donasi_medis.id_kasus_medis = tb_kasus_medis.id_kasus_medis', 'left');
        $this->db->join('tb_bank', 'tb_donasi_medis.id_bank = tb_bank.id_bank', 'left');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_donasi_medis.id_donasi_medis', 'desc');
        }
    }

    public function riwayat_donasi_medis($id_user){
        $this->datatable_donasi_medis($id_user);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function filter_riwayat_donasi_medis($id_user){
        $this->datatable_donasi_medis($id_user);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function total_riwayat_donasi_medis($id_user){
        $this->db->where('tb_donasi_medis.deleted_at','');
        $this->db->where('tb_donasi_medis.id_user', $id_user);
        $this->db->from('tb_donasi_medis');
        return $this->db->count_all_results();
    }

    public function datatable_donasi_non_medis($id_user){
        $this->db->select('
            tb_donasi_non_medis.id_donasi_non_medis,
            tb_donasi_non_medis.donasi,
            tb_donasi_non_medis.metode,
            tb_donasi_non_medis.bukti,
            tb_donasi_non_medis.status,
            tb_donasi_non_medis.created_by,
            tb_donasi_non_medis.created_at,
            tb_donasi_non_medis.updated_by,
            tb_donasi_non_medis.updated_at,
            tb_kasus_non_medis.judul,
            tb_kasus_non_medis.link,
            tb_kasus_non_medis.kategori,
            tb_bank.name,
            tb_bank.image,
            tb_bank.number,
            tb_bank.owner
        ');
        $column_order = array(null, 
            'tb_donasi_non_medis.id_donasi_non_medis',
            'tb_donasi_non_medis.donasi',
            'tb_donasi_non_medis.metode',
            'tb_donasi_non_medis.bukti',
            'tb_donasi_non_medis.status',
            'tb_donasi_non_medis.created_by',
            'tb_donasi_non_medis.created_at',
            'tb_donasi_non_medis.updated_by',
            'tb_donasi_non_medis.updated_at',
            'tb_kasus_non_medis.judul',
            'tb_kasus_non_medis.link',
            'tb_kasus_non_medis.kategori',
            'tb_bank.name',
            'tb_bank.image',
            'tb_bank.number',
            'tb_bank.owner'
        );
        $column_search = array(
            'tb_donasi_non_medis.id_donasi_non_medis',
            'tb_donasi_non_medis.donasi',
            'tb_donasi_non_medis.metode',
            'tb_donasi_non_medis.bukti',
            'tb_donasi_non_medis.status',
            'tb_donasi_non_medis.created_by',
            'tb_donasi_non_medis.created_at',
            'tb_donasi_non_medis.updated_by',
            'tb_donasi_non_medis.updated_at',
            'tb_kasus_non_medis.judul',
            'tb_kasus_non_medis.link',
            'tb_kasus_non_medis.kategori',
            'tb_bank.name',
            'tb_bank.image',
            'tb_bank.number',
            'tb_bank.owner'
        );
        $i = 0;
        $this->db->where('tb_donasi_non_medis.deleted_at','');
        $this->db->where('tb_donasi_non_medis.id_user', $id_user);
        $this->db->from('tb_donasi_non_medis');
        $this->db->join('tb_kasus_non_medis', 'tb_donasi_non_medis.id_kasus_non_medis = tb_kasus_non_medis.id_kasus_non_medis', 'left');
        $this->db->join('tb_bank', 'tb_donasi_non_medis.id_bank = tb_bank.id_bank', 'left');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_donasi_non_medis.id_donasi_non_medis', 'desc');
        }
    }

    public function riwayat_donasi_non_medis($id_user){
        $this->datatable_donasi_non_medis($id_user);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function filter_riwayat_donasi_non_medis($id_user){
        $this->datatable_donasi_non_medis($id_user);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function total_riwayat_donasi_non_medis($id_user){
        $this->db->where('tb_donasi_non_medis.deleted_at','');
        $this->db->where('tb_donasi_non_medis.id_user', $id_user);
        $this->db->from('tb_donasi_non_medis');
        return $this->db->count_all_results();
    }

    public function datatable_galang_dana_medis($id_user){
        $this->db->select('
            tb_kasus_medis.id_kasus_medis,
            tb_kasus_medis.kategori,
            tb_kasus_medis.judul,
            tb_kasus_medis.dana,
            tb_kasus_medis.link,
            tb_kasus_medis.jatuh_tempo,
            tb_kasus_medis.status,
            tb_kasus_medis.admin_verify,
            tb_kasus_medis.file_rekening,
            tb_kasus_medis.file_ktp,
            tb_kasus_medis.file_kk,
            tb_kasus_medis.created_by,
            tb_kasus_medis.created_at,
            tb_kasus_medis.updated_by,
            tb_kasus_medis.updated_at
        ');
        $column_order = array(null, 
            'tb_kasus_medis.id_kasus_medis',
            'tb_kasus_medis.kategori',
            'tb_kasus_medis.judul',
            'tb_kasus_medis.dana',
            'tb_kasus_medis.link',
            'tb_kasus_medis.jatuh_tempo',
            'tb_kasus_medis.status',
            'tb_kasus_medis.admin_verify',
            'tb_kasus_medis.file_rekening',
            'tb_kasus_medis.file_ktp',
            'tb_kasus_medis.file_kk',
            'tb_kasus_medis.created_by',
            'tb_kasus_medis.created_at',
            'tb_kasus_medis.updated_by',
            'tb_kasus_medis.updated_at'
        );
        $column_search = array(
            'tb_kasus_medis.id_kasus_medis',
            'tb_kasus_medis.kategori',
            'tb_kasus_medis.judul',
            'tb_kasus_medis.dana',
            'tb_kasus_medis.link',
            'tb_kasus_medis.jatuh_tempo',
            'tb_kasus_medis.status',
            'tb_kasus_medis.admin_verify',
            'tb_kasus_medis.file_rekening',
            'tb_kasus_medis.file_ktp',
            'tb_kasus_medis.file_kk',
            'tb_kasus_medis.created_by',
            'tb_kasus_medis.created_at',
            'tb_kasus_medis.updated_by',
            'tb_kasus_medis.updated_at'
        );
        $i = 0;
        $this->db->where('tb_kasus_medis.deleted_at','');
        $this->db->where('tb_kasus_medis.id_user', $id_user);
        $this->db->from('tb_kasus_medis');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_kasus_medis.id_kasus_medis', 'desc');
        }
    }

    public function riwayat_galang_dana_medis($id_user){
        $this->datatable_galang_dana_medis($id_user);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function filter_riwayat_galang_dana_medis($id_user){
        $this->datatable_galang_dana_medis($id_user);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function total_riwayat_galang_dana_medis($id_user){
        $this->db->where('tb_kasus_medis.deleted_at','');
        $this->db->where('tb_kasus_medis.id_user', $id_user);
        $this->db->from('tb_kasus_medis');
        return $this->db->count_all_results();
    }
    
    public function save_kabar_terbaru_medis($by){
        $data = array(
            'id_kasus_medis'    => $this->input->post('id_data'),
            'judul'             => $this->input->post('judul_terbaru_medis'),
            'status'            => '0',
            'kabar_terbaru'     => $this->input->post('info_terbaru_medis'),
            'created_at'        => date('Y-m-d H:i:s'),
            'created_by'        => $by,
        );
        $query = $this->db->insert('tb_kabar_medis', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function kabar_terbaru_medis($id_kasus){
        $this->db->select('
            id_kabar_medis,
            judul,
            status,
            kabar_terbaru,
            created_at
        ');
        $query = $this->db->get_where('tb_kabar_medis', array(
            'id_kasus_medis' => $id_kasus,
            'deleted_at' => ''
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function total_donatur_medis($id_kasus_medis){
        $this->db->where('tb_donasi_medis.status','1');
        $this->db->where('tb_donasi_medis.deleted_at','');
        $this->db->where('tb_donasi_medis.id_kasus_medis', $id_kasus_medis);
        $this->db->from('tb_donasi_medis');
        return $this->db->count_all_results();
    }

    public function total_donasi_medis($id_kasus_medis){
        $this->db->select_sum('donasi');
        $query = $this->db->get_where('tb_donasi_medis', array(
            'id_kasus_medis'    => $id_kasus_medis,
            'status'            => '1',
            'deleted_at'            => '',
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
    
    public function save_rekening_medis($id, $by, $rekening){
        $data = array(
            'file_rekening'     => $rekening,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_medis', $id);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function save_ktp_medis($id, $by, $ktp){
        $data = array(
            'file_ktp'          => $ktp,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_medis', $id);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function save_kk_medis($id, $by, $kk){
        $data = array(
            'file_kk'           => $kk,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_medis', $id);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function last_dokumen_medis($id){
        $this->db->select('id_kasus_medis');
        $query = $this->db->get_where('tb_kasus_medis', array(
            'id_kasus_medis'    => $id,
            'file_rekening !='      => '',
            'file_ktp !='           => '',
            'file_kk !='            => '',
            'deleted_at'            => ''
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function datatable_galang_dana_non_medis($id_user){
        $this->db->select('
            tb_kasus_non_medis.id_kasus_non_medis,
            tb_kasus_non_medis.kategori,
            tb_kasus_non_medis.judul,
            tb_kasus_non_medis.donasi as dana,
            tb_kasus_non_medis.link,
            tb_kasus_non_medis.jatuh_tempo,
            tb_kasus_non_medis.status,
            tb_kasus_non_medis.admin_verify,
            tb_kasus_non_medis.file_rekening,
            tb_kasus_non_medis.file_ktp,
            tb_kasus_non_medis.file_kk,
            tb_kasus_non_medis.created_by,
            tb_kasus_non_medis.created_at,
            tb_kasus_non_medis.updated_by,
            tb_kasus_non_medis.updated_at
        ');
        $column_order = array(null, 
            'tb_kasus_non_medis.id_kasus_non_medis',
            'tb_kasus_non_medis.kategori',
            'tb_kasus_non_medis.judul',
            'tb_kasus_non_medis.donasi',
            'tb_kasus_non_medis.link',
            'tb_kasus_non_medis.jatuh_tempo',
            'tb_kasus_non_medis.status',
            'tb_kasus_non_medis.admin_verify',
            'tb_kasus_non_medis.file_rekening',
            'tb_kasus_non_medis.file_ktp',
            'tb_kasus_non_medis.file_kk',
            'tb_kasus_non_medis.created_by',
            'tb_kasus_non_medis.created_at',
            'tb_kasus_non_medis.updated_by',
            'tb_kasus_non_medis.updated_at'
        );
        $column_search = array(
            'tb_kasus_non_medis.id_kasus_non_medis',
            'tb_kasus_non_medis.kategori',
            'tb_kasus_non_medis.judul',
            'tb_kasus_non_medis.donasi',
            'tb_kasus_non_medis.link',
            'tb_kasus_non_medis.jatuh_tempo',
            'tb_kasus_non_medis.status',
            'tb_kasus_non_medis.admin_verify',
            'tb_kasus_non_medis.file_rekening',
            'tb_kasus_non_medis.file_ktp',
            'tb_kasus_non_medis.file_kk',
            'tb_kasus_non_medis.created_by',
            'tb_kasus_non_medis.created_at',
            'tb_kasus_non_medis.updated_by',
            'tb_kasus_non_medis.updated_at'
        );
        $i = 0;
        $this->db->where('tb_kasus_non_medis.deleted_at','');
        $this->db->where('tb_kasus_non_medis.id_user', $id_user);
        $this->db->from('tb_kasus_non_medis');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_kasus_non_medis.id_kasus_non_medis', 'desc');
        }
    }

    public function riwayat_galang_dana_non_medis($id_user){
        $this->datatable_galang_dana_non_medis($id_user);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function filter_riwayat_galang_dana_non_medis($id_user){
        $this->datatable_galang_dana_non_medis($id_user);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function total_riwayat_galang_dana_non_medis($id_user){
        $this->db->where('tb_kasus_non_medis.deleted_at','');
        $this->db->where('tb_kasus_non_medis.id_user', $id_user);
        $this->db->from('tb_kasus_non_medis');
        return $this->db->count_all_results();
    }
    
    public function save_kabar_terbaru_non_medis($by){
        $data = array(
            'id_kasus_non_medis'=> $this->input->post('id_data'),
            'judul'             => $this->input->post('judul_terbaru_non_medis'),
            'status'            => '0',
            'kabar_terbaru'     => $this->input->post('info_terbaru_non_medis'),
            'created_at'        => date('Y-m-d H:i:s'),
            'created_by'        => $by,
        );
        $query = $this->db->insert('tb_kabar_non_medis', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function kabar_terbaru_non_medis($id_kasus){
        $this->db->select('
            id_kabar_non_medis,
            judul,
            status,
            kabar_terbaru,
            created_at
        ');
        $query = $this->db->get_where('tb_kabar_non_medis', array(
            'id_kasus_non_medis' => $id_kasus,
            'deleted_at' => ''
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function total_donatur_non_medis($id_kasus_non_medis){
        $this->db->where('tb_donasi_non_medis.status','1');
        $this->db->where('tb_donasi_non_medis.deleted_at','');
        $this->db->where('tb_donasi_non_medis.id_kasus_non_medis', $id_kasus_non_medis);
        $this->db->from('tb_donasi_non_medis');
        return $this->db->count_all_results();
    }

    public function total_donasi_non_medis($id_kasus_non_medis){
        $this->db->select_sum('donasi');
        $query = $this->db->get_where('tb_donasi_non_medis', array(
            'id_kasus_non_medis'    => $id_kasus_non_medis,
            'status'                => '1',
            'deleted_at'            => '',
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
    
    public function save_rekening_non_medis($id, $by, $rekening){
        $data = array(
            'file_rekening'     => $rekening,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_non_medis', $id);
        $this->db->update('tb_kasus_non_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function save_ktp_non_medis($id, $by, $ktp){
        $data = array(
            'file_ktp'          => $ktp,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_non_medis', $id);
        $this->db->update('tb_kasus_non_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function save_kk_non_medis($id, $by, $kk){
        $data = array(
            'file_kk'           => $kk,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_non_medis', $id);
        $this->db->update('tb_kasus_non_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function last_dokumen_non_medis($id){
        $this->db->select('id_kasus_non_medis');
        $query = $this->db->get_where('tb_kasus_non_medis', array(
            'id_kasus_non_medis'    => $id,
            'file_rekening !='      => '',
            'file_ktp !='           => '',
            'file_kk !='            => '',
            'deleted_at'            => ''
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function message_process(){
        $value = str_replace('-', '', $this->input->post('c_phone'));
        $data = array(
            'name'          => $this->input->post('c_name'),
            'email'         => $this->input->post('c_email'),
            'phone'         => $value,
            'message'       => $this->input->post('c_message'),
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => $this->input->post('c_name')
        );
        $query = $this->db->insert('message', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function detail_agenda($id_agenda){
        $this->db->select('type, title, image, description');
        $query = $this->db->get_where('tb_agenda', array('id_agenda' => $id_agenda));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }
    //Start Penggalangan Medis
    public function medis_registration($id, $by, $kategori){
        $data = array(
            'id_user'       => $id,
            'kategori'      => $kategori,
            'status'        => '0',
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => $by
        );
        $query = $this->db->insert('tb_kasus_medis', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function last_kasus_medis($id){
        $this->db->order_by('id_kasus_medis', 'desc');
        $this->db->select('id_kasus_medis');
        $query = $this->db->get_where('tb_kasus_medis', array('id_user' => $id));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function kategori_penggalangan_medis($id){
        $this->db->select('kategori');
        $query = $this->db->get_where('tb_kasus_medis', array('id_kasus_medis' => $id));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function medis_tujuan($id_kasus){
        $this->db->select('pasien, tlp_penggalang, rekening');
        $query = $this->db->get_where('tb_kasus_medis', array('id_kasus_medis' => $id_kasus));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function medis_tujuan_update($id, $by){
        $phone_val = str_replace('-', '', $this->input->post('tlp_penggalang'));
        $data = array(
            'pasien'            => $this->input->post('pasien'),
            'tlp_penggalang'    => $phone_val,
            'rekening'          => $this->input->post('rekening'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_medis', $id);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function medis_pasien($id_kasus){
        $this->db->select('nm_pasien, nm_penyakit, dokumen_medis');
        $query = $this->db->get_where('tb_kasus_medis', array('id_kasus_medis' => $id_kasus));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function medis_pasien_update($id, $by, $image){
        $data = array(
            'dokumen_medis'     => $image,
            'nm_pasien'         => $this->input->post('nm_pasien'),
            'nm_penyakit'       => $this->input->post('nm_penyakit'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_medis', $id);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function medis_medis($id_kasus){
        $this->db->select('hasil_pemeriksaan, inap, rs_inap, pengobatan, biaya');
        $query = $this->db->get_where('tb_kasus_medis', array('id_kasus_medis' => $id_kasus));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function medis_medis_update($id, $by, $image){
        $data = array(
            'hasil_pemeriksaan' => $image,
            'inap'              => $this->input->post('r_inap'),
            'rs_inap'           => $this->input->post('rs_r_inap'),
            'pengobatan'        => $this->input->post('pengobatan'),
            'biaya'             => $this->input->post('biaya'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_medis', $id);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function medis_donasi($id_kasus){
        $this->db->select('dana, jatuh_tempo, penggunaan_dana');
        $query = $this->db->get_where('tb_kasus_medis', array('id_kasus_medis' => $id_kasus));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function medis_donasi_update($id, $by){
        $dana_val = str_replace('.', '', $this->input->post('biaya_dana'));
        $data = array(
            'dana'              => $dana_val,
            'jatuh_tempo'       => $this->input->post('jatuh_tempo'),
            'penggunaan_dana'   => $this->input->post('penggunaan_dana'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_medis', $id);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function medis_judul($id_kasus){
        $this->db->select('judul, link, sampul');
        $query = $this->db->get_where('tb_kasus_medis', array('id_kasus_medis' => $id_kasus));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function check_link_medis($id, $links){
        $this->db->select('id_kasus_medis');
        $query = $this->db->get_where('tb_kasus_medis', array(
            'id_kasus_medis !='    => $id,
            'link'              => $links,
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function medis_judul_update($id, $by, $image){
        $data = array(
            'sampul'            => $image,
            'judul'             => $this->input->post('judul_penggalangan'),
            'link'              => $this->input->post('link_penggalangan'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_medis', $id);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function medis_cerita($id_kasus){
        $this->db->select('cerita');
        $query = $this->db->get_where('tb_kasus_medis', array('id_kasus_medis' => $id_kasus));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function medis_cerita_update($id, $by){
        $data = array(
            'cerita'            => $this->input->post('deskripsi_cerita'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_medis', $id);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function medis_ajakan($id_kasus){
        $query = $this->db->get_where('tb_kasus_medis', array('id_kasus_medis' => $id_kasus));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function medis_ajakan_update($id, $by, $otp){
        $data = array(
            'ajakan'            => $this->input->post('deskripsi_ajakan'),
            'status'            => '1',
            'kode_otp'          => $otp,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_medis', $id);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
    //End Penggalangan Medis
    //Start Penggalangan Non Medis
    public function non_medis_registration($id, $by, $kategori){
        $data = array(
            'id_user'       => $id,
            'kategori'      => $kategori,
            'status'        => '0',
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => $by
        );
        $query = $this->db->insert('tb_kasus_non_medis', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function last_kasus_non_medis($id){
        $this->db->order_by('id_kasus_non_medis', 'desc');
        $this->db->select('id_kasus_non_medis');
        $query = $this->db->get_where('tb_kasus_non_medis', array('id_user' => $id));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function kategori_penggalangan_non_medis($id){
        $this->db->select('kategori');
        $query = $this->db->get_where('tb_kasus_non_medis', array('id_kasus_non_medis' => $id));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function non_medis_identitas($id_kasus){
        $this->db->select('pekerjaan, organisasi, medsos, akun_medsos, domisili, tentang');
        $query = $this->db->get_where('tb_kasus_non_medis', array('id_kasus_non_medis' => $id_kasus));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function non_medis_identitas_update($id, $by){
        $data = array(
            'pekerjaan'         => $this->input->post('identitas_pekerjaan'),
            'organisasi'        => $this->input->post('identitas_organisasi'),
            'medsos'            => $this->input->post('identitas_medsos'),
            'akun_medsos'       => $this->input->post('identitas_medsos_akun'),
            'domisili'          => $this->input->post('identitas_domisili'),
            'tentang'           => $this->input->post('identitas_tentang'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_non_medis', $id);
        $this->db->update('tb_kasus_non_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function non_medis_donasi($id_kasus){
        $this->db->select('donasi, jatuh_tempo, penerima, penggunaan');
        $query = $this->db->get_where('tb_kasus_non_medis', array('id_kasus_non_medis' => $id_kasus));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function non_medis_donasi_update($id, $by){
        $donasi_val = str_replace('.', '', $this->input->post('donasi_donasi'));
        $data = array(
            'donasi'                => $donasi_val,
            'jatuh_tempo'           => $this->input->post('donasi_jatuh_tempo'),
            'penerima'              => $this->input->post('donasi_penerima'),
            'penggunaan'            => $this->input->post('donasi_penggunaan'),
            'updated_at'            => date('Y-m-d H:i:s'),
            'updated_by'            => $by,
        );
        $this->db->where('id_kasus_non_medis', $id);
        $this->db->update('tb_kasus_non_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function non_medis_detail($id_kasus){
        $this->db->select('id_bantuan_non_medis, judul, link, tujuan');
        $query = $this->db->get_where('tb_kasus_non_medis', array('id_kasus_non_medis' => $id_kasus));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function check_link_non_medis($id, $links){
        $this->db->select('id_kasus_non_medis');
        $query = $this->db->get_where('tb_kasus_non_medis', array(
            'id_kasus_non_medis !='    => $id,
            'link'              => $links,
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function non_medis_detail_update($id, $by){
        $data = array(
            'id_bantuan_non_medis'  => $this->input->post('detail_kategori'),
            'judul'                 => $this->input->post('detail_judul'),
            'link'                  => $this->input->post('detail_link'),
            'tujuan'                => $this->input->post('detail_tujuan'),
            'updated_at'            => date('Y-m-d H:i:s'),
            'updated_by'            => $by,
        );
        $this->db->where('id_kasus_non_medis', $id);
        $this->db->update('tb_kasus_non_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function non_medis_foto($id_kasus){
        $this->db->select('sampul');
        $query = $this->db->get_where('tb_kasus_non_medis', array('id_kasus_non_medis' => $id_kasus));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function non_medis_foto_update($id, $by, $image){
        $data = array(
            'sampul'            => $image,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_non_medis', $id);
        $this->db->update('tb_kasus_non_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function non_medis_deskripsi($id_kasus){
        $this->db->select('cerita, ajakan');
        $query = $this->db->get_where('tb_kasus_non_medis', array('id_kasus_non_medis' => $id_kasus));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function non_medis_deskripsi_update($id, $by){
        $data = array(
            'cerita'            => $this->input->post('deskripsi_cerita'),
            'ajakan'            => $this->input->post('deskripsi_ajakan'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_non_medis', $id);
        $this->db->update('tb_kasus_non_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function non_medis_konfirmasi($id_kasus){
        $query = $this->db->get_where('tb_kasus_non_medis', array('id_kasus_non_medis' => $id_kasus));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function non_medis_konfirmasi_update($id, $by, $otp, $phone){
        $data = array(
            'keperluan'         => $this->input->post('konfirmasi_keperluan'),
            'kota'              => $this->input->post('konfirmasi_kota'),
            'phone'             => $phone,
            'status'            => '1',
            'kode_otp'          => $otp,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_non_medis', $id);
        $this->db->update('tb_kasus_non_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
    //End Penggalangan Non Medis
    //Start OTP Medis
    public function otp_medis_code($id_kasus_medis, $otp_code, $id_user){
        $this->db->select('id_kasus_medis');
        $query = $this->db->get_where('tb_kasus_medis', array(
            'id_kasus_medis'    => $id_kasus_medis,
            'kode_otp'          => $otp_code,
            'id_user'           => $id_user,
            'deleted_at'        => ''
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function otp_medis_publish($id_kasus_medis, $by){
        $data = array(
            'status'            => '1',
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_medis', $id_kasus_medis);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function otp_medis_resend($id, $by, $otp){
        $data = array(
            'kode_otp'          => $otp,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_medis', $id);
        $this->db->update('tb_kasus_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function otp_medis_kategori($id_kasus_medis, $id_user){
        $this->db->select('
            tb_kasus_medis.kategori,
            users.email
        ');
        $this->db->where('tb_kasus_medis.id_user', $id_user);
        $this->db->where('tb_kasus_medis.id_kasus_medis', $id_kasus_medis);
        $this->db->where('tb_kasus_medis.status', '0');
        $this->db->where('tb_kasus_medis.deleted_at', '');
        $this->db->from('tb_kasus_medis');
        $this->db->join('users', 'tb_kasus_medis.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
    //End OTP Medis
    //Start OTP Non Medis
    public function otp_non_medis_code($id_kasus_non_medis, $otp_code, $id_user){
        $this->db->select('id_kasus_non_medis');
        $query = $this->db->get_where('tb_kasus_non_medis', array(
            'id_kasus_non_medis'    => $id_kasus_non_medis,
            'kode_otp'              => $otp_code,
            'id_user'               => $id_user,
            'deleted_at'            => ''
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function otp_non_medis_publish($id_kasus_non_medis, $by){
        $data = array(
            'status'            => '1',
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_non_medis', $id_kasus_non_medis);
        $this->db->update('tb_kasus_non_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function otp_non_medis_resend($id, $by, $otp){
        $data = array(
            'kode_otp'          => $otp,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by,
        );
        $this->db->where('id_kasus_non_medis', $id);
        $this->db->update('tb_kasus_non_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function otp_non_medis_kategori($id_kasus_non_medis, $id_user){
        $this->db->select('
            tb_kasus_non_medis.kategori,
            users.email
        ');
        $this->db->where('tb_kasus_non_medis.id_user', $id_user);
        $this->db->where('tb_kasus_non_medis.id_kasus_non_medis', $id_kasus_non_medis);
        $this->db->where('tb_kasus_non_medis.status', '0');
        $this->db->where('tb_kasus_non_medis.deleted_at', '');
        $this->db->from('tb_kasus_non_medis');
        $this->db->join('users', 'tb_kasus_non_medis.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
    //End OTP Non Medis
    //Start Pencarian
    public function datatable_search_donasi_medis($keysearch){
        $this->db->select('
            tb_kasus_medis.id_kasus_medis as id_kasus,
            tb_kasus_medis.judul,
            tb_kasus_medis.cerita,
            tb_kasus_medis.link,
            tb_kasus_medis.jatuh_tempo,
            tb_kasus_medis.sampul
        ');
        $column_order = array(null, 
            'tb_kasus_medis.id_kasus_medis',
            'tb_kasus_medis.judul',
            'tb_kasus_medis.cerita',
            'tb_kasus_medis.link',
            'tb_kasus_medis.jatuh_tempo',
            'tb_kasus_medis.sampul'
        );
        $column_search = array(
            'tb_kasus_medis.id_kasus_medis',
            'tb_kasus_medis.judul',
            'tb_kasus_medis.cerita',
            'tb_kasus_medis.link',
            'tb_kasus_medis.jatuh_tempo',
            'tb_kasus_medis.sampul'
        );
        $i = 0;
        $this->db->where('tb_kasus_medis.deleted_at','');
        $this->db->where('tb_kasus_medis.status','1');
        $this->db->where('tb_kasus_medis.admin_verify','1');
        $this->db->like('tb_kasus_medis.judul', $keysearch, 'both');
        $this->db->or_like('tb_kasus_medis.cerita', $keysearch, 'both');
        $this->db->from('tb_kasus_medis');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_kasus_medis.id_kasus_medis', 'desc');
        }
    }

    public function search_donasi_medis($keysearch){
        $this->datatable_search_donasi_medis($keysearch);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function filter_search_donasi_medis($keysearch){
        $this->datatable_search_donasi_medis($keysearch);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function total_search_donasi_medis($keysearch){
        $this->db->where('tb_kasus_medis.deleted_at','');
        $this->db->where('tb_kasus_medis.status','1');
        $this->db->where('tb_kasus_medis.admin_verify','1');
        $this->db->like('tb_kasus_medis.judul', $keysearch, 'both');
        $this->db->or_like('tb_kasus_medis.cerita', $keysearch, 'both');
        $this->db->from('tb_kasus_medis');
        return $this->db->count_all_results();
    }

    public function datatable_search_donasi_non_medis($keysearch){
        $this->db->select('
            tb_kasus_non_medis.id_kasus_non_medis as id_kasus,
            tb_kasus_non_medis.judul,
            tb_kasus_non_medis.cerita,
            tb_kasus_non_medis.link,
            tb_kasus_non_medis.jatuh_tempo,
            tb_kasus_non_medis.sampul
        ');
        $column_order = array(null, 
            'tb_kasus_non_medis.id_kasus_non_medis',
            'tb_kasus_non_medis.judul',
            'tb_kasus_non_medis.cerita',
            'tb_kasus_non_medis.link',
            'tb_kasus_non_medis.jatuh_tempo',
            'tb_kasus_non_medis.sampul'
        );
        $column_search = array(
            'tb_kasus_non_medis.id_kasus_non_medis',
            'tb_kasus_non_medis.judul',
            'tb_kasus_non_medis.cerita',
            'tb_kasus_non_medis.link',
            'tb_kasus_non_medis.jatuh_tempo',
            'tb_kasus_non_medis.sampul'
        );
        $i = 0;
        $this->db->where('tb_kasus_non_medis.deleted_at','');
        $this->db->where('tb_kasus_non_medis.status','1');
        $this->db->where('tb_kasus_non_medis.admin_verify','1');
        $this->db->like('tb_kasus_non_medis.judul', $keysearch, 'both');
        $this->db->or_like('tb_kasus_non_medis.cerita', $keysearch, 'both');
        $this->db->from('tb_kasus_non_medis');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_kasus_non_medis.id_kasus_non_medis', 'desc');
        }
    }

    public function search_donasi_non_medis($keysearch){
        $this->datatable_search_donasi_non_medis($keysearch);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function filter_search_donasi_non_medis($keysearch){
        $this->datatable_search_donasi_non_medis($keysearch);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function total_search_donasi_non_medis($keysearch){
        $this->db->where('tb_kasus_non_medis.deleted_at','');
        $this->db->where('tb_kasus_non_medis.status','1');
        $this->db->where('tb_kasus_non_medis.admin_verify','1');
        $this->db->like('tb_kasus_non_medis.judul', $keysearch, 'both');
        $this->db->or_like('tb_kasus_non_medis.cerita', $keysearch, 'both');
        $this->db->from('tb_kasus_non_medis');
        return $this->db->count_all_results();
    }

    public function datatable_search_agenda($keysearch){
        $this->db->select('
            tb_agenda.id_agenda,
            tb_agenda.title,
            tb_agenda.type,
            tb_agenda.image,
            tb_agenda.created_at
        ');
        $column_order = array(null, 
            'tb_agenda.id_agenda',
            'tb_agenda.title',
            'tb_agenda.type',
            'tb_agenda.image',
            'tb_agenda.created_at'
        );
        $column_search = array(
            'tb_agenda.id_agenda',
            'tb_agenda.title',
            'tb_agenda.type',
            'tb_agenda.image',
            'tb_agenda.created_at'
        );
        $i = 0;
        $this->db->where('tb_agenda.deleted_at','');
        $this->db->where('tb_agenda.status','1');
        $this->db->like('tb_agenda.title', $keysearch, 'both');
        $this->db->or_like('tb_agenda.description', $keysearch, 'both');
        $this->db->from('tb_agenda');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_agenda.id_agenda', 'desc');
        }
    }

    public function search_agenda($keysearch){
        $this->datatable_search_agenda($keysearch);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function filter_search_agenda($keysearch){
        $this->datatable_search_agenda($keysearch);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function total_search_agenda($keysearch){
        $this->db->where('tb_agenda.deleted_at','');
        $this->db->where('tb_agenda.status','1');
        $this->db->like('tb_agenda.title', $keysearch, 'both');
        $this->db->or_like('tb_agenda.description', $keysearch, 'both');
        $this->db->from('tb_agenda');
        return $this->db->count_all_results();
    }
    //End Pencarian
    //Start List Agenda
    public function datatable_list_kegiatan(){
        $this->db->select('
            tb_agenda.id_agenda,
            tb_agenda.title,
            tb_agenda.description,
            tb_agenda.image,
            tb_agenda.created_at
        ');
        $column_order = array(null, 
            'tb_agenda.id_agenda',
            'tb_agenda.title',
            'tb_agenda.description',
            'tb_agenda.image',
            'tb_agenda.created_at'
        );
        $column_search = array(
            'tb_agenda.id_agenda',
            'tb_agenda.title',
            'tb_agenda.description',
            'tb_agenda.image',
            'tb_agenda.created_at'
        );
        $i = 0;
        $this->db->where('tb_agenda.deleted_at','');
        $this->db->where('tb_agenda.status','1');
        $this->db->where('tb_agenda.type','1');
        $this->db->from('tb_agenda');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_agenda.id_agenda', 'desc');
        }
    }

    public function list_kegiatan(){
        $this->datatable_list_kegiatan();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function filter_list_kegiatan(){
        $this->datatable_list_kegiatan();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function total_list_kegiatan(){
        $this->db->where('tb_agenda.deleted_at','');
        $this->db->where('tb_agenda.status','1');
        $this->db->where('tb_agenda.type','1');
        $this->db->from('tb_agenda');
        return $this->db->count_all_results();
    }

    public function datatable_list_program(){
        $this->db->select('
            tb_agenda.id_agenda,
            tb_agenda.title,
            tb_agenda.description,
            tb_agenda.image,
            tb_agenda.created_at
        ');
        $column_order = array(null, 
            'tb_agenda.id_agenda',
            'tb_agenda.title',
            'tb_agenda.description',
            'tb_agenda.image',
            'tb_agenda.created_at'
        );
        $column_search = array(
            'tb_agenda.id_agenda',
            'tb_agenda.title',
            'tb_agenda.description',
            'tb_agenda.image',
            'tb_agenda.created_at'
        );
        $i = 0;
        $this->db->where('tb_agenda.deleted_at','');
        $this->db->where('tb_agenda.status','1');
        $this->db->where('tb_agenda.type','0');
        $this->db->from('tb_agenda');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_agenda.id_agenda', 'desc');
        }
    }

    public function list_program(){
        $this->datatable_list_program();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function filter_list_program(){
        $this->datatable_list_program();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function total_list_program(){
        $this->db->where('tb_agenda.deleted_at','');
        $this->db->where('tb_agenda.status','1');
        $this->db->where('tb_agenda.type','0');
        $this->db->from('tb_agenda');
        return $this->db->count_all_results();
    }
    //End List Agenda
    //Start List Donasi Medis
    public function datatable_list_donasi_medis($id_kategori){
        $this->db->select('
            tb_kasus_medis.id_kasus_medis,
            tb_kasus_medis.judul,
            tb_kasus_medis.link,
            tb_kasus_medis.sampul,
            tb_kasus_medis.jatuh_tempo
        ');
        $column_order = array(null, 
            'tb_kasus_medis.id_kasus_medis',
            'tb_kasus_medis.judul',
            'tb_kasus_medis.link',
            'tb_kasus_medis.sampul',
            'tb_kasus_medis.jatuh_tempo'
        );
        $column_search = array(
            'tb_kasus_medis.id_kasus_medis',
            'tb_kasus_medis.judul',
            'tb_kasus_medis.link',
            'tb_kasus_medis.sampul',
            'tb_kasus_medis.jatuh_tempo'
        );
        $i = 0;
        $this->db->where('tb_kasus_medis.deleted_at','');
        $this->db->where('tb_kasus_medis.status','1');
        $this->db->where('tb_kasus_medis.admin_verify','1');
        $this->db->where('tb_kasus_medis.kategori',$id_kategori);
        $this->db->from('tb_kasus_medis');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_kasus_medis.id_kasus_medis', 'desc');
        }
    }

    public function list_donasi_medis($id_kategori){
        $this->datatable_list_donasi_medis($id_kategori);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function filter_list_donasi_medis($id_kategori){
        $this->datatable_list_donasi_medis($id_kategori);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function total_list_donasi_medis($id_kategori){
        $this->db->where('tb_kasus_medis.deleted_at','');
        $this->db->where('tb_kasus_medis.status','1');
        $this->db->where('tb_kasus_medis.admin_verify','1');
        $this->db->where('tb_kasus_medis.kategori',$id_kategori);
        $this->db->from('tb_kasus_medis');
        return $this->db->count_all_results();
    }
    //End List Donasi Medis
    //Start List Donasi Non Medis
    public function datatable_list_donasi_non_medis($id_kategori){
        $this->db->select('
            tb_kasus_non_medis.id_kasus_non_medis,
            tb_kasus_non_medis.judul,
            tb_kasus_non_medis.link,
            tb_kasus_non_medis.sampul,
            tb_kasus_non_medis.jatuh_tempo
        ');
        $column_order = array(null, 
            'tb_kasus_non_medis.id_kasus_non_medis',
            'tb_kasus_non_medis.judul',
            'tb_kasus_non_medis.link',
            'tb_kasus_non_medis.sampul',
            'tb_kasus_non_medis.jatuh_tempo'
        );
        $column_search = array(
            'tb_kasus_non_medis.id_kasus_non_medis',
            'tb_kasus_non_medis.judul',
            'tb_kasus_non_medis.link',
            'tb_kasus_non_medis.sampul',
            'tb_kasus_non_medis.jatuh_tempo'
        );
        $i = 0;
        $this->db->where('tb_kasus_non_medis.deleted_at','');
        $this->db->where('tb_kasus_non_medis.status','1');
        $this->db->where('tb_kasus_non_medis.admin_verify','1');
        $this->db->where('tb_kasus_non_medis.kategori',$id_kategori);
        $this->db->from('tb_kasus_non_medis');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_kasus_non_medis.id_kasus_non_medis', 'desc');
        }
    }

    public function list_donasi_non_medis($id_kategori){
        $this->datatable_list_donasi_non_medis($id_kategori);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function filter_list_donasi_non_medis($id_kategori){
        $this->datatable_list_donasi_non_medis($id_kategori);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function total_list_donasi_non_medis($id_kategori){
        $this->db->where('tb_kasus_non_medis.deleted_at','');
        $this->db->where('tb_kasus_non_medis.status','1');
        $this->db->where('tb_kasus_non_medis.admin_verify','1');
        $this->db->where('tb_kasus_non_medis.kategori',$id_kategori);
        $this->db->from('tb_kasus_non_medis');
        return $this->db->count_all_results();
    }
    //End List Donasi Non Medis
    public function base_contact($name){
        $this->db->select('detail');
        $query = $this->db->get_where('tb_contact', array(
            'name'    => $name
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
}