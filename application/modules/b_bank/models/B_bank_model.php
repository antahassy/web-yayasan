<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_bank_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

    // Datatable server side processing
    public function s_side_datatables_query(){
        $this->db->select('
            tb_bank.id_bank,
            tb_bank.name,
            tb_bank.image,
            tb_bank.number,
            tb_bank.owner,
            tb_bank.created_by,
            tb_bank.created_at,
            tb_bank.updated_by,
            tb_bank.updated_at
        ');
        $column_order = array(null, 
            'tb_bank.id_bank',
            'tb_bank.name',
            'tb_bank.image',
            'tb_bank.number',
            'tb_bank.owner',
            'tb_bank.created_by',
            'tb_bank.created_at',
            'tb_bank.updated_by',
            'tb_bank.updated_at'
        );
        $column_search = array(
            'tb_bank.id_bank',
            'tb_bank.name',
            'tb_bank.image',
            'tb_bank.number',
            'tb_bank.owner',
            'tb_bank.created_by',
            'tb_bank.created_at',
            'tb_bank.updated_by',
            'tb_bank.updated_at'
        );
        $i = 0;
        $this->db->where('tb_bank.deleted_at','');
        $this->db->from('tb_bank');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_bank.id_bank', 'asc');
        }
    }

    public function s_side_data(){
        $this->s_side_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered(){
        $this->s_side_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->where('tb_bank.deleted_at','');
        $this->db->from('tb_bank');
        return $this->db->count_all_results();
    }
    // End datatable server side processing

    public function save($by){
        $number_val = str_replace('-', '', $this->input->post('number'));
        $data = array(
            'name'          => $this->input->post('name'),
            'number'        => $number_val,
            'owner'         => $this->input->post('owner'),
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => $by
        );
        $query = $this->db->insert('tb_bank', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function edit($id_bank){
        $this->db->select('id_bank, name, number, owner');
        $query = $this->db->get_where('tb_bank', array(
            'id_bank' => $id_bank
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function update($id_bank, $by){
        $number_val = str_replace('-', '', $this->input->post('number'));
        $data           = array(
            'name'          => $this->input->post('name'),
            'number'        => $number_val,
            'owner'         => $this->input->post('owner'),
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by
        );
        $this->db->where('id_bank', $id_bank);
        $this->db->update('tb_bank', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function delete($id, $by){
        $data = array(
            'deleted_at'    => date('Y-m-d H:i:s'),
            'deleted_by'    => $by
        );
        $this->db->where('id_bank', $id);
        $this->db->update('tb_bank', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
}
