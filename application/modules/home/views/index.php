<style type="text/css">
	#d_visi_misi img{
		margin: 0 10px;
	}
	p{
	    text-indent: 0 !important;
	}
	@media screen and (max-width: 600px){
		.index_harapan{
		    letter-spacing: 4px;
		}
	}
	@media screen and (max-width: 500px){
		.index_harapan{
		    letter-spacing: 3px;
		}
	}
	@media screen and (max-width: 400px){
		.index_harapan{
		    letter-spacing: 1px;
		}
	}
</style>
<div class="row" style="padding: 15px 0; min-height: 500px;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row">
			<div class="col-md-12">
				<!--<div class="w-100 text-center text_title" style="margin-bottom: 10px;">Visi Misi</div>-->
				<div style="text-align: justify;" id="d_visi_misi">
					<?php echo $visi_misi->description ?>
					<br>
				</div>
				<div class="text_title text-center" style="border-bottom: 5px solid rgb(255,197,70); margin-bottom: 25px; padding-bottom: 25px; letter-spacing: 8px;">Kami Percaya <span class="index_harapan" style="font-weight: 600;"><i>#SelaluAdaHarapan</i></span></div>
				<div class="text_title text-center" style="letter-spacing: 5px; display: none;" id="bantuan_utama">Mereka Butuh Bantuan Kalian</div>
				<table id="table_case" class="table" style="width: 100%;">
                    <thead style="display: none;">
                        <tr>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <?php if($kegiatan != ''){
                echo '<div class="text_title" style="letter-spacing: 5px; padding-left: 15px;">Cerita Orang Baik</div>';
                echo '<div id="slide_shows" class="carousel slide carousel-slide" data-ride="carousel" style="margin: 25px 0;">';
                    echo '<div class="carousel-inner" role="listbox" id="list_slideshows" style="width: 80%; margin: 0 10%;">';
                        $total_kegiatan = count($kegiatan);
                    	$i = 0;
                        foreach($kegiatan as $row){
                            $i ++;
                            if($i == $total_kegiatan){
                                echo '<div class="carousel-item active">';
                            }else{
                                echo '<div class="carousel-item">';
                            }
                            $doc = new DOMDocument();
			                $doc->loadHTML($row->description);
			                $items = array();
			                foreach ($doc->getElementsByTagName('p') as $p) {
			                    $items[] = $p->nodeValue;
			                }
			                if(count($items) == 0){
			                    foreach ($doc->getElementsByTagName('div') as $p) {
			                        $items[] = $p->nodeValue;
			                    }
			                }
			                $row->description = $items[0];
				                echo '<div class="row" style="margin: 0;">';
			                        echo '<div class="col-lg-6" style="padding: 0 15px;">';
			                            echo '<img src="' . site_url('assets/project/kegiatan/' . $row->image) . '" class="w-100 img_artikel" data="' . $row->id_agenda . '" style="cursor: pointer; max-height: 225px;">';
			                        echo '</div>';
			                        echo '<div class="col-lg-6 text_col_search">';
			                            echo '<div style="text-align: justify; font-size: 14px;">' . $row->description . '</div>';
			                            echo '<div style="text-align: justify; color: rgb(255,197,70); cursor: pointer;" class="detail_artikel" data="' . $row->id_agenda . '">Selengkapnya</div>';
			                        echo '</div>';
			                    echo '</div>';
                            echo '</div>';
                        }
                    echo '</div>';
                    echo '<div id="slider_controls">';
                        echo '<a class="carousel-control-prev" href="#slide_shows" role="button" data-slide="prev" style="background-color: rgba(0,0,0,0.15); width: 10%;">';
                            echo '<span class="carousel-control-prev-icon" aria-hidden="true"></span>';
                            echo '<span class="sr-only">Previous</span>';
                        echo '</a>';
                        echo '<a class="carousel-control-next" href="#slide_shows" role="button" data-slide="next" style="background-color: rgba(0,0,0,0.15); width: 10%;">';
                            echo '<span class="carousel-control-next-icon" aria-hidden="true"></span>';
                            echo '<span class="sr-only">Next</span>';
                        echo '</a>';
                    echo '</div>';
                echo '</div>';
                } ?>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/web_homepage.js?t=').mt_rand()?>"></script>