<style type="text/css">
	input[type="text"]:focus{
		border-color: transparent;
  		box-shadow: inset 0 0 0 transparent, 0 0 0 transparent;
	}
</style>
<script type="text/javascript">
	var id_kasus = <?php echo $id_data ?>;
</script>
<div class="row" style="padding: 15px 0; min-height: 500px;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="w-100 text-center text_title" style="margin-bottom: 50px;"><?php echo $title ?></div>
				<input type="text" name="otp" id="otp" class="form-control" maxlength="6" value="" style="font-size: 2rem; height: auto; text-align: center; border-color: transparent; border-bottom: 2px solid black;">
				<div class="w-100 text-center" style="margin-top: 50px;">Tidak menerima kode OTP ? <span id="resend" style="color: blue; font-weight: 500; cursor: pointer;"><i>Kirim Ulang</i></span></div>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/web_otp_medis.js?t=').mt_rand()?>"></script>