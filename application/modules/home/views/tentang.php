<style type="text/css">
	#d_tentang img{
		margin: 0 15px;
	}
</style>
<div class="row" style="padding: 15px 0; min-height: 500px;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row">
			<div class="col-md-12">
				<div class="w-100 text-center text_title" style="margin-bottom: 10px;">Tentang Kami</div>
				<div style="text-align: justify;" id="d_tentang">
					<?php echo $tentang_kami->description ?>
				</div>
			</div>
			<div class="col-md-12">
				<div class="w-100 text-center text_title">Kontak</div>
				<?php
					if($contact){
						foreach($contact as $row){
							echo '<div class="col-md-12">';
								echo '<div style="padding: 2.5px 0;"><i style="font-size: 25px;" class="' . $row->icon . '"></i> ' . $row->name . ' : ' . $row->detail . '</div>';
							echo '</div>';
						}
					}
				?>
			</div>
			<div class="col-md-12 w-100 text-center" style="padding-top: 15px;">
				<button type="button" class="btn btn-warning" id="btn_contact">Hubungi Kami</button>
			</div>
			<!-- <div class="col-md-12" style="text-align: justify;">
		    	<form id="form_data">
		            <div class="form-group">
		                <div class="col-md-12">
		                    <label>Nama</label>
		                    <input type="text" name="c_name" id="c_name" class="form-control" placeholder="Nama Anda">
		                    <label>Email</label>
		                    <input type="text" name="c_email" id="c_email" class="form-control" placeholder="contoh@gmail.com">
		                    <label>No. Handphone</label>
		                    <input type="text" name="c_phone" id="c_phone" class="form-control" placeholder="No. Handphone Anda">
		                    <label>Pesan</label>
		                    <textarea class="form-control" rows="5" name="c_message" id="c_message" placeholder="Pesan Anda"></textarea>
		                </div>
		            </div>
		        </form>
		        <div class="w-100 text-center">
		        	<button type="button" class="btn btn-warning" id="btn_process">Submit</button>
		        </div>
		    </div> -->
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<div class="modal animated" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
            </div>
            <div class="modal-body"> 
                <form id="form_data">
                    <div class="form-group">
		                <div class="col-md-12">
		                    <label>Nama</label>
		                    <input type="text" name="c_name" id="c_name" class="form-control" placeholder="Nama Anda">
		                    <label>Email</label>
		                    <input type="text" name="c_email" id="c_email" class="form-control" placeholder="contoh@gmail.com">
		                    <label>No. Handphone</label>
		                    <input type="text" name="c_phone" id="c_phone" class="form-control" placeholder="No. Handphone Anda">
		                    <label>Pesan</label>
		                    <textarea class="form-control" rows="5" name="c_message" id="c_message" placeholder="Pesan Anda"></textarea>
		                </div>
		            </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-outline-primary" id="btn_process">Submit</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/web_tentang.js?t=').mt_rand()?>"></script>