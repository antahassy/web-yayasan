<style type="text/css">
	#d_tentang img{
		margin: 15px;
	}
	.page-item.active .page-link{
		background-color: rgb(255,197,70);
		border-color: rgb(255,197,70);
	}
	.page-link{
		color: rgb(255,197,70);
	}
	#description_content{
        width: 60%;
        margin: 0 20%;
    }
    @media screen and (max-width: 1000px){
         #description_content{
            width: 70%;
            margin: 0 15%;
        }
    }
    @media screen and (max-width: 800px){
         #description_content{
            width: 80%;
            margin: 0 10%;
        }
    }
    @media screen and (max-width: 600px){
         #description_content{
            width: 90%;
            margin: 0 5%;
        }
    }
    @media screen and (max-width: 400px){
         #description_content{
            width: 100%;
            margin: 0;
        }
    }
</style>
<div class="row" style="padding: 15px 0; min-height: 500px;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row">
			<div class="col-md-12">
				<div class="w-100 text-center text_title">Hasil Pencarian</div>
				<div class="w-100 text-center" style="text-transform: capitalize; margin-bottom: 15px;"><i><?php echo $search ?></i></div>
				<div class="w-100" id="search_result" style="display: none;">
					<div id="donasi_result" style="display: none;">
						<div class="w-100 text-center text_title">Donasi</div>
						<div>
							<table id="table_search_donasi" class="table" style="width: 100%;">
		                        <thead style="display: none;">
		                            <tr>
		                                <th></th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        </tbody>
		                    </table>
						</div>
					</div>
					<div id="agenda_result" style="display: none;">
						<div class="w-100 text-center text_title">Agenda</div>
						<div>
							<table id="table_search_agenda" class="table" style="width: 100%;">
		                        <thead style="display: none;">
		                            <tr>
		                                <th></th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        </tbody>
		                    </table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<div class="modal animated" id="column_gambar_items" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-color: transparent;">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content" style="background-color: transparent; border-color: transparent;">
            <div class="modal-header" style="display: contents;">
                <img id="image_column_items" src="" title="" style="background-color: #fff;">
                <div id="description_content" style="text-align: justify; background-color: rgb(255,239,212); padding: 25px; height: 100vh; overflow: auto; border-radius: 25px;">
                	<div class="text_title" id="title_artikel" style="margin-bottom: 25px;"></div>
                	<div id="description_artikel"></div>
                </div>
            </div>
            <div style="text-align: center; width: 100%; position: fixed; bottom: 0;">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	var keysearch = '<?php echo $search ?>';
</script>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/web_pencarian.js?t=').mt_rand()?>"></script>