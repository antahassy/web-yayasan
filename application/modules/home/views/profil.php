<style type="text/css">
    #form_data input::placeholder{
        color: rgba(0,0,0,0.75) !important;
    }
</style>
<div class="row" style="padding: 15px 0; min-height: 500px;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row" style="padding-bottom: 25px;">
			<div class="col-md-12" style="margin-bottom: 25px;">
				<div class="w-100 text-center text_title" style="text-transform: uppercase;"><?php echo $title ?></div>
			</div>
            <div class="col-md-6">
                <form id="form_foto">
                    <input type="hidden" name="hide_foto_profil" id="hide_foto_profil" value="<?php echo $this->ion_auth->user()->row()->image ?>">
                    <input type="hidden" name="delete_foto_profil" id="delete_foto_profil" value="<?php echo $this->ion_auth->user()->row()->image ?>">
                    <input type="hidden" name="get_foto_profil" id="get_foto_profil" value="<?php echo $this->ion_auth->user()->row()->image ?>">
                    <input type="file" name="foto_profil" id="foto_profil" accept="image/*" style="width: 100%; display: none;">
                    <div style="margin-top: 10px;">
                        <div id="delete_preview_items" style="margin-bottom: 5px;">Hapus Foto</div>
                    </div>
                    <div class="text-center" style="margin-top: -15px; width: 75%; margin: 0 12.5%;">
                        <label for="foto_profil">
                        <?php
                            if($this->ion_auth->user()->row()->image != ''){
                                echo '<img id="preview_items" class="profile_pic" src="' . site_url('assets/project/user/image/' . $this->ion_auth->user()->row()->image) . '" title="">';
                            }else{
                                echo '<img id="preview_items" class="profile_pic" src="' . site_url('assets/project/blank.png') . '" title="">';
                            }
                        ?>
                        </label>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <form id="form_data">
                    <div class="form-group">
                        <input type="text" name="user_email" id="user_email" value="<?php echo $this->ion_auth->user()->row()->username ?>" class="form-control" disabled="" placeholder="Email" style="background: #fff !important; color: rgba(0,0,0,0.75) !important; border-color: transparent !important; border-bottom: 2px solid rgb(255,239,212) !important; margin-top: 15px !important;">

                        <input type="password" name="user_password" id="user_password" class="form-control" placeholder="Password (Kosongkan jika tidak diganti)" style="background: #fff !important; color: rgba(0,0,0,0.75) !important; border-color: transparent !important; border-bottom: 2px solid rgb(255,239,212) !important; margin-top: 15px !important;">

                        <input type="text" name="user_name" id="user_name" value="<?php echo $this->ion_auth->user()->row()->nama ?>" class="form-control" placeholder="Nama Lengkap" style="background: #fff !important; color: rgba(0,0,0,0.75) !important; border-color: transparent !important; border-bottom: 2px solid rgb(255,239,212) !important; margin-top: 15px !important;">

                        <input type="text" name="user_phone" id="user_phone" value="<?php echo $this->ion_auth->user()->row()->phone ?>" class="form-control" placeholder="No Ponsel" style="background: #fff !important; color: rgba(0,0,0,0.75) !important; border-color: transparent !important; border-bottom: 2px solid rgb(255,239,212) !important; margin-top: 15px !important;">
                    </div>
                </form>
            </div>
            <div class="col-md-12 text-center" style="margin-top: 25px;">
                <button type="button" class="btn btn-warning" style="color: #fff;" id="btn_profil">Update Profil</button>
            </div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/web_profil.js?t=').mt_rand()?>"></script>