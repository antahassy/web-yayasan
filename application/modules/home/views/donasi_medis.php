<style type="text/css">
	.page-item.active .page-link{
        background-color: rgb(255,197,70);
        border-color: rgb(255,197,70);
    }
    .page-link{
        color: rgb(255,197,70);
    }
</style>
<div class="row" style="padding: 15px 0; min-height: 500px;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row" style="padding-bottom: 25px;">
			<div class="col-md-12" style="margin-bottom: 25px;">
				<div class="w-100 text-center text_title">Daftar <?php echo $title ?></div>
				<table id="table_data" class="table" style="width: 100%;"> 
                    <thead style="display: none;">
                        <tr>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<script type="text/javascript">
	var id_kategori = '<?php echo $kategori ?>';
</script>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/web_list_donasi_medis.js?t=').mt_rand()?>"></script>