<style type="text/css">
	table tr th{
		font-weight: 400;
		background-color: #fff !important;
		color: rgb(255,197,70) !important;
	}
	.table .thead-dark th{
		border-color: rgb(255,197,70);
	}
	#table_donasi_medis_filter input, #table_donasi_non_medis_filter input{
		border-color: transparent;
		border-bottom: 2px solid rgb(255,197,70);
		color: rgb(255,197,70);
		box-shadow: none !important;
	}
	#table_donasi_medis_filter input::placeholder, #table_donasi_non_medis_filter input::placeholder{
		color: rgb(255,197,70);
	}
	#table_donasi_medis_length select, #table_donasi_non_medis_length select{
		border-color: transparent;
		border-bottom: 2px solid rgb(255,197,70);
		color: rgb(255,197,70);
		box-shadow: none !important;
	}
	.page-item.active .page-link{
		background-color: rgb(255,197,70);
		border-color: rgb(255,197,70);
	}
	.page-link{
		color: rgb(255,197,70);
	}
	.l_donasi{
		padding: 25px;
	}
	@media screen and (max-width: 500px){
		.l_donasi{
			padding: 5px;
			font-size: 16px;
		}
	}
</style>
<div class="row" style="padding: 15px 0; min-height: 500px;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row" style="padding-bottom: 25px;">
			<div class="col-md-12" style="margin-bottom: 25px;">
				<div class="w-100 text-center text_title" style="text-transform: capitalize;">
					<div style="margin-bottom: 25px; text-transform: uppercase;"><?php echo $title ?></div>
					<ul class="list-inline">
					  	<li class="list-inline-item l_donasi donasi_medis" style="border: 5px solid rgb(255,197,70); margin: -4px; cursor: pointer; border-top-left-radius: 25px; border-bottom-left-radius: 25px; color: rgb(255,197,70);">Medis</li>
					  	<li class="list-inline-item l_donasi donasi_non_medis" style="border: 5px solid rgb(255,197,70); margin: -3px; cursor: pointer; border-top-right-radius: 25px; border-bottom-right-radius: 25px; color: rgb(255,197,70);">Non - Medis</li>
					</ul>
				</div>
				<div class="content_table" id="content_medis" style="display: none;">
					<table id="table_donasi_medis" class="table table-vcenter" style="width: 100%;">
                        <thead class="thead-dark">
                            <tr>
                                <th>No</th>
                                <th>Order_ID</th>
                                <th>Tanggal</th>
                                <th>Donasi</th>
                                <th>Campaign</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
				</div>
				<div class="content_table" id="content_non_medis" style="display: none;">
					<table id="table_donasi_non_medis" class="table table-vcenter" style="width: 100%;">
                        <thead class="thead-dark">
                            <tr>
                                <th>No</th>
                                <th>Order_ID</th>
                                <th>Tanggal</th>
                                <th>Donasi</th>
                                <th>Campaign</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/web_riwayat_donasi.js?t=').mt_rand()?>"></script>