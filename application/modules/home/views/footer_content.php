<style type="text/css">
	#d_content img{
		margin: 0 15px;
	}
	#d_content b{
		font-weight: 600;
	}
</style>
<div class="row" style="padding: 15px 0; min-height: 500px;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row">
			<div class="col-md-12">
				<div class="w-100 text-center text_title" style="margin-bottom: 10px;"><?php echo $title?></div>
				<div style="text-align: justify;" id="d_content">
					<?php echo $footer_content->description ?>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>