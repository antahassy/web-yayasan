<style type="text/css">
	table tr th{
		font-weight: 400;
		background-color: #fff !important;
		color: rgb(255,197,70) !important;
	}
	.table .thead-dark th{
		border-color: rgb(255,197,70);
	}
	#table_galang_dana_medis_filter input, #table_galang_dana_non_medis_filter input{
		border-color: transparent;
		border-bottom: 2px solid rgb(255,197,70);
		color: rgb(255,197,70);
		box-shadow: none !important;
	}
	#table_galang_dana_medis_filter input::placeholder, #table_galang_dana_non_medis_filter input::placeholder{
		color: rgb(255,197,70);
	}
	#table_galang_dana_medis_length select, #table_galang_dana_non_medis_length select{
		border-color: transparent;
		border-bottom: 2px solid rgb(255,197,70);
		color: rgb(255,197,70);
		box-shadow: none !important;
	}
	.page-item.active .page-link{
		background-color: rgb(255,197,70);
		border-color: rgb(255,197,70);
	}
	.page-link{
		color: rgb(255,197,70);
	}
	.l_donasi{
		padding: 25px;
	}
	@media screen and (max-width: 500px){
		.l_donasi{
			padding: 5px;
			font-size: 16px;
		}
	}
</style>
<div class="row" style="padding: 15px 0; min-height: 500px;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row" style="padding-bottom: 25px;">
			<div class="col-md-12" style="margin-bottom: 25px;">
				<div class="w-100 text-center text_title" style="text-transform: capitalize;">
					<div style="margin-bottom: 25px; text-transform: uppercase;"><?php echo $title ?></div>
					<ul class="list-inline">
					  	<li class="list-inline-item l_donasi galang_dana_medis" style="border: 5px solid rgb(255,197,70); margin: -4px; cursor: pointer; border-top-left-radius: 25px; border-bottom-left-radius: 25px; color: rgb(255,197,70);">Medis</li>
					  	<li class="list-inline-item l_donasi galang_dana_non_medis" style="border: 5px solid rgb(255,197,70); margin: -3px; cursor: pointer; border-top-right-radius: 25px; border-bottom-right-radius: 25px; color: rgb(255,197,70);">Non - Medis</li>
					</ul>
				</div>
				<div class="content_table" id="content_medis" style="display: none;">
					<table id="table_galang_dana_medis" class="table table-vcenter" style="width: 100%;">
                        <thead class="thead-dark">
                            <tr>
                                <th>No</th>
                                <th>Status</th>
                                <th>Tanggal</th>
                                <th>Kategori</th>
                                <th>Judul</th>
                                <th>Dana</th>
                                <th>Donatur</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
				</div>
				<div class="content_table" id="content_non_medis" style="display: none;">
					<table id="table_galang_dana_non_medis" class="table table-vcenter" style="width: 100%;">
                        <thead class="thead-dark">
                            <tr>
                                <th>No</th>
                                <th>Status</th>
                                <th>Tanggal</th>
                                <th>Kategori</th>
                                <th>Judul</th>
                                <th>Dana</th>
                                <th>Donatur</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<div class="modal animated" id="modal_kabar_terbaru_medis" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="font-weight: 500;"></h5>
            </div>
            <div class="modal-body" style="text-align: justify;"> 
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_kabar_terbaru_non_medis" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="font-weight: 500;"></h5>
            </div>
            <div class="modal-body" style="text-align: justify;"> 
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_dokumen" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <!-- <div class="modal-header">
                <h5 class="modal-title"></h5>
            </div> -->
            <div class="modal-body"> 
                <form id="form_dokumen">
                    <input type="hidden" name="id_data" id="id_data">
                    <div class="form-group">
                        <div class="col-md-12">
                        	<label>Buku Rekening</label>
                            <input type="hidden" name="hide_rekening" id="hide_rekening" value="">
                            <input type="hidden" name="delete_rekening" id="delete_rekening" value="">
                            <input type="hidden" name="get_rekening" id="get_rekening" value="">
                            <input type="file" name="rekening" id="rekening" accept="image/*" style="width: 100%;">
                            <div>
                                <div id="delete_preview_items">Hapus Gambar</div>
                                <img id="preview_items" src="" title="">
                            </div>

                            <label>KTP Pemilik Rekening</label>
                            <input type="hidden" name="hide_ktp" id="hide_ktp" value="">
                            <input type="hidden" name="delete_ktp" id="delete_ktp" value="">
                            <input type="hidden" name="get_ktp" id="get_ktp" value="">
                            <input type="file" name="ktp" id="ktp" accept="image/*" style="width: 100%;">
                            <div>
                                <div id="delete_preview_itemss">Hapus Gambar</div>
                                <img id="preview_itemss" src="" title="">
                            </div>

                            <label>Kartu Keluarga Pemilik Rekening</label>
                            <input type="hidden" name="hide_kk" id="hide_kk" value="">
                            <input type="hidden" name="delete_kk" id="delete_kk" value="">
                            <input type="hidden" name="get_kk" id="get_kk" value="">
                            <input type="file" name="kk" id="kk" accept="image/*" style="width: 100%;">
                            <div>
                                <div id="delete_preview_itemsss">Hapus Gambar</div>
                                <img id="preview_itemsss" src="" title="">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-primary" id="btn_save_dokumen"></button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/web_riwayat_galang_dana.js?t=').mt_rand()?>"></script>