<style type="text/css">
	table tr td{
		padding: 5px;
	}
	.s_medic img:hover, .s_non_medic img:hover{
		background: rgb(55,125,61) !important;
	}
</style>
<div class="row" style="padding: 15px 0; min-height: 500px;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row" style="padding-bottom: 25px;">
			<div class="col-md-12" style="margin-bottom: 25px;">
				<div class="w-100 text-center text_title" style="text-transform: uppercase;">CARA MENJADI ORANG BAIK</div>
			</div>
			<div class="col-md-4 text-center" style="padding: 25px;">
				<img src="<?php echo site_url('assets/project/web/panduan_dana1.png')?>" style="width: 100%; margin: 0; cursor: default;">
			</div>
			<div class="col-md-4 text-center" style="padding: 25px;">
				<img src="<?php echo site_url('assets/project/web/panduan_dana2.png')?>" style="width: 100%; margin: 0; cursor: default;">
			</div>
			<div class="col-md-4 text-center" style="padding: 25px;">
				<img src="<?php echo site_url('assets/project/web/panduan_dana3.png')?>" style="width: 100%; margin: 0; cursor: default;">
			</div>
			<div class="col-md-12" style="margin: 15px 0; font-family: 'Apercu' !important; font-style: italic !important; font-weight: 300 !important; src: local('Apercu'), url('https://fonts.cdnfonts.com/s/22258/Apercu Light Italic.woff') format('woff') !important; color: rgba(0,0,0,0.5) !important;">
				<div class="text_title" style="text-transform: capitalize; padding: 10px 0;">
					Tips Galang Dana
				</div>
				<div style="padding: 10px 0;">
					<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="fas fa-circle"></i> Verifikasi Identitas & Dokumen Medis / Non - Medis
				</div>
				<div style="padding: 10px 0;">
					<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="fas fa-circle"></i> Sebarkan Informasi Galang Dana Anda Seluas Mungkin
				</div>
			</div>
			<div class="col-md-12" style="margin: 25px 0;">
				<div class="w-100 text-center text_title">Tujuan Galang Dana</div>
			</div>
			<?php
				foreach($bantuan as $row){
					$b_name = strtolower($row->name);
					if(str_contains($b_name, 'kesehatan') || str_contains($b_name, 'anak')){
						echo '<div class="col-md-6 text-center s_medic" alt="' . $b_name . '" data="home/galang_dana_' . str_replace(' ', '_', $b_name) . '" style="margin: 15px 0; padding: 25px 0;">';
							echo '<img src="' . site_url('assets/project/'. $row->icon) . '" style="width: 70%; margin: 0 15%; cursor: pointer; background: rgb(245,134,53);">';
						echo '</div>';
					}else{
						echo '<div class="col-md-6 text-center s_non_medic" alt="' . $b_name . '" data="home/galang_dana_' . str_replace(' ', '_', $b_name) . '" style="margin: 15px 0; padding: 25px 0;">';
							echo '<img src="' . site_url('assets/project/'. $row->icon) . '" style="width: 70%; margin: 0 15%; cursor: pointer; background: rgb(245,134,53);">';
						echo '</div>';
					}
                }
			?>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<div class="modal animated" id="modal_medic" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="background: rgb(255,239,212);">
        	<div class="modal-header">
                <h5 class="modal-title" style="text-transform: capitalize;"></h5>
            </div>
            <div class="modal-body"> 
                <div class="text_title" style="font-weight: 500; text-transform: capitalize;">Persyaratan :</div>
                <div id="body_content">
                	
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-warning" id="btn_medic_next">Lanjutkan</button>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_non_medic" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="background: rgb(255,239,212);">
        	<div class="modal-header">
                <h5 class="modal-title" style="text-transform: capitalize; font-weight: 400;"></h5>
            </div>
            <div class="modal-body"> 
                <div class="text_title" style="font-weight: 600; text-transform: capitalize;">Panduan Menggalang Dana :</div>
                <table style="width: 100%;">
                	<tr>
                		<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>
                		<td>Informasi yang lengkap dan akurat akan mempercepat proses review galang dana Anda</td>
                	</tr>
                	<tr>
                		<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>
                		<td>Berikan data diri penggalang dana dan penerima manfaat yang dapat dipertanggung jawabkan</td>
                	</tr>
                	<tr>
                		<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>
                		<td>Tuliskan target donasi yang sesuai dengan rincian penggunaan dana</td>
                	</tr>
                	<tr>
                		<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>
                		<td>Pastikan penerima manfaat--baik individu maupun lembaga, sudah memberi izin untuk dipublikasikan namanya dalam penggalangan dana</td>
                	</tr>
                </table>
                <div class="text_title" style="font-weight: 600; text-transform: capitalize;">Galang Dana yang Tidak Difasilitasi :</div>
                <table style="width: 100%;">
                	<tr>
                		<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>
                		<td>Galang dana fiktif atau main main</td>
                	</tr>
                	<tr>
                		<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>
                		<td>Galang dana untuk politik praktis</td>
                	</tr>
                	<tr>
                		<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>
                		<td>Galang dana untuk membayar utang</td>
                	</tr>
                	<tr>
                		<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>
                		<td>Galang dana untuk membiayai terorisme dan kegiatan-kegiatan lainnya yang melanggar hukum</td>
                	</tr>
                </table>
                <div class="text_title" style="font-weight: 600; text-transform: capitalize;">Proses review kami berlandaskan :</div>
                <table style="width: 100%;">
                	<tr>
                		<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>
                		<td>Peraturan dari Kementrian Sosial dan UU No. 9 Tahun 1961 tentang pengumpulan uang dan barang</td>
                	</tr>
                	<tr>
                		<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>
                		<td>Panduan Galang Dana Yayasan Kaizen Peduli Indonesia</td>
                	</tr>
                	<tr>
                		<td style="width: 5%;"><i style="font-size: 15px; color: rgb(255,197,70);" class="fas fa-circle"></i></td>
                		<td>Kelengkapan Informasi Yang Anda Berikan</td>
                	</tr>
                </table>
                <div style="margin-top: 15px;"><i>Kaizen berhak menutup sepihak penggalangan dana yang melanggar aturan kami</i></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-warning" id="btn_non_medic_next">Lanjutkan</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/web_galang_dana.js?t=').mt_rand()?>"></script>