<style type="text/css">
	.steps{
		cursor: pointer;
		padding: 10px;
		border-radius: 10px;
	}
	.steps:hover{
		color: #fff !important;
		background: rgb(255,197,70) !important;
	}
	.c_tab{
		display: none;
	}
	#ui-datepicker-div{
	    left: calc(50% - 365px / 2) !important;
	}
	.note-list{
		display: none;
	}
	hr{
		margin: 5px 0;
	}
	label{
		font-weight: 600;
		margin-top: 25px;
	}
	form .form-control::placeholder{
		color: rgba(0, 0, 0, 0.25) !important;
	}
	form .form-control{
		box-shadow: none !important;
		background-color: #fff !important;
		border-color: rgba(0,0,0,0.3) !important;
		color: #495057 !important;
		/*font-family: 'Geliat' !important;
	    font-style: normal !important;
	    font-weight: 200 !important;
	    src: local('Geliat'), url('https://fonts.cdnfonts.com/s/65524/Geliat-ExtraLight.woff') format('woff');*/
	}
	#cdm, #cdp, #tf{
		font-family: 'Apercu' !important;
	    font-style: italic !important;
	    font-weight: 300 !important;
	    src: local('Apercu'), url('https://fonts.cdnfonts.com/s/22258/Apercu Light Italic.woff') format('woff');
	}
	#contoh_doc_medis, #contoh_doc_pemeriksaan, #tips_foto{
		cursor: pointer; 
		color: rgb(255,197,70); 
		font-weight: 600;
		font-style: normal !important;
	}
	input[type="radio"], input[type="checkbox"] {
	    display:none;
	}
	.radio_icon{
		margin-top: 0; 
		font-weight: 400; 
		cursor: pointer;
		padding-top: 7px;
	}
</style>
<script type="text/javascript">
	var id_kasus = <?php echo $id_data ?>;
</script>
<div class="row" style="padding: 15px 0;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row">
			<div class="col-md-12">
				<div class="w-100 text-center text_title"><?php echo $title ?></div>
			</div>
			<div class="col-md-3" style="margin: 25px 0;">
				<div class="steps" id="steps_tujuan" data="tujuan">Tujuan</div>
				<hr>
				<div class="steps" id="steps_pasien" data="pasien">Detail Pasien</div>
				<hr>
				<div class="steps" id="steps_medis" data="medis">Riwayat Medis</div>
				<hr>
				<div class="steps" id="steps_donasi" data="donasi">Target Donasi</div>
				<hr>
				<div class="steps" id="steps_judul" data="judul">Judul</div>
				<hr>
				<div class="steps" id="steps_cerita" data="cerita">Cerita</div>
				<hr>
				<div class="steps" id="steps_ajakan" data="ajakan">Ajakan</div>
				<hr>
			</div>
			<div class="col-md-9" style="margin: 25px 0;">
				<div class="c_tab" id="c_tujuan">
					<form id="form_tujuan">
						<input type="hidden" name="tujuan_main_id" value="<?php echo $id_data ?>">
	            		<label>Siapa Yang Sakit ?</label>
						<div class="form-check s_pasien" style="padding: 5px 1.25rem; cursor: pointer;">
						  	<input class="form-check-input" type="radio" name="pasien" id="pasien1" value="1" style="cursor: pointer;">
						  	<label class="radio_icon radio_pasien" for="pasien1" data="1">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
						    	Saya sendiri
						  	</label>
						</div>
						<div class="form-check s_pasien" style="padding: 5px 1.25rem; cursor: pointer;">
						  	<input class="form-check-input" type="radio" name="pasien" id="pasien2" value="2" style="cursor: pointer;">
						    <label class="radio_icon radio_pasien" for="pasien2" data="2">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
						    	Keluarga satu KK dengan saya
						  	</label>
						</div>
						<div class="form-check s_pasien" style="padding: 5px 1.25rem; cursor: pointer;">
						  	<input class="form-check-input" type="radio" name="pasien" id="pasien3" value="3" style="cursor: pointer;">
						    <label class="radio_icon radio_pasien" for="pasien3" data="3">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
						    	Keluarga inti (Ayah/Ibu/Kakak/Adik/Anak) yang sudah pisah KK dengan saya
						  	</label>
						</div>

						<label>No. Ponsel Anda</label>
						<input type="text" name="tlp_penggalang" id="tlp_penggalang" class="form-control">

						<label>Rekening Bank Penggalang Dana</label>
						<div style="font-size: 13px; color: rgba(0,0,0,0.4); font-family: 'Apercu' !important; font-style: italic !important; font-weight: 300 !important; src: local('Apercu'), url('https://fonts.cdnfonts.com/s/22258/Apercu Light Italic.woff') format('woff');">Donasi hanya dapat dicairkan ke rekening ini</div>
						<div class="form-check s_rekening" style="padding: 5px 1.25rem;">
						  	<input class="form-check-input" type="radio" name="rekening" id="rekening1" value="1" style="cursor: pointer;">
						  	<label class="radio_icon radio_rekening" for="rekening1" data="1">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
						    	Pasien Langsung
						  	</label>
						</div>
						<div class="form-check s_rekening" style="padding: 5px 1.25rem;">
						  	<input class="form-check-input" type="radio" name="rekening" id="rekening2" value="2" style="cursor: pointer;">
						  	<label class="radio_icon radio_rekening" for="rekening2" data="2">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
						    	Keluarga satu KK
						    	<div class="ket_rekening" style="display: none; font-size: 13px; color: rgba(0,0,0,0.4); font-family: 'Apercu' !important; font-style: italic !important; font-weight: 300 !important; src: local('Apercu'), url('https://fonts.cdnfonts.com/s/22258/Apercu Light Italic.woff') format('woff');">Nama pemilik rekening harus tertera di Kartu Keluarga (KK) pasien atau tertera di akta kelahiran/Surat Keterangan Lahir (SKL) pasien jika pasien baru lahir atau belum ada KK.</div>
						  	</label>
						</div>
						<div class="form-check s_rekening" style="padding: 5px 1.25rem;">
						  	<input class="form-check-input" type="radio" name="rekening" id="rekening3" value="3" style="cursor: pointer;">
						  	<label class="radio_icon radio_rekening" for="rekening3" data="3">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
						    	Keluarga inti berbeda KK
						    	<div class="ket_rekening" style="display: none; font-size: 13px; color: rgba(0,0,0,0.4); font-family: 'Apercu' !important; font-style: italic !important; font-weight: 300 !important; src: local('Apercu'), url('https://fonts.cdnfonts.com/s/22258/Apercu Light Italic.woff') format('woff');">Harus terdapat penghubung di antara KK pasien dan KK pemilik rekening (cth. ada nama orang tua yang sama, nama pemilik rekening merupakan orang tua pasien atau sebaliknya)</div>
						  	</label>
						</div>
						<div class="form-check s_rekening" style="padding: 5px 1.25rem;">
						  	<input class="form-check-input" type="radio" name="rekening" id="rekening4" value="4" style="cursor: pointer;">
						  	<label class="radio_icon radio_rekening" for="rekening4" data="4">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
						    	Rumah Sakit
						  	</label>
						</div>
	            	</form>
					<div class="w-100 text-center" style="margin-top: 25px;">
						<button type="button" class="btn btn-warning" id="btn_tujuan">Selanjutnya</button>
					</div>
				</div>
				<div class="c_tab" id="c_pasien">
					<form id="form_pasien">
						<input type="hidden" name="pasien_main_id" value="<?php echo $id_data ?>">
						<label>Nama Pasien</label>
						<input type="text" name="nm_pasien" id="nm_pasien" class="form-control" placeholder="Nama pasien sesuai KK dan dokumen medis">
						<label>Penyakit Yang Diderita</label>
						<input type="text" name="nm_penyakit" id="nm_penyakit" class="form-control" placeholder="Nama penyakit sesuai dokumen medis">
						<label>Foto Dokumen Medis</label>
						<div style="margin-bottom: 10px;" id="cdm">Lihat contoh dokumen medis <span id="contoh_doc_medis">Disini</span></div>
                        <input type="hidden" name="hide_dokumen_medis" id="hide_dokumen_medis" value="">
                        <input type="hidden" name="delete_dokumen_medis" id="delete_dokumen_medis" value="">
                        <input type="hidden" name="get_dokumen_medis" id="get_dokumen_medis" value="">
                        <input type="file" name="dokumen_medis" id="dokumen_medis" accept="image/*" style="width: 100%; display: none;">
                        <label for="dokumen_medis" class="btn btn-warning" style="background-color: rgb(255,239,212);color: rgb(255,197,70) !important; border-color: transparent;">Upload File</label>
                        <div>
                            <div id="delete_preview_itemss">Hapus Foto</div>
                            <img id="preview_itemss" src="" title="">
                        </div>
	            	</form>
					<div class="w-100 text-center" style="margin-top: 25px;">
						<button type="button" class="btn btn-warning" id="btn_pasien">Selanjutnya</button>
					</div>
				</div>
				<div class="c_tab" id="c_medis">
					<form id="form_medis">
						<input type="hidden" name="medis_main_id" value="<?php echo $id_data ?>">
						<label>Foto Diagnosa Hasil Pemeriksaan</label>
						<div style="margin-bottom: 10px;" id="cdp">Lihat contoh diagnosa hasil pemeriksaan <span id="contoh_doc_pemeriksaan">Disini</span></div>
                        <input type="hidden" name="hide_hasil_pemeriksaan" id="hide_hasil_pemeriksaan" value="">
                        <input type="hidden" name="delete_hasil_pemeriksaan" id="delete_hasil_pemeriksaan" value="">
                        <input type="hidden" name="get_hasil_pemeriksaan" id="get_hasil_pemeriksaan" value="">
                        <input type="file" name="hasil_pemeriksaan" id="hasil_pemeriksaan" accept="image/*" style="width: 100%; display: none;">
                        <label for="hasil_pemeriksaan" class="btn btn-warning" style="background-color: rgb(255,239,212);color: rgb(255,197,70) !important; border-color: transparent;">Upload File</label>
                        <div>
                            <div id="delete_preview_itemsss">Hapus Foto</div>
                            <img id="preview_itemsss" src="" title="">
                        </div>
						<label>Apakah Pasien Sedang Menjalani Rawat Inap di Rumah Sakit ?</label>
						<div class="form-check s_r_inap" style="padding: 5px 1.25rem;">
						  	<input class="form-check-input" type="radio" name="r_inap" id="r_inap1" value="1" style="cursor: pointer;">
						  	<label class="radio_icon radio_inap" for="r_inap1" data="1">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
						    	Ya
						    	<div class="ket_r_inap" id="ket_r_inap" style="display: none;">
						    		<label>Rumah Sakit</label>
									<input type="text" name="rs_r_inap" id="rs_r_inap" class="form-control" placeholder="Rumah Sakit Tempat Pasien Dirawat Inap">
									<div style="font-size: 13px; color: rgba(0,0,0,0.4); font-family: 'Apercu' !important; font-style: italic !important; font-weight: 300 !important; src: local('Apercu'), url('https://fonts.cdnfonts.com/s/22258/Apercu Light Italic.woff') format('woff');">Tanggal pada surat keterangan rawat inap maks. 6 bulan ke belakang</div>
						    	</div>
						  	</label>
						</div>
						<div class="form-check s_r_inap" style="padding: 5px 1.25rem;">
						  	<input class="form-check-input" type="radio" name="r_inap" id="r_inap2" value="2" style="cursor: pointer;">
						  	<label class="radio_icon radio_inap" for="r_inap2" data="2">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
						    	Tidak
						    	<div class="ket_r_inap" style="display: none;">
						    	</div>
						  	</label>
						</div>

						<label>Upaya Pengobatan Yang Sudah Dilakukan</label>
						<textarea class="form-control" name="pengobatan" id="pengobatan" rows="5" placeholder="Jelaskan upaya pengobatan apa saja yang sudah dilakukan terhadap pasien"></textarea>

						<label>Sumber Biaya Pengobatan Yang Sudah Dilakukan</label>
						<div class="form-check s_biaya" style="padding: 5px 1.25rem;">
						  	<input class="form-check-input" type="radio" name="biaya" id="biaya1" value="1" style="cursor: pointer;">
						  	<label class="radio_icon radio_biaya" for="biaya1" data="1">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
						    	Biaya Mandiri
						  	</label>
						</div>
						<div class="form-check s_biaya" style="padding: 5px 1.25rem;">
						  	<input class="form-check-input" type="radio" name="biaya" id="biaya2" value="2" style="cursor: pointer;">
						  	<label class="radio_icon radio_biaya" for="biaya2" data="2">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
						    	Biaya Asuransi (BPJS/Swasta)
						  	</label>
						</div>
						<div class="form-check s_biaya" style="padding: 5px 1.25rem;">
						  	<input class="form-check-input" type="radio" name="biaya" id="biaya3" value="3" style="cursor: pointer;">
						  	<label class="radio_icon radio_biaya" for="biaya3" data="3">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
						    	Biaya Mandiri & Asuransi
						  	</label>
						</div>
	            	</form>
					<div class="w-100 text-center" style="margin-top: 25px;">
						<button type="button" class="btn btn-warning" id="btn_medis">Selanjutnya</button>
					</div>
				</div>
				<div class="c_tab" id="c_donasi">
					<form id="form_donasi">
						<input type="hidden" name="donasi_main_id" value="<?php echo $id_data ?>">
						<label>Perkiraan Biaya Yang Dibutuhkan</label>
						<div class="input-group">
				        	<div class="input-group-prepend">
				          		<div class="input-group-text" style="background-color: rgb(255,239,212); border-color: rgb(255,239,212); color: rgb(255,197,70);">Rp</div>
				        	</div>
				        	<input type="text" name="biaya_dana" id="biaya_dana" class="form-control" placeholder="Jumlah Perkiraan Biaya">
				      	</div>
						<label>Batas Akhir Penggalangan Dana</label>
						<input type="text" name="jatuh_tempo" id="jatuh_tempo" class="form-control" placeholder="Pilih Tanggal" readonly="" style="cursor: pointer;">
						<div style="font-size: 15px;"><i>Minimal penggalangan dana adalah 30 hari</i></div>
						<label>Sisa Waktu Penggalangan Dana</label>
						<div class="input-group">
							<input type="text" name="lama_tempo" id="lama_tempo" class="form-control" readonly="">
				        	<div class="input-group-prepend">
				          		<div class="input-group-text" style="background-color: rgb(255,239,212); border-color: rgb(255,239,212); color: rgb(255,197,70);">Hari</div>
				        	</div>
				      	</div>
				      	<div style="font-size: 15px;"><i>Jumlah waktu belum dipotong verifikasi oleh pihak admin maks 3 hari</i></div>
						<label>Rincian Penggunaan Dana</label>
						<textarea class="form-control" name="penggunaan_dana" id="penggunaan_dana" rows="5" placeholder="Jelaskan rencana penggunaan dana yang terkumpul, contoh : biaya operasi Rp. 10.000.000, rawat inap 5 hari Rp. 5.000.000"></textarea>
	            	</form>
					<div class="w-100 text-center" style="margin-top: 25px;">
						<button type="button" class="btn btn-warning" id="btn_donasi">Selanjutnya</button>
					</div>
				</div>
				<div class="c_tab" id="c_judul">
					<form id="form_judul">
						<input type="hidden" name="judul_main_id" value="<?php echo $id_data ?>">
						<label>Judul Penggalangan Dana</label>
				        <input type="text" name="judul_penggalangan" id="judul_penggalangan" class="form-control" placeholder="Contoh : Bantu Raka Untuk Melawan Kanker">
						<label>Link Penggalangan Dana</label>
						<input type="text" name="link_penggalangan" id="link_penggalangan" class="form-control" placeholder="Contoh : banturakalawankanker">
						<label>Foto Judul Utama</label>
						<div style="font-size: 14px; margin-bottom: 10px;" id="tf">Pilih foto yang menggambarkan perjuangan pasien untuk menggalang dana. Lihat Tips <span id="tips_foto">Disini</span></div>
                        <input type="hidden" name="hide_sampul" id="hide_sampul" value="">
                        <input type="hidden" name="delete_sampul" id="delete_sampul" value="">
                        <input type="hidden" name="get_sampul" id="get_sampul" value="">
                        <input type="file" name="sampul" id="sampul" accept="image/*" style="width: 100%; display: none;">
                        <label for="sampul" class="btn btn-warning" style="background-color: rgb(255,239,212);color: rgb(255,197,70) !important; border-color: transparent;">Upload File</label>
                        <div>
                            <div id="delete_preview_items">Hapus Foto</div>
                            <img id="preview_items" src="" title="">
                        </div>
	            	</form>
					<div class="w-100 text-center" style="margin-top: 25px;">
						<button type="button" class="btn btn-warning" id="btn_judul">Selanjutnya</button>
					</div>
				</div>
				<div class="c_tab" id="c_cerita">
					<form id="form_cerita">
						<input type="hidden" name="cerita_main_id" value="<?php echo $id_data ?>">
						<label>Uraian Cerita</label>
						<div style="font-size: 14px; margin-bottom: 10px;">
							Cerita Lengkap Meliputi :
							<ul>
								<li>Identitas penggalang dana & pasien</li>
								<li>Penyakit yang diderita</li>
								<li>Kondisi terkini pasien</li>
								<li>Total biaya yang dibutuhkan</li>
								<li>Rencana penggunaan dana</li>
							</ul>
						</div>
						<textarea class="form-control" name="deskripsi_cerita" id="deskripsi_cerita" rows="10" placeholder="Perkenalkan identitas anda dan pasie, serta hubungan anda dengan pasien"></textarea>
	            	</form>
					<div class="w-100 text-center" style="margin-top: 25px;">
						<button type="button" class="btn btn-warning" id="btn_cerita">Selanjutnya</button>
					</div>
				</div>
				<div class="c_tab" id="c_ajakan">
					<form id="form_ajakan">
						<input type="hidden" name="ajakan_main_id" value="<?php echo $id_data ?>">
						<label>Email Anda</label>
						<input type="text" name="deskripsi_email" id="deskripsi_email" class="form-control" value="<?php echo $this->ion_auth->user()->row()->username ?>" readonly="">
						<label>Ajakan Singkat</label>
						<div style="font-size: 14px; margin-bottom: 10px;">Ajakan singkat akan digunakan saat anda membagikan penggalangan dana ke media sosial</div>
						<textarea class="form-control" name="deskripsi_ajakan" id="deskripsi_ajakan" rows="5" placeholder="Contoh: Penghasilan saya yang hanya seorang becak tidak mampu mencukupi biaya pengobatan anak saya yang membutuhkan Rp 20 Juta. Bantu saya mengobati anak saya untuk melawan kanker."></textarea>

						<div>
							<div class="text_title">Komitmen</div>
							<p>Yayasan Kaizen Peduli Indonesia berkomitmen untuk memastikan <b>dana donasi benar-benar diterima oleh penerima manfaat</b>, baik dengan memverifikasi, mendampingi, hingga kunjungan langsung ke lapangan jika diperlukan</p>
							<p>Jika penerima manfaat tidak memiliki rekening, Yayasan Kaizen Peduli Indonesia akan membantu penyaluran donasi melalui yayasan atau komunitas terpercaya yang menjadi partner Yayasan Kaizen Peduli Indonesia</p>
						</div>
						<div class="form-check">
						  	<input class="form-check-input konfirmasi_persetujuan" type="checkbox" value="1" name="konfirmasi_persetujuan[]" id="persetujuan1">
						  	<label class="radio_icon konfirm_persetujuan label_form_rule1" for="persetujuan1" data="1">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-square"></i>
						    	Dengan klik "Setuju", Anda menjamin kebenaran dari informasi yang diberikan dan menyetujui untuk patuh dengan segala ketentuan, tindakan, dan keputusan dari Yayasan Kaizen Peduli Indonesia.
						  	</label>
						</div>
	            	</form>
					<div class="w-100 text-center" style="margin-top: 25px;">
						<button type="button" class="btn btn-warning" id="btn_ajakan">Ajukan</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<div class="modal animated" id="modal_tujuan" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
        	<div class="modal-header">
                <h5 class="modal-title" style="text-transform: capitalize;">Setujui syarat penggalangan</h5>
            </div>
            <div class="modal-body"> 
                <div class="form-check">
				  	<input class="form-check-input" type="checkbox" value="1" name="syarat_penggalangan[]" id="syarat1">
				  	<label class="radio_icon check_syarat" for="syarat1" data="1">
				  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-square"></i>
				    	Pemilik rekening bertanggung jawab atas penggunaan dana yang diterima dari galang dana ini
				  	</label>
				</div>
				<div class="form-check">
				  	<input class="form-check-input" type="checkbox" value="2" name="syarat_penggalangan[]" id="syarat2">
				  	<label class="radio_icon check_syarat" for="syarat2" data="2">
				  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-square"></i>
				    	Anda sebagai penggalang dana bertanggung jawab atas permintaan pencairan dan pelaporan penggunaan dana
				  	</label>
				</div>
				<div class="form-check">
				  	<input class="form-check-input" type="checkbox" value="3" name="syarat_penggalangan[]" id="syarat3">
				  	<label class="radio_icon check_syarat" for="syarat3" data="3">
				  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-square"></i>
				    	Pasien dan/atau keluarga benar-benar membutuhkan biaya dari galang dana ini
				  	</label>
				</div>
				<div class="form-check">
				  	<input class="form-check-input" type="checkbox" value="4" name="syarat_penggalangan[]" id="syarat4">
				  	<label class="radio_icon check_syarat" for="syarat4" data="4">
				  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-square"></i>
				    	Pasien dan keluarga satu KK pasien menyetujui pembuatan galang dana ini
				  	</label>
				</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="column_gambar_items" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content" style="background-color: transparent; border-color: transparent;">
            <div class="modal-header" style="display: contents;">
                <img id="image_column_items" src="" title="" style="background-color: #fff;">
            </div>
            <div style="text-align: center; width: 100%; position: fixed; bottom: 0;">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_tips_foto" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body"> 
                <ul>
					<li style="margin: 5px 0;">Pasien di foto terlihat jelas dan tajam</li>
					<li style="margin: 5px 0;">Foto tidak menge-blur, tidak buram, dan tidak goyang</li>
					<li style="margin: 5px 0;">Pencahayaan foto yang terang</li>
					<li style="margin: 5px 0;">Pasien di foto tidak terlihat terpotong, tampak utuh terutama memperlihatkan wajah</li>
					<li style="margin: 5px 0;">Tunjukkan ekspresi wajah pasien di foto</li>
					<li style="margin: 5px 0;">Foto pasien dalam keadaan natural, bukan dalam keadaan berpose ke kamera</li>
				</ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/web_galang_dana_medis.js?t=').mt_rand()?>"></script>