<style type="text/css">
	img:hover{
		background: rgb(55,125,61) !important;
	}
</style>
<div class="row" style="padding: 15px 0; min-height: 500px;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row" style="padding-bottom: 25px;">
			<div class="col-md-12" style="margin-bottom: 25px;">
				<div class="w-100 text-center text_title">Tujuan Donasi</div>
			</div>
			<?php
				foreach($bantuan as $row){
					echo '<div class="col-md-6 text-center s_donasi" data="home/donasi_' . str_replace(' ', '_', strtolower($row->name)) . '" style="margin: 15px 0; padding: 25px 0;">';
							echo '<img src="' . site_url('assets/project/'. $row->icon) . '" style="width: 70%; margin: 0 15%; cursor: pointer; background: rgb(245,134,53);">';
					echo '</div>';
                }
			?>
		</div>
	</div>
	<div class="col-md-2"></div> 
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.s_donasi').on('click', function(){
			location.href = site + $(this).attr('data');
		});
	});
</script>