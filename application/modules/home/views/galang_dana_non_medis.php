<style type="text/css">
	.steps{
		cursor: pointer;
		padding: 10px;
		border-radius: 10px;
	}
	.steps:hover{
		color: #fff !important;
		background: rgb(255,197,70) !important;
	}
	.c_tab{
		display: none;
	}
	#ui-datepicker-div{
	    left: calc(50% - 365px / 2) !important;
	}
	.note-list{
		display: none;
	}
	hr{
		margin: 5px 0;
	}
	label{
		font-weight: 600;
		margin-top: 25px;
	}
	form .form-control::placeholder{
		color: rgba(0, 0, 0, 0.25) !important;
	}
	form .form-control{
		box-shadow: none !important;
		background-color: #fff !important;
		border-color: rgba(0,0,0,0.3) !important;
		color: #495057 !important;
		/*font-family: 'Geliat' !important;
	    font-style: normal !important;
	    font-weight: 200 !important;
	    src: local('Geliat'), url('https://fonts.cdnfonts.com/s/65524/Geliat-ExtraLight.woff') format('woff');*/
	}
	input[type="radio"], input[type="checkbox"] {
	    display:none;
	}
	.radio_icon{
		margin-top: 0; 
		font-weight: 400; 
		cursor: pointer;
		padding-top: 7px;
	}
</style>
<script type="text/javascript">
	var id_kasus = <?php echo $id_data ?>;
</script>
<div class="row" style="padding: 15px 0;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row">
			<div class="col-md-12">
				<div class="w-100 text-center text_title"><?php echo $title ?></div>
			</div>
			<div class="col-md-4" style="margin: 25px 0;">
				<div class="steps" id="steps_identitas" data="identitas">Identitas</div>
				<hr>
				<div class="steps" id="steps_donasi" data="donasi">Target Donasi</div>
				<hr>
				<div class="steps" id="steps_detail" data="detail">Detail</div>
				<hr>
				<div class="steps" id="steps_foto" data="foto">Foto</div>
				<hr>
				<div class="steps" id="steps_deskripsi" data="deskripsi">Deskripsi</div>
				<hr>
				<div class="steps" id="steps_konfirmasi" data="konfirmasi">Konfirmasi</div>
				<hr>
			</div>
			<div class="col-md-8" style="margin: 25px 0;">
				<div class="c_tab" id="c_identitas">
					<form id="form_identitas">
						<input type="hidden" name="identitas_main_id" value="<?php echo $id_data ?>">
	            		<label>Pekerjaan</label>
	            		<input type="text" name="identitas_pekerjaan" id="identitas_pekerjaan" class="form-control" placeholder="Contoh : Swasta/Mahasiswa/Wiraswasta">
	            		<label>Sekolah/Perusahaan/Lembaga</label>
	            		<input type="text" name="identitas_organisasi" id="identitas_organisasi" class="form-control" placeholder="Contoh : Universitas Diponegoro/Kaizen">
	            		<label>Media Sosial</label>
	            		<select class="form-control" name="identitas_medsos" id="identitas_medsos">
	            			<option value="">Pilih Media Sosial</option>
	            			<option value="1">Facebook</option>
	            			<option value="2">Instagram</option>
	            			<option value="3">Twitter</option>
	            			<option value="4">Linkedin</option>
	            		</select>
	            		<label>Akun Media Sosial</label>
	            		<input type="text" name="identitas_medsos_akun" id="identitas_medsos_akun" class="form-control" placeholder="Contoh : Kaizen">
	            		<label>Kecamatan Dan Kota Domisili</label>
	            		<input type="text" name="identitas_domisili" id="identitas_domisili" class="form-control" placeholder="Contoh : Kelapa Dua, Tangerang">
	            		<label>Tentang Anda</label>
	            		<textarea name="identitas_tentang" id="identitas_tentang" class="form-control" rows="5" placeholder="Jelaskan bidang kepedulian anda, aktivitas atau hal yang menggambarkan diri anda"></textarea>
	            	</form>
					<div class="w-100 text-center" style="margin-top: 25px;">
						<button type="button" class="btn btn-warning" id="btn_identitas">Selanjutnya</button>
					</div>
				</div>
				<div class="c_tab" id="c_donasi">
					<form id="form_donasi">
						<input type="hidden" name="donasi_main_id" value="<?php echo $id_data ?>">
	            		<label>Perkiraan Jumlah Dana</label>
						<div class="input-group">
				        	<div class="input-group-prepend">
				          		<div class="input-group-text" style="background-color: rgb(255,239,212); border-color: rgb(255,239,212); color: rgb(255,197,70);">Rp</div>
				        	</div>
				        	<input type="text" name="donasi_donasi" id="donasi_donasi" class="form-control" placeholder="Jumlah Target Donasi">
				      	</div>
				      	<label>Batas Akhir Penggalangan Dana</label>
						<input type="text" name="donasi_jatuh_tempo" id="donasi_jatuh_tempo" class="form-control" placeholder="Pilih Tanggal" readonly="" style="cursor: pointer;">
						<div style="font-size: 15px;"><i>Minimal penggalangan dana adalah 30 hari</i></div>
						<label>Sisa Waktu Penggalangan Dana</label>
						<div class="input-group">
							<input type="text" name="donasi_lama_tempo" id="donasi_lama_tempo" class="form-control" readonly="">
				        	<div class="input-group-prepend">
				          		<div class="input-group-text" style="background-color: rgb(255,239,212); border-color: rgb(255,239,212); color: rgb(255,197,70);">Hari</div>
				        	</div>
				      	</div>
				      	<div style="font-size: 15px;"><i>Jumlah waktu belum dipotong verifikasi oleh pihak admin maks 3 hari</i></div>
				      	<label>Penerima Manfaat</label>
						<input type="text" name="donasi_penerima" id="donasi_penerima" class="form-control" placeholder="Contoh : Yayasan Sekolah">
						<label>Rincian Penggunaan Dana Jika Donasi Terkumpul</label>
						<textarea class="form-control" name="donasi_penggunaan" id="donasi_penggunaan" rows="5" placeholder="Jelaskan sedetail mungkin. Contoh : Semen 10 sak, Besi slup 10 batang"></textarea>
	            	</form>
					<div class="w-100 text-center" style="margin-top: 25px;">
						<button type="button" class="btn btn-warning" id="btn_donasi">Selanjutnya</button>
					</div>
				</div>
				<div class="c_tab" id="c_detail">
					<form id="form_detail">
						<input type="hidden" name="detail_main_id" value="<?php echo $id_data ?>">
						<label>Kategori Penggalangan Dana</label>
						<select class="form-control" name="detail_kategori" id="detail_kategori">
	            			<option value="">Pilih Kategori <?php echo $titles ?></option>
	            			<?php
	            				foreach($kategori_bantuan as $row){
	            					echo '<option value="' . $row->id_bantuan_non_medis . '">' . ucfirst($row->bantuan) . '</option>';
	            				}
	            			?>
	            		</select>
						<label>Judul Penggalangan Dana</label>
	            		<input type="text" name="detail_judul" id="detail_judul" class="form-control" placeholder="Contoh : Bantu anak - anak untuk sekolah">
				      	<label>Link Penggalangan Dana</label>
						<input type="text" name="detail_link" id="detail_link" class="form-control" placeholder="Contoh : sekolahanakpedalamanlombok">
						<label>Tujuan Penggalangan Dana</label>
						<textarea name="detail_tujuan" id="detail_tujuan" rows="3" class="form-control" placeholder="Contoh : Pemulihan pendidikan anak di pedalaman lombok"></textarea>
	            	</form>
					<div class="w-100 text-center" style="margin-top: 25px;">
						<button type="button" class="btn btn-warning" id="btn_detail">Selanjutnya</button>
					</div>
				</div>
				<div class="c_tab" id="c_foto">
					<form id="form_foto">
						<input type="hidden" name="foto_main_id" value="<?php echo $id_data ?>">
						<label>Foto Utama</label>
						<div style="font-size: 14px; margin-bottom: 10px;">Pilih foto utama untuk penggalangan dana anda</div>
                        <input type="hidden" name="hide_sampul" id="hide_sampul" value="">
                        <input type="hidden" name="delete_sampul" id="delete_sampul" value="">
                        <input type="hidden" name="get_sampul" id="get_sampul" value="">
                        <input type="file" name="sampul" id="sampul" accept="image/*" style="width: 100%; display: none;">
                        <label for="sampul" class="btn btn-warning" style="background-color: rgb(255,239,212);color: rgb(255,197,70) !important; border-color: transparent;">Upload File</label>
                        <div>
                            <div id="delete_preview_items">Hapus Foto</div>
                            <img id="preview_items" src="" title="">
                        </div>
	            	</form>
					<div class="w-100 text-center" style="margin-top: 25px;">
						<button type="button" class="btn btn-warning" id="btn_foto">Selanjutnya</button>
					</div>
				</div>
				<div class="c_tab" id="c_deskripsi">
					<form id="form_deskripsi">
						<input type="hidden" name="deskripsi_main_id" value="<?php echo $id_data ?>">
						<label>Cerita</label>
						<div style="font-size: 14px; margin-bottom: 10px;">Ceritakan tentang diri anda, alasan penggalangan dana</div>
						<textarea class="form-control" name="deskripsi_cerita" id="deskripsi_cerita" rows="10"></textarea>
						<label>Ajakan Singkat</label>
						<textarea class="form-control" name="deskripsi_ajakan" id="deskripsi_ajakan" rows="5" placeholder="Contoh : Mohon doa dan donasi untuk pemulihan pendidikan di pelosok lombok barat"></textarea>
	            	</form>
					<div class="w-100 text-center" style="margin-top: 25px;">
						<button type="button" class="btn btn-warning" id="btn_deskripsi">Selanjutnya</button>
					</div>
				</div>
				<div class="c_tab" id="c_konfirmasi">
					<form id="form_konfirmasi">
						<input type="hidden" name="konfirmasi_main_id" value="<?php echo $id_data ?>">
						<label>Email Anda</label>
						<input type="text" name="konfirmasi_email" id="konfirmasi_email" class="form-control" value="<?php echo $this->ion_auth->user()->row()->username ?>" readonly="">
						<label>Galang Dana Ini Ditujukan Untuk Keperluan ?</label>
						<div class="form-check s_keperluan" style="padding: 5px 1.25rem;">
						  	<input class="form-check-input" type="radio" name="konfirmasi_keperluan" id="keperluan1" value="1" style="cursor: pointer;">
						  	<label class="radio_icon radio_keperluan" for="keperluan1" data="1">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
						    	Lingkungan disekitar saya
						  	</label>
						</div>
						<div class="form-check s_keperluan" style="padding: 5px 1.25rem;">
						  	<input class="form-check-input" type="radio" name="konfirmasi_keperluan" id="keperluan2" value="2" style="cursor: pointer;">
						  	<label class="radio_icon radio_keperluan" for="keperluan2" data="2">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
						    	Keluarga/Kerabat
						  	</label>
						</div>
						<div class="form-check s_keperluan" style="padding: 5px 1.25rem;">
						  	<input class="form-check-input" type="radio" name="konfirmasi_keperluan" id="keperluan3" value="3" style="cursor: pointer;">
						  	<label class="radio_icon radio_keperluan" for="keperluan3" data="3">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
						    	Organisasi/Lembaga
						  	</label>
						</div>
						<div id="konfirmasi_kota_content" style="display: none;">
							<label>Apakah Anda Tinggal Satu Kota Dengan Penerima Dana ?</label>
							<div class="form-check s_lokasi" style="padding: 5px 1.25rem;">
							  	<input class="form-check-input" type="radio" name="konfirmasi_kota" id="lokasi1" value="1" style="cursor: pointer;">
							  	<label class="radio_icon radio_lokasi" for="lokasi1" data="1">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
							    	Ya
							  	</label>
							</div>
							<div class="form-check s_lokasi" style="padding: 5px 1.25rem;">
							  	<input class="form-check-input" type="radio" name="konfirmasi_kota" id="lokasi2" value="2" style="cursor: pointer;">
							  	<label class="radio_icon radio_lokasi" for="lokasi2" data="2">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-circle"></i>
							    	Tidak
							  	</label>
							</div>
						</div>

						<div>
							<div class="text_title">Komitmen</div>
							<p>Yayasan Kaizen Peduli Indonesia berkomitmen untuk memastikan <b>dana donasi benar-benar diterima oleh penerima manfaat</b>, baik dengan memverifikasi, mendampingi, hingga kunjungan langsung ke lapangan jika diperlukan</p>
							<p>Jika penerima manfaat tidak memiliki rekening, Yayasan Kaizen Peduli Indonesia akan membantu penyaluran donasi melalui yayasan atau komunitas terpercaya yang menjadi partner Yayasan Kaizen Peduli Indonesia</p>
						</div>
						<div class="form-check">
						  	<input class="form-check-input konfirmasi_persetujuan" type="checkbox" value="1" name="konfirmasi_persetujuan[]" id="persetujuan1">
						  	<label class="radio_icon konfirm_persetujuan label_form_rule1" for="persetujuan1" data="1">
						  		<i style="font-size: 20px; color: rgb(255,197,70); margin-right: 10px;" class="far fa-square"></i>
						    	Dengan klik "Setuju", Anda menjamin kebenaran dari informasi yang diberikan dan menyetujui untuk patuh dengan segala ketentuan, tindakan, dan keputusan dari Yayasan Kaizen Peduli Indonesia.
						  	</label>
						</div>
	            	</form>
					<div class="w-100 text-center" style="margin-top: 25px;">
						<button type="button" class="btn btn-warning" id="btn_konfirmasi">Ajukan</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/web_galang_dana_non_medis.js?t=').mt_rand()?>"></script>