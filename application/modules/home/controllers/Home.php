<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Home_model', 'model');
        require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
        date_default_timezone_set('Asia/Jakarta');
    }
 
    public function index(){
        $data['title'] = 'Homepage';
        $data['slideshow'] = $this->model->all_slideshow();
        $data['visi_misi'] = $this->model->visi_misi();
        $kegiatan = $this->model->kegiatan();
        if($kegiatan){
            $data['kegiatan'] = $kegiatan;
        }else{
            $data['kegiatan'] = '';
        }
        $this->load->view('header_front', $data);
        $this->load->view('index');
        $this->load->view('footer_front');
    }

    public function jenis_donasi(){
        $data['title'] = 'Jenis Donasi';
        $data['slideshow'] = $this->model->all_slideshow();
        $data['bantuan'] = $this->model->all_bantuan();
        $this->load->view('header_front', $data);
        $this->load->view('jenis_donasi');
        $this->load->view('footer_front');
        // }
    }

    public function jenis_galang_dana(){
        if (! $this->ion_auth->logged_in()){
            redirect(site_url());
        }else{
            $data['title'] = 'Jenis Galang Dana';
            $data['slideshow'] = $this->model->all_slideshow();
            $data['bantuan'] = $this->model->all_bantuan();
            $this->load->view('header_front', $data);
            $this->load->view('jenis_galang_dana');
            $this->load->view('footer_front');
        }
    }

    public function kegiatan(){
        $data['title'] = 'Kegiatan';
        $data['slideshow'] = $this->model->all_slideshow();
        $this->load->view('header_front', $data);
        $this->load->view('kegiatan');
        $this->load->view('footer_front');
    }

    public function program(){
        $data['title'] = 'Program';
        $data['slideshow'] = $this->model->all_slideshow();
        $this->load->view('header_front', $data);
        $this->load->view('program');
        $this->load->view('footer_front');
    }

    public function tentang(){
        $data['title'] = 'Tentang Kami';
        $data['slideshow'] = $this->model->all_slideshow();
        $data['contact'] = $this->model->all_contact();
        $data['tentang_kami'] = $this->model->tentang_kami();
        $this->load->view('header_front', $data);
        $this->load->view('tentang');
        $this->load->view('footer_front');
    }
    
    public function syarat_ketentuan(){
        $data['title'] = 'Syarat Ketentuan';
        $data['slideshow'] = $this->model->all_slideshow();
        $data['footer_content'] = $this->model->syarat_ketentuan();
        $this->load->view('header_front', $data);
        $this->load->view('footer_content');
        $this->load->view('footer_front');
    }

    public function kebijakan_privasi(){
        $data['title'] = 'Kebijakan Privasi';
        $data['slideshow'] = $this->model->all_slideshow();
        $data['footer_content'] = $this->model->kebijakan_privasi();
        $this->load->view('header_front', $data);
        $this->load->view('footer_content');
        $this->load->view('footer_front');
    }

    public function seputar_pertanyaan(){
        $data['title'] = 'Seputar Pertanyaan';
        $data['slideshow'] = $this->model->all_slideshow();
        $data['footer_content'] = $this->model->seputar_pertanyaan();
        $this->load->view('header_front', $data);
        $this->load->view('footer_content');
        $this->load->view('footer_front');
    }

    public function csr(){
        $data['title'] = 'Tanggung Jawab Sosial Perusahaan (CSR)';
        $data['slideshow'] = $this->model->all_slideshow();
        $data['footer_content'] = $this->model->csr();
        $this->load->view('header_front', $data);
        $this->load->view('footer_content');
        $this->load->view('footer_front');
    }
    
    public function pencarian($search){
        $data['search'] = str_replace('_', ' ', $search);
        $data['title'] = 'Hasil Pencarian';
        $data['slideshow'] = $this->model->all_slideshow();
        $this->load->view('header_front', $data);
        $this->load->view('pencarian');
        $this->load->view('footer_front');
    }

    public function profil(){
        if (! $this->ion_auth->logged_in()){
            redirect(site_url());
        }else{
            $data['title'] = 'Profil';
            $data['slideshow'] = $this->model->all_slideshow();
            $this->load->view('header_front', $data);
            $this->load->view('profil');
            $this->load->view('footer_front');
        }
    }

    public function riwayat_donasi(){
        if (! $this->ion_auth->logged_in()){
            redirect(site_url());
        }else{
            $data['title'] = 'Riwayat Donasi';
            $data['slideshow'] = $this->model->all_slideshow();
            $this->load->view('header_front', $data);
            $this->load->view('riwayat_donasi');
            $this->load->view('footer_front');
        }
    }

    public function riwayat_galang_dana(){
        if (! $this->ion_auth->logged_in()){
            redirect(site_url());
        }else{
            $data['title'] = 'Riwayat Galang Dana';
            $data['slideshow'] = $this->model->all_slideshow();
            $this->load->view('header_front', $data);
            $this->load->view('riwayat_galang_dana');
            $this->load->view('footer_front');
        }
    }

    public function donasi_kesehatan(){
        $data['kategori'] = '2';
        $data['title'] = 'Donasi Kesehatan';
        $data['slideshow'] = $this->model->all_slideshow();
        $this->load->view('header_front', $data);
        $this->load->view('donasi_medis');
        $this->load->view('footer_front');
        // }
    }

    public function donasi_anak_sakit(){
        $data['kategori'] = '1';
        $data['title'] = 'Donasi Anak Sakit';
        $data['slideshow'] = $this->model->all_slideshow();
        $this->load->view('header_front', $data);
        $this->load->view('donasi_medis');
        $this->load->view('footer_front');   
        // }
    }

    public function donasi_kemanusiaan(){
        $data['kategori'] = '1';
        $data['title'] = 'Donasi Kemanusiaan';
        $data['slideshow'] = $this->model->all_slideshow();
        $this->load->view('header_front', $data);
        $this->load->view('donasi_non_medis');
        $this->load->view('footer_front');   
        // }
    }

    public function donasi_bencana(){
        $data['kategori'] = '2';
        $data['title'] = 'Donasi Bencana';
        $data['slideshow'] = $this->model->all_slideshow();
        $this->load->view('header_front', $data);
        $this->load->view('donasi_non_medis');
        $this->load->view('footer_front');   
        // }
    }

    public function galang_dana_kesehatan($id_kasus_medis){
        if (! $this->ion_auth->logged_in()){
            redirect(site_url());
        }else{
            if(! is_numeric($id_kasus_medis)){
                redirect(site_url());
            }
            $id_user = $this->ion_auth->user()->row()->id;
            $kategori_medis = $this->model->kategori_medis($id_kasus_medis, $id_user);
            if(! $kategori_medis){
                redirect(site_url());
            }
            if($kategori_medis->kategori == '1'){
                redirect(site_url('home/galang_dana_anak_sakit/' . $id_kasus_medis));
            }else{
                $title = 'Kesehatan';
            }
            $data['titles'] = $title;
            $data['id_data'] = $id_kasus_medis;
            $data['title'] = 'Galang Dana ' . $title;
            $data['slideshow'] = $this->model->all_slideshow();
            $this->load->view('header_front', $data);
            $this->load->view('galang_dana_medis');
            $this->load->view('footer_front');
        }
    }

    public function galang_dana_anak_sakit($id_kasus_medis){
        if (! $this->ion_auth->logged_in()){
            redirect(site_url());
        }else{
            if(! is_numeric($id_kasus_medis)){
                redirect(site_url());
            }
            $id_user = $this->ion_auth->user()->row()->id;
            $kategori_medis = $this->model->kategori_medis($id_kasus_medis, $id_user);
            if(! $kategori_medis){
                redirect(site_url());
            }
            if($kategori_medis->kategori == '1'){
                $title = 'Anak Sakit';
            }else{
                redirect(site_url('home/galang_dana_kesehatan/' . $id_kasus_medis));
            }
            $data['titles'] = $title;
            $data['id_data'] = $id_kasus_medis;
            $data['title'] = 'Galang Dana ' . $title;
            $data['slideshow'] = $this->model->all_slideshow();
            $this->load->view('header_front', $data);
            $this->load->view('galang_dana_medis');
            $this->load->view('footer_front');   
        }
    }

    public function galang_dana_kemanusiaan($id_kasus_non_medis){
        if (! $this->ion_auth->logged_in()){
            redirect(site_url());
        }else{
            if(! is_numeric($id_kasus_non_medis)){
                redirect(site_url());
            }
            $id_user = $this->ion_auth->user()->row()->id;
            $kategori_non_medis = $this->model->kategori_non_medis($id_kasus_non_medis, $id_user);
            if(! $kategori_non_medis){
                redirect(site_url());
            }
            if($kategori_non_medis->kategori == '2'){
                redirect(site_url('home/galang_dana_bencana/' . $id_kasus_non_medis));
            }else{
                $title = 'Kemanusiaan';
            }
            $data['kategori_bantuan'] = $this->model->kategori_bantuan($kategori_non_medis->kategori);
            $data['titles'] = $title;
            $data['id_data'] = $id_kasus_non_medis;
            $data['title'] = 'Galang Dana ' . $title;
            $data['slideshow'] = $this->model->all_slideshow();
            $this->load->view('header_front', $data);
            $this->load->view('galang_dana_non_medis');
            $this->load->view('footer_front');   
        }
    }

    public function galang_dana_bencana($id_kasus_non_medis){
        if (! $this->ion_auth->logged_in()){
            redirect(site_url());
        }else{
            if(! is_numeric($id_kasus_non_medis)){
                redirect(site_url());
            }
            $id_user = $this->ion_auth->user()->row()->id;
            $kategori_non_medis = $this->model->kategori_non_medis($id_kasus_non_medis, $id_user);
            if(! $kategori_non_medis){
                redirect(site_url());
            }
            if($kategori_non_medis->kategori == '2'){
                $title = 'Bencana';
            }else{
                redirect(site_url('home/galang_dana_kemanusiaan/' . $id_kasus_non_medis));
            }
            $data['kategori_bantuan'] = $this->model->kategori_bantuan($kategori_non_medis->kategori);
            $data['titles'] = $title;
            $data['id_data'] = $id_kasus_non_medis;
            $data['title'] = 'Galang Dana ' . $title;
            $data['slideshow'] = $this->model->all_slideshow();
            $this->load->view('header_front', $data);
            $this->load->view('galang_dana_non_medis');
            $this->load->view('footer_front');   
        }
    }

    public function otp_medis($id_kasus_medis){
        if (! $this->ion_auth->logged_in()){
            redirect(site_url());
        }else{
            if(! is_numeric($id_kasus_medis)){
                redirect(site_url());
            }
            $id_user = $this->ion_auth->user()->row()->id;
            $kategori_medis = $this->model->kategori_medis($id_kasus_medis, $id_user);
            if(! $kategori_medis){
                redirect(site_url());
            }
            if($kategori_medis->kategori == '1'){
                $title = 'Anak Sakit';
            }else{
                $title = 'Kesehatan';
            }
            $data['titles'] = $title;
            $data['id_data'] = $id_kasus_medis;
            $data['title'] = 'Galang Dana ' . $title;
            $data['slideshow'] = $this->model->all_slideshow();
            $this->load->view('header_front', $data);
            $this->load->view('otp_medis');
            $this->load->view('footer_front');
        }
    }

    public function otp_non_medis($id_kasus_non_medis){
        if (! $this->ion_auth->logged_in()){
            redirect(site_url());
        }else{
            if(! is_numeric($id_kasus_non_medis)){
                redirect(site_url());
            }
            $id_user = $this->ion_auth->user()->row()->id;
            $kategori_non_medis = $this->model->kategori_non_medis($id_kasus_non_medis, $id_user);
            if(! $kategori_non_medis){
                redirect(site_url());
            }
            if($kategori_non_medis->kategori == '2'){
                $title = 'Bencana';
            }else{
                $title = 'Kemanusiaan';
            }
            $data['titles'] = $title;
            $data['id_data'] = $id_kasus_non_medis;
            $data['title'] = 'Galang Dana ' . $title;
            $data['slideshow'] = $this->model->all_slideshow();
            $this->load->view('header_front', $data);
            $this->load->view('otp_non_medis');
            $this->load->view('footer_front');   
        }
    }
}