<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

    public function __construct(){
        parent::__construct(); 
        $this->db = $this->load->database('default', TRUE);
        date_default_timezone_set('Asia/Jakarta');
    }

    public function visi_misi(){
        $this->db->select('description');
        $query = $this->db->get_where('tb_rule', array(
            'id_rule'       => '5',
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
    
    public function kegiatan(){
        $this->db->select('id_agenda, image, description');
        $query = $this->db->get_where('tb_agenda', array(
            'type'       => '1',
            'status'       => '1',
            'deleted_at'    => ''
        ), 3);
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function all_slideshow(){
        $this->db->order_by('tb_slideshow.id_slideshow', 'desc');
        $this->db->select('image');
        $query = $this->db->get_where('tb_slideshow', array(
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function all_contact(){
        $this->db->order_by('tb_contact.id_contact', 'asc');
        $this->db->select('name, detail, icon');
        $query = $this->db->get_where('tb_contact', array(
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function all_bantuan(){
        $this->db->order_by('tb_bantuan.id_bantuan', 'asc');
        $this->db->select('id_bantuan, name, icon, color');
        $query = $this->db->get_where('tb_bantuan', array(
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }
    
    public function syarat_ketentuan(){
        $this->db->select('description');
        $query = $this->db->get_where('tb_rule', array(
            'id_rule'       => '1',
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function kebijakan_privasi(){
        $this->db->select('description');
        $query = $this->db->get_where('tb_rule', array(
            'id_rule'       => '2',
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function seputar_pertanyaan(){
        $this->db->select('description');
        $query = $this->db->get_where('tb_rule', array(
            'id_rule'       => '3',
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function csr(){
        $this->db->select('description');
        $query = $this->db->get_where('tb_rule', array(
            'id_rule'       => '6',
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function tentang_kami(){
        $this->db->select('description');
        $query = $this->db->get_where('tb_rule', array(
            'id_rule'       => '4',
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function kategori_medis($id_kasus_medis, $id_user){
        $this->db->select('
            tb_kasus_medis.kategori,
            users.email
        ');
        $this->db->where('tb_kasus_medis.id_user', $id_user);
        $this->db->where('tb_kasus_medis.id_kasus_medis', $id_kasus_medis);
        $this->db->where('tb_kasus_medis.status', '0');
        $this->db->where('tb_kasus_medis.deleted_at', '');
        $this->db->from('tb_kasus_medis');
        $this->db->join('users', 'tb_kasus_medis.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function kategori_non_medis($id_kasus_non_medis, $id_user){
        $this->db->select('
            tb_kasus_non_medis.kategori,
            users.email
        ');
        $this->db->where('tb_kasus_non_medis.id_user', $id_user);
        $this->db->where('tb_kasus_non_medis.id_kasus_non_medis', $id_kasus_non_medis);
        $this->db->where('tb_kasus_non_medis.status', '0');
        $this->db->where('tb_kasus_non_medis.deleted_at', '');
        $this->db->from('tb_kasus_non_medis');
        $this->db->join('users', 'tb_kasus_non_medis.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function kategori_bantuan($kategori){
        $this->db->order_by('bantuan', 'asc');
        $this->db->select('id_bantuan_non_medis, bantuan');
        $query = $this->db->get_where('tb_bantuan_non_medis', array(
            'kategori'    => $kategori
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }
}