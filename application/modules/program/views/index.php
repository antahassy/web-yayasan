<style type="text/css">
	#d_content img{
		margin: 0 15px;
	}
	#d_content b{
		font-weight: 600;
	}
</style>
<div class="row" style="padding: 15px 0; min-height: 500px;">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="main_content">
		<div class="row">
			<div class="col-md-12">
				<div class="w-100 text-center text_title" style="margin-bottom: 10px;">Program</div>
				<?php if($dd->image != ''){ ?>
					<img src="<?php echo $dd->image ?>" class="donasi_sampul">
				<?php } ?>
				<div class="w-100 text-center text_title" style="margin: 10px 0; font-weight: 600; text-transform: capitalize;"><?php echo $title?></div>
				<div style="margin-bottom: 25px;">
					<button type="button" class="btn btn-warning" id="btn_share" style="background: rgb(255,239,212); border-color: rgb(255,239,212); color: rgb(255,197,70) !important;"><i class="fas fa-share-alt"></i> Bagikan</button>
				</div>
				<div style="text-align: justify;" id="d_content">
					<?php echo $dd->description ?>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#btn_share').on('click', function(){
			var temporary = $("<input>");
		    $("body").append(temporary);
		    temporary.val(window.location.href).select();
		    document.execCommand("copy");
		    temporary.remove();
		    swal({
                background  : 'transparent',
                html        : '<pre>Link tersalin</pre>',
            });
		});
	});
</script>