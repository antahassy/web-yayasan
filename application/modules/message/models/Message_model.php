<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	// Datatable server side processing
    public function s_side_datatables_query(){
        $this->db->select('
            message.id,
            message.name,
            message.email,
            message.phone,
            message.message,
            message.respond,
            message.created_by,
            message.created_at,
            message.updated_by,
            message.updated_at
        ');
        $column_order = array(null, 
            'message.id',
            'message.name',
            'message.email',
            'message.phone',
            'message.message',
            'message.respond',
            'message.created_by',
            'message.created_at',
            'message.updated_by',
            'message.updated_at'
        );
        $column_search = array(
            'message.id',
            'message.name',
            'message.email',
            'message.phone',
            'message.message',
            'message.respond',
            'message.created_by',
            'message.created_at',
            'message.updated_by',
            'message.updated_at'
        );
        $i = 0;
        $this->db->where('message.deleted_at','');
        $this->db->from('message');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('message.id', 'desc');
        }
    }

    public function s_side_data(){
        $this->s_side_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered(){
        $this->s_side_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->where('message.deleted_at','');
        $this->db->from('message');
        return $this->db->count_all_results();
    }
    // End datatable server side processing

    public function edit($id){
        $this->db->select('id, message, email, respond, name');
        $query = $this->db->get_where('message', array(
            'id' => $id
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function respond($id, $by, $reply){
        $data           = array(
            'respond'       => $reply,
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by
        );
        $this->db->where('id', $id);
        $this->db->update('message', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
}
