<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller {

	public function __construct(){
		parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Message_model','model');
        require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
		if (! $this->ion_auth->logged_in() || ! $this->ion_auth->is_admin()){
            redirect('auth', 'refresh');
        }
	}
	
	// public $mail_host = 'kaizenpeduli.com';
    // public $mail_username = 'cs@kaizenpeduli.com';
    public $mail_host = 'peduliindonesia.org';
    public $mail_username = 'cs@peduliindonesia.org';
    public $mail_password = 'R]Xg,4uBw5A5';
    public $mail_dns = '103.147.154.41';

	public function index(){
		$data['title'] = 'Visitor Message';
        $this->load->view('header', $data);
        $this->load->view('index');
        $this->load->view('footer');
	}

    public function s_side_data(){
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = $this->model->s_side_data();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->name . '<br>' . $field->email . '<br>' . $field->phone;
            $row[] = $field->message;

            if($field->respond == ''){
                $row[] = '<button data="' . $field->id . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_reply" title="Reply" style="margin: 2.5px;">Reply</button>';
            }else{
                $row[] = '<span data="' . $field->id . '" id="btn_respond" title="Respond" style="cursor: pointer; color: limegreen; font-weight: 700;">Responded</span>';
            }

            if($field->created_at != ''){
                $s_created = explode(' ', $field->created_at);
                $s_created_date = explode('-', $s_created[0]);
                $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<br>' . $s_created[1];
            }else{
                $row[] = '';
            }

            if($field->updated_at != ''){
                $s_updated = explode(' ', $field->updated_at);
                $s_updated_date = explode('-', $s_updated[0]);
                $row[] = $s_updated_date[2] .'/'. $month_format[(int)$s_updated_date[1]] .'/'. $s_updated_date[0] . '<br>' . $s_updated[1];
            }else{
                $row[] = '';
            }
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function edit(){
        $id = $this->input->post('id');
        $data = $this->model->edit($id);
        echo json_encode($data);
    }

    public function respond(){
        $by = $this->ion_auth->user()->row()->username;
        $id = $this->input->post('id_data');
        $name = $this->input->post('user_name');
        $email = $this->input->post('user_email');
        $reply = $this->input->post('reply');
        ///////////////////////////////////
        $mail   = new PHPMailer(); 
        $mail->IsSMTP(); 
        $mail->SMTPAuth     = true; 
        $mail->SMTPSecure   = "ssl";   
        $mail->Host         = $this->mail_host;      
        $mail->Port         = 465;                   
        $mail->Username     = $this->mail_username;  
        $mail->Password     = $this->mail_password;            
        $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
        $mail->isHTML(true);
        $mail->SMTPAutoTLS  = false;
        $mail->Hostname     = $this->mail_dns;
        $mail->Subject      = "Pesan Anda";
        // $mail     = new PHPMailer(); 
        // $mail->IsSMTP(); 
        // $mail->SMTPAuth   = true; 
        // $mail->SMTPSecure = "ssl";   
        // $mail->Host       = "smtp.gmail.com";      
        // $mail->Port       = 465;                   
        // $mail->Username   = "ascoglobalindo@gmail.com";  
        // $mail->Password   = "Ascoglobal2021";            
        // $mail->SetFrom('ascoglobalindo@gmail.com', 'Kaizen Peduli Indonesia'); 
        // $mail->isHTML(true);
        // $mail->Subject    = "Pesan Anda";
        $mail->Body         = 
        '<!DOCTYPE html>
        <html> 
            <head>
                <title></title>
            </head>
            <body>
                <div style="font-weight: 600;">Hallo</div>
                <div style="font-weight: 600; font-size: 20px;">' . $name . '</div>
                <br>
                <div>Sesuai dengan pesan Anda kepada kami</div>
                <div style="text-align: justify;">' . $reply . '</div>
            </body>
        </html>';
        $mail->AddAddress($email);
        $send = $mail->Send();
        if($send){
            $data   = $this->model->respond($id, $by, $reply);
            if($data){
                $message['success'] = true;
                echo json_encode($message);
            }
        }else{
            $message['unsent'] = true;
            echo json_encode($message);
        }
    }

    public function editor_upload_img(){
        $config['upload_path']   = './assets/project/message';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image')){
            $file   = array('image' => $this->upload->data());
            $image  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/message/'.$image;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['new_image']        = './assets/project/message/'.$image;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            echo site_url('assets/project/message/'.$image);
        }
    }

    public function editor_delete_img(){
        $src = $this->input->post('src');
        $file_name = str_replace(base_url(), '', $src);
        if(unlink($file_name))
        {
            echo 'File Delete Successfully';
        }
    }
}
