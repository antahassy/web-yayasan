<?php
$setting = $this->Ref_model->ref_setting()->result_array();
$data['judul_navbar'] = $setting[3]['value'];
$data['judul_web'] = $setting[0]['value'];
$data['deskripsi'] = $setting[1]['value'];
$data['logo'] = $setting[2]['value'];
?> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $data['judul_web']; ?> - <?php echo $title; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
        <meta name="description" content="<?php echo $data['judul_web'] . '. ' . $data['deskripsi'] ?>">
        <meta name="keywords" content="Yayasan,Kaizen,Donasi,Galang,Dana,Kegiatan,Program,Kesehatan,Sosial,Bencana">
        <meta name="googlebot-news" content="index,follow">
        <meta name="googlebot" content="index,follow">
        <meta name="author" content="Antahassy Wibawa">
        <meta name="robots" content="index,follow">
        <meta name="language" content="id">
        <meta name="Classification" content="Level">
        <meta name="geo.country" content="Indonesia">
        <meta name="geo.placename" content="Indonesia"> 
        <meta name="geo.position" content="-6.5899176; 106.8230479">
        <meta http-equiv="content-language" content="In-Id">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Cache-Control" content="no-cache">
        <meta http-equiv="Copyright" content="<?php echo $data['judul_web']?>">
        <meta property="og:title" content="<?php echo $data['judul_web']?>">
        <meta property="og:url" content="http://127.0.0.1:8000/">
        <meta property="og:type" content="JobList">
        <meta property="og:site_name" content="<?php echo $data['judul_web']?>">
        <meta itemprop="name" content="<?php echo $data['judul_web']?>">

        <link rel="shortcut icon" href="<?php echo site_url('assets/project/' . $data['logo'] . '?t=').mt_rand()?>" />

        <link href="https://fonts.cdnfonts.com/css/geliat" rel="stylesheet">
        <link href="https://fonts.cdnfonts.com/css/apercu" rel="stylesheet">
                
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/antahassy.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/project/css/style.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/bootstrap.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/fontawesome5/css/all.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/animation.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/dataTables.bootstrap4.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/buttons.bootstrap4.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/dataTables.buttons.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/jquery-ui.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/sweetalert2.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/treeflex.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/toogle.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/summernote/summernote-lite.min.css?t=').mt_rand()?>">

        <script src="<?php echo site_url('assets/antahassy/js/jquery-3.3.1.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/bootstrap.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/fontawesome5/js/all.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/jquery.dataTables.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/dataTables.bootstrap4.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/dataTables.buttons.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.print.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.html5.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.flash.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.colVis.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/be_tables_datatables.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/zip.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/pdfmake.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/vfs_fonts.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/jquery-ui.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/chart.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/chart.plugin.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/sweetalert2.all.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/treeify.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/toogle.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/summernote/summernote-lite.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/summernote/summernote-img-attribute.js?t=').mt_rand()?>"></script>

        <script async src="https://www.googletagmanager.com/gtag/js?id=G-ZX8D6WX3E0"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-ZX8D6WX3E0');
        </script>
    </head>
    <style type="text/css">
        label{
            margin-top: 5px;
            font-weight: 500;
        }
        pre{
            font-family: verdana !important; 
            font-size: 14px !important;
            color: #fff;
            background-color: transparent;
            border-color: transparent;
        }
        .web_search::placeholder{
            color: #fff !important;
        }
        .web_search{
            box-shadow: none !important;
        }
        #main_content{
            padding-top: 50px;
        }
        .ul_menu .list-inline-item{
            padding: 5px 0;
        }
        .swal2-popup .swal2-styled.swal2-confirm{
             background-color: rgb(255,197,70) !important;
        }
        p{
            text-indent: 50px !important;
        }
        #form_data input{
            background-color: rgba(0,0,0,0.1) !important;
            color: rgba(0,0,0,0.75) !important;
        }
        #form_data input::placeholder{
            color: rgba(0,0,0,0.4) !important;
        }
    </style>
    <script>
        var site = "<?php echo site_url('')?>";
        var ip_address = '';
        $.get('https://jsonip.com', function(address){
            ip_address = address.ip;
        });
    </script>
    <body>
        <header>
            <div id="top_header" class="row">
                <div class="col-md-2" id="left_logo">
                    <a href="#">
                        <img src="<?php echo site_url('assets/project/logo.png?t=')?>" alt="Text Logo" />
                    </a>
                </div>
                <div class="col-md-12" id="center_logo">
                    <a href="#">
                        <img src="<?php echo site_url('assets/project/logo1.png?t=')?>" alt="Text Logo" style="border: 4px solid rgb(255,197,70)"/>
                    </a>
                    <div id="slide_show" class="carousel slide carousel-fade" data-ride="carousel">
                        <div class="carousel-inner" role="listbox" id="list_slideshow">
                            <?php
                                $total_slide = count($slideshow);
                                $i = 0;
                                foreach($slideshow as $row){
                                    $i ++;
                                    if($i == $total_slide){
                                        echo '<div class="carousel-item active">';
                                    }else{
                                        echo '<div class="carousel-item">';
                                    }

                                        echo '<div class="d-block image_content" style="background-image: url(' . site_url() . 'assets/project/slideshow/' . $row->image . '); background-position: center; background-size: cover; background-repeat: no-repeat"></div>';
                                    echo '</div>';
                                }
                            ?>
                        </div>
                        <div id="slider_control">
                            <a class="carousel-control-prev" href="#slide_show" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#slide_show" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="bottom_header" class="row" style="padding: 20px 0;">
                
            </div>
        </header>
        <div class="row" style="padding: 15px 0; min-height: 500px;">
            <div class="col-md-2"></div>
            <div class="col-md-8" id="main_content">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="w-100 text-center text_title">
                            <?php echo $title ?>
                        </div>
                        <img src="<?php echo site_url('assets/project/web/pass_recovery.png')?>" style="width: 80%; margin: 0 10%;">
                        <form id="form_data">
                            <input type="hidden" name="id_data" id="id_data" value="<?php echo $id_user ?>">
                            <input type="hidden" name="username" id="username" value="<?php echo $username ?>">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="recovery_email" id="recovery_email" value="<?php echo $email?>" readonly="">

                                    <label>Password</label>
                                    <input type="password" class="form-control" name="recovery_password" id="recovery_password" placeholder="Password Baru">

                                    <label>Konfirmasi Password</label>
                                    <input type="password" class="form-control" name="confirm_recovery_password" id="confirm_recovery_password" placeholder="Konfirmasi Password Baru">
                                </div>
                            </div>
                        </form>
                        <div class="w-100 text-center" style="margin-top: 25px;">
                            <button type="button" class="btn btn-warning" id="btn_process">Reset</button>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <script type="text/javascript" src="<?php echo site_url('assets/project/js/web_recovery.js?t=').mt_rand()?>"></script>
        <footer>
            <p style="margin: 0;">
                <img alt="Logo" src="<?php echo site_url('assets/project/logo1.png?t=').mt_rand()?>" /> Copyright <script>document.write(new Date().getFullYear());</script> by Kaizen Peduli Indonesia
            </p>
        </footer>
        <script type="text/javascript" src="<?php echo site_url('assets/project/js/web_footer.js?t=').mt_rand()?>"></script>
    </body>
</html>