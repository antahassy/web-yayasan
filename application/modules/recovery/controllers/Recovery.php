<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recovery extends CI_Controller {

    public function __construct(){ 
        parent::__construct();
		$this->load->model('Recovery_model', 'model');
		date_default_timezone_set('Asia/Jakarta');
    }

    public function index(){
    	$data['sess_hash'] = $this->session->userdata('sess_hash');
		$sess_hash = $this->session->userdata('sess_hash');
     	$recovery = $this->model->user_identity($sess_hash);
     	if($recovery){
     		$data['username'] = $recovery->username;
	     	$data['id_user'] = $recovery->id;
	     	$data['email'] = $recovery->email;
	     	$data['title'] = 'Pemulihan Password';
	     	$data['slideshow'] = $this->model->all_slideshow();
	        $this->load->view('index', $data);
     	}else{
			redirect(site_url());
		}
    }

    public function password_recovery(){
        $username = $this->input->post('username');
        $password = $this->input->post('confirm_recovery_password');
        $data = $this->ion_auth->reset_password($username, $password);
        if($data){
        	$sess_hash = $this->session->userdata('sess_hash');
	        $session = array(
	            'sess_hash' => $sess_hash
	        );
	        $this->session->unset_userdata($session);
	        $this->session->sess_destroy();
            $message['success'] = true;
            echo json_encode($message);
        }
    }
}
