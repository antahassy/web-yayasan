 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recovery_model extends CI_Model{

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}
	
	public function all_slideshow(){
        $this->db->order_by('tb_slideshow.id_slideshow', 'desc');
        $this->db->select('image');
        $query = $this->db->get_where('tb_slideshow', array(
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

	public function user_identity($data){
		$this->db->select('id, username, email');
		$query = $this->db->get_where('users', array('password' => $data));
		if($query->num_rows() > 0){
        	return $query->row();
    	}else{
        	return false;
    	}
	}
}