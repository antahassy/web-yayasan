<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class K_kemanusiaan extends CI_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->helper('directory');
        date_default_timezone_set('Asia/Jakarta');
		$this->load->model('K_kemanusiaan_model','model');
		require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
		if (! $this->ion_auth->logged_in()){
            redirect('auth', 'refresh');
        }
	}
	
	// public $mail_host = 'kaizenpeduli.com';
    // public $mail_username = 'noreply@kaizenpeduli.com';
    public $mail_host = 'peduliindonesia.org';
    public $mail_username = 'noreply@peduliindonesia.org';
    public $mail_password = '#5(9~-)ZV6j~';
    public $mail_dns = '103.147.154.41';

    public $id_menu = '18';

	public function index(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
            if (in_array('3', $akses_temp)){
                $data['akses_temp'] = $akses_temp;
                $data['title'] = 'Kasus Kemanusiaan';
                $this->load->view('header', $data);
                $this->load->view('index');
                $this->load->view('footer');
            }else{
                echo "Access Denied";
            }
        }else{
            echo "Access Denied";
        }
	}

	public function s_side_data(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
        }

        $month_format = array (1 => 
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        );
        $list = $this->model->s_side_data();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->nama . '<br>' . $field->username;
            
            $d_total = $this->model->total_donasi_non_medis($field->id_kasus_non_medis);
            $row[] = 'Rp.' . number_format($d_total->donasi , 0, ',', '.') . '<br>Dari<br>Rp.' . number_format($field->donasi , 0, ',', '.');
            $row[] = '<span style="text-transform: capitalize;">' . $field->bantuan . '</span>';
            if($field->sampul != ''){
                $row[] = '<img src="' . site_url('assets/project/kasus_non_medis/' . $field->sampul) . '" class="file_data" id="' . $field->id_kasus_non_medis . '" style="width: 200px; height: 100px; cursor: pointer;" title="Zoom In"><br><div style="text-align: center; width: 100%; font-weight: 600; text-transform: capitalize;">' . $field->judul . '</div>';
            }else{
                $row[] = '<img src="' . site_url('assets/project/img.jpg') . '" class="file_data" id="' . $field->id_kasus_non_medis . '" style="width: 200px; height: 100px; cursor: pointer;" title="Zoom In"><br><div style="text-align: center; width: 100%; font-weight: 600; text-transform: capitalize;">' . $field->judul . '</div>';
            }

            if($field->status == '0'){
                $row[] = '<div style="text-align: center; font-weight: 600;">Draft</div>';
            }
            if($field->status == '1'){
                if (in_array('2', $akses_temp)){
                    $count_terbaru = $this->model->unverif_news($field->id_kasus_non_medis);
                    if($count_terbaru == 0){
                        $count_terbaru = '';
                    }else{
                        $count_terbaru = $count_terbaru;
                    }
                    $row[] = '<div style="text-align: center; font-weight: 600;">Publik</div> <button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-primary" id="btn_terbaru" style="margin: 2.5px;">Kabar Terbaru <span style="color: red; font-weight: 600;">' . $count_terbaru . '</span></button>';
                }else{
                    $row[] = '<div style="text-align: center; font-weight: 600;">Publik</div>';
                }
            }
            if($field->status == '2'){
                if (in_array('2', $akses_temp)){
                    $count_terbaru = $this->model->unverif_news($field->id_kasus_non_medis);
                    if($count_terbaru == 0){
                        $count_terbaru = '';
                    }else{
                        $count_terbaru = $count_terbaru;
                    }
                    $row[] = '<div style="text-align: center; font-weight: 600;">Selesai</div> <button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-primary" id="btn_terbaru" style="margin: 2.5px;">Kabar Terbaru <span style="color: red; font-weight: 600;">' . $count_terbaru . '</span></button> <button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_transfer" style="margin: 2.5px;">Transfer</button>';
                }else{
                    $row[] = '<div style="text-align: center; font-weight: 600;">Selesai</div>';
                }
            }

            if (in_array('2', $akses_temp) && in_array('4', $akses_temp)){
                if($field->admin_verify == '0'){
                    if($field->status == '0'){
                        $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-primary" id="btn_detail" style="margin: 2.5px;">Detail</button> <button data="' . $field->id_kasus_non_medis . '" alt="' . $field->sampul . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" style="margin: 2.5px;">Delete</button>';
                        if($field->homepage == '0'){
                            $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_homepage" style="margin: 2.5px;" alt="' . $field->homepage . '">No</button>';
                        }else{
                            $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-success" id="btn_homepage" style="margin: 2.5px;" alt="' . $field->homepage . '">Yes</button>';
                        }
                    }else{
                        $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_verify" style="margin: 2.5px;">Verifikasi</button> <button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-primary" id="btn_detail" style="margin: 2.5px;">Detail</button> <button data="' . $field->id_kasus_non_medis . '" alt="' . $field->sampul . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" style="margin: 2.5px;">Delete</button>';
                        if($field->homepage == '0'){
                            $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_homepage" style="margin: 2.5px;" alt="' . $field->homepage . '">No</button>';
                        }else{
                            $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-success" id="btn_homepage" style="margin: 2.5px;" alt="' . $field->homepage . '">Yes</button>';
                        }
                    }
                }else{
                    $row[] = 'Terverifikasi <button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-primary" id="btn_detail" style="margin: 2.5px;">Detail</button> <button data="' . $field->id_kasus_non_medis . '" alt="' . $field->sampul . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" style="margin: 2.5px;">Delete</button>';
                    if($field->homepage == '0'){
                        $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_homepage" style="margin: 2.5px;" alt="' . $field->homepage . '">No</button>';
                    }else{
                        $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-success" id="btn_homepage" style="margin: 2.5px;" alt="' . $field->homepage . '">Yes</button>';
                    }
                }
            }
            if (in_array('2', $akses_temp) && ! in_array('4', $akses_temp)){
                if($field->admin_verify == '0'){
                    if($field->status == '0'){
                        $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-primary" id="btn_detail" style="margin: 2.5px;">Detail</button>';
                        if($field->homepage == '0'){
                            $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_homepage" style="margin: 2.5px;" alt="' . $field->homepage . '">No</button>';
                        }else{
                            $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-success" id="btn_homepage" style="margin: 2.5px;" alt="' . $field->homepage . '">Yes</button>';
                        }
                    }else{
                        $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_verify" style="margin: 2.5px;">Verifikasi</button> <button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-primary" id="btn_detail" style="margin: 2.5px;">Detail</button>';
                        if($field->homepage == '0'){
                            $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_homepage" style="margin: 2.5px;" alt="' . $field->homepage . '">No</button>';
                        }else{
                            $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-success" id="btn_homepage" style="margin: 2.5px;" alt="' . $field->homepage . '">Yes</button>';
                        }
                    }
                }else{
                    $row[] = 'Terverifikasi <button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-primary" id="btn_detail" style="margin: 2.5px;">Detail</button>';
                    if($field->homepage == '0'){
                        $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_homepage" style="margin: 2.5px;" alt="' . $field->homepage . '">No</button>';
                    }else{
                        $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-success" id="btn_homepage" style="margin: 2.5px;" alt="' . $field->homepage . '">Yes</button>';
                    }
                }
            }
            if (! in_array('2', $akses_temp) && in_array('4', $akses_temp)){
                $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-primary" id="btn_detail" style="margin: 2.5px;">Detail</button> <button data="' . $field->id_kasus_non_medis . '" alt="' . $field->sampul . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" style="margin: 2.5px;">Delete</button>';
                if($field->homepage == '0'){
                    $row[] = '<div style="color: red;">No</div>';
                }else{
                    $row[] = '<div style="color: limegreen;">Yes</div>';
                }
            }
            if (! in_array('2', $akses_temp) && ! in_array('4', $akses_temp)){
                $row[] = '<button data="' . $field->id_kasus_non_medis . '" class="btn btn-sm btn-rounded btn-outline-primary" id="btn_detail" style="margin: 2.5px;">Detail</button>';
                if($field->homepage == '0'){
                    $row[] = '<div style="color: red;">No</div>';
                }else{
                    $row[] = '<div style="color: limegreen;">Yes</div>';
                }
            }

            if($field->created_at != ''){
                $s_created = explode(' ', $field->created_at);
                $s_created_date = explode('-', $s_created[0]);
                $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<br>' . $s_created[1];
            }else{
                $row[] = '';
            }

            if($field->updated_at != ''){
                $s_updated = explode(' ', $field->updated_at);
                $s_updated_date = explode('-', $s_updated[0]);
                $row[] = $s_updated_date[2] .'/'. $month_format[(int)$s_updated_date[1]] .'/'. $s_updated_date[0] . '<br>' . $s_updated[1];
            }else{
                $row[] = '';
            }
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function detail(){
        $month_format = array (1 => 
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        );
        $id_kasus = $this->input->post('id_kasus');
        $data = $this->model->detail($id_kasus);
        unset($data->created_at);
        unset($data->updated_at);
        unset($data->deleted_at);
        unset($data->created_by);
        unset($data->updated_by);
        unset($data->deleted_by);
        if($data->medsos == '0'){
            $data->medsos = '-';
        }
        if($data->medsos == '1'){
            $data->medsos = 'Facebook';
        }
        if($data->medsos == '2'){
            $data->medsos = 'Instagram';
        }
        if($data->medsos == '3'){
            $data->medsos = 'Twitter';
        }
        if($data->medsos == '3'){
            $data->medsos = 'Linkedin';
        }
        /////////////////////////////////////
        if($data->keperluan == '0'){
            $data->keperluan = '-';
        }
        if($data->keperluan == '1'){
            $data->keperluan = 'Penggalang Sendiri';
        }
        if($data->keperluan == '2'){
            $data->keperluan = 'Keluarga/Kerabat';
        }
        if($data->keperluan == '3'){
            $data->keperluan = 'Organisasi/Lembaga';
        }
        /////////////////////////////////////
        if($data->kota == '0'){
            $data->kota = '-';
        }
        if($data->kota == '1'){
            $data->kota = 'Satu Kota Dengan Penerima';
        }
        if($data->kota == '2'){
            $data->kota = 'Tidak Satu Kota Dengan Penerima';
        }
        /////////////////////////////////////
        $s_tempo = explode('-', $data->jatuh_tempo);
        $data->terakhir = $s_tempo[2] .'/'. $month_format[(int)$s_tempo[1]] .'/'. $s_tempo[0];
        $data->now_date = date('Y-m-d');
        echo json_encode($data);
    }

    public function delete(){
        $id = $this->input->post('id_kasus');
        $img = $this->input->post('img_kasus');
        $by = $this->ion_auth->user()->row()->username;
        $data = $this->model->delete($id, $by); 
        if($data){
            //unlink('./assets/project/kesehatan/'.$img);
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function verify(){
        $email = 'Email';
        $d_email = $this->model->base_contact($email);
        $email = $d_email->detail;

        $telepon = 'Telepon';
        $d_telepon = $this->model->base_contact($telepon);
        $telepon = $d_telepon->detail;

        $id_kasus = $this->input->post('id_kasus');
        $by = $this->ion_auth->user()->row()->username;
        $data = $this->model->verify($id_kasus, $by); 
        if($data){
            $detail = $this->model->detail($id_kasus); 
            $mail   = new PHPMailer(); 
            $mail->IsSMTP(); 
            $mail->SMTPAuth     = true; 
            $mail->SMTPSecure   = "ssl";   
            $mail->Host         = $this->mail_host;      
            $mail->Port         = 465;                   
            $mail->Username     = $this->mail_username;  
            $mail->Password     = $this->mail_password;            
            $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
            $mail->isHTML(true);
            $mail->SMTPAutoTLS  = false;
            $mail->Hostname     = $this->mail_dns;
            $mail->Subject      = "Galang Dana";
            $mail->Body         = 
            '<!DOCTYPE html>
            <html>
                <head>
                     <title></title>
                </head>
                <body>
                    <div> 
                      <div style="width: 100%;">
                           <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
                      </div>
                      <br>
                      <br>
                      <div style="width: 100%; text-align: center; text-transform: capitalize;">Hi ' . $detail->user_nama . ',</div>
                      <br>
                      <div style="width: 100%; text-align: center;">Penggalangan dana kamu dengan judul</div>
                      <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize; font-size: 18px;">' . $detail->judul . '</div>
                      <div style="width: 100%; text-align: center;">Sudah tayang dan dapat dilihat pada halaman berikut ini : </div>
                      <br>
                      <br>
                      <div style="width: 100%; text-align: center;"><a href="'.site_url('donasi/non_medis/'.$detail->link).'" class="btn btn-warning" target="_blank" style="border-radius: 10px; background: rgb(255,197,70); padding: 10px; color: #fff; text-decoration: none; font-weight: 600;">Lihat Galang Dana</a></div>
                      <br>
                      <br>
                      <div style="width: 100%; text-align: center;">
                            <div>Jangan lupa untuk menyebarkan link galang dana kamu</div>
                            <div>Ke media sosial dan whatsapp untuk mempercepat proses penggalangan dana</div>
                      </div>
                      <br>
                      <br>
                      <div style="width: 100%; text-align: center;">
                            <div>Salam,</div>
                            <div style="font-weight: 600;">Peduli Indonesia</div>
                      </div>
                      <br>
                      <div style="width: 100%; text-align: center;">
                            <div style="width: auto; padding: 0 10px; display: inline-block;">
                                <a href="https://www.facebook.com/kaizenpeduliindonesia" target="_blank">
                                    <img src="https://imgur.com/TiFD2VP.png" style="height: 30px; cursor: pointer;">
                                </a>
                            </div>
                            <div style="width: auto; padding: 0 10px; display: inline-block;">
                                <a href="https://www.instagram.com/peduliindo" target="_blank">
                                    <img src="https://imgur.com/aCbCLu1.png" style="height: 30px; cursor: pointer;">
                                </a>
                            </div>
                            <div style="width: auto; padding: 0 10px; display: inline-block;">
                                <a href="https://web.whatsapp.com/send?phone=' . preg_replace('/0/', '62', $telepon, 1) . '" target="_blank">
                                    <img src="https://imgur.com/ClkBQ1z.png" style="height: 30px; cursor: pointer;">
                                </a>
                            </div>
                      </div>
                      <br>
                      <div style="width: 100%; text-align: center;">
                            <div>Email : ' . $email . '</div>
                            <div>Whatsapp : ' . preg_replace('/0/', '+62 ', $telepon, 1) . '</div>
                      </div>
                    </div>
                </body>
            </html>';
            $mail->AddAddress($detail->user_email);
            $mail->Send();

            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function public_case(){
        $id_kasus = $this->input->post('id_kasus');
        $data = $this->model->public_case($id_kasus);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function privat_case(){
        $id_kasus = $this->input->post('id_kasus');
        $data = $this->model->privat_case($id_kasus);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function kabar_terbaru(){
        $month_format = array (1 => 
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        );
        $id_kasus = $this->input->post('id_kasus');
        $data = $this->model->kabar_terbaru($id_kasus);
        if($data){
            foreach($data as $row){
                if($row->updated_at != ''){
                    $s_updated = explode(' ', $row->updated_at);
                    $s_updated_date = explode('-', $s_updated[0]);
                    $row->verified = $s_updated_date[2] .'/'. $month_format[(int)$s_updated_date[1]] .'/'. $s_updated_date[0] . ' ' . $s_updated[1];
                }else{
                    $row->verified = '';
                }
                
                $tgl1 = strtotime(date('Y-m-d H:i:s')); 
                $tgl2 = strtotime($row->created_at); 
                $detik = $tgl1 - $tgl2;
                $row->created = number_format($detik , 0, ',', '.') . ' detik';
                if((int)$detik > 60){
                    $menit = $detik / 60;//menit
                    $row->created = number_format($menit , 0, ',', '.') . ' menit';
                    if((int)$menit > 60){
                        $jam = $menit / 60;//jam
                        $row->created = number_format($jam , 0, ',', '.') . ' jam';
                        if((int)$jam > 24){
                            $hari = $jam / 24;//hari
                            $row->created = number_format($hari , 0, ',', '.') . ' hari';
                        }
                    }
                }
            }
            echo json_encode($data);
        }else{
            $message['empty'] = true;
            echo json_encode($message);
        }
    }

    public function verifikasi_kabar_terbaru(){
        $by = $this->ion_auth->user()->row()->nama;
        $id_kabar = $this->input->post('id_kabar_non_medis');
        $data = $this->model->verifikasi_kabar_terbaru($id_kabar, $by);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function delete_kabar_terbaru(){
        $by = $this->ion_auth->user()->row()->nama;
        $id_kabar = $this->input->post('id_kabar_non_medis');
        $data = $this->model->delete_kabar_terbaru($id_kabar, $by);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }
    
    public function get_transfer(){
        $id_kasus = $this->input->post('id_kasus');
        $donasi = $this->model->total_donasi_non_medis($id_kasus);
        $data = $this->model->get_transfer($id_kasus);
        $data->donasi = $donasi->donasi;
        echo json_encode($data);
    }
}
