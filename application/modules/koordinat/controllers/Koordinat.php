<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Koordinat extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Koordinat_model', 'model');
        if (! $this->ion_auth->logged_in()){
            redirect('auth', 'refresh');
        }
        date_default_timezone_set('Asia/Jakarta');
    }  

    public $id_menu = '28';

    public function index(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
            if (in_array('3', $akses_temp)){
                $data['akses_temp'] = $akses_temp;
                $data['title'] = 'Koordinat';
                $this->load->view('header', $data);
                $this->load->view('index');
                $this->load->view('footer');
            }else{
                echo "Access Denied";
            }
        }else{
            echo "Access Denied";
        }
    }

    public function list_provinsi(){
        $data = $this->model->list_provinsi();
        echo json_encode($data);
    }

    public function list_kota(){
        $id_provinsi = $this->input->post('id_provinsi');
        $data = $this->model->list_kota($id_provinsi);
        echo json_encode($data);
    }

    public function list_kecamatan(){
        $id_kota = $this->input->post('id_kota');
        $data = $this->model->list_kecamatan($id_kota);
        echo json_encode($data);
    }

    public function list_kelurahan(){
        $id_kecamatan = $this->input->post('id_kecamatan');
        $data = $this->model->list_kelurahan($id_kecamatan);
        echo json_encode($data);
    }

    public function update_koordinat(){
        $id = $this->input->post('id');
        $kategori = $this->input->post('data');
        $lat = $this->input->post('lat');
        $lng = $this->input->post('lng');
        $update = $this->model->update_koordinat($id, $kategori, $lat, $lng);
        if($update){
            $message['success'] = true;
            echo json_encode($message);
        }
    }
}