<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Koordinat_model extends CI_Model {

    public function __construct(){
        parent::__construct(); 
        $this->db = $this->load->database('default', TRUE);
        date_default_timezone_set('Asia/Jakarta');
    }

    public function list_provinsi(){
        $this->db->order_by('nama', 'asc');
        $this->db->select('
            id_provinsi,
            nama as nama_provinsi,
            lat as lat_provinsi,
            lng as lng_provinsi
        ');
        $query = $this->db->get('tb_provinsi');
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function list_kota($id_provinsi){
        $this->db->order_by('nama', 'asc');
        $this->db->select('
            id_kota,
            nama as nama_kota,
            lat as lat_kota,
            lng as lng_kota
        ');
        $query = $this->db->get_where('tb_kota', array(
            'id_provinsi' => $id_provinsi
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function list_kecamatan($id_kota){
        $this->db->order_by('nama', 'asc');
        $this->db->select('
            id_kecamatan,
            nama as nama_kecamatan,
            lat as lat_kecamatan,
            lng as lng_kecamatan
        ');
        $query = $this->db->get_where('tb_kecamatan', array(
            'id_kota' => $id_kota
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function list_kelurahan($id_kecamatan){
        $this->db->order_by('nama', 'asc');
        $this->db->select('
            id_kelurahan,
            nama as nama_kelurahan,
            lat as lat_kelurahan,
            lng as lng_kelurahan
        ');
        $query = $this->db->get_where('tb_kelurahan', array(
            'id_kecamatan' => $id_kecamatan
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function update_koordinat($id, $kategori, $lat, $lng){
        $data = array(
            'lat'   => $lat,
            'lng'   => $lng
        );
        if($kategori == 'provinsi'){
            $this->db->where('id_provinsi', $id);
            $this->db->update('tb_provinsi', $data);
        }elseif($kategori == 'kota'){
            $this->db->where('id_kota', $id);
            $this->db->update('tb_kota', $data);
        }elseif($kategori == 'kecamatan'){
            $this->db->where('id_kecamatan', $id);
            $this->db->update('tb_kecamatan', $data);
        }elseif($kategori == 'kelurahan'){
            $this->db->where('id_kelurahan', $id);
            $this->db->update('tb_kelurahan', $data);
        }

        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
}