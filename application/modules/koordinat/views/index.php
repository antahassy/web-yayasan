<style type="text/css">
    .thead-dark tr th{
        color: #fff !important;
    }
    form label{
        margin-top: 5px;
        font-weight: 600 !important;
    }
    .marker-cluster-small{
        background-color: transparent !important;
    }
    .marker-cluster-small div{
        background-color: rgba(0, 0, 255, 0.75) !important;
    }
    /*.leaflet-marker-icon{
        background: #fff;
        border-radius: 100%;
        padding: 5px;
    }*/
    .search_btn{
        background-color: rgb(255,239,212) !important;
        color: rgb(255,197,70) !important;
        border-color: transparent !important;
    }
    #map{
        width: 100%; 
        height: 70vh; 
        margin-top: 5px;
        cursor: default;
        z-index: 0;
    }
    .leaflet-control-attribution{
        display: none;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/map/css/leaflet.css?t=').mt_rand()?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/map/css/marker-cluster.css?t=').mt_rand()?>" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/map/css/marker-cluster-default.css?t=').mt_rand()?>" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/map/css/esri-leaflet-geocoder.css?t=').mt_rand()?>" />

<script src="<?php echo site_url('assets/map/js/leaflet.js?t=').mt_rand()?>"></script>
<script src="<?php echo site_url('assets/map/js/leaflet-ajax.js?t=').mt_rand()?>"></script>
<script src="<?php echo site_url('assets/map/js/marker-cluster.js?t=').mt_rand()?>"></script>
<script src="<?php echo site_url('assets/map/js/leaflet-button.js?t=').mt_rand()?>"></script>
<script src="<?php echo site_url('assets/map/js/esri-leaflet.js?t=').mt_rand()?>"></script>
<script src="<?php echo site_url('assets/map/js/esri-leaflet-geocoder.js?t=').mt_rand()?>"></script>

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <?php if (in_array('1', $akses_temp)){ ?>
                    <h5 class="font-weight-bold mt-2 mb-2 mr-5" id="menu_title"><?php echo $title ?></h5>
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                    <button class="btn btn-light-success font-weight-bolder btn-sm" id="btn_add">Add Data</button>
                <?php }else{ ?>
                    <h5 class="font-weight-bold mt-2 mb-2 mr-5" id="menu_title"><?php echo $title ?></h5>
                <?php } ?>
            </div>
            <div class="d-flex align-items-center">
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row" style="min-height: 68vh;">
                <div class="col-lg-12 col-xxl-4 order-1 order-xxl-2">
                    <div class="card card-custom card-stretch gutter-b">
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bolder text-dark"><?php echo $title ?> List</h3>
                        </div>
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-md-6" style="padding-top: 5px; padding-bottom: 5px;">
                                    <label>Provinsi</label>
                                    <select class="form-control s_input" name="s_provinsi" id="s_provinsi">
                                        <option value="">Pilih Provinsi</option>
                                    </select>
                                </div>
                                <div class="col-md-6" style="padding-top: 5px; padding-bottom: 5px;">
                                    <label>Kota/Kabupaten</label>
                                    <select class="form-control s_input" name="s_kota" id="s_kota">
                                        <option value="">Pilih Kota/Kabupaten</option>
                                    </select>
                                </div>
                                <div class="col-md-6" style="padding-top: 5px; padding-bottom: 5px; display: none;">
                                    <label>Kecamatan</label>
                                    <select class="form-control s_input" name="s_kecamatan" id="s_kecamatan">
                                        <option value="">Pilih Kecamatan</option>
                                    </select>
                                </div>
                                <div class="col-md-6" style="padding-top: 5px; padding-bottom: 5px; display: none;">
                                    <label>Desa/Kelurahan</label>
                                    <select class="form-control s_input" name="s_kelurahan" id="s_kelurahan">
                                        <option value="">Pilih Desa/Kelurahan</option>
                                    </select>
                                </div>
                            </div>
                            <div id="map"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/project/js/koordinat.js?t=').mt_rand()?>"></script>
