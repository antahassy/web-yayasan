<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F_bantuan extends CI_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->helper('directory');
        date_default_timezone_set('Asia/Jakarta');
		$this->load->model('F_bantuan_model','model');
		if (! $this->ion_auth->logged_in()){
            redirect('auth', 'refresh');
        }
	}

    public $id_menu = '12';

	public function index(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
            if (in_array('3', $akses_temp)){
                $data['akses_temp'] = $akses_temp;
                $data['title'] = 'Bantuan';
                $this->load->view('header', $data);
                $this->load->view('index');
                $this->load->view('footer');
            }else{
                echo "Access Denied";
            }
        }else{
            echo "Access Denied";
        }
	}

	public function s_side_data(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
        }

        $month_format = array (1 => 
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        );
        $list = $this->model->s_side_data();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;

            if($field->icon != ''){
                $row[] = '<img src="' . site_url('assets/project/' . $field->icon) . '" class="file_data" id="' . $field->id_bantuan . '" style="width: 200px; height: 100px; cursor: pointer; background: rgb(255,197,70);" title="Zoom in">';
            }else{
                $row[] = '<img src="' . site_url('assets/project/img.jpg') . '" class="file_data" id="' . $field->id_bantuan . '" style="width: 200px; height: 100px; cursor: pointer;" title="Zoom in">';
            }
            $row[] = $field->name;

            if (in_array('2', $akses_temp) && in_array('4', $akses_temp)){
                $row[] = '<button data="' . $field->id_bantuan . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_edit" title="Edit" style="margin: 2.5px;">Edit</button> <button data="' . $field->id_bantuan . '" alt="' . $field->icon . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" title="Delete" style="margin: 2.5px;">Delete</button>';
            }
            if (! in_array('2', $akses_temp) && in_array('4', $akses_temp)){
                $row[] = '<button data="' . $field->id_bantuan . '" alt="' . $field->icon . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" title="Delete" style="margin: 2.5px;">Delete</button>';
            }
            if (in_array('2', $akses_temp) && ! in_array('4', $akses_temp)){
                $row[] = '<button data="' . $field->id_bantuan . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_edit" title="Edit" style="margin: 2.5px;">Edit</button>';
            }
            if (! in_array('2', $akses_temp) && ! in_array('4', $akses_temp)){
                $row[] = '';
            }

            if($field->created_at != ''){
                $s_created = explode(' ', $field->created_at);
                $s_created_date = explode('-', $s_created[0]);
                $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<br>' . $s_created[1];
            }else{
                $row[] = '';
            }

            if($field->updated_at != ''){
                $s_updated = explode(' ', $field->updated_at);
                $s_updated_date = explode('-', $s_updated[0]);
                $row[] = $s_updated_date[2] .'/'. $month_format[(int)$s_updated_date[1]] .'/'. $s_updated_date[0] . '<br>' . $s_updated[1];
            }else{
                $row[] = '';
            }
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function save(){
        $hide_image = $this->input->post('hide_image');
        if($hide_image == ''){
            $message['image'] = true;
            echo json_encode($message);
        }else{
            $by = $this->ion_auth->user()->row()->username;
            $config['upload_path']   = './assets/project/web';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size']      = 0;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if($this->upload->do_upload('image')){
                $file   = array('image' => $this->upload->data());
                $image  = $file['image']['file_name'];
                $config['image_library']    = 'gd2';
                $config['source_image']     = './assets/project/web/'.$image;
                $config['create_thumb']     = FALSE;
                $config['maintain_ratio']   = FALSE;
                $config['quality']          = '75%';
                $config['width']            = 1200;
                $config['height']           = 900;
                $config['new_image']        = './assets/project/web/'.$image;
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $data   = $this->model->save($by, $image); 
                if($data){
                    $last_image = $this->input->post('get_image');
                    $delete_image = $this->input->post('delete_image');
                    if($last_image != ''){
                        unlink('./assets/project/web/'.$last_image);
                        $message['success'] = $image;
                        echo json_encode($message);
                    }else if($delete_image != ''){
                        unlink('./assets/project/web/'.$delete_image);
                        $message['success'] = $image;
                        echo json_encode($message);
                    }else{
                        $message['success'] = $image;
                        echo json_encode($message);
                    }
                }
            }else{
                $last_image = $this->input->post('get_image');
                $delete_image = $this->input->post('delete_image');
                if($last_image == '' && $delete_image != ''){
                    unlink('./assets/project/web/'.$delete_image);
                    $image  = '';
                    $data   = $this->model->save($by, $image);
                    if($data){
                        $message['success'] = $image;
                        echo json_encode($message);
                    }
                }else if($last_image == '' && $delete_image == ''){
                    $image  = '';
                    $data   = $this->model->save($by, $image);
                    if($data){
                        $message['success'] = $image;
                        echo json_encode($message);
                    }
                }else{
                    $image  = $last_image;
                    $data   = $this->model->save($by, $image);
                    if($data){
                        $message['success'] = $image;
                        echo json_encode($message);
                    }
                }
            }
        }
    }

    public function edit(){
        $id_bantuan = $this->input->post('id_bantuan');
        $data = $this->model->edit($id_bantuan);
        echo json_encode($data);
    }

    public function update(){
        $hide_image = $this->input->post('hide_image');
        if($hide_image == ''){
            $message['image'] = true;
            echo json_encode($message);
        }else{
            $by = $this->ion_auth->user()->row()->username;
            $id_bantuan = $this->input->post('id_data');
            $config['upload_path']   = './assets/project/web';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size']      = 0;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if($this->upload->do_upload('image')){
                $file   = array('image' => $this->upload->data());
                $image  = $file['image']['file_name'];
                $config['image_library']    = 'gd2';
                $config['source_image']     = './assets/project/web/'.$image;
                $config['create_thumb']     = FALSE;
                $config['maintain_ratio']   = FALSE;
                $config['quality']          = '75%';
                $config['width']            = 1200;
                $config['height']           = 900;
                $config['new_image']        = './assets/project/web/'.$image;
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $data   = $this->model->update($id_bantuan, $by, $image); 
                if($data){
                    $last_image = $this->input->post('get_image');
                    $delete_image = $this->input->post('delete_image');
                    if($last_image != ''){
                        unlink('./assets/project/web/'.$last_image);
                        $message['success'] = $image;
                        echo json_encode($message);
                    }else if($delete_image != ''){
                        unlink('./assets/project/web/'.$delete_image);
                        $message['success'] = $image;
                        echo json_encode($message);
                    }else{
                        $message['success'] = $image;
                        echo json_encode($message);
                    }
                }
            }else{
                $last_image = $this->input->post('get_image');
                $delete_image = $this->input->post('delete_image');
                if($last_image == '' && $delete_image != ''){
                    unlink('./assets/project/web/'.$delete_image);
                    $image  = '';
                    $data   = $this->model->update($id_bantuan, $by, $image);
                    if($data){
                        $message['success'] = $image;
                        echo json_encode($message);
                    }
                }else if($last_image == '' && $delete_image == ''){
                    $image  = '';
                    $data   = $this->model->update($id_bantuan, $by, $image);
                    if($data){
                        $message['success'] = $image;
                        echo json_encode($message);
                    }
                }else{
                    $image  = $last_image;
                    $data   = $this->model->update($id_bantuan, $by, $image);
                    if($data){
                        $message['success'] = $image;
                        echo json_encode($message);
                    }
                }
            }
        }
    }

    public function delete(){
        $jumlah = $this->model->count_all();
        if($jumlah > 1){
            $id_bantuan = $this->input->post('id_bantuan');
            $img = $this->input->post('img_bantuan');
            $by = $this->ion_auth->user()->row()->username;
            $data = $this->model->delete($id_bantuan, $by); 
            if($data){
                //unlink('./assets/project/web/'.$img);
                $message['success'] = true;
                echo json_encode($message);
            }
        }else{
            $message['last'] = true;
            echo json_encode($message);
        }
    }
}
