<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_model extends CI_Model {

    public function __construct(){
        parent::__construct(); 
        $this->db = $this->load->database('default', TRUE);
        date_default_timezone_set('Asia/Jakarta');
    }
    
    public function bca_manual_medis($id_kasus, $by, $user_email, $user_phone, $id_user, $donasi, $datetime){
        $phone_val = str_replace('-', '', $user_phone);
        $data = array(
            'id_kasus_medis'    => $id_kasus,
            'id_user'           => $id_user,
            'id_bank'           => '1',
            'metode'            => '2',
            'email'             => $user_email,
            'nama'              => $by,
            'tlp'               => $phone_val,
            'doa'               => $this->input->post('doa_donasi'),
            'hide_name'         => $this->input->post('donatur_name'),
            'donasi'            => $donasi,
            'status'            => '0',
            'created_at'        => $datetime,
            'created_by'        => $by
        );
        $query = $this->db->insert('tb_donasi_medis', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function bca_manual_kasus_medis($email, $datetime){
        $this->db->order_by('tb_donasi_medis.id_donasi_medis', 'desc');
        $this->db->select('
            tb_donasi_medis.id_donasi_medis as id_donasi,
            tb_kasus_medis.judul
        ');
        $this->db->where('tb_donasi_medis.email', $email);
        $this->db->where('tb_donasi_medis.created_at', $datetime);
        $this->db->from('tb_donasi_medis');
        $this->db->join('tb_kasus_medis', 'tb_donasi_medis.id_kasus_medis = tb_kasus_medis.id_kasus_medis', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
    
    public function donasi_medis_last_id(){
        $this->db->order_by('id_donasi_medis', 'desc');
        $this->db->select('id_donasi_medis as id_donasi');
        $query = $this->db->get('tb_donasi_medis');
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function save_donasi_medis($id_kasus, $by, $user_email, $user_phone, $id_user, $donasi){
        $phone_val = str_replace('-', '', $user_phone);
        $data = array(
            'id_kasus_medis'    => $id_kasus,
            'id_user'           => $id_user,
            'email'             => $user_email,
            'nama'              => $by,
            'tlp'               => $phone_val,
            'doa'               => $this->input->post('doa_donasi'),
            'hide_name'         => $this->input->post('donatur_name'),
            'donasi'            => $donasi,
            'status'            => '0',
            'created_at'        => date('Y-m-d H:i:s'),
            'created_by'        => $by
        );
        $query = $this->db->insert('tb_donasi_medis', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }
    
    public function bca_manual_non_medis($id_kasus, $by, $user_email, $user_phone, $id_user, $donasi, $datetime){
        $phone_val = str_replace('-', '', $user_phone);
        $data = array(
            'id_kasus_non_medis'=> $id_kasus,
            'id_user'           => $id_user,
            'id_bank'           => '1',
            'metode'            => '2',
            'email'             => $user_email,
            'nama'              => $by,
            'tlp'               => $phone_val,
            'doa'               => $this->input->post('doa_donasi'),
            'hide_name'         => $this->input->post('donatur_name'),
            'donasi'            => $donasi,
            'status'            => '0',
            'created_at'        => $datetime,
            'created_by'        => $by
        );
        $query = $this->db->insert('tb_donasi_non_medis', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function bca_manual_kasus_non_medis($email, $datetime){
        $this->db->order_by('tb_donasi_non_medis.id_donasi_non_medis', 'desc');
        $this->db->select('
            tb_donasi_non_medis.id_donasi_non_medis as id_donasi,
            tb_kasus_non_medis.judul
        ');
        $this->db->where('tb_donasi_non_medis.email', $email);
        $this->db->where('tb_donasi_non_medis.created_at', $datetime);
        $this->db->from('tb_donasi_non_medis');
        $this->db->join('tb_kasus_non_medis', 'tb_donasi_non_medis.id_kasus_non_medis = tb_kasus_non_medis.id_kasus_non_medis', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
    
    public function donasi_non_medis_last_id(){
        $this->db->order_by('id_donasi_non_medis', 'desc');
        $this->db->select('id_donasi_non_medis as id_donasi');
        $query = $this->db->get('tb_donasi_non_medis');
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function save_donasi_non_medis($id_kasus, $by, $user_email, $user_phone, $id_user, $donasi){
        $phone_val = str_replace('-', '', $user_phone);
        $data = array(
            'id_kasus_non_medis'=> $id_kasus,
            'id_user'           => $id_user,
            'email'             => $user_email,
            'nama'              => $by,
            'tlp'               => $phone_val,
            'doa'               => $this->input->post('doa_donasi'),
            'hide_name'         => $this->input->post('donatur_name'),
            'donasi'            => $donasi,
            'status'            => '0',
            'created_at'        => date('Y-m-d H:i:s'),
            'created_by'        => $by
        );
        $query = $this->db->insert('tb_donasi_non_medis', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function user_donasi_medis($id_user, $user_email, $id_kasus){
        $this->db->order_by('tb_donasi_medis.id_donasi_medis', 'desc');
        $this->db->select('
            tb_donasi_medis.id_donasi_medis,
            tb_donasi_medis.email,
            tb_donasi_medis.nama as donatur,
            tb_donasi_medis.tlp,
            tb_donasi_medis.created_at,
            users.username,
            users.phone,
            users.nama
        ');
        $this->db->where('tb_donasi_medis.id_user', $id_user);
        $this->db->where('tb_donasi_medis.email', $user_email);
        $this->db->where('tb_donasi_medis.id_kasus_medis', $id_kasus);
        $this->db->from('tb_donasi_medis');
        $this->db->join('users', 'tb_donasi_medis.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function user_donasi_non_medis($id_user, $user_email, $id_kasus){
        $this->db->order_by('tb_donasi_non_medis.id_donasi_non_medis', 'desc');
        $this->db->select('
            tb_donasi_non_medis.id_donasi_non_medis,
            tb_donasi_non_medis.email,
            tb_donasi_non_medis.nama as donatur,
            tb_donasi_non_medis.tlp,
            tb_donasi_non_medis.created_at,
            users.username,
            users.phone,
            users.nama
        ');
        $this->db->where('tb_donasi_non_medis.id_user', $id_user);
        $this->db->where('tb_donasi_non_medis.email', $user_email);
        $this->db->where('tb_donasi_non_medis.id_kasus_non_medis', $id_kasus);
        $this->db->from('tb_donasi_non_medis');
        $this->db->join('users', 'tb_donasi_non_medis.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function hapus_donasi_medis($id_donasi){
        $this->db->where('id_donasi_medis', $id_donasi);
        $this->db->delete('tb_donasi_medis');
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function hapus_donasi_non_medis($id_donasi){
        $this->db->where('id_donasi_non_medis', $id_donasi);
        $this->db->delete('tb_donasi_non_medis');
        if($this->db->affected_rows() > 0){
            return true; 
        }else{
            return false;
        }
    }

    public function bank_id($bank){
        $this->db->select('id_bank, link, number, owner');
        $query = $this->db->get_where('tb_bank', array(
            'name'    => $bank
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function finish_mt_medis($id_donasi, $id_bank, $bank_number, $bank_code, $pdf_url, $time, $code, $status, $main_status){
        $data = array(
            'id_bank'       => $id_bank,
            'va_number'     => $bank_number,
            'va_code'       => $bank_code,
            'metode'        => '1',
            'bukti'         => $pdf_url,
            'mt_time'       => $time,
            'mt_code'       => $code,
            'mt_status'     => $status,
            'status'        => $main_status,
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => 'Midtrans API'
        );
        $this->db->where('id_donasi_medis', $id_donasi);
        $this->db->update('tb_donasi_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function finish_mt_non_medis($id_donasi, $id_bank, $bank_number, $bank_code, $pdf_url, $time, $code, $status, $main_status){
        $data = array(
            'id_bank'       => $id_bank,
            'va_number'     => $bank_number,
            'va_code'       => $bank_code,
            'metode'        => '1',
            'bukti'         => $pdf_url,
            'mt_time'       => $time,
            'mt_code'       => $code,
            'mt_status'     => $status,
            'status'        => $main_status,
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => 'Midtrans API'
        );
        $this->db->where('id_donasi_non_medis', $id_donasi);
        $this->db->update('tb_donasi_non_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function notification_mt_medis($id_donasi, $code, $status, $main_status){
        $data = array(
            'mt_code'       => $code,
            'mt_status'     => $status,
            'status'        => $main_status,
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => 'Midtrans Notification'
        );
        $this->db->where('id_donasi_medis', $id_donasi);
        $this->db->update('tb_donasi_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function notification_mt_non_medis($id_donasi, $code, $status, $main_status){
        $data = array(
            'mt_code'       => $code,
            'mt_status'     => $status,
            'status'        => $main_status,
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => 'Midtrans Notification'
        );
        $this->db->where('id_donasi_non_medis', $id_donasi);
        $this->db->update('tb_donasi_non_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function kasus_medis($id_donasi){
        $this->db->select('
            tb_donasi_medis.donasi,
            tb_donasi_medis.email,
            tb_donasi_medis.nama as donatur,
            tb_kasus_medis.judul,
            tb_kasus_medis.nm_pasien as korban,
            users.username,
            users.nama
        ');
        $this->db->where('tb_donasi_medis.id_donasi_medis', $id_donasi);
        $this->db->from('tb_donasi_medis');
        $this->db->join('tb_kasus_medis', 'tb_donasi_medis.id_kasus_medis = tb_kasus_medis.id_kasus_medis', 'left');
        $this->db->join('users', 'tb_donasi_medis.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function kasus_non_medis($id_donasi){
        $this->db->select('
            tb_donasi_non_medis.donasi,
            tb_donasi_non_medis.email,
            tb_donasi_non_medis.nama as donatur,
            tb_kasus_non_medis.judul,
            tb_kasus_non_medis.penerima as korban,
            users.username,
            users.nama
        ');
        $this->db->where('tb_donasi_non_medis.id_donasi_non_medis', $id_donasi);
        $this->db->from('tb_donasi_non_medis');
        $this->db->join('tb_kasus_non_medis', 'tb_donasi_non_medis.id_kasus_non_medis = tb_kasus_non_medis.id_kasus_non_medis', 'left');
        $this->db->join('users', 'tb_donasi_non_medis.id_user = users.id', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function base_contact($name){
        $this->db->select('detail');
        $query = $this->db->get_where('tb_contact', array(
            'name'    => $name
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }
}