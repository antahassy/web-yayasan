<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Payment_model', 'model');
        // $params = array('server_key' => 'SB-Mid-server-fdRNt2-2vcOVVIyBSig3NRqr', 'production' => false);
        $params = array('server_key' => 'Mid-server-71l7hZvBjL_PhRyf-xDAIX3z', 'production' => true);
        $this->load->library('midtrans');
        $this->midtrans->config($params);
        require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
        date_default_timezone_set('Asia/Jakarta');
    }

    // public $mail_host = 'kaizenpeduli.com';
    // public $mail_username = 'noreply@kaizenpeduli.com';
    public $mail_host = 'peduliindonesia.org';
    public $mail_username = 'noreply@peduliindonesia.org';
    public $mail_password = '#5(9~-)ZV6j~';
    public $mail_dns = '103.147.154.41';
    
    public function bca_manual(){
        $datetime = date('Y-m-d H:i:s');
        $user_email = $this->input->post('user_email');
        $by = $this->input->post('user_nama');
        $user_phone = $this->input->post('user_phone');
        if (! $this->ion_auth->logged_in()){
            $id_user = '0';
        }else{
            $id_user = $this->ion_auth->user()->row()->id;
        }
        $id_kasus = $this->input->post('donasi_main_id');
        $donasi = (int)$this->input->post('hide_donasi');
        $judul_kasus = $this->input->post('judul_kasus');
        $kategori = $this->input->post('kategori');

        if($kategori == 'medis'){
            $last_data = $this->model->donasi_medis_last_id();
            $insert_id = ((int)$last_data->id_donasi + 1);
            $donasi = $donasi + $insert_id;
            $data = $this->model->bca_manual_medis($id_kasus, $by, $user_email, $user_phone, $id_user, $donasi, $datetime);
        }else{
            $last_data = $this->model->donasi_non_medis_last_id();
            $insert_id = ((int)$last_data->id_donasi + 1);
            $donasi = $donasi + $insert_id;
            $data = $this->model->bca_manual_non_medis($id_kasus, $by, $user_email, $user_phone, $id_user, $donasi, $datetime);
        }
        if($data){
            $email = 'Email';
            $d_email = $this->model->base_contact($email);
            $email = $d_email->detail;

            $telepon = 'Telepon';
            $d_telepon = $this->model->base_contact($telepon);
            $telepon = $d_telepon->detail;

            $bank = 'bca';
            $bank_id = $this->model->bank_id($bank);
            $id_bank = $bank_id->id_bank;
            $bank_src = $bank_id->link;

            $msg_content = '
            <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">No. Rekening ' . $bank_id->number . '</div>
            <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">A/n ' . $bank_id->owner . '</div>';

            if($kategori == 'medis'){
                $kasus = $this->model->bca_manual_kasus_medis($user_email, $datetime);
                $mail   = new PHPMailer(); 
                $mail->IsSMTP(); 
                $mail->SMTPAuth     = true; 
                $mail->SMTPSecure   = "ssl";   
                $mail->Host         = $this->mail_host;      
                $mail->Port         = 465;                   
                $mail->Username     = $this->mail_username;  
                $mail->Password     = $this->mail_password;            
                $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
                $mail->isHTML(true);
                $mail->SMTPAutoTLS  = false;
                $mail->Hostname     = $this->mail_dns;
                $mail->Subject      = "Donasi";
                $mail->Body         = 
                '<!DOCTYPE html>
                <html>
                    <head>
                         <title></title>
                    </head>
                    <body>
                        <div> 
                          <div style="width: 100%;">
                               <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
                          </div>
                          <br>
                          <br>
                          <div style="width: 100%; text-align: center;">Hi ' . $by . ',</div>
                          <div style="width: 100%; text-align: center;">Selesaikan donasi kamu melalui :</div>
                          <br>
                          <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">Transfer Bank</div>
                          <div style="width: 100%; text-align: center;">
                                <img src="' . $bank_src . '" style="height: 50px;">
                          </div>
                          <br>
                          ' . $msg_content . '
                          <br>
                          <div style="width: 100%; text-align: center; padding: 25px 0; font-weight: 600; border-radius: 15px; background: rgba(0,0,0,0.1);">
                               <div style="font-size: 20px;">' . $kasus->judul . '</div>
                               <br>
                               <div>Total Donasi</div>
                               <div>Rp. ' . number_format($donasi , 0, ',', '.') . '</div>
                          </div>
                          <br>
                          <br>
                          <div style="width: 100%; text-align: center;"><a href="'.site_url('donasi/medis_pay/'.$kasus->id_donasi).'" class="btn btn-warning" target="_blank" style="border-radius: 10px; background: rgb(255,197,70); padding: 10px; color: #fff; text-decoration: none; font-weight: 600;">Cek Status Donasi</a></div>
                          <br>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div>Salam,</div>
                                <div style="font-weight: 600;">Peduli Indonesia</div>
                          </div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://www.facebook.com/kaizenpeduliindonesia" target="_blank">
                                        <img src="https://imgur.com/TiFD2VP.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://www.instagram.com/kaizenpeduli.ind" target="_blank">
                                        <img src="https://imgur.com/aCbCLu1.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://web.whatsapp.com/send?phone=' . preg_replace('/0/', '62', $telepon, 1) . '" target="_blank">
                                        <img src="https://imgur.com/ClkBQ1z.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                          </div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div>Email : ' . $email . '</div>
                                <div>Whatsapp : ' . preg_replace('/0/', '+62 ', $telepon, 1) . '</div>
                          </div>
                        </div>
                    </body>
                </html>';
                $mail->AddAddress($user_email);
                $mail->Send();
                $message['success'] = 'donasi/medis_pay/' . $kasus->id_donasi;
                echo json_encode($message);
            }else{
                $kasus = $this->model->bca_manual_kasus_non_medis($user_email, $datetime);
                $mail   = new PHPMailer(); 
                $mail->IsSMTP(); 
                $mail->SMTPAuth     = true; 
                $mail->SMTPSecure   = "ssl";   
                $mail->Host         = $this->mail_host;      
                $mail->Port         = 465;                   
                $mail->Username     = $this->mail_username;  
                $mail->Password     = $this->mail_password;            
                $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
                $mail->isHTML(true);
                $mail->SMTPAutoTLS  = false;
                $mail->Hostname     = $this->mail_dns;
                $mail->Subject      = "Donasi";
                $mail->Body         = 
                '<!DOCTYPE html>
                <html>
                    <head>
                         <title></title>
                    </head>
                    <body>
                        <div> 
                          <div style="width: 100%;">
                               <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
                          </div>
                          <br>
                          <br>
                          <div style="width: 100%; text-align: center;">Hi ' . $by . ',</div>
                          <div style="width: 100%; text-align: center;">Selesaikan donasi kamu melalui :</div>
                          <br>
                          <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">Transfer Bank</div>
                          <div style="width: 100%; text-align: center;">
                                <img src="' . $bank_src . '" style="height: 50px;">
                          </div>
                          <br>
                          ' . $msg_content . '
                          <br>
                          <div style="width: 100%; text-align: center; padding: 25px 0; font-weight: 600; border-radius: 15px; background: rgba(0,0,0,0.1);">
                               <div style="font-size: 20px;">' . $kasus->judul . '</div>
                               <br>
                               <div>Total Donasi</div>
                               <div>Rp. ' . number_format($donasi , 0, ',', '.') . '</div>
                          </div>
                          <br>
                          <br>
                          <div style="width: 100%; text-align: center;"><a href="'.site_url('donasi/non_medis_pay/'.$kasus->id_donasi).'" class="btn btn-warning" target="_blank" style="border-radius: 10px; background: rgb(255,197,70); padding: 10px; color: #fff; text-decoration: none; font-weight: 600;">Cek Status Donasi</a></div>
                          <br>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div>Salam,</div>
                                <div style="font-weight: 600;">Peduli Indonesia</div>
                          </div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://www.facebook.com/kaizenpeduliindonesia" target="_blank">
                                        <img src="https://imgur.com/TiFD2VP.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://www.instagram.com/kaizenpeduli.ind" target="_blank">
                                        <img src="https://imgur.com/aCbCLu1.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://web.whatsapp.com/send?phone=' . preg_replace('/0/', '62', $telepon, 1) . '" target="_blank">
                                        <img src="https://imgur.com/ClkBQ1z.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                          </div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div>Email : ' . $email . '</div>
                                <div>Whatsapp : ' . preg_replace('/0/', '+62 ', $telepon, 1) . '</div>
                          </div>
                        </div>
                    </body>
                </html>';
                $mail->AddAddress($user_email);
                $mail->Send();
                $message['success'] = 'donasi/non_medis_pay/' . $kasus->id_donasi;
                echo json_encode($message);
            }
        }else{
            $message['failed'] = true;
            echo json_encode($message);
        }
    }

    public function token(){
        $user_email = $this->input->post('user_email');
        $by = $this->input->post('user_nama');
        $user_phone = $this->input->post('user_phone');
        if (! $this->ion_auth->logged_in()){
            $id_user = '0';
        }else{
            $id_user = $this->ion_auth->user()->row()->id;
        }
        $id_kasus = $this->input->post('donasi_main_id');
        $donasi = (int)$this->input->post('hide_donasi');
        $judul_kasus = $this->input->post('judul_kasus');
        $kategori = $this->input->post('kategori');

        if($kategori == 'medis'){
            $data = $this->model->save_donasi_medis($id_kasus, $by, $user_email, $user_phone, $id_user, $donasi);
        }else{
            $data = $this->model->save_donasi_non_medis($id_kasus, $by, $user_email, $user_phone, $id_user, $donasi);
        }
        if($data){
            if($kategori == 'medis'){
                $user_donasi = $this->model->user_donasi_medis($id_user, $user_email, $id_kasus);
                $order_id = 'm_' . $user_donasi->id_donasi_medis;
            }else{
                $user_donasi = $this->model->user_donasi_non_medis($id_user, $user_email, $id_kasus);
                $order_id = 'nm_' . $user_donasi->id_donasi_non_medis;
            }
            $transaction_details = array(
                'order_id'      => $order_id,
                'gross_amount'  => $donasi, // no decimal allowed for creditcard
            );
            $customer_details = array(
                'first_name'    => $user_donasi->donatur,
                'last_name'     => "",
                'email'         => $user_donasi->email,
                'phone'         => $user_donasi->tlp
            );
            $custom_expiry = array(
                'start_time' => $user_donasi->created_at . ' ' . date('O'),
                'unit' => 'minutes', 
                'duration'  => 90
            );
            $callback = array(
                'finish'        => site_url('payment/finish')
            );
            $transaction_data = array(
                'transaction_details'=> $transaction_details,
                'customer_details'   => $customer_details,
                'expiry'             => $custom_expiry,
                'callbacks'          => $callback
            );

            error_log(json_encode($transaction_data));
            $snapToken = $this->midtrans->getSnapToken($transaction_data);
            error_log($snapToken);
            echo $snapToken;
        }else{
            $message['failed'] = true;
            echo json_encode($message);
        }
    }

    public function hapus_donasi(){
        $user_email = $this->input->post('user_email');
        $by = $this->input->post('user_nama');
        $user_phone = $this->input->post('user_phone');
        if (! $this->ion_auth->logged_in()){
            $id_user = '0';
        }else{
            $id_user = $this->ion_auth->user()->row()->id;
        }
        $id_kasus = $this->input->post('donasi_main_id');
        $kategori = $this->input->post('kategori');
        if($kategori == 'medis'){
            $user_donasi = $this->model->user_donasi_medis($id_user, $user_email, $id_kasus);
            $id_donasi = $user_donasi->id_donasi_medis;
            $delete = $this->model->hapus_donasi_medis($id_donasi);
            if($delete){
                $message['success'] = true;
                echo json_encode($message);
            }
        }else{
            $user_donasi = $this->model->user_donasi_non_medis($id_user, $user_email, $id_kasus);
            $id_donasi = $user_donasi->id_donasi_non_medis;
            $delete = $this->model->hapus_donasi_non_medis($id_donasi);
            if($delete){
                $message['success'] = true;
                echo json_encode($message);
            }
        }
    }

    public function finish(){
        $id_donasi = $this->input->post('id_donasi');
        $pdf_url = $this->input->post('pdf_url');
        $code = $this->input->post('code');
        if($code == '200'){
            $main_status = '1';
        }else{
            $main_status = '0';
        }
        $status = $this->input->post('status');
        $time = $this->input->post('time');
        $bank_number = $this->input->post('bank_number');
        $bank_code = $this->input->post('bank_code');
        if($bank_code != ''){
            $msg_content = '
            <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">Kode Perusahaan ' . $bank_code . '</div>
            <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">Kode Pembayaran ' . $bank_number . '</div>';
        }else{
            $msg_content = '<div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">Nomor Va ' . $bank_number . '</div>';
        }

        $bank = $this->input->post('bank');
        $bank_id = $this->model->bank_id($bank);
        $id_bank = $bank_id->id_bank;
        $bank_src = $bank_id->link;

        $s_id = explode('_', $id_donasi);
        if($s_id[0] == 'm'){//untuk medis
            $mt_response = $this->model->finish_mt_medis($s_id[1], $id_bank, $bank_number, $bank_code, $pdf_url, $time, $code, $status, $main_status);
            if($mt_response){
                $email = 'Email';
                $d_email = $this->model->base_contact($email);
                $email = $d_email->detail;

                $telepon = 'Telepon';
                $d_telepon = $this->model->base_contact($telepon);
                $telepon = $d_telepon->detail;

                $kasus = $this->model->kasus_medis($s_id[1]);
                $mail   = new PHPMailer(); 
                $mail->IsSMTP(); 
                $mail->SMTPAuth     = true; 
                $mail->SMTPSecure   = "ssl";   
                $mail->Host         = $this->mail_host;      
                $mail->Port         = 465;                   
                $mail->Username     = $this->mail_username;  
                $mail->Password     = $this->mail_password;            
                $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
                $mail->isHTML(true);
                $mail->SMTPAutoTLS  = false;
                $mail->Hostname     = $this->mail_dns;
                $mail->Subject      = "Donasi";
                $mail->Body         = 
                '<!DOCTYPE html>
                <html>
                    <head>
                         <title></title>
                    </head>
                    <body>
                        <div> 
                          <div style="width: 100%;">
                               <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
                          </div>
                          <br>
                          <br>
                          <div style="width: 100%; text-align: center;">Hi ' . $kasus->donatur . ',</div>
                          <div style="width: 100%; text-align: center;">Selesaikan donasi kamu melalui :</div>
                          <br>
                          <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">Virtual Account</div>
                          <div style="width: 100%; text-align: center;">
                                <img src="' . $bank_src . '" style="height: 50px;">
                          </div>
                          <br>
                          ' . $msg_content . '
                          <br>
                          <div style="width: 100%; text-align: center; padding: 25px 0; font-weight: 600; border-radius: 15px; background: rgba(0,0,0,0.1);">
                               <div style="font-size: 20px;">' . $kasus->judul . '</div>
                               <br>
                               <div>Total Donasi</div>
                               <div>Rp. ' . number_format($kasus->donasi , 0, ',', '.') . '</div>
                          </div>
                          <br>
                          <br>
                          <div style="width: 100%; text-align: center;"><a href="'.site_url('donasi/medis_pay/'.$s_id[1]).'" class="btn btn-warning" target="_blank" style="border-radius: 10px; background: rgb(255,197,70); padding: 10px; color: #fff; text-decoration: none; font-weight: 600;">Cek Status Donasi</a></div>
                          <br>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div>Salam,</div>
                                <div style="font-weight: 600;">Peduli Indonesia</div>
                          </div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://www.facebook.com/kaizenpeduliindonesia" target="_blank">
                                        <img src="https://imgur.com/TiFD2VP.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://www.instagram.com/kaizenpeduli.ind" target="_blank">
                                        <img src="https://imgur.com/aCbCLu1.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://web.whatsapp.com/send?phone=' . preg_replace('/0/', '62', $telepon, 1) . '" target="_blank">
                                        <img src="https://imgur.com/ClkBQ1z.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                          </div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div>Email : ' . $email . '</div>
                                <div>Whatsapp : ' . preg_replace('/0/', '+62 ', $telepon, 1) . '</div>
                          </div>
                        </div>
                    </body>
                </html>';
                $mail->AddAddress($kasus->email);
                $mail->Send();
                $message['success'] = 'donasi/medis_pay/' . $s_id[1];
                echo json_encode($message);
            }
        }else{
            $mt_response = $this->model->finish_mt_non_medis($s_id[1], $id_bank, $bank_number, $bank_code, $pdf_url, $time, $code, $status, $main_status);
            if($mt_response){
                $email = 'Email';
                $d_email = $this->model->base_contact($email);
                $email = $d_email->detail;

                $telepon = 'Telepon';
                $d_telepon = $this->model->base_contact($telepon);
                $telepon = $d_telepon->detail;
        
                $kasus = $this->model->kasus_non_medis($s_id[1]);
                $mail   = new PHPMailer(); 
                $mail->IsSMTP(); 
                $mail->SMTPAuth     = true; 
                $mail->SMTPSecure   = "ssl";   
                $mail->Host         = $this->mail_host;      
                $mail->Port         = 465;                   
                $mail->Username     = $this->mail_username;  
                $mail->Password     = $this->mail_password;            
                $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
                $mail->isHTML(true);
                $mail->SMTPAutoTLS  = false;
                $mail->Hostname     = $this->mail_dns;
                $mail->Subject      = "Donasi";
                $mail->Body         = 
                '<!DOCTYPE html>
                <html>
                    <head>
                         <title></title>
                    </head>
                    <body>
                        <div> 
                          <div style="width: 100%;">
                               <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
                          </div>
                          <br>
                          <br>
                          <div style="width: 100%; text-align: center;">Hi ' . $kasus->donatur . ',</div>
                          <div style="width: 100%; text-align: center;">Selesaikan donasi kamu melalui :</div>
                          <br>
                          <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">Virtual Account</div>
                          <div style="width: 100%; text-align: center;">
                                <img src="' . $bank_src . '" style="height: 50px;">
                          </div>
                          <br>
                          ' . $msg_content . '
                          <br>
                          <div style="width: 100%; text-align: center; padding: 25px 0; font-weight: 600; border-radius: 15px; background: rgba(0,0,0,0.1);">
                               <div style="font-size: 20px;">' . $kasus->judul . '</div>
                               <br>
                               <div>Total Donasi</div>
                               <div>Rp. ' . number_format($kasus->donasi , 0, ',', '.') . '</div>
                          </div>
                          <br>
                          <br>
                          <div style="width: 100%; text-align: center;"><a href="'.site_url('donasi/non_medis_pay/'.$s_id[1]).'" class="btn btn-warning" target="_blank" style="border-radius: 10px; background: rgb(255,197,70); padding: 10px; color: #fff; text-decoration: none; font-weight: 600;">Cek Status Donasi</a></div>
                          <br>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div>Salam,</div>
                                <div style="font-weight: 600;">Peduli Indonesia</div>
                          </div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://www.facebook.com/kaizenpeduliindonesia" target="_blank">
                                        <img src="https://imgur.com/TiFD2VP.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://www.instagram.com/kaizenpeduli.ind" target="_blank">
                                        <img src="https://imgur.com/aCbCLu1.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                                <div style="width: auto; padding: 0 10px; display: inline-block;">
                                    <a href="https://web.whatsapp.com/send?phone=' . preg_replace('/0/', '62', $telepon, 1) . '" target="_blank">
                                        <img src="https://imgur.com/ClkBQ1z.png" style="height: 30px; cursor: pointer;">
                                    </a>
                                </div>
                          </div>
                          <br>
                          <div style="width: 100%; text-align: center;">
                                <div>Email : ' . $email . '</div>
                                <div>Whatsapp : ' . preg_replace('/0/', '+62 ', $telepon, 1) . '</div>
                          </div>
                        </div>
                    </body>
                </html>';
                $mail->AddAddress($kasus->email);
                $mail->Send();
                $message['success'] = 'donasi/non_medis_pay/' . $s_id[1];
                echo json_encode($message);
            }
        }
    }

    public function notification(){
        $json_result = file_get_contents('php://input');
        $result = json_decode($json_result);

        $code = $result->status_code;
        if($code == '200'){
            $main_status = '1';
            $status = 'success';
        }else{
            $main_status = '0';
            $status = 'pending';
        }
        $id_donasi = $result->order_id;
        $s_id = explode('_', $id_donasi);
        if($s_id[0] == 'm'){//untuk medis
            $this->model->notification_mt_medis($s_id[1], $code, $status, $main_status);
            // $email = 'Email';
            // $d_email = $this->model->base_contact($email);
            // $email = $d_email->detail;

            // $telepon = 'Telepon';
            // $d_telepon = $this->model->base_contact($telepon);
            // $telepon = $d_telepon->detail;

            // $kasus = $this->model->kasus_medis($s_id[1]);
            // $mail   = new PHPMailer(); 
            // $mail->IsSMTP(); 
            // $mail->SMTPAuth     = true; 
            // $mail->SMTPSecure   = "ssl";   
            // $mail->Host         = $this->mail_host;      
            // $mail->Port         = 465;                   
            // $mail->Username     = $this->mail_username;  
            // $mail->Password     = $this->mail_password;            
            // $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
            // $mail->isHTML(true);
            // $mail->SMTPAutoTLS  = false;
            // $mail->Hostname     = $this->mail_dns;
            // $mail->Subject      = "Terima Kasih";
            // $mail->Body         = 
            // '<!DOCTYPE html>
            // <html>
            //     <head>
            //          <title></title>
            //     </head>
            //     <body>
            //         <div> 
            //           <div style="width: 100%;">
            //               <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
            //           </div>
            //           <br>
            //           <br>
            //           <div style="width: 100%; text-align: center;">Terima kasih ' . $kasus_nama . ',</div>
            //           <div style="width: 100%; text-align: center;">Donasi kamu sebesar :</div>
            //           <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">Rp. ' . number_format($kasus->donasi , 0, ',', '.') . '</div>
            //           <div style="width: 100%; text-align: center;">Sudah berhasil terkirim</div>
            //           <br>
            //           <div style="width: 100%; text-align: center;">Kamu dapat membantu lebih banyak dengan ikut menyebarkan link penggalangan dana</div>
            //           <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">' . $kasus->judul . '</div>
            //           <div style="width: 100%; text-align: center;">Ke media sosial atau whatsapp</div>
            //           <br>
            //           <div style="width: 100%; text-align: center;">Salam dari</div>
            //           <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">Keluarga ' . $kasus->korban . '</div>
            //           <br>
            //           <div style="width: 100%; text-align: center;">
            //                 <div style="width: auto; padding: 0 10px; display: inline-block;">
            //                     <a href="https://www.facebook.com/kaizenpeduliindonesia" target="_blank">
            //                         <img src="https://imgur.com/TiFD2VP.png" style="height: 30px; cursor: pointer;">
            //                     </a>
            //                 </div>
            //                 <div style="width: auto; padding: 0 10px; display: inline-block;">
            //                     <a href="https://www.instagram.com/kaizenpeduli.ind" target="_blank">
            //                         <img src="https://imgur.com/aCbCLu1.png" style="height: 30px; cursor: pointer;">
            //                     </a>
            //                 </div>
            //                 <div style="width: auto; padding: 0 10px; display: inline-block;">
            //                     <a href="https://web.whatsapp.com/send?phone=' . preg_replace('/0/', '62', $telepon, 1) . '" target="_blank">
            //                         <img src="https://imgur.com/ClkBQ1z.png" style="height: 30px; cursor: pointer;">
            //                     </a>
            //                 </div>
            //           </div>
            //           <br>
            //           <div style="width: 100%; text-align: center;">
            //                 <div>Email : ' . $email . '</div>
            //                 <div>Whatsapp : ' . preg_replace('/0/', '+62 ', $telepon, 1) . '</div>
            //           </div>
            //         </div>
            //     </body>
            // </html>';
            // $mail->AddAddress($kasus->username);
            // $mail->Send();
        }else{
            $this->model->notification_mt_non_medis($s_id[1], $code, $status, $main_status);
            // $email = 'Email';
            // $d_email = $this->model->base_contact($email);
            // $email = $d_email->detail;

            // $telepon = 'Telepon';
            // $d_telepon = $this->model->base_contact($telepon);
            // $telepon = $d_telepon->detail;
    
            // $kasus = $this->model->kasus_non_medis($s_id[1]);
            // $mail   = new PHPMailer(); 
            // $mail->IsSMTP(); 
            // $mail->SMTPAuth     = true; 
            // $mail->SMTPSecure   = "ssl";   
            // $mail->Host         = $this->mail_host;      
            // $mail->Port         = 465;                   
            // $mail->Username     = $this->mail_username;  
            // $mail->Password     = $this->mail_password;            
            // $mail->SetFrom($this->mail_username, 'Peduli Indonesia');
            // $mail->isHTML(true);
            // $mail->SMTPAutoTLS  = false;
            // $mail->Hostname     = $this->mail_dns;
            // $mail->Subject      = "Terima Kasih";
            // $mail->Body         = 
            // '<!DOCTYPE html>
            // <html>
            //     <head>
            //          <title></title>
            //     </head>
            //     <body>
            //         <div> 
            //           <div style="width: 100%;">
            //               <img src="https://i.ibb.co/PcydhYL/banner-kode-otp-03.png" style="width: 100%;">
            //           </div>
            //           <br>
            //           <br>
            //           <div style="width: 100%; text-align: center;">Terima kasih ' . $kasus_nama . ',</div>
            //           <div style="width: 100%; text-align: center;">Donasi kamu sebesar :</div>
            //           <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">Rp. ' . number_format($kasus->donasi , 0, ',', '.') . '</div>
            //           <div style="width: 100%; text-align: center;">Sudah berhasil terkirim</div>
            //           <br>
            //           <div style="width: 100%; text-align: center;">Kamu dapat membantu lebih banyak dengan ikut menyebarkan link penggalangan dana</div>
            //           <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">' . $kasus->judul . '</div>
            //           <div style="width: 100%; text-align: center;">Ke media sosial atau whatsapp</div>
            //           <br>
            //           <div style="width: 100%; text-align: center;">Salam dari</div>
            //           <div style="width: 100%; text-align: center; font-weight: 600; text-transform: capitalize;">' . $kasus->korban . '</div>
            //           <br>
            //           <div style="width: 100%; text-align: center;">
            //                 <div style="width: auto; padding: 0 10px; display: inline-block;">
            //                     <a href="https://www.facebook.com/kaizenpeduliindonesia" target="_blank">
            //                         <img src="https://imgur.com/TiFD2VP.png" style="height: 30px; cursor: pointer;">
            //                     </a>
            //                 </div>
            //                 <div style="width: auto; padding: 0 10px; display: inline-block;">
            //                     <a href="https://www.instagram.com/kaizenpeduli.ind" target="_blank">
            //                         <img src="https://imgur.com/aCbCLu1.png" style="height: 30px; cursor: pointer;">
            //                     </a>
            //                 </div>
            //                 <div style="width: auto; padding: 0 10px; display: inline-block;">
            //                     <a href="https://web.whatsapp.com/send?phone=' . preg_replace('/0/', '62', $telepon, 1) . '" target="_blank">
            //                         <img src="https://imgur.com/ClkBQ1z.png" style="height: 30px; cursor: pointer;">
            //                     </a>
            //                 </div>
            //           </div>
            //           <br>
            //           <div style="width: 100%; text-align: center;">
            //                 <div>Email : ' . $email . '</div>
            //                 <div>Whatsapp : ' . preg_replace('/0/', '+62 ', $telepon, 1) . '</div>
            //           </div>
            //         </div>
            //     </body>
            // </html>';
            // $mail->AddAddress($kasus->username);
            // $mail->Send();
        }
    }
}