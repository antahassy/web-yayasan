<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_program extends CI_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->helper('directory');
        date_default_timezone_set('Asia/Jakarta');
		$this->load->model('B_program_model','model');
		if (! $this->ion_auth->logged_in()){
            redirect('auth', 'refresh');
        }
	}

    public $id_menu = '4';

	public function index(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
            if (in_array('3', $akses_temp)){
                $data['akses_temp'] = $akses_temp;
                $data['title'] = 'Program';
                $this->load->view('header', $data);
                $this->load->view('index');
                $this->load->view('footer');
            }else{
                echo "Access Denied";
            }
        }else{
            echo "Access Denied";
        }
	}

	public function s_side_data(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
        }

        $month_format = array (1 => 
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        );
        $list = $this->model->s_side_data();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->title;
            if($field->image != ''){
                $row[] = '<img src="' . site_url('assets/project/program/' . $field->image) . '" class="file_data" id="' . $field->id_agenda . '" style="width: 300px; height: 150px; cursor: pointer;" title="Zoom In">';
            }else{
                $row[] = '<img src="' . site_url('assets/project/img.jpg') . '" class="file_data" id="' . $field->id_agenda . '" style="width: 300px; height: 150px; cursor: pointer;" title="Zoom In">';
            }
            $row[] = '<button id="btn_description" data="' . $field->id_agenda . '" class="btn btn-sm btn-rounded btn-outline-success" style="margin: 2.5px;">Description</button>';

            if (in_array('2', $akses_temp) && in_array('4', $akses_temp)){
                if($field->status == '0'){
                    $row[] = '<button id="btn_activated" data="' . $field->id_agenda . '" alt="' . $field->status . '" class="btn btn-sm btn-rounded btn-outline-primary" style="margin: 2.5px;">Private</button> <button data="' . $field->id_agenda . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_edit" title="Edit" style="margin: 2.5px;">Edit</button> <button data="' . $field->id_agenda . '" alt="' . $field->image . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" title="Delete" style="margin: 2.5px;">Delete</button>';
                }else{
                    $row[] = '<button id="btn_activated" data="' . $field->id_agenda . '" alt="' . $field->status . '" class="btn btn-sm btn-rounded btn-outline-primary" style="margin: 2.5px;">Public</button> <button data="' . $field->id_agenda . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_edit" title="Edit" style="margin: 2.5px;">Edit</button> <button data="' . $field->id_agenda . '" alt="' . $field->image . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" title="Delete" style="margin: 2.5px;">Delete</button>';
                }
            }
            if (! in_array('2', $akses_temp) && in_array('4', $akses_temp)){
                $row[] = '<button data="' . $field->id_agenda . '" alt="' . $field->image . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" title="Delete" style="margin: 2.5px;">Delete</button>';
            }
            if (in_array('2', $akses_temp) && ! in_array('4', $akses_temp)){
                if($field->status == '0'){
                    $row[] = '<button id="btn_activated" data="' . $field->id_agenda . '" alt="' . $field->status . '" class="btn btn-sm btn-rounded btn-outline-success" style="margin: 2.5px;">Private</button> <button data="' . $field->id_agenda . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_edit" title="Edit" style="margin: 2.5px;">Edit</button>';
                }else{
                    $row[] = '<button id="btn_activated" data="' . $field->id_agenda . '" alt="' . $field->status . '" class="btn btn-sm btn-rounded btn-outline-success" style="margin: 2.5px;">Public</button> <button data="' . $field->id_agenda . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_edit" title="Edit" style="margin: 2.5px;">Edit</button>';
                }
            }
            if (! in_array('2', $akses_temp) && ! in_array('4', $akses_temp)){
                $row[] = '';
            }

            if($field->created_at != ''){
                $s_created = explode(' ', $field->created_at);
                $s_created_date = explode('-', $s_created[0]);
                $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<br>' . $s_created[1];
            }else{
                $row[] = '';
            }

            if($field->updated_at != ''){
                $s_updated = explode(' ', $field->updated_at);
                $s_updated_date = explode('-', $s_updated[0]);
                $row[] = $s_updated_date[2] .'/'. $month_format[(int)$s_updated_date[1]] .'/'. $s_updated_date[0] . '<br>' . $s_updated[1];
            }else{
                $row[] = '';
            }
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function save(){
        $hide_program = $this->input->post('hide_program');
        // if($hide_program == ''){
        //     $message['program'] = true;
        //     echo json_encode($message);
        // }else{
            
        // }
        $by = $this->ion_auth->user()->row()->username;
        $config['upload_path']   = './assets/project/program';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('program')){
            $file   = array('image' => $this->upload->data());
            $image  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/program/'.$image;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['width']            = 1200;
            $config['height']           = 900;
            $config['new_image']        = './assets/project/program/'.$image;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $data   = $this->model->save($by, $image); 
            if($data){
                $last_image = $this->input->post('get_program');
                $delete_image = $this->input->post('delete_program');
                if($last_image != ''){
                    unlink('./assets/project/program/'.$last_image);
                    $message['success'] = true;
                    echo json_encode($message);
                }else if($delete_image != ''){
                    unlink('./assets/project/program/'.$delete_image);
                    $message['success'] = true;
                    echo json_encode($message);
                }else{
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }
        }else{
            $last_image = $this->input->post('get_program');
            $delete_image = $this->input->post('delete_program');
            if($last_image == '' && $delete_image != ''){
                unlink('./assets/project/program/'.$delete_image);
                $image  = '';
                $data   = $this->model->save($by, $image);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }else if($last_image == '' && $delete_image == ''){
                $image  = '';
                $data   = $this->model->save($by, $image);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }else{
                $image  = $last_image;
                $data   = $this->model->save($by, $image);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }
        }
    }

    public function edit(){
        $id_agenda = $this->input->post('id_agenda');
        $data = $this->model->edit($id_agenda);
        echo json_encode($data);
    }

    public function update(){
        $hide_program = $this->input->post('hide_program');
        // if($hide_program == ''){
        //     $message['program'] = true;
        //     echo json_encode($message);
        // }else{
            
        // }
        $by = $this->ion_auth->user()->row()->username;
        $id_agenda = $this->input->post('id_data');
        $config['upload_path']   = './assets/project/program';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('program')){
            $file   = array('image' => $this->upload->data());
            $image  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/program/'.$image;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['width']            = 1200;
            $config['height']           = 900;
            $config['new_image']        = './assets/project/program/'.$image;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $data   = $this->model->update($id_agenda, $by, $image); 
            if($data){
                $last_image = $this->input->post('get_program');
                $delete_image = $this->input->post('delete_program');
                if($last_image != ''){
                    unlink('./assets/project/program/'.$last_image);
                    $message['success'] = true;
                    echo json_encode($message);
                }else if($delete_image != ''){
                    unlink('./assets/project/program/'.$delete_image);
                    $message['success'] = true;
                    echo json_encode($message);
                }else{
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }
        }else{
            $last_image = $this->input->post('get_program');
            $delete_image = $this->input->post('delete_program');
            if($last_image == '' && $delete_image != ''){
                unlink('./assets/project/program/'.$delete_image);
                $image  = '';
                $data   = $this->model->update($id_agenda, $by, $image);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }else if($last_image == '' && $delete_image == ''){
                $image  = '';
                $data   = $this->model->update($id_agenda, $by, $image);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }else{
                $image  = $last_image;
                $data   = $this->model->update($id_agenda, $by, $image);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }
        }
    }

    public function delete(){
        $jumlah = $this->model->count_all();
        if($jumlah > 1){
            $id = $this->input->post('id_agenda');
            $img = $this->input->post('img_agenda');
            $by = $this->ion_auth->user()->row()->username;
            $data = $this->model->delete($id, $by); 
            if($data){
                //unlink('./assets/project/program/'.$img);
                $message['success'] = true;
                echo json_encode($message);
            }
        }else{
            $message['last'] = true;
            echo json_encode($message);
        }
    }

    public function aktifkan(){
        $id_agenda = $this->input->post('id_agenda');
        $by = $this->ion_auth->user()->row()->username;
        $data = $this->model->aktifkan($id_agenda, $by);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function non_aktifkan(){
        $id_agenda = $this->input->post('id_agenda');
        $by = $this->ion_auth->user()->row()->username;
        $data = $this->model->non_aktifkan($id_agenda, $by);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function editor_upload_img(){
        $config['upload_path']   = './assets/project/program';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['max_size']      = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('image')){
            $file   = array('image' => $this->upload->data());
            $image  = $file['image']['file_name'];
            $config['image_library']    = 'gd2';
            $config['source_image']     = './assets/project/program/'.$image;
            $config['create_thumb']     = FALSE;
            $config['maintain_ratio']   = FALSE;
            $config['quality']          = '75%';
            $config['new_image']        = './assets/project/program/'.$image;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            echo site_url('assets/project/program/'.$image);
        }
    }

    public function editor_delete_img(){
        $src = $this->input->post('src');
        $file_name = str_replace(base_url(), '', $src);
        if(unlink($file_name))
        {
            echo 'File Delete Successfully';
        }
    }
}
