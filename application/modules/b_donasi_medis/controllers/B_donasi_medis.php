<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_donasi_medis extends CI_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->helper('directory');
        date_default_timezone_set('Asia/Jakarta');
		$this->load->model('B_donasi_medis_model','model');
		if (! $this->ion_auth->logged_in()){
            redirect('auth', 'refresh');
        }
	}

    public $id_menu = '2';

	public function index(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
            if (in_array('3', $akses_temp)){
                $data['akses_temp'] = $akses_temp;
                $data['title'] = 'Donasi Medis';
                $this->load->view('header', $data);
                $this->load->view('index');
                $this->load->view('footer');
            }else{
                echo "Access Denied";
            }
        }else{
            echo "Access Denied";
        }
	}

	public function s_side_data(){
        $id_group = $this->db->get_where('users_groups', array(
            'user_id' => $this->ion_auth->user()->row()->id
        ))->row()->group_id;
        $akses = $this->db->get_where('rel_group', array(
            'id_group'  => $id_group,
            'id_menu'   => $this->id_menu
        ))->result();

        if($akses){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
        }

        $month_format = array (1 => 
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        );
        $list = $this->model->s_side_data();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = 'm_'. $field->id_donasi_medis;
            if($field->id_user == '0'){
                $row[] = '<div style="font-weight: 600;"><i style="font-size: 18px; color: #F64E60;">False</i><div>';
            }else{
                $row[] = '<div style="font-weight: 600;"><i style="font-size: 18px; color: limegreen;">True</i><div>';
            }
            if($field->email == ''){
                $row[] = $field->donatur_nama . '<br>' . $field->donatur_email . '<br>' . $field->donatur_tlp;
            }else{
                $row[] = $field->nama . '<br>' . $field->email . '<br>' . $field->phone;
            }

            if($field->status == '0'){
                if($field->mt_code != ''){
                    $row[] = 'Rp. ' . number_format($field->donasi , 0, ',', '.') . '<br><div style="font-weight: 600;"><i style="font-size: 18px; color: #F64E60;">Pending</i><div>';
                }else{
                    $row[] = 'Rp. ' . number_format($field->donasi , 0, ',', '.') . '<br><button class="btn btn-outline-danger btn_pending" data="' . $field->id_donasi_medis . '">Pending</button>';   
                }
            }elseif($field->status == '1'){
                $row[] = 'Rp. ' . number_format($field->donasi , 0, ',', '.') . '<br><div style="font-weight: 600;"><i style="font-size: 18px; color: limegreen;">Sukses</i><div>';
            }else{
                $row[] = 'Rp. ' . number_format($field->donasi , 0, ',', '.') . '<br><div style="font-weight: 600;"><i style="font-size: 18px; color: orange;">Kadaluarsa</i><div>';
            }

            if($field->metode == '1'){//virtual
                $midtrans = '';
                if($field->mt_code != ''){
                    $midtrans = 'Midtrans';
                }
                $row[] = '<div>' . $midtrans . ' Virtual Account</div><img src="' . site_url('assets/project/web/' . $field->image) . '" style="height: 25px;"><div>' . $field->va_number . '</div>';
            }else{
                $row[] = '<div>Transfer Bank</div><img src="' . site_url('assets/project/web/' . $field->image) . '" style="height: 25px;"><div>' . $field->owner . '<br>' . number_format($field->number , 0, ',', '-') . '</div>';
            }

            if($field->kategori == '1'){
                $row[] = 'Anak Sakit';
            }else{
                $row[] = 'Kesehatan';
            }

            $row[] = '<a href="' . site_url('donasi/medis/' . $field->link) . '" target="_blank" style="text-transform: capitalize; font-weight: 600;">' . $field->judul . '</a>';
            
            if($field->group_id == '1'){
                if (in_array('2', $akses_temp) && in_array('4', $akses_temp)){
                    if($field->status != '1'){
                        $row[] = '<button data="' . $field->id_donasi_medis . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_edit" title="Edit" style="margin: 2.5px;">Edit</button> <button data="' . $field->id_donasi_medis . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" title="Delete" style="margin: 2.5px;">Delete</button>';
                    }else{
                        $row[] = '';
                    }
                }
                if (! in_array('2', $akses_temp) && in_array('4', $akses_temp)){
                    if($field->status != '1'){
                        $row[] = '<button data="' . $field->id_donasi_medis . '" class="btn btn-sm btn-rounded btn-outline-danger" id="btn_delete" title="Delete" style="margin: 2.5px;">Delete</button>';
                    }else{
                        $row[] = '';
                    }
                }
                if (in_array('2', $akses_temp) && ! in_array('4', $akses_temp)){
                    if($field->status != '1'){
                        $row[] = '<button data="' . $field->id_donasi_medis . '" class="btn btn-sm btn-rounded btn-outline-warning" id="btn_edit" title="Edit" style="margin: 2.5px;">Edit</button>';
                    }else{
                        $row[] = '';
                    }
                }
                if (! in_array('2', $akses_temp) && ! in_array('4', $akses_temp)){
                    $row[] = '';
                }
            }else{
                $row[] = '';
            }
            
            if($field->created_at != ''){
                $s_created = explode(' ', $field->created_at);
                $s_created_date = explode('-', $s_created[0]);
                $row[] = $s_created_date[2] .'/'. $month_format[(int)$s_created_date[1]] .'/'. $s_created_date[0] . '<br>' . $s_created[1];
            }else{
                $row[] = '';
            }

            if($field->updated_at != ''){
                $s_updated = explode(' ', $field->updated_at);
                $s_updated_date = explode('-', $s_updated[0]);
                $row[] = $s_updated_date[2] .'/'. $month_format[(int)$s_updated_date[1]] .'/'. $s_updated_date[0] . '<br>' . $s_updated[1] . '<br>By<br>' . $field->updated_by;
            }else{
                $row[] = '';
            }
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    public function donasi_sukses(){
        $by = $this->ion_auth->user()->row()->nama;
        $id_data = $this->input->post('id_data');
        $data = $this->model->donasi_sukses($id_data, $by);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }
    
    public function list_bank(){
        $data = $this->model->list_bank();
        if($data){
            echo json_encode($data);
        }else{
            $message['empty'] = true;
            echo json_encode($message);
        }
    }

    public function autocomplete_judul_kasus(){
        if(isset($_GET['term'])){
            $result = $this->model->autocomplete_judul_kasus($_GET['term']); 
            if(count($result) > 0){
                foreach ($result as $row){
                    $all_result[] = $row->judul;
                }
                echo json_encode($all_result);
            }
        }
    }

    public function id_judul_kasus(){
        $judul = $this->input->post('judul');
        $data = $this->model->id_judul_kasus($judul);
        echo json_encode($data);
    }

    public function save(){
        $message['type'] = 'disimpan';
        $id_user = $this->ion_auth->user()->row()->id;
        $by = $this->ion_auth->user()->row()->nama;
        $data   = $this->model->save($id_user, $by);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }

    public function edit(){
        $id_donasi = $this->input->post('id');
        $data = $this->model->edit($id_donasi); 
        echo json_encode($data);
    }

    public function update(){
        $message['type'] = 'diupdate';
        $id = $this->input->post('id_data');
        $by = $this->ion_auth->user()->row()->nama;
        $data = $this->model->update($id, $by);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }
}
