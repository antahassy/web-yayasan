<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_donasi_medis_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

    // Datatable server side processing
    public function s_side_datatables_query(){
        $this->db->select('
            tb_donasi_medis.id_donasi_medis,
            tb_donasi_medis.id_user,
            tb_donasi_medis.donasi,
            tb_donasi_medis.bukti,
            tb_donasi_medis.status,
            tb_donasi_medis.metode,
            tb_donasi_medis.email as donatur_email,
            tb_donasi_medis.nama as donatur_nama,
            tb_donasi_medis.tlp as donatur_tlp,
            tb_donasi_medis.va_name,
            tb_donasi_medis.va_number,
            tb_donasi_medis.mt_code,
            tb_donasi_medis.created_by,
            tb_donasi_medis.created_at,
            tb_donasi_medis.updated_by,
            tb_donasi_medis.updated_at,
            tb_kasus_medis.judul,
            tb_kasus_medis.link,
            tb_kasus_medis.kategori,
            tb_bank.image,
            tb_bank.number,
            tb_bank.owner,
            users.email,
            users.nama,
            users.phone,
            users_groups.group_id
        ');
        $column_order = array(null, 
            'tb_donasi_medis.id_donasi_medis',
            'tb_donasi_medis.id_user',
            'tb_donasi_medis.donasi',
            'tb_donasi_medis.bukti',
            'tb_donasi_medis.status',
            'tb_donasi_medis.metode',
            'tb_donasi_medis.email',
            'tb_donasi_medis.nama',
            'tb_donasi_medis.tlp',
            'tb_donasi_medis.va_name',
            'tb_donasi_medis.va_number',
            'tb_donasi_medis.mt_code',
            'tb_donasi_medis.created_by',
            'tb_donasi_medis.created_at',
            'tb_donasi_medis.updated_by',
            'tb_donasi_medis.updated_at',
            'tb_kasus_medis.judul',
            'tb_kasus_medis.link',
            'tb_kasus_medis.kategori',
            'tb_bank.image',
            'tb_bank.number',
            'tb_bank.owner',
            'users.email',
            'users.nama',
            'users.phone',
            'users_groups.group_id'
        );
        $column_search = array(
            'tb_donasi_medis.id_donasi_medis',
            'tb_donasi_medis.id_user',
            'tb_donasi_medis.donasi',
            'tb_donasi_medis.bukti',
            'tb_donasi_medis.status',
            'tb_donasi_medis.metode',
            'tb_donasi_medis.email',
            'tb_donasi_medis.nama',
            'tb_donasi_medis.tlp',
            'tb_donasi_medis.va_name',
            'tb_donasi_medis.va_number',
            'tb_donasi_medis.mt_code',
            'tb_donasi_medis.created_by',
            'tb_donasi_medis.created_at',
            'tb_donasi_medis.updated_by',
            'tb_donasi_medis.updated_at',
            'tb_kasus_medis.judul',
            'tb_kasus_medis.link',
            'tb_kasus_medis.kategori',
            'tb_bank.image',
            'tb_bank.number',
            'tb_bank.owner',
            'users.email',
            'users.nama',
            'users.phone',
            'users_groups.group_id'
        );
        $i = 0;
        $this->db->where('tb_donasi_medis.deleted_at','');
        $this->db->from('tb_donasi_medis');
        $this->db->join('tb_kasus_medis', 'tb_donasi_medis.id_kasus_medis = tb_kasus_medis.id_kasus_medis', 'left');
        $this->db->join('users', 'tb_donasi_medis.id_user = users.id', 'left');
        $this->db->join('tb_bank', 'tb_donasi_medis.id_bank = tb_bank.id_bank', 'left');
        $this->db->join('users_groups', 'tb_donasi_medis.id_user = users_groups.user_id', 'left');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_donasi_medis.id_donasi_medis', 'desc');
        }
    }

    public function s_side_data(){
        $this->s_side_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered(){
        $this->s_side_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->where('tb_donasi_medis.deleted_at','');
        $this->db->from('tb_donasi_medis');
        return $this->db->count_all_results();
    }
    // End datatable server side processing
    public function donasi_sukses($id_data, $by){
        $data = array(
            'status' => '1',
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => $by
        );
        $this->db->where('id_donasi_medis', $id_data);
        $this->db->update('tb_donasi_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
    
    public function list_bank(){
        $this->db->select('id_bank, image, name, number, owner');
        $query = $this->db->get_where('tb_bank', array(
            'deleted_at'    => ''
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function autocomplete_judul_kasus($keyword){
        $this->db->select('judul');
        $this->db->like('judul', $keyword, 'both');
        return $this->db->get('tb_kasus_medis')->result();
    }

    public function id_judul_kasus($judul){
        $this->db->select('id_kasus_medis as id_kasus');
        $query = $this->db->get_where('tb_kasus_medis', array(
            'judul' => $judul,
            'deleted_at' => ''
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function save($id_user, $by){
        $donasi_val = str_replace('.', '', $this->input->post('donasi'));
        $metode = $this->input->post('s_metode');
        if($metode == '2'){
            $va_number = '-';
            $va_name = '-';
        }else{
            $va_number = $this->input->post('va_number');
            $va_name = $this->input->post('va_name');
        }
        $data = array(
            'id_kasus_medis'    => $this->input->post('id_kasus'),
            'id_user'           => $id_user,
            'id_bank'           => $this->input->post('s_bank'),
            'va_number'         => $va_number,
            'va_name'           => $va_name,
            'metode'            => $metode,
            'hide_name'         => '1',
            'doa'               => $this->input->post('doa'),
            'donasi'            => $donasi_val,
            'status'            => $this->input->post('s_status'),
            'created_at'        => $this->input->post('tgl_donasi') . ' ' . date('H:i:s'),
            'created_by'        => $by
        );
        $query = $this->db->insert('tb_donasi_medis', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function edit($id_donasi){
        $this->db->select('
            tb_donasi_medis.id_donasi_medis,
            tb_donasi_medis.id_kasus_medis,
            tb_donasi_medis.id_bank,
            tb_donasi_medis.va_number,
            tb_donasi_medis.va_name,
            tb_donasi_medis.metode,
            tb_donasi_medis.doa,
            tb_donasi_medis.donasi,
            tb_donasi_medis.status,
            MID(tb_donasi_medis.created_at,1,10) as tgl_donasi,
            tb_kasus_medis.judul
        ');
        $this->db->where('tb_donasi_medis.id_donasi_medis', $id_donasi);
        $this->db->from('tb_donasi_medis');
        $this->db->join('tb_kasus_medis', 'tb_donasi_medis.id_kasus_medis = tb_kasus_medis.id_kasus_medis', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function update($id, $by){
        $donasi_val = str_replace('.', '', $this->input->post('donasi'));
        $metode = $this->input->post('s_metode');
        if($metode == '2'){
            $va_number = '-';
            $va_name = '-';
        }else{
            $va_number = $this->input->post('va_number');
            $va_name = $this->input->post('va_name');
        }
        $data = array(
            'id_kasus_medis'    => $this->input->post('id_kasus'),
            'id_bank'           => $this->input->post('s_bank'),
            'va_number'         => $va_number,
            'va_name'           => $va_name,
            'metode'            => $metode,
            'doa'               => $this->input->post('doa'),
            'donasi'            => $donasi_val,
            'status'            => $this->input->post('s_status'),
            'created_at'        => $this->input->post('tgl_donasi') . ' ' . date('H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => $by
        );
        $this->db->where('id_donasi_medis', $id);
        $this->db->update('tb_donasi_medis', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
}
