<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct(){
        parent::__construct(); 
        $this->db = $this->load->database('default', TRUE);
        date_default_timezone_set('Asia/Jakarta');
    }
 
    // Datatable server side processing
    public function s_side_datatables_query(){
        $this->db->select('
            users.username,
            users.nama,
            users.image,
            users.created_at, 
            users.created_by,
            users.updated_at, 
            users.updated_by,
            users.active,
            users.id,
            groups.description,
            groups.deleted_at as groups_deleted,
        ');
        $column_order = array(null, 
            'users.username',
            'users.nama',
            'users.image',
            'users.created_at',
            'users.created_by',
            'users.updated_at',
            'users.updated_by',
            'users.active',
            'users.id',
            'groups.description'
        );
        $column_search = array(
            'users.username',
            'users.nama',
            'users.image',
            'users.created_at',
            'users.created_by',
            'users.updated_at',
            'users.updated_by',
            'users.active',
            'users.id',
            'groups.description'
        ); 
        $this->db->where('users.deleted_at','');
        $this->db->from('users_groups');
        $this->db->join('users', 'users_groups.user_id = users.id', 'left');
        $this->db->join('groups', 'users_groups.group_id = groups.id', 'left');
        $i = 0;
        foreach ($column_search as $item){
            if($_POST['search']['value']  != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('users_groups.user_id', 'desc');
        }
    }
 
    public function s_side_data(){
        $this->s_side_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered(){
        $this->s_side_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->where('users.deleted_at','');
        $this->db->from('users_groups');
        $this->db->join('users', 'users_groups.user_id = users.id', 'left');
        return $this->db->count_all_results();
    }
    // End datatable server side processing

    public function check_username($username){
        $query = $this->db->get_where('users', array('username' => $username));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function identity_id($identity){
        $this->db->select('id');
        $query = $this->db->get_where('users', array(
            'username'  => $identity
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function save($image, $by, $username){
        $data = array(
            'username'      => $username,
            'nama'          => $this->input->post('nama'),
            'phone'         => $this->input->post('telepon'),
            'email'         => $this->input->post('email'),
            'image'         => $image,
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => $by,
            'active'        => 1
        );
        $query = $this->db->insert('users', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function save_user_grup($id_user, $s_level){
        $data = array(
            'user_id'                   => $id_user,
            'group_id'                  => $s_level
        );
        $query = $this->db->insert('users_groups', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function edit($id){
        $this->db->select('
            id,
            username,
            email,
            phone,
            nama,
            image
        ');
        $this->db->where('id', $id);
        $query = $this->db->get('users');
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function user_grup($id_user){
        $this->db->select('group_id');
        $query = $this->db->get_where('users_groups', array(
            'user_id'  => $id_user
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function update($id, $image, $by){
        $data = array(
            'nama'          => $this->input->post('nama'),
            'phone'         => $this->input->post('telepon'),
            'email'         => $this->input->post('email'),
            'image'         => $image,
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by,
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function update_user_grup($id_user, $s_level){
        $data = array('group_id' => $s_level);
        $this->db->where('user_id', $id_user);
        $this->db->update('users_groups', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function aktifkan($id){
        $data = array('active' => 1);
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function non_aktifkan($id){
        $data = array('active' => 0);
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function level_grup(){
    	$this->db->select('id, description');
    	$query = $this->db->get_where('groups', array(
            'deleted_at'  => ''
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }
} 