<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_kegiatan_model extends CI_Model {

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    // Datatable server side processing
    public function s_side_datatables_query(){
        $this->db->select('
            tb_agenda.id_agenda,
            tb_agenda.title,
            tb_agenda.image,
            tb_agenda.description,
            tb_agenda.status,
            tb_agenda.created_by,
            tb_agenda.created_at,
            tb_agenda.updated_by,
            tb_agenda.updated_at
        ');
        $column_order = array(null, 
            'tb_agenda.id_agenda',
            'tb_agenda.title',
            'tb_agenda.image',
            'tb_agenda.description',
            'tb_agenda.status',
            'tb_agenda.created_by',
            'tb_agenda.created_at',
            'tb_agenda.updated_by',
            'tb_agenda.updated_at'
        );
        $column_search = array(
            'tb_agenda.id_agenda',
            'tb_agenda.title',
            'tb_agenda.image',
            'tb_agenda.description',
            'tb_agenda.status',
            'tb_agenda.created_by',
            'tb_agenda.created_at',
            'tb_agenda.updated_by',
            'tb_agenda.updated_at'
        );
        $i = 0;
        $this->db->where('tb_agenda.deleted_at','');
        $this->db->where('tb_agenda.type','1');
        $this->db->from('tb_agenda');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_agenda.id_agenda', 'desc');
        }
    }

    public function s_side_data(){
        $this->s_side_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered(){
        $this->s_side_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->where('tb_agenda.deleted_at','');
        $this->db->where('tb_agenda.type','1');
        $this->db->from('tb_agenda');
        return $this->db->count_all_results();
    }
    // End datatable server side processing

    public function save($by, $image){
        $data = array(
            'title'         => $this->input->post('title'),
            'image'         => $image,
            'type'          => '1',
            'description'   => $this->input->post('description'),
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => $by
        );
        $query = $this->db->insert('tb_agenda', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function edit($id_agenda){
        $this->db->select('id_agenda, title, image, description');
        $query = $this->db->get_where('tb_agenda', array(
            'id_agenda' => $id_agenda
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function update($id_agenda, $by, $image){
        $data           = array(
            'title'         => $this->input->post('title'),
            'image'         => $image,
            'type'          => '1',
            'description'   => $this->input->post('description'),
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by
        );
        $this->db->where('id_agenda', $id_agenda);
        $this->db->update('tb_agenda', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function delete($id, $by){
        $data = array(
            'deleted_at'    => date('Y-m-d H:i:s'),
            'deleted_by'    => $by
        );
        $this->db->where('id_agenda', $id);
        $this->db->update('tb_agenda', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function aktifkan($id_agenda, $by){
        $data           = array(
            'status'        => '1',
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by
        );
        $this->db->where('id_agenda', $id_agenda);
        $this->db->update('tb_agenda', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }

    public function non_aktifkan($id_agenda, $by){
        $data           = array(
            'status'        => '0',
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => $by
        );
        $this->db->where('id_agenda', $id_agenda);
        $this->db->update('tb_agenda', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
}
