<?php
$setting = $this->Ref_model->ref_setting()->result_array();
$data['judul_navbar'] = $setting[3]['value'];
$data['judul_web'] = $setting[0]['value'];
$data['deskripsi'] = $setting[1]['value'];
$data['logo'] = $setting[2]['value'];
?> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $data['judul_web']; ?> - <?php echo $title; ?></title>
        <meta charset="utf-8">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no"> -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php echo $data['judul_web'] . '. ' . $data['deskripsi'] ?>">
        <meta name="keywords" content="Yayasan,Kaizen,Donasi,Galang,Dana,Kegiatan,Program,Kesehatan,Sosial,Bencana,Bantuan">
        <meta name="googlebot-news" content="index,follow">
        <meta name="googlebot" content="index,follow">
        <meta name="author" content="Antahassy Wibawa">
        <meta name="robots" content="index,follow">
        <meta name="language" content="id">
        <meta name="Classification" content="Level">
        <meta name="geo.country" content="Indonesia">
        <meta name="geo.placename" content="Indonesia"> 
        <meta name="geo.position" content="-6.5899176; 106.8230479">
        <meta http-equiv="content-language" content="In-Id">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Cache-Control" content="no-cache">
        <meta http-equiv="Copyright" content="<?php echo $data['judul_web']?>">
        <meta property="og:title" content="<?php echo $data['judul_web']?>">
        <meta property="og:url" content="<?php echo site_url()?>">
        <meta property="og:type" content="Donasi">
        <meta property="og:site_name" content="<?php echo $data['judul_web']?>">
        <meta itemprop="name" content="<?php echo $data['judul_web']?>">

        <link rel="shortcut icon" href="<?php echo site_url('assets/project/' . $data['logo'] . '?t=').mt_rand()?>" />

        <link href="https://fonts.cdnfonts.com/css/geliat" rel="stylesheet">
        <link href="https://fonts.cdnfonts.com/css/apercu" rel="stylesheet">
                
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/antahassy.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/project/css/style.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/bootstrap.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/fontawesome5/css/all.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/animation.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/dataTables.bootstrap4.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/buttons.bootstrap4.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/dataTables.buttons.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/jquery-ui.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/sweetalert2.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/treeflex.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/css/toogle.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/antahassy/summernote/summernote-lite.min.css?t=').mt_rand()?>">

        <script src="<?php echo site_url('assets/antahassy/js/jquery-3.3.1.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/bootstrap.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/fontawesome5/js/all.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/jquery.dataTables.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/dataTables.bootstrap4.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/dataTables.buttons.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.print.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.html5.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.flash.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/buttons.colVis.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/be_tables_datatables.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/zip.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/pdfmake.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/vfs_fonts.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/jquery-ui.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/chart.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/chart.plugin.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/sweetalert2.all.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/treeify.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/js/toogle.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/summernote/summernote-lite.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/antahassy/summernote/summernote-img-attribute.js?t=').mt_rand()?>"></script>
        
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-ZX8D6WX3E0"></script>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-ZX8D6WX3E0');
        </script>
    </head>
    <style type="text/css">
        label{
            margin-top: 5px;
            font-weight: 500;
        }
        pre{
            font-family: verdana !important; 
            font-size: 14px !important;
            color: #fff;
            background-color: transparent;
            border-color: transparent;
        }
        .web_search::placeholder{
            color: #fff !important;
        }
        .web_search{
            box-shadow: none !important;
            background-color: transparent !important;
            border-color: transparent !important;
            color: #fff !important;
            padding-left: 0 !important;
            text-transform: uppercase !important;
            font-family: 'Geliat' !important;
            font-style: normal !important;
            font-weight: 200 !important;
            src: local('Geliat'), url('https://fonts.cdnfonts.com/s/65524/Geliat-ExtraLight.woff') format('woff');
        }
        #main_content{
            padding-top: 50px;
        }
        .ul_menu .list-inline-item{
            padding: 5px 0;
        }
        .swal2-popup .swal2-styled.swal2-confirm{
             background-color: rgb(255,197,70) !important;
        }
        p{
            text-indent: 25px !important;
        }
    </style>
    <script>
        var site = "<?php echo site_url('')?>";
        var ip_address = '';
        $.get('https://jsonip.com', function(address){
            $('#ip_address').val(address.ip);
        });
    </script>
    <body>
        <header>
            <div id="top_header" class="row">
                <div class="col-md-2" id="left_logo">
                    <a href="<?php echo site_url('')?>">
                        <img src="<?php echo site_url('assets/project/main_logo.png?t=').mt_rand()?>" alt="Text Logo" />
                    </a>
                    <div id="mobile_logo">
                        <i class="fas fa-bars" style="font-size: 30px;"></i>
                    </div>
                </div>
                <?php if(current_url() == site_url('map')){ ?>
                    <div class="col-md-12" id="center_logo" style="display: none;">
                <?php }else{ ?>
                    <div class="col-md-12" id="center_logo">
                <?php } ?>
                    <a href="<?php echo site_url('')?>">
                        <img src="<?php echo site_url('assets/project/logo.png?t=').mt_rand()?>" alt="Text Logo"/>
                    </a>
                    <div id="slide_show" class="carousel slide carousel-fade" data-ride="carousel">
                        <div class="carousel-inner" role="listbox" id="list_slideshow">
                            <?php
                                $total_slide = count($slideshow);
                                $i = 0;
                                foreach($slideshow as $row){
                                    $i ++;
                                    if($i == $total_slide){
                                        echo '<div class="carousel-item active">';
                                    }else{
                                        echo '<div class="carousel-item">';
                                    }

                                        echo '<div class="d-block image_content" style="background-image: url(' . site_url() . 'assets/project/slideshow/' . $row->image . '); background-position: center; background-size: 100vw; background-repeat: no-repeat"></div>';
                                    echo '</div>';
                                }
                            ?>
                        </div>
                        <!--<div id="slider_control">-->
                        <!--    <a class="carousel-control-prev" href="#slide_show" role="button" data-slide="prev">-->
                        <!--        <span class="carousel-control-prev-icon" aria-hidden="true"></span>-->
                        <!--        <span class="sr-only">Previous</span>-->
                        <!--    </a>-->
                        <!--    <a class="carousel-control-next" href="#slide_show" role="button" data-slide="next">-->
                        <!--        <span class="carousel-control-next-icon" aria-hidden="true"></span>-->
                        <!--        <span class="sr-only">Next</span>-->
                        <!--    </a>-->
                        <!--</div>-->
                    </div>
                </div>
            </div>
            <div id="bottom_header" class="row">
                <div class="col-md-10 text-center" style="padding: 0;">
                    <ul class="list-inline ul_menu">
                        <li class="list-inline-item">
                            <a href="<?php echo site_url('')?>">Beranda</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="<?php echo site_url('home/jenis_donasi')?>">Donasi</a>
                        </li>
                        <li class="list-inline-item">
                            <?php
                                if(! $this->ion_auth->logged_in()){
                                    echo '<a class="must_logged_in">Galang Dana</a>';
                                }else{
                                    echo '<a href="' . site_url('home/jenis_galang_dana') . '">Galang Dana</a>';
                                }
                            ?>
                        </li>
                        <li class="list-inline-item ul_menu_agenda">
                            <a>Agenda</a>
                            <ul class="ul_menu_dropdown agenda_dropdown">
                                <li><a href="<?php echo site_url('home/kegiatan')?>">Kegiatan</a></li>
                                <li><a href="<?php echo site_url('home/program')?>">Program</a></li>
                            </ul>
                        </li>
                        <li class="list-inline-item">
                            <a href="<?php echo site_url('home/tentang')?>">Tentang Kami</a>
                        </li>
                        <li class="list-inline-item ul_menu_akun">
                            <?php
                                if (! $this->ion_auth->logged_in()){
                                    echo '<a class="user_login">Login</a>';
                                }else{
                                    $nick = explode(' ', $this->ion_auth->user()->row()->nama);
                                    echo '<a>Hai, ' . $nick[0] . '!</a>';
                                    echo '<ul class="ul_menu_dropdown akun_dropdown">';
                                        echo '<li><a href="' . site_url('home/profil') . '">Profil</a></li>';
                                        echo '<li><a href="' . site_url('home/riwayat_donasi') . '">Riwayat Donasi</a></li>';
                                        echo '<li><a href="' . site_url('home/riwayat_galang_dana') . '">Riwayat Galang Dana</a></li>';
                                        echo '<li><a class="user_logout">Logout</a></li>';
                                    echo '</ul>';
                                }
                            ?>
                        </li>
                    </ul>
                </div>
                <div class="col-md-2" style="padding: 0;">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text" style="background-color: transparent; border-color: transparent; color: #fff; font-size: 1.5rem; padding: .375rem .25rem;"><i class="fas fa-search"></i></div>
                        </div>
                        <input type="text" class="form-control web_search" placeholder="Cari">
                    </div>
                    <!-- <input type="text" class="form-control web_search" placeholder="Cari..." style="background-color: transparent; border-color: transparent; color: #fff;"> -->
                </div>
            </div>
            <div id="mobile_menu" class="row">
                <div class="col-md-12" style="padding: 0;">
                    <ul class="list-group ul_menu">
                        <li class="list-group-item">
                            <a href="<?php echo site_url('')?>">Beranda</a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?php echo site_url('home/jenis_donasi')?>">Donasi</a>
                        </li>
                        <li class="list-group-item">
                            <?php
                                if(! $this->ion_auth->logged_in()){
                                    echo '<a class="must_logged_in">Galang Dana</a>';
                                }else{
                                    echo '<a href="' . site_url('home/jenis_galang_dana') . '">Galang Dana</a>';
                                }
                            ?>
                        </li>
                        <li class="list-group-item ul_menu_agenda">
                            <a>Agenda</a>
                            <ul class="ul_menu_dropdown agenda_dropdown">
                                <li><a href="<?php echo site_url('home/kegiatan')?>">Kegiatan</a></li>
                                <li><a href="<?php echo site_url('home/program')?>">Program</a></li>
                            </ul>
                        </li>
                        <li class="list-group-item">
                            <a href="<?php echo site_url('home/tentang')?>">Tentang Kami</a>
                        </li>
                        <li class="list-group-item ul_menu_akun">
                            <?php
                                if (! $this->ion_auth->logged_in()){
                                    echo '<a class="user_login">Login</a>';
                                }else{
                                    $nick = explode(' ', $this->ion_auth->user()->row()->nama);
                                    echo '<a>Hai, ' . $nick[0] . '!</a>';
                                    echo '<ul class="ul_menu_dropdown akun_dropdown">';
                                        echo '<li><a href="' . site_url('home/profil') . '">Profil</a></li>';
                                        echo '<li><a href="' . site_url('home/riwayat_donasi') . '">Riwayat Donasi</a></li>';
                                        echo '<li><a href="' . site_url('home/riwayat_galang_dana') . '">Riwayat Galang Dana</a></li>';
                                        echo '<li><a class="user_logout">Logout</a>';
                                    echo '</ul>';
                                }
                            ?>
                        </li>
                        <li class="list-group-item" style="padding: 0 1.25rem;">
                            <input type="text" class="form-control web_search shadow-none" placeholder="Cari" id="mobile_web_search">
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <div class="modal animated" id="modal_login" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Login</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="form_login">
                            <input type="hidden" name="ip_address" id="ip_address">
                            <div class="row">
                                <div class="col-md-12" style="margin: 5px 0;">
                                    <input type="text" name="login_email" id="login_email" class="form-control" placeholder="Email" style="background-color: rgb(255,239,212); border-color: rgb(255,239,212);">
                                </div>
                                <div class="col-md-12" style="margin: 5px 0;">
                                    <input type="password" name="login_password" id="login_password" class="form-control" placeholder="Password" style="background-color: rgb(255,239,212); border-color: rgb(255,239,212);">
                                </div>
                                <div class="col-md-12">
                                    <center>
                                        <div class="g-recaptcha" style="margin: 15px 0; transform-origin:0 0;" data-sitekey="6Ld3FLodAAAAAIGj5kwXPmEtKgsbgM1Cz7jvJsbg"></div>
                                    </center>
                                </div>
                                <div class="col-md-12" style="margin: 5px 0;">
                                    <div>Lupa password ? Bantuan <b style="color: #dc3545; cursor: pointer;" id="email_link">Disini</b></div>
                                    <div>Belum punya akun ? Daftar <b style="color: #007bff; cursor: pointer;" class="user_register">Disini</b></div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Tutup</button>
                        <button type="button" class="btn btn-warning" id="btn_login">Login</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal animated" id="modal_register" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" style="font-weight: 400;">DAFTAR</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12" style="margin: 5px 0;">
                                <input type="text" name="register_name" id="register_name" class="form-control" placeholder="Nama Sesuai KTP" style="background-color: rgb(255,239,212); border-color: rgb(255,239,212);">
                            </div>
                            <div class="col-md-12" style="margin: 5px 0;">
                                <input type="text" name="register_email" id="register_email" class="form-control" placeholder="Email" style="background-color: rgb(255,239,212); border-color: rgb(255,239,212);">
                            </div>
                            <div class="col-md-12" style="margin: 5px 0;">
                                <input type="text" name="register_phone" id="register_phone" class="form-control" placeholder="No. Handphone" style="background-color: rgb(255,239,212); border-color: rgb(255,239,212);">
                            </div>
                            <div class="col-md-12" style="margin: 5px 0;">
                                <input type="password" name="register_password" id="register_password" class="form-control" placeholder="Password" style="background-color: rgb(255,239,212); border-color: rgb(255,239,212);">
                            </div>
                            <div class="col-md-12" style="margin: 5px 0;">
                                <input type="password" name="register_password_confirm" id="register_password_confirm" class="form-control" placeholder="Konfirmasi Password" style="background-color: rgb(255,239,212); border-color: rgb(255,239,212);">
                            </div>
                            <div class="col-md-12" style="margin: 5px 0; font-size: 14px;">
                                <div style="font-family: 'Apercu' !important; font-style: italic !important; font-weight: 300 !important; src: local('Apercu'), url('https://fonts.cdnfonts.com/s/22258/Apercu Light Italic.woff') format('woff'); color: rgba(0,0,0,0.3) !important;"><i>*Dengan mendaftarkan akun, kamu bisa mulai melakukan donasi, penggalangan donasi dan melihat riwayat donasi kamu</i></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Tutup</button>
                        <button type="button" class="btn btn-warning" id="btn_register">Daftar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal animated" id="modal_search_email" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Lupa Password</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12" style="margin: 5px 0;">
                                <input type="text" name="search_email" id="search_email" class="form-control" placeholder="Email Anda" style="background-color: rgb(255,239,212); border-color: rgb(255,239,212);">
                            </div>
                            <div class="col-md-12" style="margin: 5px 0; text-align: center; display: none;" id="password_reset_text">
                                Reset password <b style="color: blue; cursor: pointer;" id="password_reset">disini</b>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Tutup</button>
                        <button type="button" class="btn btn-warning" id="btn_search_email">Cari</button>
                    </div>
                </div>
           </div>
        </div>