        <style type="text/css">
            .foot_icon:hover{
                color: #fff !important;
            }
            #subscribe::placeholder{
                color: rgba(0,0,0,0.75) !important;
            }
        </style>
        <?php if(current_url() == site_url('map')){ ?>
            <div style="display: none;">
        <?php }else{ ?>
            <div>
        <?php } ?>
            <div class="row" style="background-color: rgb(255,239,212); padding: 25px 0;">
                <div class="col-md-3 foot_col">
                    <ul class="list-group" id="ul_foot_left">
                        <li class="list-group-item">
                            <a href="<?php echo site_url('home/tentang')?>">Kontak</a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?php echo site_url('home/tentang')?>">Tentang Kami</a>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td style="font-size: 25px; color: rgb(255,197,70);">
                                        <a href="#" style="color: rgb(255, 197, 70);" class="foot_icon facebook_sosmed"><i class="fab fa-facebook-f"></i></a>
                                    </td>
                                    <td style="font-size: 30px; padding-left: 25px; color: rgb(255,197,70);">
                                        <a href="#" style="color: rgb(255, 197, 70);" class="foot_icon instagram_sosmed"><i class="fab fa-instagram"></i></a>
                                    </td>
                                    <td style="font-size: 30px; padding-left: 25px; color: rgb(255,197,70);">
                                        <a href="#" style="color: rgb(255, 197, 70);" class="foot_icon whatsapp_sosmed"><i class="fab fa-whatsapp"></i></a>
                                    </td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 foot_col foot_border">
                    <ul class="list-group" id="ul_foot_center">
                        <li class="list-group-item">
                            <a href="<?php echo site_url('home/syarat_ketentuan')?>">Syarat Dan Ketentuan</a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?php echo site_url('home/kebijakan_privasi')?>">Kebijakan Privasi</a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?php echo site_url('home/seputar_pertanyaan')?>">Seputar Pertanyaan</a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?php echo site_url('home/csr')?>">Tanggung Jawab Sosial Perusahaan (CSR)</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 foot_col foot_border" style="padding: 15px 50px !important;">
                    <label style="font-weight: 400;">Daftarkan Email Untuk Mendapatkan Update Terbaru</label>
                    <input type="text" name="subscribe" id="subscribe" class="form-control" placeholder="Email Anda" style="background-color: #fff !important; border-color: #fff !important; color: #212529 !important;">
                </div>
            </div>
            <footer>
                <p style="margin: 0;">
                    <img alt="Logo" src="<?php echo site_url('assets/project/logo1.png?t=').mt_rand()?>" style="border-radius: 100%;"/> Copyright <script>document.write(new Date().getFullYear());</script> by Peduli Indonesia
                </p>
            </footer>
        </div>
        <script type="text/javascript" src="<?php echo site_url('assets/project/js/web_footer.js?t=').mt_rand()?>"></script>
        <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/618f3e796885f60a50bb97df/1fkboo37o';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
        </script>
    </body>
</html>