-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2021 at 09:15 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_yayasan`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'Administrator', 'System', 'administrator', '', '2021-03-23 04:22:20', '2021-10-04 15:26:58', ''),
(2, 'user', 'User', 'administrator', 'administrator', '', '2021-09-13 14:43:17', '2021-09-25 12:00:12', '');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(150) NOT NULL,
  `url` varchar(500) DEFAULT NULL,
  `rel` int(11) NOT NULL,
  `rel2` int(11) NOT NULL,
  `is_trash` int(11) NOT NULL,
  `urutan` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `nama_menu`, `url`, `rel`, `rel2`, `is_trash`, `urutan`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_by`) VALUES
(1, 'Dashboard', 'dashboard', 0, 0, 0, 1, 'System', 'administrator', '2021-04-01 12:38:31', '2021-05-10 10:01:49', ''),
(2, 'Donasi', 'b_donasi', 13, 0, 0, 2, 'administrator', 'administrator', '2021-10-04 15:29:04', '2021-10-05 11:26:14', ''),
(3, 'Kegiatan', 'b_kegiatan', 15, 0, 0, 1, 'administrator', 'administrator', '2021-10-04 15:29:46', '2021-10-05 11:27:59', ''),
(4, 'Program', 'b_program', 15, 0, 0, 2, 'administrator', 'administrator', '2021-10-04 15:30:24', '2021-10-05 11:28:07', ''),
(5, 'Konten', 'master', 0, 0, 0, 5, 'administrator', 'administrator', '2021-10-04 15:31:54', '2021-10-05 11:26:52', ''),
(6, 'Slideshow', 'f_slideshow', 5, 0, 0, 1, 'administrator', 'administrator', '2021-10-04 15:32:12', '2021-10-04 15:43:32', ''),
(7, 'Kontak', 'f_kontak', 5, 0, 0, 2, 'administrator', 'administrator', '2021-10-04 15:40:53', '2021-10-04 15:43:28', ''),
(8, 'Aturan', 'master', 0, 0, 0, 6, 'administrator', 'administrator', '2021-10-04 15:43:06', '2021-10-05 11:26:59', ''),
(9, 'Syarat Ketentuan', 'b_syarat_ketentuan', 8, 0, 0, 1, 'administrator', 'administrator', '2021-10-04 15:44:05', '2021-10-04 15:45:01', ''),
(10, 'Kebijakan Privasi', 'b_kebijakan_privasi', 8, 0, 0, 2, 'administrator', 'administrator', '2021-10-04 15:44:30', '2021-10-04 15:45:06', ''),
(11, 'Seputar Pertanyaan', 'b_seputar_pertanyaan', 8, 0, 0, 3, 'administrator', 'administrator', '2021-10-04 15:44:55', '2021-10-04 15:45:11', ''),
(12, 'Bantuan', 'f_bantuan', 5, 0, 0, 3, 'administrator', 'administrator', '2021-10-05 11:21:12', '2021-10-06 14:42:55', ''),
(13, 'Finansial', 'master', 0, 0, 0, 2, 'administrator', 'administrator', '2021-10-05 11:25:39', '2021-10-06 14:43:08', ''),
(14, 'Bank', 'b_bank', 13, 0, 0, 1, 'administrator', 'administrator', '2021-10-05 11:26:04', '2021-10-06 14:43:13', ''),
(15, 'Agenda', 'master', 0, 0, 0, 4, 'administrator', 'administrator', '2021-10-05 11:27:46', '2021-10-06 15:05:11', ''),
(16, 'Kasus', 'master', 0, 0, 0, 3, 'administrator', '', '2021-10-06 15:05:37', '', ''),
(17, 'Kesehatan', 'k_kesehatan', 16, 0, 0, 1, 'administrator', '', '2021-10-06 15:08:07', '', ''),
(18, 'Sosial', 'k_sosial', 16, 0, 0, 2, 'administrator', '', '2021-10-06 15:08:40', '', ''),
(19, 'Bencana', 'k_bencana', 16, 0, 0, 3, 'administrator', '', '2021-10-06 15:08:52', '', ''),
(20, 'Tentang Kami', 'f_tentang', 5, 0, 0, 4, 'administrator', 'administrator', '2021-10-07 14:37:56', '2021-10-07 14:38:11', '');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `message` longtext NOT NULL,
  `respond` longtext NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `name`, `email`, `phone`, `message`, `respond`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'antahassy', 'antahassyw@gmail.com', '087770067003', 'tes pesan', '<p>iya saya sedang sehat</p>\r\n', 'antahassy', 'administrator', '', '2021-10-07 14:31:08', '2021-10-07 14:34:28', ''),
(2, 'anta', 'anta@gmail.com', '0811111', 'tes pesan, saya mebutuhkan bla bla bla', '<p>iya ada ap</p>\r\n', 'anta', 'administrator', '', '2021-10-08 10:17:34', '2021-10-08 10:21:17', '');

-- --------------------------------------------------------

--
-- Table structure for table `rel_group`
--

CREATE TABLE `rel_group` (
  `id_group` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `akses` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `rel_group`
--

INSERT INTO `rel_group` (`id_group`, `id_menu`, `akses`) VALUES
(1, 6, 1),
(1, 11, 2),
(1, 10, 2),
(1, 4, 1),
(1, 9, 2),
(1, 11, 3),
(1, 10, 3),
(1, 9, 3),
(1, 8, 3),
(1, 3, 1),
(1, 20, 2),
(1, 19, 1),
(1, 18, 1),
(1, 6, 4),
(1, 4, 4),
(1, 3, 4),
(1, 19, 4),
(1, 18, 4),
(1, 20, 3),
(1, 12, 3),
(1, 7, 3),
(1, 6, 3),
(1, 5, 3),
(1, 4, 3),
(1, 3, 3),
(1, 15, 3),
(1, 19, 3),
(1, 18, 3),
(1, 12, 2),
(1, 7, 2),
(1, 6, 2),
(1, 4, 2),
(1, 3, 2),
(1, 19, 2),
(1, 18, 2),
(1, 17, 3),
(1, 17, 2),
(1, 17, 1),
(1, 2, 1),
(1, 14, 1),
(1, 16, 3),
(1, 2, 3),
(1, 14, 3),
(1, 13, 3),
(1, 17, 4),
(1, 2, 4),
(1, 14, 4),
(1, 2, 2),
(1, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id_setting` int(11) NOT NULL,
  `meta_id` varchar(50) DEFAULT NULL,
  `value` varchar(350) DEFAULT NULL,
  `updated_by` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id_setting`, `meta_id`, `value`, `updated_by`, `updated_at`) VALUES
(1, 'judul_web', 'Yayasan Kaizen', 'administrator', '2021-10-07 14:01:09'),
(2, 'deskripsi', 'Yayasan Kaizen', 'administrator', '2021-10-07 14:01:09'),
(3, 'logo', 'icon.png', 'administrator', '2021-10-07 14:01:09'),
(4, 'judul_navbar', 'Yayasan Kaizen', 'administrator', '2021-10-07 14:01:09');

-- --------------------------------------------------------

--
-- Table structure for table `tb_agenda`
--

CREATE TABLE `tb_agenda` (
  `id_agenda` bigint(20) NOT NULL,
  `type` enum('0','1') NOT NULL COMMENT '0=program\r\n1=kegiatan',
  `title` varchar(50) NOT NULL,
  `image` text NOT NULL,
  `description` longtext NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_agenda`
--

INSERT INTO `tb_agenda` (`id_agenda`, `type`, `title`, `image`, `description`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 'Test Kegiatan', '4.jpg', '<p>Asco global has been established from the year 2008. Asco global is one of the leading brokerage house in the world, introducing the finest trading platform in the world. Asco global has grown to become a leading Canadian company and international broker of financial industries and property and other services.</p>\r\n\r\n<p>Whether your organization operates around the globe or locally, we are your connection to the international finance world. Our licensed professionals take a pro-active consultative approach to understanding the risks in your business, resolving problems and recommending insurance that covers your operations from all angles.</p>\r\n', '1', 'administrator', 'administrator', '', '2021-10-06 15:01:45', '2021-10-08 10:37:50', ''),
(2, '0', 'Test Program', '5.jpg', '<p>Test Program</p>\r\n', '1', 'administrator', 'administrator', '', '2021-10-06 15:02:01', '2021-10-06 15:10:08', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_bank`
--

CREATE TABLE `tb_bank` (
  `id_bank` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `number` varchar(100) NOT NULL,
  `owner` varchar(100) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_bank`
--

INSERT INTO `tb_bank` (`id_bank`, `name`, `number`, `owner`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BCA', '111111111111', 'Test Owner', 'administrator', 'administrator', '', '2021-10-05 13:21:58', '2021-10-05 14:10:38', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_bantuan`
--

CREATE TABLE `tb_bantuan` (
  `id_bantuan` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `color` varchar(50) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_bantuan`
--

INSERT INTO `tb_bantuan` (`id_bantuan`, `name`, `icon`, `color`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Kesehatan', 'fas fa-medkit', 'rgba(246, 36, 89, 1)', 'administrator', 'administrator', '', '2021-10-05 11:46:21', '2021-10-08 11:20:05', ''),
(2, 'Sosial', 'fas fa-handshake', 'blue', 'administrator', 'administrator', '', '2021-10-05 11:47:24', '2021-10-08 11:15:55', ''),
(3, 'Bencana', 'fas fa-house-damage', 'darkorange', 'administrator', 'administrator', '', '2021-10-05 11:48:37', '2021-10-05 12:00:01', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_contact`
--

CREATE TABLE `tb_contact` (
  `id_contact` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `detail` varchar(100) NOT NULL,
  `icon` varchar(250) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_contact`
--

INSERT INTO `tb_contact` (`id_contact`, `name`, `detail`, `icon`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Alamat', 'Summarecon Serpong\r\nGading Serpong\r\nRuko Jasmine No 30', 'fas fa-map-marker-alt', 'administrator', '', '', '2021-10-04 16:32:44', '', ''),
(2, 'Telepon', '088833339992', 'fas fa-phone', 'administrator', '', '', '2021-10-04 16:35:49', '', ''),
(3, 'Email', 'company@gmail.com', 'fas fa-envelope', 'administrator', '', '', '2021-10-04 16:36:04', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_donasi`
--

CREATE TABLE `tb_donasi` (
  `id_donasi` bigint(20) NOT NULL,
  `id_bantuan` int(11) NOT NULL,
  `id_bank` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `image` text NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_donasi`
--

INSERT INTO `tb_donasi` (`id_donasi`, `id_bantuan`, `id_bank`, `name`, `email`, `phone`, `amount`, `image`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 1, 'Antahassy Wibawa', 'antahassy@gmail.com', '087770067003', '5000000', '28.jpg', '1', 'Antahassy Wibawa', 'administrator', '', '2021-10-07 11:50:12', '2021-10-07 11:52:22', ''),
(2, 3, 1, 'Pak iyus', 'company@gmail.com', '081177733332', '2500000', '25.jpg', '0', 'Pak iyus', '', '', '2021-10-09 11:19:37', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus`
--

CREATE TABLE `tb_kasus` (
  `id_kasus` bigint(20) NOT NULL,
  `id_bantuan` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `image` text NOT NULL,
  `description` longtext NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kasus`
--

INSERT INTO `tb_kasus` (`id_kasus`, `id_bantuan`, `title`, `image`, `description`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'kasus kesehatan 1', '15.jpg', '<p>test kasus kesehatan 1</p>\r\n', '1', 'administrator', 'administrator', '', '2021-10-06 15:42:57', '2021-10-08 10:34:58', ''),
(2, 2, 'kasus sosial', '24.jpg', '<p>Test Kasus Sosial</p>\r\n', '1', 'administrator', 'administrator', '', '2021-10-06 15:50:22', '2021-10-06 15:50:41', ''),
(3, 3, 'kasus bencana', '17.jpg', '<p>Test Kasus Bencana</p>\r\n', '1', 'administrator', 'administrator', '', '2021-10-06 15:55:21', '2021-10-06 15:55:35', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_rule`
--

CREATE TABLE `tb_rule` (
  `id_rule` bigint(20) NOT NULL,
  `rule` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_rule`
--

INSERT INTO `tb_rule` (`id_rule`, `rule`, `description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Syarat Ketentuan', '<p>tes syarat dan ketentuan</p>\r\n', 'administrator', 'administrator', '', '2021-09-24 14:16:16', '2021-10-05 13:09:52', ''),
(2, 'Kebijakan Privasi', '<p>tes kebijakan privasi</p>\r\n', 'administrator', 'administrator', '', '2021-09-24 14:16:16', '2021-10-05 13:10:17', ''),
(3, 'Seputar Pertanyaan', '<p>tes seputar pertanyaan</p>\r\n', 'administrator', 'administrator', '', '2021-09-24 14:16:16', '2021-10-05 13:10:39', ''),
(4, 'Tentang Kami', '<p>tes tentang kami bla bla bla</p>\r\n', 'administrator', 'administrator', '', '2021-09-24 14:16:16', '2021-10-08 10:42:56', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_slideshow`
--

CREATE TABLE `tb_slideshow` (
  `id_slideshow` bigint(20) NOT NULL,
  `image` text NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_slideshow`
--

INSERT INTO `tb_slideshow` (`id_slideshow`, `image`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '14.jpg', 'administrator', 'administrator', '', '2021-10-04 16:46:27', '2021-10-05 13:11:35', ''),
(2, '6.jpg', 'administrator', 'administrator', '', '2021-10-04 16:46:54', '2021-10-05 13:12:05', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(250) NOT NULL,
  `logged_in` varchar(50) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` text NOT NULL,
  `active` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `image` longtext NOT NULL,
  `nama` varchar(100) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `deleted_by` varchar(50) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  `deleted_at` varchar(50) NOT NULL,
  `remember_selector` varchar(250) NOT NULL,
  `remember_code` varchar(250) NOT NULL,
  `forgotten_password_selector` varchar(250) NOT NULL,
  `forgotten_password_code` varchar(250) NOT NULL,
  `forgotten_password_time` int(11) NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `logged_in`, `username`, `password`, `active`, `email`, `phone`, `image`, `nama`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`, `remember_selector`, `remember_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `last_login`) VALUES
(1, '61.5.27.9', '2021-10-09 13:25:44', 'admin@admin.com', '$2y$12$rUSpsAsJgzDYHK4mCodPlegFDhykWay8hw3oSoaw..w2arWi1u32i', 1, 'admin@admin.com', '', '', 'Administrator', 'System', '', '', '2021-03-23 04:22:20', '', '', '', '', '', '', 0, 1633760744),
(2, '61.5.27.9', '2021-10-09 13:25:09', 'antahassyw@gmail.com', '$2y$10$cPb6osOIIVBR9pVGP4JJSeL7OhsgZdstRV8UkKTwkd9tiy8WHXZ8G', 1, 'antahassyw@gmail.com', '', '', '', 'antahassyw@gmail.com', '', '', '2021-10-08 15:47:42', '', '', '', '', '', '', 0, 1633760709);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indexes for table `tb_agenda`
--
ALTER TABLE `tb_agenda`
  ADD PRIMARY KEY (`id_agenda`);

--
-- Indexes for table `tb_bank`
--
ALTER TABLE `tb_bank`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `tb_bantuan`
--
ALTER TABLE `tb_bantuan`
  ADD PRIMARY KEY (`id_bantuan`);

--
-- Indexes for table `tb_contact`
--
ALTER TABLE `tb_contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indexes for table `tb_donasi`
--
ALTER TABLE `tb_donasi`
  ADD PRIMARY KEY (`id_donasi`);

--
-- Indexes for table `tb_kasus`
--
ALTER TABLE `tb_kasus`
  ADD PRIMARY KEY (`id_kasus`);

--
-- Indexes for table `tb_rule`
--
ALTER TABLE `tb_rule`
  ADD PRIMARY KEY (`id_rule`);

--
-- Indexes for table `tb_slideshow`
--
ALTER TABLE `tb_slideshow`
  ADD PRIMARY KEY (`id_slideshow`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_agenda`
--
ALTER TABLE `tb_agenda`
  MODIFY `id_agenda` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_bank`
--
ALTER TABLE `tb_bank`
  MODIFY `id_bank` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_bantuan`
--
ALTER TABLE `tb_bantuan`
  MODIFY `id_bantuan` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_contact`
--
ALTER TABLE `tb_contact`
  MODIFY `id_contact` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_donasi`
--
ALTER TABLE `tb_donasi`
  MODIFY `id_donasi` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_kasus`
--
ALTER TABLE `tb_kasus`
  MODIFY `id_kasus` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_rule`
--
ALTER TABLE `tb_rule`
  MODIFY `id_rule` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_slideshow`
--
ALTER TABLE `tb_slideshow`
  MODIFY `id_slideshow` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
